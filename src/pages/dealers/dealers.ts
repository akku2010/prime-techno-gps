import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, ModalController, ToastController, AlertController, Events, LoadingController } from "ionic-angular";
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { TranslateService } from "@ngx-translate/core";
import { DealerPermModalPage } from "./dealer-perm/dealer-perm";
import { AddPointsModalPage } from "./AddPointsModalPage";

@IonicPage()
@Component({
    selector: 'page-customers',
    templateUrl: './dealers.html'
})

export class DealerPage {

    islogin: any = {};
    page: number = 1;
    limit: number = 5;
    DealerArraySearch: any = [];
    // DealerArray: any = [];
    ndata: any = [];
    // DealerData: any = [];
    CratedeOn: string;
    expirydate: string;
    time: string;
    date: string;
    searchKey: any;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public apiCall: ApiServiceProvider,
        public modalCtrl: ModalController,
        public toastCtrl: ToastController,
        public alerCtrl: AlertController,
        public events: Events,
        private translate: TranslateService,
        private loadingCtrl: LoadingController
    ) {
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    }

    ionViewDidEnter() {
        this.getDealersListCall();
    }

    getDealersListCall() {

        this.page = 1;
        this.DealerArraySearch = [];
        this.apiCall.startLoading().present();
        this.apiCall.getDealersCall(this.islogin._id, this.page, this.limit, this.searchKey)
            .subscribe(data => {
                this.apiCall.stopLoading();
                // this.DealerArray = data;
                this.DealerArraySearch = data;
            },
                err => {
                    this.apiCall.stopLoading();
                    console.log("getting error from server=> ", err);
                    let toast = this.toastCtrl.create({
                        message: this.translate.instant('No Dealer(s) found'),
                        duration: 2000,
                        position: "bottom"
                    })

                    toast.present();

                    toast.onDidDismiss(() => {
                        this.navCtrl.setRoot("DashboardPage");
                    })
                });
    }

    addDealersModal() {
        let modal = this.modalCtrl.create('AddDealerPage');
        modal.onDidDismiss(() => {
            this.getDealersListCall();
        })
        modal.present();
    }

    _editDealer(item) {
        let modal = this.modalCtrl.create('EditDealerPage', {
            param: item
        });
        modal.onDidDismiss(() => {
            this.getDealersListCall();
        })
        modal.present();
    }

    DeleteDealer(_id) {
        let alert = this.alerCtrl.create({
            message: this.translate.instant('doyouwanttodeletedealer', { value: this.translate.instant('Dealers') }),
            buttons: [{
                text: this.translate.instant('NO')
            },
            {
                text: this.translate.instant('Yes'),
                handler: () => {
                    this.deleteDeal(_id);
                }
            }]
        });
        alert.present();
    }

    deleteDeal(_id) {
        var deletePayload = {
            'userId': _id,
            'deleteuser': true
        }
        this.apiCall.startLoading().present();
        this.apiCall.deleteDealerCall(deletePayload).
            subscribe(data => {
                this.apiCall.stopLoading();
                let toast = this.toastCtrl.create({
                    message: this.translate.instant("Deleted successfully.", { value: this.translate.instant('Dealers') }),
                    position: 'bottom',
                    duration: 2000
                });

                toast.onDidDismiss(() => {
                    this.getDealersListCall();
                });

                toast.present();
            },
                err => {
                    this.apiCall.stopLoading();
                    console.log(err);
                });
    }

    callSearch(ev) {
        this.page = 1;
        this.searchKey = ev.target.value;
        let loading = this.loadingCtrl.create({
            content: "Searching dealer please wait..."
        });
        loading.present();
        this.apiCall.getDealersCall(this.islogin._id, this.page, this.limit, this.searchKey)
            .subscribe(data => {
                loading.dismiss();
                this.DealerArraySearch = data;
            }, err => {
                loading.dismiss();
                let toast = this.toastCtrl.create({
                    message: this.translate.instant("No Dealer found for search key '") + ev.target.value + "' ..",
                    duration: 1500,
                    position: "bottom"
                })
                toast.present();
                toast.onDidDismiss(() => { })
            })
    }

    onClear(ev) {
        this.searchKey = undefined;
        this.getDealersListCall();
        ev.target.value = '';
    }

    switchDealer(_id) {
        localStorage.setItem('superAdminData', JSON.stringify(this.islogin));
        localStorage.setItem('custumer_status', 'OFF');
        localStorage.setItem('dealer_status', 'ON');

        this.apiCall.getcustToken(_id)
            .subscribe(res => {
                let custToken = res;
                let logindata = JSON.stringify(custToken);
                let logindetails = JSON.parse(logindata);
                let userDetails = window.atob(logindetails.custumer_token.split('.')[1]);
                let details = JSON.parse(userDetails);
                localStorage.setItem("loginflag", "loginflag");
                localStorage.setItem('details', JSON.stringify(details));

                let dealerSwitchObj = {
                    "logindata": logindata,
                    "details": userDetails,
                    'condition_chk': details.isDealer
                }

                let temp = localStorage.getItem('isDealervalue');
                this.events.publish("event_sidemenu", JSON.stringify(dealerSwitchObj));
                this.events.publish("sidemenu:event", temp);
                this.navCtrl.setRoot('DashboardPage');

            }, err => {
                console.log(err);
            })
    }

    addPonits(item) {
        console.log("add points: ", item);
        this.presentModal(item);
    }

    presentModal(param) {
        const modal = this.modalCtrl.create(AddPointsModalPage, { params: param });
        modal.present();

        modal.onDidDismiss((data) => {
            console.log("onDidDismiss", data);
            //   this.getIdlePoints(data);
        })
    }

    dealerStatus(data) {
        let msg;
        if (data.status) {
            msg = this.translate.instant('deactivateDealer', { value: this.translate.instant('Dealers') });
        } else {
            msg = this.translate.instant('deactivateDealer', { value: this.translate.instant('Dealers') });
        }
        let alert = this.alerCtrl.create({
            message: msg,
            buttons: [{
                text: this.translate.instant('Yes'),
                handler: () => {
                    this.user_status(data);
                }
            },
            {
                text: this.translate.instant('NO'),
                handler: () => {
                    this.getDealersListCall();
                }
            }]
        });
        alert.present();
    }

    user_status(data) {
        let stat;
        if (data.status) {
            stat = false;
        } else {
            stat = true;
        }

        let ddata = {
            "uId": data._id,
            "loggedIn_id": this.islogin._id,
            "status": stat
        };
        this.apiCall.startLoading().present();
        this.apiCall.user_statusCall(ddata)
            .subscribe(data => {
                this.apiCall.stopLoading();
                let toast = this.toastCtrl.create({
                    message: this.translate.instant('Dealer status updated successfully!'),
                    position: 'bottom',
                    duration: 2000
                });

                toast.onDidDismiss(() => {
                    this.getDealersListCall();
                });

                toast.present();
            },
                () => {
                    this.apiCall.stopLoading();
                });
    }

    doInfinite(infiniteScroll) {
        // debugger
        let that = this;
        that.page = that.page + 1;

        // setTimeout(() => {
        // that.ndata = [];
        this.apiCall.getDealersCall(that.islogin._id, that.page, that.limit, that.searchKey)
            .subscribe(data => {
                // that.ndata = data;

                // for (let i = 0; i < that.ndata.length; i++) {
                //     that.DealerData.push(that.ndata[i]);
                // }
                for (let i = 0; i < data.length; i++) {
                    that.DealerArraySearch.push(data[i]);
                }
                // that.DealerArraySearch = that.DealerData;
                infiniteScroll.complete();
            },
                err => {
                    this.apiCall.stopLoading();
                    console.log("error found=> " + err);
                });
        // }, 500);
    }

    dealerPerm(dealer) {
        const modal = this.modalCtrl.create(DealerPermModalPage, {
            param: dealer
        });
        modal.present();

        modal.onDidDismiss((data) => {
            console.log("onDidDismiss", data);
            this.getDealersListCall();
        })
    }
}
