import { Component } from "@angular/core";
import { ViewController } from "ionic-angular";

@Component({
    // selector: 'page-modal',
    template: `
        <div class="mainDiv">
            <div class="secondDiv">
                <ion-list radio-group [(ngModel)]="min_time">
                    <ion-item>
                        <ion-label>10 Mins</ion-label>
                        <ion-radio value="10" checked></ion-radio>
                    </ion-item>
                    <ion-item>
                        <ion-label>20 Mins</ion-label>
                        <ion-radio value="20"></ion-radio>
                    </ion-item>
                    <ion-item>
                        <ion-label>30 Mins</ion-label>
                        <ion-radio value="30"></ion-radio>
                    </ion-item>
                    <ion-item>
                        <ion-label>40 Mins</ion-label>
                        <ion-radio value="40"></ion-radio>
                    </ion-item>
                    <ion-item>
                        <ion-label>50 Mins</ion-label>
                        <ion-radio value="50"></ion-radio>
                    </ion-item>
                    <ion-item>
                        <ion-label>60 Mins</ion-label>
                        <ion-radio value="60"></ion-radio>
                    </ion-item>
                    
                </ion-list>
                <div style="padding: 5px;"><button ion-button (click)="dismiss()" color="gpsc" block>Submit</button></div>
            </div>
        </div>
    `,
    styles: [`
      
            .mainDiv {
                padding: 50px;
                height: 100%;
                background-color: rgba(0, 0, 0, 0.7);
            }

            .secondDiv {
                padding-top: 20px;
                border-radius: 18px;
                border: 2px solid black;
                background: white;
            }
        
    `]
})

export class ModalPage {

    min_time: any = "10";
    constructor(
        // private navParams: NavParams,
        private viewCtrl: ViewController
    ) {

    }

    dismiss() {
        this.viewCtrl.dismiss(this.min_time);
    }

}