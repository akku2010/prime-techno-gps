import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateTripPage } from './create-trip';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    CreateTripPage,
  ],
  imports: [
    IonicPageModule.forChild(CreateTripPage),
    TranslateModule.forChild()
  ],
})
export class CreateTripPageModule {}
