import { Component, OnInit, ViewChild, ElementRef, ViewChildren, QueryList } from '@angular/core';
import * as moment from 'moment';
import { IonicPage, NavController, NavParams, AlertController, ToastController, ModalController, PopoverController, ViewController, Navbar, Platform, Events } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { GeocoderProvider } from '../../providers/geocoder/geocoder';
import { TranslateService } from '@ngx-translate/core';
import { ImmobilizeModelPage } from './immobilize-modal';
import { DrawerState } from 'ion-bottom-drawer';
import { TimePickerModal } from '../profile/settings/notif-setting/time-picker/time-picker';
import { CallNumber } from '@ionic-native/call-number';
import { SocialSharing } from '@ionic-native/social-sharing';
import * as TinyURL from 'tinyurl';

@IonicPage()
@Component({
  selector: 'page-add-devices',
  templateUrl: 'add-devices.html',
})
export class AddDevicesPage implements OnInit {
  @ViewChildren("step") steps: QueryList<ElementRef>;
  public loadProgress: number = 0;
  @ViewChildren('myGauge') myGauge: QueryList<ElementRef>;
  customTime: any;
  islogin: any;
  isDealer: any;
  isPayNow: any = false;
  isExpIred: any = false;
  isPayAlert: any = false;
  selectedArray: any[] = [];
  setselect: any = false;
  islogindealer: string;
  stausdevice: string;
  device_types: { vehicle: string; name: string; }[];
  GroupType: { vehicle: string; name: string; }[];
  GroupStatus: { name: string; }[];
  allDevices: any = [];
  allDevicesSearch: any = [];
  users_data: any = [];
  isPaymentPage: any[] = [];
  ddata: any;
  socket: any;
  veh: any;
  isdevice: string;
  userPermission: any;
  option_switch: boolean = false;
  dataEngine: any;
  DeviceConfigStatus: any;
  messages: string;
  editdata: any;
  responseMessage: string;
  searchCountryString = ''; // initialize your searchCountryString string empty
  countries: any;
  latLang: any;
  showActionSheet: boolean;
  drawerState1 = DrawerState.Docked;
  showFooter: boolean;
  dockedHeight1 = 300;
  distanceTop1 = 378;
  minimumHeight1 = 0;
  condition: string = 'gpsc';
  condition1: string = 'whiteee';
  condition2: string = 'whiteee';
  condition3: string = 'whiteee';
  tttime: number;
  resToken: any;
  deviceDeatils: any = {};

  @ViewChild('popoverContent', { read: ElementRef }) content: ElementRef;
  @ViewChild('popoverText', { read: ElementRef }) text: ElementRef;
  @ViewChild(Navbar) navBar: Navbar;
  page: number = 0;
  limit: number = 8;
  ndata: any;
  searchItems: any;
  searchQuery: string = '';
  items: any[];
  deivceId: any;
  immobType: any;
  respMsg: any;
  commandStatus: any;
  intervalID: any;
  now: any;
  datePipe: any;
  isSuperAdmin: any;
  clicked: boolean = false;
  checkedPass: string;
  datetimeEnd: string;
  datetimeStart: string;
  liveDataShare: any;
  dealer_Permission: boolean;
  checkIfStat: string = "ALL";
  public toggled: boolean = false;
  progressIntervalId: any;
  drawerData: any;
  intervalid123: any;
  devIcon: string;
  all_devices: any = [];
  singleVehicleCount: number;
  t_veicle_count: any;
  showEmptyStatus: boolean = false;
  distanceReport: any;
  mili: any;
  a: any;
  d: any;
  n: any;
  supid: any;
  relay_timer: any;

  public toggle(): void {
    this.toggled = !this.toggled;
  }
  cancelSearch() {
    this.toggled = false;
  }
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public modalCtrl: ModalController,
    public popoverCtrl: PopoverController,
    private geocoderApi: GeocoderProvider,
    private plt: Platform,
    public translate: TranslateService,
    private events: Events,
    private callNumber: CallNumber,
    private socialSharing: SocialSharing
  ) {
    this.toggled = false;
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    this.datetimeStart = moment({ hours: 0 }).format();
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
    if (localStorage.getItem('Total_Vech') !== null) {
      this.t_veicle_count = JSON.parse(localStorage.getItem('Total_Vech'));
    }
    this.isDealer = this.islogin.isDealer;
    this.isSuperAdmin = this.islogin.isSuperAdmin;
    this.islogindealer = localStorage.getItem('isDealervalue');
    if (this.isDealer == false && this.isSuperAdmin == false) {
      this.dealer_Permission = false;
    } else {
      this.dealer_Permission = this.islogin.device_add_permission;
    }
    if (navParams.get("label") && navParams.get("value")) {
      this.stausdevice = localStorage.getItem('status');
      this.checkIfStat = this.stausdevice;
      this.singleVehicleCount = navParams.get("value");
    } else {
      this.stausdevice = undefined;
    }

    ////////////
    this.events.subscribe("Released:Dismiss", () => {
      this.getdevices();
    });

    this.apiCall.getRechargePlanApi(this.islogin.supAdmin).subscribe((respData) => {
      this.isPaymentPage = respData
    });
    this.getUserSettings();
  }
  showDrawer: boolean = false;
  shouldBounce = true;
  dockedHeight = 220;
  distanceTop = 556;
  drawerState = DrawerState.Docked;
  states = DrawerState;
  minimumHeight = 0;

  getUserSettings() {
    let url = this.apiCall.mainUrl + "users/get_user_setting";
    let payload = {
      uid: this.islogin._id,
    };
    this.apiCall.urlpasseswithdatatwo(url, payload).subscribe(
      (respData) => {
        this.ddata = respData;
        this.ddata = JSON.parse(this.ddata._body).service1;
        let tempD: any = respData;
        this.users_data = JSON.parse(tempD._body);
        if (this.users_data.unit_measurement !== undefined) {
          this.measurementUnit = this.users_data.unit_measurement;
        } else {
          if (localStorage.getItem('MeasurementType') !== null) {
            let measureType = localStorage.getItem('MeasurementType');
            this.measurementUnit = measureType;
          } else {
            this.measurementUnit = 'MKS';
          }
        }
      },
      (err) => {
        console.log("oops got error: ", err);
      });
  }

  showBottomDrawer(data) {
    this.drawerData = data;
    this.showDrawer = true;
    this.drawerState = DrawerState.Docked;
  }

  fonctionTest(d) {
    if (this.showDrawer) {
      this.showDrawer = false;
    }
    const modal = this.modalCtrl.create(TimePickerModal, {
      data: d,
      key: 'parking'
    });
    modal.onDidDismiss(() => {
      this.getdevices();
    })
    modal.present();
  }

  towAlertCall(d) {
    if (this.showDrawer) {
      this.showDrawer = false;
    }
    const modal = this.modalCtrl.create(TimePickerModal, {
      data: d,
      key: 'tow'
    });

    modal.onDidDismiss(() => {
      this.getdevices();
    })
    modal.present();
  }

  SetTimerForTenSec: boolean = false;
  ngOnInit() {
    this.now = new Date().toISOString();
  }

  activateVehicles() {
    if (this.selectedArray.length == 0) {
      let alert = this.alertCtrl.create({
        message: "Please Select Vehicle",
        buttons: [
          {
            text: "ok",
            handler: () => {
              (err) => {
                console.log(err);
              };
            },
          },
        ],
      });
      alert.present();
    }
    if (this.isPayAlert) {
      if (this.users_data.paymentgateway == true && this.isPaymentPage.length > 0) {
        this.navCtrl.push("PaytmwalletloginPage", {
          'param': this.selectedArray
        });
      }
      else {
        this.navCtrl.push(PaymantgatweyPage);
      }

    }
  }
  supportCall() {
    let number = this.ddata;
    if (number !== "" || number !== undefined) {
      if (this.plt.is('android')) {
        window.open('tel:' + number, '_system');
      } else if (this.plt.is('ios')) {
        this.callNumber.callNumber(number.toString(), true)
          .then(res => console.log('Launched dialer!', res))
          .catch(err => console.log('Error launching dialer', err));
      }
    } else {
      this.toastCtrl.create({
        message: 'Contact number not found!',
        position: 'middle',
        duration: 2000
      }).present();
    }
  }
  activateVehiclesess() {
    let that = this;
    if (this.checkIfStat !== 'Expired') {
      let Exp = "Expired"
      let alert = that.alertCtrl.create({
        message: "GPS subscription for this vehicle expired.Do you want to renew ?",
        buttons: [{
          text: "Cancel",
          handler: () => {
            (err) => {
              console.log(err);
            };
          },
        },
        {
          text: "Renew",
          handler: () => {
            that.segmentChanged(Exp);
            (err) => {
              console.log(err);
            };
          },
        },

        ],
      });
      alert.present();
    }
  }
  isSelected(d) {
    const item = this.selectedArray.filter(item => item.Device_ID == d.Device_ID);
    return item.length > 0;
  }
  selected(event, d) {
    if (this.checkIfStat === 'Expired') {
      this.isPayAlert = true;
      event.stopPropagation();
      const isSelected = this.selectedArray.findIndex(item => item.Device_ID == d.Device_ID);

      if (isSelected > -1) {
        this.selectedArray = this.selectedArray.filter(item => item.Device_ID != d.Device_ID);
      } else {
        this.selectedArray.push(d)
      }
      this.setselect = true
    }
    else {
      this.activateVehiclesess();
    }
  }
  ngOnDestroy() { }
  hideMe: boolean = false;

  ionViewDidLeave() {
    if (this.timerInterval != undefined) {
      clearInterval(this.timerInterval);
    }
  }
  ionViewDidEnter() {
    let tempVar = localStorage.getItem("AutoRefreshVehicleList");
    if (tempVar != null) {
      if (tempVar == "ON") {
        this.SetTimerForTenSec = true;
      } else {
        this.SetTimerForTenSec = false;
      }
    }
    this.getDefaultUserSetting();
    this.navBar.backButtonClick = (ev: UIEvent) => {
      this.hideMe = true;
      this.navCtrl.pop({
        animate: true, animation: 'transition-ios', direction: 'back'
      });
    }
    this.getdevices();
    this.cancelSearch();

    if (localStorage.getItem("SCREEN") != null) {
      this.navBar.backButtonClick = (e: UIEvent) => {
        if (localStorage.getItem("SCREEN") != null) {
          if (localStorage.getItem("SCREEN") === 'live') {
            this.navCtrl.setRoot('LivePage');
          } else {
            if (localStorage.getItem("SCREEN") === 'dashboard') {
              this.navCtrl.setRoot('DashboardPage')
            }
          }
        }
      }
    }

  }
  measurementUnit: string = 'MKS';


  getDefaultUserSetting() {
    this.apiCall.startLoading().present();
    this.apiCall.getcustToken(this.islogin._id)
      .subscribe(data => {
        this.apiCall._customerData = data;
        this.apiCall.stopLoading();
        if (data.cust.user_settings) {
          this.relay_timer = data.cust.user_settings.immoblizer
        }
      })
  }

  upload() {
    this.navCtrl.push('UploadDocPage', { vehData: this.drawerData });
  }

  segmentChanged(ev: any) {
    this.selectedArray = [];
    if (this.showDrawer) {
      this.showDrawer = false;
    }
    if (ev == "Expired") {
      this.checkIfStat = ev;
    }
    else {
      this.checkIfStat = ev._value;
    }

    if (this.checkIfStat !== 'ALL') {
      if (this.checkIfStat == "Expired") {
        this.stausdevice = ev;
      } else {
        this.stausdevice = ev._value
      }
    } else {
      this.stausdevice = undefined;
    }
    this.page = 0;
    this.loadProgress = 100;
    this.getdevices();
  }

  timerInterval: any;
  getdevices() {
    this.showEmptyStatus = false;
    this.showDrawer = false;
    let baseURLp, mainUrlNew;
    if (localStorage.getItem('BASE_URL') != null) {
      mainUrlNew = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    }
    if (this.stausdevice) {
      baseURLp = (mainUrlNew ? mainUrlNew : this.apiCall.mainUrl) + 'devices/getDeviceByUserMobile?email=' + this.islogin.email + '&id=' + this.islogin._id + '&statuss=' + this.stausdevice + '&skip=' + this.page + '&limit=' + this.limit;
    }
    else {
      baseURLp = (mainUrlNew ? mainUrlNew : this.apiCall.mainUrl) + 'devices/getDeviceByUserMobile?id=' + this.islogin._id + '&email=' + this.islogin.email + '&skip=' + this.page + '&limit=' + this.limit;
    }

    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.progressBar();
    if (this.loadProgress == 100) {
      this.loadProgress = 0;
    }
    if (this.SetTimerForTenSec == true) {
      this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
        .subscribe(data => {
          let that = this;
          that.loadProgress = 100;
          clearInterval(that.progressIntervalId);
          this.allDevicesSearch = [];
          this.ndata = data.devices;
          this.allDevices = this.ndata;
          this.allDevicesSearch = this.ndata;
          if (this.allDevicesSearch.length === 0) {
            this.showEmptyStatus = true;
          }
          that.userPermission = JSON.parse(localStorage.getItem('details'));
          if (that.userPermission.isDealer == true || that.userPermission.isSuperAdmin == true) {
            that.option_switch = true;
          } else {
            if (localStorage.getItem('isDealervalue') == 'true') {
              that.option_switch = true;
            } else {
              if (that.userPermission.isDealer == false) {
                that.option_switch = false;
              }
            }
          }
        },
          err => {
            console.log("error=> ", err);
            let that = this;
            this.showEmptyStatus = false;
            clearInterval(that.progressIntervalId);
          });
      this.timerInterval = setInterval(() => {
        this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
          .subscribe(data => {
            let that = this;
            that.loadProgress = 100;
            clearInterval(that.progressIntervalId);
            this.allDevicesSearch = [];
            this.ndata = data.devices;
            this.allDevices = this.ndata;
            this.allDevicesSearch = this.ndata;
            if (this.allDevicesSearch.length === 0) {
              this.showEmptyStatus = true;
            }
            that.userPermission = JSON.parse(localStorage.getItem('details'));
            if (that.userPermission.isDealer == true || that.userPermission.isSuperAdmin == true) {
              that.option_switch = true;
            } else {
              if (localStorage.getItem('isDealervalue') == 'true') {
                that.option_switch = true;
              } else {
                if (that.userPermission.isDealer == false) {
                  that.option_switch = false;
                }
              }
            }
          },
            err => {
              console.log("error=> ", err);
              let that = this;
              this.showEmptyStatus = false;
              clearInterval(that.progressIntervalId);
            });
      }, 10000)
    } else {
      this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
        .subscribe(data => {
          // this.apiCall.stopLoading();
          // this.apiCall.toastMsgDismised();
          let that = this;
          that.loadProgress = 100;
          clearInterval(that.progressIntervalId);
          this.allDevicesSearch = [];
          this.ndata = data.devices;
          this.allDevices = this.ndata;
          this.allDevicesSearch = this.ndata;
          // this.allDevicesSearch = this.ndata;
          if (this.allDevicesSearch.length === 0) {
            this.showEmptyStatus = true;
          }
          that.userPermission = JSON.parse(localStorage.getItem('details'));
          if (that.userPermission.isDealer == true || that.userPermission.isSuperAdmin == true) {
            that.option_switch = true;
          } else {
            if (localStorage.getItem('isDealervalue') == 'true') {
              that.option_switch = true;
            } else {
              if (that.userPermission.isDealer == false) {
                that.option_switch = false;
              }
            }
          }
        },
          err => {
            console.log("error=> ", err);
            let that = this;
            this.showEmptyStatus = false;
            clearInterval(that.progressIntervalId);
          });
    }

  }

  AllData: any = [];
  runningData: any = [];
  stoppedData: any = [];
  idlingData: any = [];
  outOfReachData: any = [];

  getdevicesTemp123() {
    var baseURLp;
    if (this.checkIfStat != undefined && this.checkIfStat !== 'ALL') {
      baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&statuss=' + this.checkIfStat + '&skip=' + this.page + '&limit=' + this.limit;
    } else {
      this.limit = 10;
      baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&skip=' + this.page + '&limit=' + this.limit;
    }

    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.progressBar();
    if (this.loadProgress == 100) {
      this.loadProgress = 0;
    }
    this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.loadProgress = 100;
        this.ndata = data.devices;
        this.allDevices = data.devices;
        // this.allDevicesSearch = this.ndata;
        let that = this;
        ////////////////////////////////////////////
        this.AllData = [];
        this.runningData = [];
        this.stoppedData = [];
        this.idlingData = [];
        this.outOfReachData = [];
        let expiredDevices = [];
        let nodataDevices = [];
        that.all_devices = [];
        for (var i = 0; i < data.devices.length; i++) {
          if (data.devices[i].status === 'RUNNING') {
            that.runningData.push(data.devices[i]);
          } else if (data.devices[i].status === 'IDLING') {
            that.idlingData.push(data.devices[i]);
          } else if (data.devices[i].status === 'STOPPED') {
            that.stoppedData.push(data.devices[i]);
          } else if (data.devices[i].status === 'OUT OF REACH') {
            that.outOfReachData.push(data.devices[i]);
          } else if (data.devices[i].status === 'Expired') {
            expiredDevices.push(data.devices[i]);
          } else if (data.devices[i].status === 'NO DATA') {
            nodataDevices.push(data.devices[i]);
          }
          that.all_devices.push(data.devices[i])

        }
        if (this.checkIfStat === 'ALL') {
          this.allDevicesSearch = that.all_devices;
        } else if (this.checkIfStat === 'RUNNING') {
          this.allDevicesSearch = this.runningData;
        } else if (this.checkIfStat === 'IDLING') {
          this.allDevicesSearch = this.idlingData;
        } else if (this.checkIfStat === 'OUT OF REACH') {
          this.allDevicesSearch = this.outOfReachData;
        } else if (this.checkIfStat === 'STOPPED') {
          this.allDevicesSearch = this.stoppedData;
        } else if (this.checkIfStat === 'Expired') {
          this.allDevicesSearch = expiredDevices;
        } else if (this.checkIfStat === 'NO DATA') {
          this.allDevicesSearch = nodataDevices;
        }
        ////////////////////////////////////////////
        that.userPermission = JSON.parse(localStorage.getItem('details'));
        if (that.userPermission.isDealer == true || that.userPermission.isSuperAdmin == true) {
          that.option_switch = true;
        } else {
          if (localStorage.getItem('isDealervalue') == 'true') {
            that.option_switch = true;
          } else {
            if (that.userPermission.isDealer == false) {
              that.option_switch = false;
            }
          }
        }
      },
        err => {
          console.log("error=> ", err);
        });
  }

  settings() {
    let that = this;
    let profileModal = this.modalCtrl.create('DeviceSettingsPage', {
      param: that.drawerData
    });
    profileModal.present();

    profileModal.onDidDismiss(() => {
      that.callObjFunc(that.drawerData);

    })
  }

  customT() {
    let that = this;
    let customTModal = this.modalCtrl.create(CustomDurationPage);
    customTModal.present();
    customTModal.onDidDismiss((param) => {
      if (param != undefined) {
        that.tttime = (Number(param.param) * 60);
      } else {
        that.tttime = 15;
      }

    })
  }

  geofence() {
    this.navCtrl.push('GeofencePage');
  }




  callObjFunc(d) {
    let that = this;
    let _bUrl = that.apiCall.mainUrl + 'devices/getDevicebyId?deviceId=' + d.Device_ID;
    this.apiCall.startLoading().present();
    this.apiCall.getSOSReportAPI(_bUrl)
      .subscribe(resp => {
        this.apiCall.stopLoading();
        if (!resp) {
          return;
        } else {
          that.drawerData = resp;
        }
      })
  }

  activateVehicle(data) {
    this.navCtrl.push("PaytmwalletloginPage", {
      "param": data
    })
  }

  timeoutAlert() {
    let alerttemp = this.alertCtrl.create({
      message: "the server is taking much time to respond. Please try in some time.",
      buttons: [{
        text: this.translate.instant('Okay'),
        handler: () => {
          this.navCtrl.setRoot("DashboardPage");
        }
      }]
    });
    alerttemp.present();
  }

  showVehicleDetails(vdata) {

    this.navCtrl.push('VehicleDetailsPage', {
      param: vdata,
      option_switch: this.option_switch
    });
    this.showDrawer = false;
  }

  sharedevice(param) {
    let that = this;
    if (param == '15mins') {
      that.condition = 'gpsc';
      that.condition1 = 'whiteee';
      that.condition2 = 'whiteee';
      that.condition3 = 'whiteee';
      that.tttime = 15;
      // that.tttime  = (15 * 60000); //for miliseconds
    } else {
      if (param == '1hour') {
        that.condition1 = 'gpsc';
        that.condition = 'whiteee';
        that.condition2 = 'whiteee';
        that.condition3 = 'whiteee';
        that.tttime = 60;
        // that.tttime  = (1 * 3600000); //for miliseconds
      } else {
        if (param == '8hours') {
          that.condition2 = 'gpsc';
          that.condition = 'whiteee';
          that.condition1 = 'whiteee';
          that.condition3 = 'whiteee';
          that.tttime = (8 * 60);
          // that.tttime  = (8 * 3600000);
        } else {
          if (param == 'custom') {
            that.condition3 = 'gpsc';
            that.condition = 'whiteee';
            that.condition1 = 'whiteee';
            that.condition2 = 'whiteee';
            that.customT();
            // that.tttime = 60;
            // that.tttime  = (1 * 3600000); //for miliseconds
          }
        }
      }
    }
  }

  shareLivetemp() {
    let that = this;
    if (that.tttime == undefined) {
      that.tttime = 15;
    }
    var data = {
      id: that.drawerData._id,
      imei: that.drawerData.Device_ID,
      sh: this.islogin._id,
      ttl: that.tttime   // set to 1 hour by default
    };
    this.apiCall.startLoading().present();
    this.apiCall.shareLivetrackCall(data)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.resToken = data.t;
        this.liveShare();
      },
        err => {
          this.apiCall.stopLoading();
          console.log(err);
        });
  }

  liveShare() {
    debugger
    let that = this;
    var link = 'https://www.oneqlik.in/' + "share/liveShare?t=" + that.resToken;
    TinyURL.shorten(link).then((res) => {
      console.log("tinyurl: " + res);
      that.socialSharing.share(that.islogin.fn + " " + that.islogin.ln + " has shared " + that.drawerData.Device_Name + " live trip with you. Please follow below link to track", "OneQlik- Live Trip", "", res);

    })
    // that.socialSharing.share(that.userdetails.fn + " " + that.userdetails.ln + " has shared " + that.liveDataShare.Device_Name + " live trip with you. Please follow below link to track", "OneQlik- Live Trip", "", link);
    that.showActionSheet = true;
    that.showFooter = false;
    that.tttime = undefined;
  }

  doRefresh(refresher) {
    let that = this;
    that.page = 0;
    that.limit = 5;
    this.getdevices();
    this.getdevicesTemp123();
    refresher.complete();
  }

  shareVehicle() {
    let that = this;
    that.showActionSheet = false;
    that.drawerState1 = DrawerState.Docked;
    that.showFooter = true;
  }

  sharedevices(data, d_data) {
    let that = this;
    var devicedetails = {
      "did": d_data._id,
      "email": data.shareId,
      "uid": that.islogin._id
    }

    that.apiCall.startLoading().present();
    that.apiCall.deviceShareCall(devicedetails)
      .subscribe(data => {
        that.apiCall.stopLoading();
        let toast = that.toastCtrl.create({
          message: data.message,
          position: 'bottom',
          duration: 2000
        });

        toast.present();
      },
        err => {
          that.apiCall.stopLoading();
          var body = err._body;
          var msg = JSON.parse(body);
          let alert = this.alertCtrl.create({
            message: msg.message,
            buttons: [this.translate.instant('Okay')]
          });
          alert.present();
        });
  }

  showDeleteBtn(b) {
    // debugger;
    let that = this;
    if (localStorage.getItem('isDealervalue') == 'true') {
      return false;
    } else {
      if (b) {
        var u = b.split(",");
        for (let p = 0; p < u.length; p++) {
          if (that.islogin._id == u[p]) {
            return true;
          }
        }
      }
      else {
        return false;
      }
    }

  }

  sharedVehicleDelete(device) {
    let that = this;
    that.deivceId = device;
    let alert = that.alertCtrl.create({
      message: this.translate.instant('Do you want to delete this share vehicle ?'),
      buttons: [{
        text: this.translate.instant('YES PROCEED'),
        handler: () => {
          that.removeDevice(that.deivceId._id);
        }
      },
      {
        text: this.translate.instant('NO')
      }]
    });
    alert.present();
  }

  removeDevice(did) {
    this.apiCall.startLoading().present();
    this.apiCall.dataRemoveFuncCall(this.islogin._id, did)
      .subscribe(() => {
        this.apiCall.stopLoading();
        let toast = this.toastCtrl.create({
          message: this.translate.instant('Shared Device was deleted successfully!'),
          duration: 1500
        });
        toast.onDidDismiss(() => {
        });

        toast.present();
      },
        err => {
          this.apiCall.stopLoading();
          console.log(err)
        });
  }

  showSharedBtn(a, b) {
    // debugger
    if (b) {
      return !(b.split(",").indexOf(a) + 1);
    }
    else {
      return true;
    }
  }

  presentPopover(ev, data) {
    let popover = this.popoverCtrl.create(PopoverPage,
      {
        vehData: data
      },
      {
        cssClass: 'contact-popover'
      });

    popover.onDidDismiss(() => {
      this.getdevices();
    })

    popover.present({
      ev: ev
    });
  }

  doInfinite(infiniteScroll) {
    let that = this;
    that.page = that.page + 1;
    setTimeout(() => {
      var baseURLp;
      if (that.stausdevice) {
        baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + that.islogin._id + '&email=' + that.islogin.email + '&statuss=' + that.stausdevice + '&skip=' + that.page + '&limit=' + that.limit;
      }
      else {
        baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + that.islogin._id + '&email=' + that.islogin.email + '&skip=' + that.page + '&limit=' + that.limit;
      }

      if (this.islogin.isSuperAdmin == true) {
        baseURLp += '&supAdmin=' + this.islogin._id;
      } else {
        if (this.islogin.isDealer == true) {
          baseURLp += '&dealer=' + this.islogin._id;
        }
      }
      that.ndata = [];
      that.apiCall.getdevicesForAllVehiclesApi(baseURLp)
        .subscribe(
          res => {
            if (res.devices.length > 0) {
              that.ndata = res.devices;
              for (let i = 0; i < that.ndata.length; i++) {
                // that.allDevices.push(that.ndata[i]);
                that.allDevicesSearch.push(that.ndata[i]);
              }
              // that.allDevicesSearch = that.allDevices;

            }
            infiniteScroll.complete();
          },
          error => {
            console.log(error);
          });

    }, 100);
  }

  progressBar() {
    // Test interval to show the progress bar
    this.progressIntervalId = setInterval(() => {
      if (this.loadProgress < 100) {
        this.loadProgress += 1;
      }
      else {
        clearInterval(this.loadProgress);
      }
    }, 100);
  }


  livetrack(device) {
    if (this.showDrawer) {
      this.showDrawer = false;
    }
    localStorage.setItem("LiveDevice", "LiveDevice");
    let animationsOptions;
    if (this.plt.is('android')) {
      this.navCtrl.push('LiveSingleDevice', { device: device });
    } else {
      if (this.plt.is('ios')) {
        animationsOptions = {
          animation: 'ios-transition',
          duration: 1000
        }
        this.navCtrl.push('LiveSingleDevice', { device: device }, animationsOptions);
      }
    }
  }

  showHistoryDetail(device) {

    this.navCtrl.push('HistoryDevicePage', {
      device: device
    });
    if (this.showDrawer) {
      this.showDrawer = false;
    }
  }
  device_address(device, index) {
    let that = this;
    let tempcord = {};
    if (!device.last_location) {
      that.allDevicesSearch[index].address = "N/A";
      if (!device.last_lat && !device.last_lng) {
        that.allDevicesSearch[index].address = "N/A";
      } else {
        tempcord = {
          "lat": device.last_lat,
          "long": device.last_lng
        }
        this.apiCall.getAddress(tempcord)
          .subscribe(res => {
            if (res.message === "Address not found in databse") {
              this.geocoderApi.reverseGeocode(device.last_lat, device.last_lng)
                .then(res => {
                  let str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
                  that.saveAddressToServer(str, device.last_lat, device.last_lng);
                  that.allDevicesSearch[index].address = str;

                })
            } else {
              that.allDevicesSearch[index].address = res.address;
            }
          })
      }
    } else {
      tempcord = {
        "lat": device.last_location.lat,
        "long": device.last_location.long
      }
      this.apiCall.getAddress(tempcord)
        .subscribe(res => {
          if (res.message === "Address not found in databse") {
            this.geocoderApi.reverseGeocode(device.last_location.lat, device.last_location.long)
              .then(res => {
                let str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
                that.saveAddressToServer(str, device.last_location.lat, device.last_location.long);
                that.allDevicesSearch[index].address = str;
              })
          } else {
            that.allDevicesSearch[index].address = res.address;
          }
        })
    }
  }

  saveAddressToServer(address, lat, lng) {
    let payLoad = {
      "lat": lat,
      "long": lng,
      "address": address
    }
    this.apiCall.saveGoogleAddressAPI(payLoad)
      .subscribe(() => {
      },
        err => {
          console.log("getting err while trying to save the address: ", err);
        });
  }

  getDuration(device, index) {
    let that = this;
    that.allDevicesSearch[index].duration = "0hrs 0mins";
    if (!device.status_updated_at) {
      that.allDevicesSearch[index].duration = "0hrs 0mins";
    } else if (device.status_updated_at) {
      let a = moment(new Date().toISOString());//now
      let b = moment(new Date(device.status_updated_at).toISOString());
      let mins;
      mins = a.diff(b, 'minutes') % 60;
      that.allDevicesSearch[index].duration = a.diff(b, 'hours') + 'hrs ' + mins + 'mins';
    }
  }

  callSearch(ev) {
    let searchKey = ev.target.value;
    let _baseURL;
    if (this.islogin.isDealer == true) {
      _baseURL = this.apiCall.mainUrl + "devices/getDeviceByUser?email=" + this.islogin.email + "&id=" + this.islogin._id + "&skip=0&limit=10&search=" + searchKey + "&dealer=" + this.islogin._id;
    } else {
      if (this.islogin.isSuperAdmin == true) {
        _baseURL = this.apiCall.mainUrl + "devices/getDeviceByUser?email=" + this.islogin.email + "&id=" + this.islogin._id + "&skip=0&limit=10&search=" + searchKey + "&supAdmin=" + this.islogin._id;
      } else {
        _baseURL = this.apiCall.mainUrl + "devices/getDeviceByUser?email=" + this.islogin.email + "&id=" + this.islogin._id + "&skip=0&limit=10&search=" + searchKey;
      }
    }
    this.apiCall.callSearchService(_baseURL)
      .subscribe(data => {
        this.allDevicesSearch = data.devices;
        this.allDevices = data.devices;
      },
        err => {
          console.log(err);
        });
  }

  getDistanceReport(device, i) {
    let distanceReport = [];
    let user = device.user;
    let user_id;
    if (typeof user === 'object') {
      user_id = user._id;
    } else {
      user_id = user;
    }
    this.apiCall.getDistanceReportApi(new Date(this.datetimeStart).toISOString(), new Date(this.datetimeEnd).toISOString(), user_id, device._id)
      .subscribe(data => {
        distanceReport = data;
        let dd123 = (distanceReport.length > 0) ? parseFloat(distanceReport[0].distance).toFixed(2) : 0;
        device['distance'] = dd123;
      }, error => {
        device['distance'] = 0;
        console.log(error);
      })
  }

  innerFunc(distanceReport) {
    let outerthis = this;
    let i = 0, howManyTimes = distanceReport.length;
    function f() {
      outerthis.allDevicesSearch.push({
        'distance': distanceReport[i].distance,
      });
      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }
    }
    f();
  }

  checkImmobilizePassword() {
    // const rurl = this.apiCall.mainUrl + 'users/get_user_setting';
    // var Var = { uid: this.islogin._id };
    // this.apiCall.urlpasseswithdata(rurl, Var)
    //   .subscribe(data => {
    //     if (!data.engine_cut_psd) {
    //       this.checkedPass = 'PASSWORD_NOT_SET';
    //     } else {
    //       this.checkedPass = 'PASSWORD_SET';
    //     }
    //   })
    let that = this;
    if (!that.users_data.engine_cut_psd) {
      this.checkedPass = 'PASSWORD_NOT_SET';
    } else {
      this.checkedPass = 'PASSWORD_SET';
    }
  }

  // checkPointsAvailability() {
  //   let url = this.apiCall.mainUrl + "users/checkPointAvailablity?u_id=" + this.islogin._id + "&demanded_points=0";
  //   this.apiCall.getSOSReportAPI(url)
  //     .subscribe((resp) => {
  //     },
  //       err => {
  //         console.log("error ", err);
  //       })
  // }

  IgnitionOnOff(d) {
    this.mili = new Date(d.last_ping_on).getTime();
    this.d = new Date();
    this.n = this.d.getTime();
    this.a = this.n - this.mili;
    if (this.a >= 900000) {
      let Popup = this.alertCtrl.create({
        title: this.translate.instant('Warning'),
        message: this.translate.instant('Device has not updated data since more than 15 mins'),
        buttons: [this.translate.instant('Okay')]
      });
      Popup.present();
    } else {
      if (this.showDrawer) {
        this.showDrawer = false;
      }
      let pModal = this.modalCtrl.create(ImmobilizeModelPage, {
        param: d
      });
      pModal.onDidDismiss(() => {
        this.getdevices();
      })
      pModal.present();
    }
  };

  toastmsg(msg) {
    this.toastCtrl.create({
      message: msg,
      duration: 1500,
      position: 'bottom'
    }).present();
  }

  dialNumber(number) {
    if (this.showDrawer) {
      this.showDrawer = false;
    }
    if (number !== "" || number !== undefined) {
      if (this.plt.is('android')) {
        window.open('tel:' + number, '_system');
      } else if (this.plt.is('ios')) {
        this.callNumber.callNumber(number.toString(), true)
          .then(res => console.log('Launched dialer!', res))
          .catch(err => console.log('Error launching dialer', err));
      }
    } else {
      this.toastCtrl.create({
        message: 'Contact number not found!',
        position: 'middle',
        duration: 2000
      }).present();
    }
  }

  getItems(ev: any) {
    const val = ev.target.value.trim();
    this.allDevicesSearch = this.allDevices.filter((item) => {
      return (item.Device_Name.toLowerCase().indexOf(val.toLowerCase()) > -1);
    });
  }

  onClear(ev) {
    this.getdevices();
    ev.target.value = '';
  }

  openAdddeviceModal() {
    let profileModal = this.modalCtrl.create('AddDeviceModalPage');
    profileModal.onDidDismiss(() => {
      this.getdevices();
    });
    profileModal.present();
  }

  iconCheck(status, iconType) {
    let devStatus = status.split(" ");
    if ((iconType == 'car') && (devStatus[0] == "OUT")) {
      this.devIcon = "../../assets/imgs/car_blue_icon.png";
      return this.devIcon;
    } else if ((iconType == 'car') && (devStatus[0] == "RUNNING")) {
      this.devIcon = "../../assets/imgs/car_green_icon.png";
      return this.devIcon;
    } else if ((iconType == 'car') && (devStatus[0] == "STOPPED")) {
      this.devIcon = "../../assets/imgs/car_red_icon.png";
      return this.devIcon;
    } else if ((iconType == 'car') && (devStatus[0] == "IDLING")) {
      this.devIcon = "../../assets/imgs/car_yellow_icon.png";
      return this.devIcon;
    } else if ((iconType == 'car') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
      this.devIcon = "../../assets/imgs/car_grey_icon.png";
      return this.devIcon;
    }
    else if ((iconType == 'bike') && (devStatus[0] == "OUT")) {
      this.devIcon = "../../assets/imgs/bike_blue_icon.png";
      return this.devIcon;
    } else if ((iconType == 'bike') && (devStatus[0] == "RUNNING")) {
      this.devIcon = "../../assets/imgs/bike_green_icon.png";
      return this.devIcon;
    } else if ((iconType == 'bike') && (devStatus[0] == "STOPPED")) {
      this.devIcon = "../../assets/imgs/bike_red_icon.png";
      return this.devIcon;
    } else if ((iconType == 'bike') && (devStatus[0] == "IDLING")) {
      this.devIcon = "../../assets/imgs/bike_yellow_icon.png";
      return this.devIcon;
    } else if ((iconType == 'bike') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
      this.devIcon = "../../assets/imgs/bike_grey_icon.png";
      return this.devIcon;
    }
    else if ((iconType == 'bus') && (devStatus[0] == "OUT")) {
      this.devIcon = "../../assets/imgs/bus_blue.png";
      return this.devIcon;
    } else if ((iconType == 'bus') && (devStatus[0] == "RUNNING")) {
      this.devIcon = "../../assets/imgs/bus_green.png";
      return this.devIcon;
    } else if ((iconType == 'bus') && (devStatus[0] == "STOPPED")) {
      this.devIcon = "../../assets/imgs/bus_red.png";
      return this.devIcon;
    } else if ((iconType == 'bus') && (devStatus[0] == "IDLING")) {
      this.devIcon = "../../assets/imgs/bus_yellow.jpg";
      return this.devIcon;
    } else if ((iconType == 'bus') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
      this.devIcon = "../../assets/imgs/bus_gray.png";
      return this.devIcon;
    }
    else if ((iconType == 'truck') && (devStatus[0] == "OUT")) {
      this.devIcon = "../../assets/imgs/truck_icon_blue.png";
      return this.devIcon;
    } else if ((iconType == 'truck') && (devStatus[0] == "RUNNING")) {
      this.devIcon = "../../assets/imgs/truck_icon_green.png";
      return this.devIcon;
    } else if ((iconType == 'truck') && (devStatus[0] == "STOPPED")) {
      this.devIcon = "../../assets/imgs/truck_icon_red.png";
      return this.devIcon;
    } else if ((iconType == 'truck') && (devStatus[0] == "IDLING")) {
      this.devIcon = "../../assets/imgs/truck_icon_yellow.png";
      return this.devIcon;
    } else if ((iconType == 'truck') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
      this.devIcon = "../../assets/imgs/truck_icon_grey.png";
      return this.devIcon;
    }
    else if ((iconType == 'tractor') && (devStatus[0] == "OUT")) {
      this.devIcon = "../../assets/imgs/tractor_blue.png";
      return this.devIcon;
    } else if ((iconType == 'tractor') && (devStatus[0] == "RUNNING")) {
      this.devIcon = "../../assets/imgs/tractor_green.png";
      return this.devIcon;
    } else if ((iconType == 'tractor') && (devStatus[0] == "STOPPED")) {
      this.devIcon = "../../assets/imgs/tractor_red.png";
      return this.devIcon;
    } else if ((iconType == 'tractor') && (devStatus[0] == "IDLING")) {
      this.devIcon = "../../assets/imgs/tractor_yellow.png";
      return this.devIcon;
    } else if ((iconType == 'tractor') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
      this.devIcon = "../../assets/imgs/tractor_gray.png";
      return this.devIcon;
    }
    else if ((!iconType) && (devStatus[0] == "OUT")) {
      this.devIcon = "../../assets/imgs/car_blue_icon.png";
      return this.devIcon;
    } else if ((!iconType) && (devStatus[0] == "RUNNING")) {
      this.devIcon = "../../assets/imgs/car_green_icon.png";
      return this.devIcon;
    } else if ((!iconType) && (devStatus[0] == "STOPPED")) {
      this.devIcon = "../../assets/imgs/car_red_icon.png";
      return this.devIcon;
    } else if ((!iconType) && (devStatus[0] == "IDLING")) {
      this.devIcon = "../../assets/imgs/car_yellow_icon.png";
      return this.devIcon;
    } else if ((!iconType) && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
      this.devIcon = "../../assets/imgs/car_grey_icon.png";
      return this.devIcon;
    }
    else if ((iconType == 'user') && (devStatus[0] == "OUT")) {
      this.devIcon = "../../assets/imgs/user.png";
      return this.devIcon;
    } else if ((iconType == 'user') && (devStatus[0] == "RUNNING")) {
      this.devIcon = "../../assets/imgs/user.png";
      return this.devIcon;
    } else if ((iconType == 'user') && (devStatus[0] == "STOPPED")) {
      this.devIcon = "../../assets/imgs/user.png";
      return this.devIcon;
    } else if ((iconType == 'user') && (devStatus[0] == "IDLING")) {
      this.devIcon = "../../assets/imgs/user.png";
      return this.devIcon;
    } else if ((iconType == 'user') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
      this.devIcon = "../../assets/imgs/user.png";
      return this.devIcon;
    }
    else if ((iconType == 'jcb') && (devStatus[0] == "OUT")) {
      this.devIcon = "../../assets/imgs/jcb_blue.png";
      return this.devIcon;
    } else if ((iconType == 'jcb') && (devStatus[0] == "RUNNING")) {
      this.devIcon = "../../assets/imgs/jcb_green.png";
      return this.devIcon;
    } else if ((iconType == 'jcb') && (devStatus[0] == "STOPPED")) {
      this.devIcon = "../../assets/imgs/jcb_red.png";
      return this.devIcon;
    } else if ((iconType == 'jcb') && (devStatus[0] == "IDLING")) {
      this.devIcon = "../../assets/imgs/jcb_yellow.png";
      return this.devIcon;
    } else if ((iconType == 'jcb') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
      this.devIcon = "../../assets/imgs/jcb_gray.png";
      return this.devIcon;
    }
    else if ((iconType == 'ambulance') && (devStatus[0] == "OUT")) {
      this.devIcon = "../../assets/imgs/ambulance_blue.png";
      return this.devIcon;
    } else if ((iconType == 'ambulance') && (devStatus[0] == "RUNNING")) {
      this.devIcon = "../../assets/imgs/ambulance_green.png";
      return this.devIcon;
    } else if ((iconType == 'ambulance') && (devStatus[0] == "STOPPED")) {
      this.devIcon = "../../assets/imgs/ambulance_red.png";
      return this.devIcon;
    } else if ((iconType == 'ambulance') && (devStatus[0] == "IDLING")) {
      this.devIcon = "../../assets/imgs/ambulance_yellow.png";
      return this.devIcon;
    } else if ((iconType == 'ambulance') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
      this.devIcon = "../../assets/imgs/ambulance_gray.png";
      return this.devIcon;
    }
    else if ((iconType == 'auto') && (devStatus[0] == "OUT")) {
      this.devIcon = "../../assets/imgs/auto_blue.png";
      return this.devIcon;
    } else if ((iconType == 'auto') && (devStatus[0] == "RUNNING")) {
      this.devIcon = "../../assets/imgs/auto_green.png";
      return this.devIcon;
    } else if ((iconType == 'auto') && (devStatus[0] == "STOPPED")) {
      this.devIcon = "../../assets/imgs/auto_red.png";
      return this.devIcon;
    } else if ((iconType == 'auto') && (devStatus[0] == "IDLING")) {
      this.devIcon = "../../assets/imgs/auto_yellow.png";
      return this.devIcon;
    } else if ((iconType == 'auto') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
      this.devIcon = "../../assets/imgs/auto_gray.png";
      return this.devIcon;
    }
    else {
      this.devIcon = "../../assets/imgs/noIcon.png";
      return this.devIcon
    }
  }
}

@Component({
  template: `
    <ion-list>
      <ion-item class="text-palatino" (click)="editItem()">
        <ion-icon name="create"></ion-icon>&nbsp;&nbsp;{{'edit' | translate}}
      </ion-item>
      <ion-item class="text-san-francisco" (click)="deleteItem()">
        <ion-icon name="trash"></ion-icon>&nbsp;&nbsp;{{'delete' | translate}}
      </ion-item>
      <ion-item class="text-seravek" (click)="shareItem()">
        <ion-icon name="share"></ion-icon>&nbsp;&nbsp;{{'share' | translate}}
      </ion-item>
    </ion-list>
  `
})


export class PopoverPage implements OnInit {
  contentEle: any;
  textEle: any;
  vehData: any;
  islogin: any;
  veh: any;
  constructor(
    navParams: NavParams,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public apiCall: ApiServiceProvider,
    public toastCtrl: ToastController,
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public translate: TranslateService
  ) {
    this.vehData = navParams.get("vehData");
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
  }

  ngOnInit() { }

  editItem() {
    let modal = this.modalCtrl.create('UpdateDevicePage', {
      vehData: this.vehData
    });
    modal.onDidDismiss(() => {
      this.viewCtrl.dismiss();
    })
    modal.present();
  }

  deleteItem() {
    let that = this;
    let alert = this.alertCtrl.create({
      message: this.translate.instant('Do you want to delete this vehicle ?'),
      buttons: [{
        text: this.translate.instant('YES PROCEED'),
        handler: () => {
          that.deleteDevice(that.vehData.Device_ID);
        }
      },
      {
        text: this.translate.instant('NO')
      }]
    });
    alert.present();
  }


  deleteDevice(d_id) {
    this.apiCall.startLoading().present();
    this.apiCall.deleteDeviceCall(d_id, this.islogin._id)
      .subscribe(() => {
        this.apiCall.stopLoading();
        let toast = this.toastCtrl.create({
          message: this.translate.instant('Vehicle deleted successfully!'),
          position: 'bottom',
          duration: 2000
        });

        toast.onDidDismiss(() => {
          this.viewCtrl.dismiss();
        });

        toast.present();
      },
        err => {
          this.apiCall.stopLoading();
          let body = err._body;
          let msg = JSON.parse(body);
          let alert = this.alertCtrl.create({
            message: msg.message,
            buttons: [this.translate.instant('Okay')]
          });
          alert.present();
        });
  }

  shareItem() {
    let that = this;
    const prompt = this.alertCtrl.create({
      title: this.translate.instant('Share Vehicle'),
      inputs: [
        {
          name: 'device_name',
          value: that.vehData.Device_Name
        },
        {
          name: 'shareId',
          placeholder: this.translate.instant('Enter Email Id/Mobile Number')
        },
      ],
      buttons: [
        {
          text: this.translate.instant('Cancel'),
          handler: () => {
          }
        },
        {
          text: this.translate.instant('Share'),
          handler: data => {
            that.sharedevices(data)
          }
        }
      ]
    });
    prompt.present();
  }

  sharedevices(data) {
    let that = this;
    let devicedetails = {
      "did": that.vehData._id,
      "email": data.shareId,
      "uid": that.islogin._id
    }

    that.apiCall.startLoading().present();
    that.apiCall.deviceShareCall(devicedetails)
      .subscribe(data => {
        that.apiCall.stopLoading();
        let toast = that.toastCtrl.create({
          message: data.message,
          position: 'bottom',
          duration: 2000
        });

        toast.present();
      },
        err => {
          that.apiCall.stopLoading();
          let body = err._body;
          let msg = JSON.parse(body);
          let alert = this.alertCtrl.create({
            message: msg.message,
            buttons: [this.translate.instant('Okay')]
          });
          alert.present();
        });
  }
}


@Component({
  selector: 'page-custom-duration',
  templateUrl: './custom-duration.html'
})

export class CustomDurationPage {
  constructor(private viewCtrl: ViewController, private toastCtrl: ToastController) {

  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
  c_duration: any;
  _submit() {
    if (this.c_duration == undefined) {
      let toast = this.toastCtrl.create({
        message: "Please enter duration to proceed further.",
        duration: 2000,
        position: "middle"
      });
      toast.present()
    } else {
      this.viewCtrl.dismiss({
        param: this.c_duration
      });
    }

  }
}

@Component({
  templateUrl: "./paymantgatwey-false.html",
  styles: [
    `
      .col {
        padding: 0px;
      }
    `,
  ],
})
export class PaymantgatweyPage implements OnInit {
  fdate: any;
  tdate: any;
  uid: any;
  paramData: any = {};
  ddata: any = [];
  vname: any;
  customerId: any;
  paymentHistory: any[] = [];
  date: any;
  expDate: any[] = [];
  support_contact: any;
  islogin: any;
  constructor(public apiCall: ApiServiceProvider, public navParam: NavParams, public navCtrl: NavController, private callNumbers: CallNumber, public plt: Platform, public toastCtrl: ToastController,) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
  }

  ngOnInit() {
    this.getDetails();
  }

  callNumber(number) {
    if (number !== "" || number !== undefined) {
      if (this.plt.is('android')) {
        window.open('tel:' + number, '_system');
      } else if (this.plt.is('ios')) {
        this.callNumbers.callNumber(number.toString(), true)
          .then(res => console.log('Launched dialer!', res))
          .catch(err => console.log('Error launching dialer', err));
      }
    } else {
      this.toastCtrl.create({
        message: 'Contact number not found!',
        position: 'middle',
        duration: 2000
      }).present();
    }
  }
  getDetails() {
    let url = this.apiCall.mainUrl + "users/get_user_setting";
    let payload = {
      uid: this.islogin._id,
    };
    this.apiCall.startLoading().present();
    this.apiCall.urlpasseswithdatatwo(url, payload).subscribe(
      (respData) => {
        this.ddata = respData;
        this.ddata = JSON.parse(this.ddata._body).Service;
        this.apiCall.stopLoading();
      },
      (err) => {
        console.log("oops got error: ", err);
        this.apiCall.stopLoading();
      }
    );
  }
}
