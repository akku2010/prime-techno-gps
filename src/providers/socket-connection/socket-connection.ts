import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular';
import io from 'socket.io-client';
import { ApiServiceProvider } from '../api-service/api-service';


@Injectable()
export class SocketConnectionProvider {
  // public _socketLastLocation: any;
  public _notifIO: any;
  public _gps: any;
  public _userACC: any;
  socketurl: string;
  userdetails: any = {};
  socketIO: any;
  public initialMapLoad: boolean = true;
  // public multipleLiveData: any = {};

  constructor(public http: HttpClient, private apiCall: ApiServiceProvider, private events: Events) {
    console.log('Hello SocketConnectionProvider Provider');
    this.events.subscribe('user:updated', (udata) => {
      this.userdetails = udata;
    })
    // this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
    this.getSocketUrl();

  }

  getSocketUrl() {
    // if (localStorage.getItem('SOCKET_URL') != null) {
    //   this.socketurl = JSON.parse(localStorage.getItem('SOCKET_URL')) + '/';
    // } else {
    //   this.socketurl = this.apiCall.mainUrl;
    // }
    this.socketurl = "https://soc.oneqlik.in/"
  }

  // getSocket_gps(){
  //   return  this._gps;
  // }

  gpsSocketConnect() {
    let that = this;
    that._gps = io.connect(this.socketurl + 'gps?userId=' + this.userdetails._id, { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
    that._gps.on('connect', () => {
      console.log('GPS IO Connected: '+ that._gps.connected);
      that.events.subscribe("_socketReconnected" , () =>{
        that.events.publish("Reconnected");
      });
    });

    that._gps.on('connect_failed', function () {
      console.log("Sorry, there seems to be an issue with the socket connection!");
    });
    that._gps.on('error', function () {
      console.log("socket io encountered an error!");
    })
    that._gps.on('reconnecting', function () {
      console.log("socket io trying to reconnect!");
      that.events.publish("_socketReconnected");
    })
    that._gps.on('reconnect_failed', function () {
      console.log("reconnecting failed!");
    })
    that._gps.on('disconnect', function () {
      console.log('socket is disconnect!')
    });
  }

  userACCSocketConnect() {
    let that = this;
    that._userACC = io.connect(this.socketurl + 'userACC?userId=' + this.userdetails._id, { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false, forceNew: false });
    that._userACC.on('connect', () => {
      console.log('userACC IO Connected: ', that._userACC.connected);
    });
  }

  userACCSocketDisconnect() {
    this._userACC.disconnect();
    delete this._userACC.io.nsps[this._userACC.nsp];
    delete this._userACC;
    // return new Promise((resolve, reject) => {
    //   this._userACC.on('disconnect', function (result) {
    //     console.log('userACC socket-io disconnect!')
    //     resolve(result);
    //   });
    // });
  }

  gpsSocketDisconnect() {
    return new Promise((resolve, reject) => {
      this._gps.on('disconnect', function (result) {
        console.log('GPS socket-io disconnect!')
        resolve(result);
      });
    });
  }

  disconnectUserACC(imeiList) {
    let that = this;
    imeiList.forEach(imei => {
      that._userACC.off(imei + 'acc');
    });
    that._userACC.emit('leaveRoom', imeiList);
    that.leavePromise = that.onceFunc();
  }

  leavePromise: any;
  gpsRemoveAllListners(imeiList) {
    let that = this;
    imeiList.forEach(imei => {
      that._gps.off(imei + 'acc');
    });
    that._gps.emit('leaveRoom', imeiList);
    that.leavePromise = that.onceFunc();
  }

  onceFunc() {
    return new Promise((resolve, reject) => {
      this._gps.once('leaveRoomConfirm', function (result) {
        console.log('GPS socket-io leaveRoomConfirm!')
        resolve(result);
      });
    });
  }

  socketDisconnect() {
    let that = this;
    debugger
    if (that._gps != undefined)
      that._gps.disconnect();
    // console.log("connected ", that._gps.connected);
    // console.log("disconnected ", that._gps.disconnected);

    // that._gps.on("disconnect", () => {
    //   console.log(that._gps.connected); // false
    // });
    // that._gps.on("disconnect", (reason) => {
    // //   debugger
    //   // console.log('GPS IO disconnected: ', reason);
    // that.events.publish("SocketDisconnected");
    // console.log("event publish executed")
    //   // if (reason === "io server disconnect") {
    //   //   // the disconnection was initiated by the server, you need to reconnect manually
    //   //   that.gpsSocketConnect();
    //   // }
    // });
  }

  notifSocketDisconnect() {
    let that = this;
    if (that._notifIO != undefined)
      that._notifIO.disconnect();
  }

  notifIOSocketConnect() {
    this._notifIO = io.connect(this.socketurl + 'notifIOV2?userId=' + this.userdetails._id, { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
    this._notifIO.on('connect', () => {
      console.log('NotifIO Connected');
    });

    this._notifIO.on(this.userdetails._id, (msg) => {
      this.events.publish("cart:addmore", msg);
    })
  }
}
