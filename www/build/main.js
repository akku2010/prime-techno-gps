webpackJsonp([79],{

/***/ 10:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(340);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(497);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_timeout__ = __webpack_require__(498);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_timeout___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_timeout__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ApiServiceProvider = /** @class */ (function () {
    function ApiServiceProvider(http, loadingCtrl, toastCtrl) {
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json; charset=utf-8' });
        this.googleMapsAPIKey = "AIzaSyCmSXnoBBGekErvqBvu5N7dbeqdH1pHp7c";
        this.mainUrl = "https://www.oneqlik.in/";
        this.appId = "OneQlikVTS";
        console.log('Hello ApiServiceProvider Provider');
        this.callBaseURL();
    }
    ApiServiceProvider.prototype.callBaseURL = function () {
        if (localStorage.getItem("ENTERED_BASE_URL") === null) {
            var url = this.mainUrl + "pullData/getUrlnew";
            this.getSOSReportAPI(url)
                .subscribe(function (data) {
                if (data.url) {
                    localStorage.setItem("BASE_URL", JSON.stringify(data.url));
                }
            });
        }
        else {
            localStorage.setItem("BASE_URL", localStorage.getItem("ENTERED_BASE_URL"));
        }
        if (localStorage.getItem('ENTERED_BASE_URL') !== null) {
            this.mainUrl = JSON.parse(localStorage.getItem('ENTERED_BASE_URL')) + '/';
            this.usersURL = this.mainUrl + "users/";
            this.devicesURL = this.mainUrl + "devices";
            this.gpsURL = this.mainUrl + "gps";
            this.geofencingURL = this.mainUrl + "geofencing";
            this.trackRouteURL = this.mainUrl + "trackRoute";
            this.groupURL = this.mainUrl + "groups/";
            this.notifsURL = this.mainUrl + "notifs";
            this.stoppageURL = this.mainUrl + "stoppage";
            this.summaryURL = this.mainUrl + "summary";
            this.shareURL = this.mainUrl + "share";
        }
        else {
            if (localStorage.getItem('BASE_URL') !== null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.usersURL = this.mainUrl + "users/";
                this.devicesURL = this.mainUrl + "devices";
                this.gpsURL = this.mainUrl + "gps";
                this.geofencingURL = this.mainUrl + "geofencing";
                this.trackRouteURL = this.mainUrl + "trackRoute";
                this.groupURL = this.mainUrl + "groups/";
                this.notifsURL = this.mainUrl + "notifs";
                this.stoppageURL = this.mainUrl + "stoppage";
                this.summaryURL = this.mainUrl + "summary";
                this.shareURL = this.mainUrl + "share";
            }
        }
    };
    ////////////////// LOADING SERVICE /////////////////
    ApiServiceProvider.prototype.toastMsgStarted = function () {
        this.toast = this.toastCtrl.create({
            message: "Loading/Refreshing contents please wait...",
            position: "bottom",
            duration: 2000
        });
        return this.toast.present();
    };
    ApiServiceProvider.prototype.toastMsgDismised = function () {
        return this.toast.dismiss();
    };
    ApiServiceProvider.prototype.startLoadingnew = function (key) {
        var str;
        if (key == 1) {
            str = 'unlocking';
        }
        else {
            str = 'locking';
        }
        return this.loading1 = this.loadingCtrl.create({
            content: "Please wait for some time, as we are " + str + " your vehicle...",
            spinner: "bubbles"
        });
    };
    ApiServiceProvider.prototype.stopLoadingnw = function () {
        return this.loading1.dismiss();
    };
    ApiServiceProvider.prototype.startLoading = function () {
        return this.loading = this.loadingCtrl.create({
            content: "Please wait...",
            spinner: "bubbles"
        });
    };
    ApiServiceProvider.prototype.stopLoading = function () {
        return this.loading.dismiss();
    };
    ApiServiceProvider.prototype.getCurrency = function () {
        return this.http.get('./assets/json/currency.json')
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getCountryCode = function () {
        return this.http.get('./assets/json/countryCode.json')
            .map(function (res) { return res.json(); });
    };
    ////////////////// END LOADING SERVICE /////////////
    ApiServiceProvider.prototype.getDealerInfo = function () {
        return this.http.get(this.mainUrl + "users/getDealerInfo?&url=" + "everplusgps")
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.updatePOIAPI123 = function (pay) {
        return this.http.post(this.mainUrl + "poi/updatePOI", pay, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.updatePOIAPI = function (pay) {
        return this.http.post(this.mainUrl + "vehtra/poi/updatePOI", pay, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.addPOIAPI = function (payload) {
        return this.http.post(this.mainUrl + "poi/addpoi", payload, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deletePOIAPI = function (_id) {
        return this.http.get(this.mainUrl + "poi/deletePoi?_id=" + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.siginupverifyCall = function (usersignup) {
        this.callBaseURL();
        return this.http.post(this.usersURL + "/signUpZogo", usersignup, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.resendOtp = function (phnum) {
        this.callBaseURL();
        return this.http.post(this.usersURL + "/sendOtpZRides", phnum)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getpoireportAPI = function (_id, pageNo, fromT, toT, poiId, devid) {
        return this.http.get(this.mainUrl + "poi/poiReport?user=" + _id + "&s=" + pageNo + "&l=9&from=" + fromT + "&to=" + toT + "&poi=" + poiId + "&device=" + devid, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getPoisAPI = function (id) {
        return this.http.get(this.mainUrl + "poi/getPois?user=" + id)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.updateDL = function (updateDL) {
        return this.http.post(this.mainUrl + 'users' + "/zogoUserUpdate", updateDL, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getsingledevice = function (id) {
        this.callBaseURL();
        return this.http.get(this.devicesURL + "/getDevicebyId?deviceId=" + id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getSOSReportAPI = function (url) {
        return this.http.get(url, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.saveGoogleAddressAPI = function (data) {
        var url = this.mainUrl + "googleAddress/addGoogleAddress";
        return this.http.post(url, data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.urlpasseswithdata = function (url, data) {
        return this.http.post(url, data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.DealerSearchService = function (_id, pageno, limit, key) {
        this.callBaseURL();
        return this.http.get(this.usersURL + 'getAllDealerVehicles?supAdmin=' + _id + '&pageNo=' + pageno + '&size=' + limit + '&search=' + key, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deleteDealerCall = function (deletePayload) {
        this.callBaseURL();
        return this.http.post(this.usersURL + 'deleteUser', deletePayload, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDealersCall = function (_id, pageno, limit, searchKey) {
        this.callBaseURL();
        if (searchKey != undefined)
            return this.http.get(this.usersURL + 'getAllDealerDetailsMobile?supAdmin=' + _id + '&pageNo=' + pageno + '&size=' + limit + '&search=' + searchKey, { headers: this.headers })
                .map(function (res) { return res.json(); });
        else
            return this.http.get(this.usersURL + 'getAllDealerDetailsMobile?supAdmin=' + _id + '&pageNo=' + pageno + '&size=' + limit, { headers: this.headers })
                .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDealers = function (_id, pageno, limit) {
        this.callBaseURL();
        return this.http.get(this.usersURL + 'getDealers?supAdmin=' + _id + '&pageNo=' + pageno + '&size=' + limit, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.callSearchService = function (baseURL) {
        return this.http.get(baseURL, { headers: this.headers })
            .map(function (resp) { return resp.json(); });
    };
    ApiServiceProvider.prototype.callResponse = function (_id) {
        return this.http.get(this.mainUrl + "trackRouteMap/getRideStatusApp?_id=" + _id, { headers: this.headers })
            .map(function (resp) { return resp.json(); });
    };
    ApiServiceProvider.prototype.serverLevelonoff = function (data) {
        this.callBaseURL();
        return this.http.post(this.devicesURL + "/addCommandQueue", data, { headers: this.headers })
            .timeout(5000)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.updatePassword = function (data) {
        this.callBaseURL();
        return this.http.post(this.usersURL + "updatePassword", data, { headers: this.headers })
            .timeout(5000)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.updateprofile = function (data) {
        this.callBaseURL();
        return this.http.post(this.usersURL + 'Account_Edit', data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getGeofenceCall = function (_id) {
        this.callBaseURL();
        return this.http.get(this.geofencingURL + '/getgeofence?uid=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.announcementData = function (data) {
        var url = this.usersURL + "notificationMessage";
        return this.http.post(url, data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.announcementDataa = function (data) {
        var url = this.mainUrl + "users/" + "translate";
        return this.http.post(url, data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.get7daysData = function (a, t) {
        this.callBaseURL();
        return this.http.get(this.gpsURL + '/getDashGraph?imei=' + a + '&t=' + t, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.dataRemoveFuncCall = function (_id, did) {
        this.callBaseURL();
        return this.http.get(this.devicesURL + '/RemoveShareDevice?did=' + did + '&uid=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.tripReviewCall = function (device_id, stime, etime) {
        this.callBaseURL();
        return this.http.get(this.gpsURL + '?id=' + device_id + '&from=' + stime + '&to=' + etime, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.sendTokenCall = function (payLoad) {
        this.callBaseURL();
        return this.http.post(this.shareURL + "/propagate", payLoad, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.shareLivetrackCall = function (data) {
        this.callBaseURL();
        return this.http.post(this.shareURL, data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDriverList = function (_id) {
        return this.http.get(this.mainUrl + "driver/getDrivers?userid=" + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.filterByDateCall = function (_id, skip, limit, dates) {
        this.callBaseURL();
        var from = new Date(dates.fromDate).toISOString();
        var to = new Date(dates.toDate).toISOString();
        return this.http.get(this.notifsURL + '/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit + '&f=' + from + '&t=' + to, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getWorkingHourReportApi = function (starttime, endtime, Ignitiondevice_id, _id) {
        if (this.notifsURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.notifsURL = this.mainUrl + "notifs/";
            }
        }
        return this.http.get(this.notifsURL + "/workingReport?from_date=" + starttime + '&to_date=' + endtime + '&_u=' + _id + '&device=' + Ignitiondevice_id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.filterByType = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getFilteredcall = function (_id, skip, limit, key) {
        this.callBaseURL();
        return this.http.get(this.notifsURL + '/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit + '&device=' + key, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDataOnScroll = function (_id, skip, limit) {
        this.callBaseURL();
        return this.http.get(this.notifsURL + '/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getACReportAPI = function (fdate, tdate, userid, imei) {
        this.callBaseURL();
        return this.http.get(this.notifsURL + '/acReport?from_date=' + fdate + '&to_date=' + tdate + '&_u=' + userid + '&device=' + imei, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDetailACReportAPI = function (fdate, tdate, userid, imei) {
        this.callBaseURL();
        return this.http.get(this.notifsURL + "/acReport?from_date=" + fdate + "&to_date=" + tdate + "&_u=" + userid + "&vname=" + imei, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.distanceDaywiseData = function (fdate, tdate, userid, device, skip, limit) {
        this.callBaseURL();
        return this.http.get(this.summaryURL + "/distanceNew?from=" + fdate + "&to=" + tdate + "&user=" + userid + "&skip=" + skip + "&limit=" + limit + "&device=" + device)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getVehicleListCall = function (_url) {
        return this.http.get(_url, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.trip_detailCall = function (_id, starttime, endtime, did) {
        this.callBaseURL();
        if (did == undefined) {
            return this.http.get(this.mainUrl + 'user_trip/trip_detail?uId=' + _id + '&from_date=' + starttime + '&to_date=' + endtime, { headers: this.headers })
                .map(function (res) { return res.json(); });
        }
        else {
            return this.http.get(this.mainUrl + 'user_trip/trip_detail?uId=' + _id + '&from_date=' + starttime + '&to_date=' + endtime + '&device=' + did, { headers: this.headers })
                .map(function (res) { return res.json(); });
        }
    };
    ApiServiceProvider.prototype.trackRouteDataCall = function (data) {
        this.callBaseURL();
        return this.http.post(this.trackRouteURL, data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getRechargePlanApi = function (supAdmin) {
        this.callBaseURL();
        return this.http.get(this.mainUrl + "RechargePlan/get?SupAdmin=" + supAdmin)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.gettrackRouteCall = function (_id, data) {
        this.callBaseURL();
        return this.http.post(this.trackRouteURL + '/' + _id, data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.trackRouteCall = function (_id) {
        this.callBaseURL();
        return this.http.delete(this.trackRouteURL + '/' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getRoutesCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getStoppageApi = function (starttime, endtime, Ignitiondevice_id, _id) {
        this.callBaseURL();
        return this.http.get(this.stoppageURL + "/stoppageReport?from_date=" + starttime + '&to_date=' + endtime + '&vname=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getIgiApi = function (starttime, endtime, Ignitiondevice_id, _id) {
        this.callBaseURL();
        return this.http.get(this.notifsURL + "/ignitionReport?from_date=" + starttime + '&to_date=' + endtime + '&vname=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getOverSpeedApi = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getGeogenceReportApi = function (starttime, endtime, Ignitiondevice_id, _id) {
        this.callBaseURL();
        return this.http.get(this.notifsURL + "/GeoFencingReport?from_date=" + starttime + '&to_date=' + endtime + '&geoid=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getFuelApi = function (starttime, endtime, Ignitiondevice_id, _id) {
        this.callBaseURL();
        return this.http.get(this.notifsURL + "/fuelReport?from_date=" + starttime + '&to_date=' + endtime + '&vname=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDistanceReportApi = function (starttime, endtime, _id, Ignitiondevice_id) {
        this.callBaseURL();
        return this.http.get(this.summaryURL + "/distance?from=" + starttime + '&to=' + endtime + '&user=' + _id + '&device=' + Ignitiondevice_id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDailyReport = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    // getRenewalChareges(supid) {
    //   return this.http.get(this.mainUrl + 'users/getCustumerDetail?uid=' + supid, { headers: this.headers })
    //     .map(res => res.json());
    // }
    ApiServiceProvider.prototype.getAddressApi = function (data) {
        return this.http.post(this.mainUrl + "gps/getaddress", data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDailyReport1 = function (url, payload) {
        return this.http.post(url, payload, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.contactusApi = function (contactdata) {
        this.callBaseURL();
        return this.http.post(this.usersURL + "contactous", contactdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.createTicketApi = function (contactdata) {
        return this.http.post(this.mainUrl + "customer_support/post_inquiry", contactdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getAllNotificationCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.addgeofenceCall = function (data) {
        this.callBaseURL();
        return this.http.post(this.geofencingURL + '/addgeofence', data, { headers: this.headers })
            .map(function (res) { return res; });
    };
    ApiServiceProvider.prototype.getdevicegeofenceCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.geofencestatusCall = function (_id, status, entering, exiting) {
        this.callBaseURL();
        return this.http.get(this.geofencingURL + '/geofencestatus?gid=' + _id + '&status=' + status + '&entering=' + entering + '&exiting=' + exiting, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deleteGeoCall = function (_id) {
        this.callBaseURL();
        return this.http.get(this.geofencingURL + '/deletegeofence?id=' + _id, { headers: this.headers })
            .map(function (res) { return res; });
    };
    ApiServiceProvider.prototype.getallgeofenceCall = function (_id) {
        this.callBaseURL();
        return this.http.get(this.mainUrl + 'geofencing/getallgeofence1?uid=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getnotifsignitionReportForMobile = function (datetimestart, datetimeend, ul, _id) {
        this.callBaseURL();
        return this.http.get(this.mainUrl + 'notifs/ignitionReportForMobile?from_date=' + datetimestart + '&to_date=' + datetimeend + '&_u=' + ul + '&device=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getnotifsacReportForMobile = function (datetimestart, datetimeend, ul, _id) {
        this.callBaseURL();
        return this.http.get(this.mainUrl + 'notifs/acReportForMobile?from_date=' + datetimestart + '&to_date=' + datetimeend + '&_u=' + ul + '&device=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.urlpasseswithdatatwo = function (url, data) {
        return this.http.post(url, data, { headers: this.headers })
            .map(function (res) { return res; });
    };
    ApiServiceProvider.prototype.get_user_Detail = function (_id) {
        console.log("ddddddd--->", _id);
        this.callBaseURL();
        return this.http.post(this.mainUrl + 'users/getuserDetail?uid=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.user_statusCall = function (data) {
        this.callBaseURL();
        return this.http.post(this.usersURL + 'user_status', data, { headers: this.headers })
            .map(function (res) { return res; });
    };
    ApiServiceProvider.prototype.editUserDetailsCall = function (devicedetails) {
        this.callBaseURL();
        return this.http.post(this.usersURL + 'editUserDetails', devicedetails, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getAllDealerVehiclesCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.addGroupCall = function (devicedetails) {
        this.callBaseURL();
        return this.http.post(this.groupURL + 'addGroup', devicedetails, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getVehicleTypesCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getAllUsersCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDeviceModelCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDevModelById = function (id) {
        this.callBaseURL();
        return this.http.get(this.mainUrl + "deviceModel/getDevModelById?id=" + id)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.groupsCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.addDeviceCall = function (devicedetails) {
        this.callBaseURL();
        return this.http.post(this.devicesURL + '/addDevice', devicedetails, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getAddress = function (cord) {
        return this.http.post(this.mainUrl + "googleAddress/getGoogleAddress", cord, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getCustomersCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getTollApi = function (starttime, endtime, Ignitiondevice_id, _id) {
        this.callBaseURL();
        return this.http.get(this.mainUrl + "notifs/tollReport?from_date=" + starttime + '&to_date=' + endtime + '&device=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.payment = function (data) {
        return this.http.post(this.mainUrl + "pullData/getSetuLink", data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.geofenceCall = function (_id) {
        this.callBaseURL();
        return this.http.get(this.geofencingURL + "/getgeofence?uid=" + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.forgotPassApi = function (mobno) {
        this.callBaseURL();
        return this.http.get(this.usersURL + "forgotpwd?cred=" + mobno.cred, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.forgotPassMobApi = function (Passwordset) {
        this.callBaseURL();
        return this.http.get(this.usersURL + "forgotpwd?phone=" + Passwordset.otpMess + "&otp=" + Passwordset.otp + "&newpwd=" + Passwordset.newpwd + "&cred=" + Passwordset.otpMess, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.loginApi = function (userdata) {
        this.callBaseURL();
        return this.http.post(this.usersURL + "LoginWithOtp", userdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.signupApi = function (usersignupdata) {
        this.callBaseURL();
        return this.http.post(this.usersURL + "signUp", usersignupdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.dashboardcall = function (_baseUrl) {
        return this.http.get(_baseUrl, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.stoppedDevices = function (_id, email, off_ids) {
        this.callBaseURL();
        return this.http.get(this.devicesURL + '/getDeviceByUser?id=' + _id + '&email=' + email + '&dev=' + off_ids, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.livedatacall = function (_id, email) {
        this.callBaseURL();
        return this.http.get(this.devicesURL + "/getDeviceByUser?id=" + _id + "&email=" + email, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getdevicesApi = function (_id, email) {
        this.callBaseURL();
        return this.http.get(this.devicesURL + '/getDeviceByUser?id=' + _id + '&email=' + email, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getdevicesForAllVehiclesApi = function (link) {
        return this.http.get(link, { headers: this.headers })
            .timeout(500000000)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.ignitionoffCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deviceupdateCall = function (devicedetail) {
        this.callBaseURL();
        return this.http.post(this.devicesURL + "/deviceupdate", devicedetail, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDistanceSpeedCall = function (device_id, from, to) {
        this.callBaseURL();
        return this.http.get(this.gpsURL + '/getDistanceSpeed?imei=' + device_id + '&from=' + from + '&to=' + to, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.stoppage_detail = function (_id, from, to, device_id) {
        this.callBaseURL();
        return this.http.get(this.stoppageURL + '/stoppage_detail?uId=' + _id + '&from_date=' + from + '&to_date=' + to + '&device=' + device_id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.gpsCall = function (device_id, from, to) {
        this.callBaseURL();
        return this.http.get(this.gpsURL + '?id=' + device_id + '&from=' + from + '&to=' + to, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.addCustomerPayment = function (data) {
        return this.http.post(this.mainUrl + "CustomerPayment/add", data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.addRenewDevices = function (data) {
        return this.http.post(this.mainUrl + "devices/renewDevices", data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getCustomerPayment = function (cusId) {
        return this.http.get(this.mainUrl + "CustomerPayment/get?Customer_ID=" + cusId)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getcustToken = function (id) {
        this.callBaseURL();
        return this.http.get(this.usersURL + "getCustumerDetail?uid=" + id)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getSummaryReportApi = function (starttime, endtime, _id, device_id) {
        this.callBaseURL();
        return this.http.get(this.mainUrl + "summary/summaryReport?from=" + starttime + '&to=' + endtime + '&user=' + _id + '&device=' + device_id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDaywiseReportApi = function (starttime, endtime, _id, device_id) {
        this.callBaseURL();
        return this.http.get(this.summaryURL + "/getDayWiseReport?from=" + starttime + '&to=' + endtime + '&user=' + _id + '&device=' + device_id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getallrouteCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getSpeedReport = function (_id, time) {
        this.callBaseURL();
        return this.http.get(this.gpsURL + '/getGpsSpeedReport?imei=' + _id + '&time=' + time, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deleteDeviceCall = function (d_id, _id) {
        this.callBaseURL();
        return this.http.get(this.devicesURL + "/deleteDevice?did=" + d_id + '&userId=' + _id, { headers: this.headers })
            .map(function (res) { return res; });
    };
    ApiServiceProvider.prototype.deviceShareCall = function (data) {
        this.callBaseURL();
        return this.http.get(this.devicesURL + "/deviceShare?email=" + data.email + "&uid=" + data.uid + "&did=" + data.did, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.pushnotifyCall = function (pushdata) {
        this.callBaseURL();
        return this.http.post(this.usersURL + "PushNotification", pushdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.pullnotifyCall = function (pushdata) {
        this.callBaseURL();
        return this.http.post(this.usersURL + "PullNotification", pushdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getGroupCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deleteGroupCall = function (d_id) {
        this.callBaseURL();
        return this.http.get(this.groupURL + "deleteGroup?_id=" + d_id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deleteCustomerCall = function (data) {
        this.callBaseURL();
        return this.http.post(this.usersURL + 'deleteUser', data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getAllDealerCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.route_details = function (_id, user_id) {
        this.callBaseURL();
        return this.http.get(this.trackRouteURL + '/routepath/getRoutePathWithPoi?id=' + _id + '&user=' + user_id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.callcustomerSearchService = function (uid, pageno, limit, seachKey) {
        this.callBaseURL();
        return this.http.get(this.usersURL + 'getCust?uid=' + uid + '&pageNo=' + pageno + '&size=' + limit + '&search=' + seachKey)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["ToastController"]])
    ], ApiServiceProvider);
    return ApiServiceProvider;
}());

//# sourceMappingURL=api-service.js.map

/***/ }),

/***/ 111:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return TimePickerModal; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ParkingAlarmPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_native_audio__ = __webpack_require__(144);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import { DatePicker } from '@ionic-native/date-picker';


// import * as moment from 'moment';

var TimePickerModal = /** @class */ (function () {
    function TimePickerModal(navParams, apiCall, viewCtrl, alertCtrl, translate, navCtrl, toastCtrl) {
        this.apiCall = apiCall;
        this.viewCtrl = viewCtrl;
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.start_ampm = "AM";
        this.end_ampm = "AM";
        this.schedule = false;
        this.showScheduler = false;
        this.showUpdateBtn = false;
        this.showSubmitBtn = true;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.data = navParams.get('data');
        this.screenKey = navParams.get('key');
        if (navParams.get('key') == 'tow') {
            this.showKey = this.translate.instant('Tow');
        }
        else {
            if (navParams.get('key') == 'parking') {
                this.showKey = this.translate.instant('Parking');
            }
        }
        var startHr, startMin, endHr, endMin;
        if (this.screenKey === 'tow') {
            if (this.data.towAlert || this.data.towTime) {
                this.schedule = this.data.towAlert;
                this.showScheduler = true;
                this.showUpdateBtn = true;
                this.showSubmitBtn = false;
            }
            if (this.data.towTime !== undefined) {
                // this.schedule = "true";
                startHr = new Date(this.data.towTime.start).getHours();
                startMin = new Date(this.data.towTime.start).getMinutes();
                this.startHourNumber = this.pad(startHr, 2);
                this.startMinutesNumber = this.pad(startMin, 2);
                endHr = new Date(this.data.towTime.end).getHours();
                endMin = new Date(this.data.towTime.end).getMinutes();
                this.endHourNumber = this.pad(endHr, 2);
                this.endMinutesNumber = this.pad(endMin, 2);
            }
            else {
                this.startHourNumber = this.pad(1, 2);
                this.startMinutesNumber = this.pad(0, 2);
                this.endHourNumber = this.pad(1, 2);
                this.endMinutesNumber = this.pad(0, 2);
            }
        }
        else if (this.screenKey === 'parking') {
            if (this.data.theftAlert || this.data.theftTime) {
                this.schedule = this.data.theftAlert;
                this.showScheduler = true;
                this.showUpdateBtn = true;
                this.showSubmitBtn = false;
            }
            if (this.data.theftTime) {
            }
            if (this.data.theftTime !== undefined) {
                // this.schedule = "true";
                startHr = new Date(this.data.theftTime.start).getHours();
                startMin = new Date(this.data.theftTime.start).getMinutes();
                this.startHourNumber = this.pad(startHr, 2);
                this.startMinutesNumber = this.pad(startMin, 2);
                endHr = new Date(this.data.theftTime.end).getHours();
                endMin = new Date(this.data.theftTime.end).getMinutes();
                this.endHourNumber = this.pad(endHr, 2);
                this.endMinutesNumber = this.pad(endMin, 2);
            }
            else {
                this.startHourNumber = this.pad(1, 2);
                this.startMinutesNumber = this.pad(0, 2);
                this.endHourNumber = this.pad(1, 2);
                this.endMinutesNumber = this.pad(0, 2);
            }
        }
        console.log("data parameters: ", this.data);
    }
    TimePickerModal.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    TimePickerModal.prototype.pad = function (number, length) {
        var str = '' + number;
        while (str.length < length) {
            str = '0' + str;
        }
        return str;
    };
    TimePickerModal.prototype.setScheduler = function (sch) {
        // console.log('schedluler: ', sch)
        if (this.screenKey === 'tow') {
            this.towAlertCall(sch);
        }
        else if (this.screenKey === 'parking') {
            this.fonctionTest(sch);
        }
    };
    TimePickerModal.prototype.fonctionTest = function (d) {
        var _this = this;
        debugger;
        var theft;
        var that = this;
        theft = !(that.data.theftAlert);
        if (theft) {
            var alert_1 = this.alertCtrl.create({
                title: this.translate.instant('Confirm'),
                message: this.translate.instant('Are you sure you want to activate parking alarm? On activating this alert you will get receive notification if vehicle moves.'),
                buttons: [
                    {
                        text: this.translate.instant('YES PROCEED'),
                        handler: function () {
                            var payload = {
                                "_id": that.data._id,
                                "theftAlert": theft
                            };
                            _this.apiCall.startLoading();
                            _this.apiCall.deviceupdateCall(payload)
                                .subscribe(function (data) {
                                _this.apiCall.stopLoading();
                                var toast = _this.toastCtrl.create({
                                    message: _this.translate.instant('Parking alarm Activated!'),
                                    position: "bottom",
                                    duration: 1000
                                });
                                toast.present();
                                _this.getdevices();
                            }, function (err) {
                                _this.apiCall.stopLoading();
                            });
                        }
                    },
                    {
                        text: this.translate.instant('Back'),
                        handler: function () {
                            that.schedule = false;
                            // that.showScheduler = false;
                            // that.data.theftAlert = !(that.data.theftAlert);
                        }
                    }
                ]
            });
            alert_1.present();
        }
        else {
            var alert_2 = this.alertCtrl.create({
                title: this.translate.instant('Confirm'),
                message: this.translate.instant('Are you sure you want to deactivate parking alarm?'),
                buttons: [
                    {
                        text: this.translate.instant('YES PROCEED'),
                        handler: function () {
                            // theft = d.theftAlert;
                            var payload = {
                                "_id": that.data._id,
                                "theftAlert": theft
                            };
                            _this.apiCall.startLoading();
                            _this.apiCall.deviceupdateCall(payload)
                                .subscribe(function (data) {
                                _this.apiCall.stopLoading();
                                console.log("resp: ", data);
                                var toast = _this.toastCtrl.create({
                                    message: _this.translate.instant('Parking alarm Deactivated!'),
                                    position: "bottom",
                                    duration: 1000
                                });
                                toast.present();
                                _this.getdevices();
                            });
                        }
                    },
                    {
                        text: this.translate.instant('Back'),
                        handler: function () {
                            that.schedule = that.data.theftAlert;
                            // that.data.theftAlert = !(that.data.theftAlert);
                        }
                    }
                ]
            });
            alert_2.present();
        }
    };
    TimePickerModal.prototype.towAlertCall = function (d) {
        var _this = this;
        var tow;
        var that = this;
        tow = !(that.data.towAlert);
        if (tow) {
            var alert_3 = this.alertCtrl.create({
                title: this.translate.instant("Confirm"),
                message: this.translate.instant("Are you sure you want to activate Tow Alert alarm? On activating this alert you will get receive notification if vehicle has been towed."),
                buttons: [
                    {
                        text: this.translate.instant('YES PROCEED'),
                        handler: function () {
                            // theft = !(d.theftAlert);
                            // theft = d.theftAlert;
                            var payload = {
                                "_id": that.data._id,
                                "towAlert": tow
                            };
                            _this.apiCall.startLoading();
                            _this.apiCall.deviceupdateCall(payload)
                                .subscribe(function (data) {
                                _this.apiCall.stopLoading();
                                console.log("resp: ", data);
                                var toast = _this.toastCtrl.create({
                                    message: "Tow Alert alarm Activated!",
                                    position: "bottom",
                                    duration: 1000
                                });
                                toast.present();
                                // this.callObjFunc(d);
                                _this.getdevices();
                            }, function (err) {
                                _this.apiCall.stopLoading();
                            });
                        }
                    },
                    {
                        text: this.translate.instant('Back'),
                        handler: function () {
                            that.schedule = false;
                            // that.showScheduler = false;
                            // d.theftAlert = !(d.theftAlert);
                        }
                    }
                ]
            });
            alert_3.present();
        }
        else {
            var alert_4 = this.alertCtrl.create({
                title: this.translate.instant("Confirm"),
                message: this.translate.instant("Are you sure you want to deactivate Tow Alert alarm?"),
                buttons: [
                    {
                        text: this.translate.instant('YES PROCEED'),
                        handler: function () {
                            // theft = d.theftAlert;
                            var payload = {
                                "_id": that.data._id,
                                "towAlert": tow
                            };
                            _this.apiCall.startLoading();
                            _this.apiCall.deviceupdateCall(payload)
                                .subscribe(function (data) {
                                _this.apiCall.stopLoading();
                                console.log("resp: ", data);
                                var toast = _this.toastCtrl.create({
                                    message: "Tow Alert alarm Deactivated!",
                                    position: "bottom",
                                    duration: 1000
                                });
                                toast.present();
                                // this.callObjFunc(d);
                                _this.getdevices();
                            });
                        }
                    },
                    {
                        text: this.translate.instant('Back'),
                        handler: function () {
                            that.schedule = that.data.towAlert;
                            // d.theftAlert = !(d.theftAlert);
                        }
                    }
                ]
            });
            alert_4.present();
        }
    };
    TimePickerModal.prototype.checkSettings = function () {
        this.showScheduler = true;
    };
    TimePickerModal.prototype.reset = function () {
        this.startHourNumber = this.pad(1, 2);
        this.startMinutesNumber = this.pad(0, 2);
        this.endHourNumber = this.pad(1, 2);
        this.endMinutesNumber = this.pad(0, 2);
    };
    TimePickerModal.prototype.onHourClicked = function (key1, key) {
        // debugger
        if (key1 === 'start') {
            if (key === 'up') {
                if (Number(this.startHourNumber) < 24) {
                    this.startHourNumber = Number(this.startHourNumber) + 1;
                    this.startHourNumber = this.pad(this.startHourNumber, 2);
                }
                else {
                    this.startHourNumber = this.pad(1, 2);
                }
            }
            else if (key === 'down') {
                if (Number(this.startHourNumber) > 1) {
                    this.startHourNumber = Number(this.startHourNumber) - 1;
                    this.startHourNumber = this.pad(this.startHourNumber, 2);
                }
                else {
                    this.startHourNumber = this.pad(24, 2);
                }
            }
        }
        else if (key1 === 'end') {
            if (key === 'up') {
                if (Number(this.endHourNumber) < 24) {
                    this.endHourNumber = Number(this.endHourNumber) + 1;
                    this.endHourNumber = this.pad(this.endHourNumber, 2);
                }
                else {
                    this.endHourNumber = this.pad(1, 2);
                }
            }
            else if (key === 'down') {
                if (Number(this.endHourNumber) > 1) {
                    this.endHourNumber = Number(this.endHourNumber) - 1;
                    this.endHourNumber = this.pad(this.endHourNumber, 2);
                }
                else {
                    this.endHourNumber = this.pad(24, 2);
                }
            }
        }
    };
    TimePickerModal.prototype.onMinuteClicked = function (key1, key) {
        if (key1 === 'start') {
            if (key === 'up') {
                if (Number(this.startMinutesNumber) < 59) {
                    this.startMinutesNumber = Number(this.startMinutesNumber) + 1;
                    this.startMinutesNumber = this.pad(this.startMinutesNumber, 2);
                }
                else {
                    this.startMinutesNumber = this.pad(1, 2);
                }
            }
            else if (key === 'down') {
                if (Number(this.startMinutesNumber) >= 1) {
                    this.startMinutesNumber = Number(this.startMinutesNumber) - 1;
                    this.startMinutesNumber = this.pad(this.startMinutesNumber, 2);
                }
                else {
                    this.startMinutesNumber = this.pad(59, 2);
                }
            }
        }
        else if (key1 === 'end') {
            if (key === 'up') {
                if (Number(this.endMinutesNumber) < 59) {
                    this.endMinutesNumber = Number(this.endMinutesNumber) + 1;
                    this.endMinutesNumber = this.pad(this.endMinutesNumber, 2);
                }
                else {
                    this.endMinutesNumber = this.pad(1, 2);
                }
            }
            else if (key === 'down') {
                if (Number(this.endMinutesNumber) >= 1) {
                    this.endMinutesNumber = Number(this.endMinutesNumber) - 1;
                    this.endMinutesNumber = this.pad(this.endMinutesNumber, 2);
                }
                else {
                    this.endMinutesNumber = this.pad(59, 2);
                }
            }
        }
    };
    TimePickerModal.prototype.ampmClicked = function (key) {
        if (key === 'start') {
            if (this.start_ampm === "AM") {
                this.start_ampm = "PM";
            }
            else if (this.start_ampm === "PM") {
                this.start_ampm = "AM";
            }
        }
        else if (key === 'end') {
            if (this.end_ampm === "AM") {
                this.end_ampm = "PM";
            }
            else if (this.end_ampm === "PM") {
                this.end_ampm = "AM";
            }
        }
    };
    TimePickerModal.prototype.submit = function () {
        var _this = this;
        var startTime, endTime;
        var d = new Date();
        d.setHours(Number(this.startHourNumber));
        // var d = new Date();
        d.setMinutes(Number(this.startMinutesNumber));
        startTime = d;
        var d1 = new Date();
        d1.setHours(Number(this.endHourNumber));
        // var d = new Date();
        d1.setMinutes(Number(this.endMinutesNumber));
        endTime = d1;
        console.log("start time:", startTime);
        console.log("end time:", endTime);
        // console.log(d);
        var that = this;
        var payload;
        if (this.screenKey === 'parking') {
            payload = {
                "_id": that.data._id,
                "deviceid": that.data.Device_ID,
                "theftTime": {
                    "start": startTime,
                    "end": endTime
                }
            };
        }
        else if (this.screenKey === 'tow') {
            payload = {
                "_id": that.data._id,
                "deviceid": that.data.Device_ID,
                "towTime": {
                    "start": startTime,
                    "end": endTime
                }
            };
        }
        this.apiCall.startLoading().present();
        this.apiCall.deviceupdateCall(payload)
            .subscribe(function (resp) {
            console.log('response language code ' + resp);
            _this.apiCall.stopLoading();
            var toast = _this.toastCtrl.create({
                message: _this.translate.instant('Alert is scheduled sucessfully.'),
                duration: 1500,
                position: 'bottom'
            });
            toast.onDidDismiss(function () {
                _this.dismiss();
            });
            toast.present();
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    TimePickerModal.prototype.getdevices = function () {
        var _this = this;
        var url = this.apiCall.mainUrl + 'devices/getDevicebyId?deviceId=' + this.data.Device_ID;
        this.apiCall.startLoading().present();
        this.apiCall.getSOSReportAPI(url).subscribe(function (resp) {
            _this.apiCall.stopLoading();
            console.log("check updated data: ", resp);
            _this.data = resp;
        }, function (err) {
            _this.apiCall.stopLoading();
        });
    };
    TimePickerModal.prototype.openalarm = function () {
        this.navCtrl.push(ParkingAlarmPage);
    };
    TimePickerModal = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/profile/settings/notif-setting/time-picker/time-picker.html"*/'<ion-content padding style="background: #a60033;">\n    <ion-row>\n        <ion-col col-10>\n            <!-- <p style="font-size: 1.5em; font-weight: 350;">{{screenKey | titlecase}} Scheduler</p> -->\n        </ion-col>\n        <ion-col col-2 (click)="dismiss()" style="padding-right: 16px;text-align: right;">\n            <ion-icon name="close-circle" style="color: white; font-size: 1.8em;"></ion-icon>\n        </ion-col>\n    </ion-row>\n    <ion-row>\n        <ion-col col-12 style="text-align: center;">\n            <p style="font-size: 1.5em; font-weight: 350; color: white; font-weight: bold; text-decoration: underline;">{{showKey | titlecase}} {{"Scheduler" | translate}}</p>\n        </ion-col>\n    </ion-row>\n    <ion-item class="itemStyle">\n        <ion-label style="font-size: 1.5em;">{{data.Device_Name}}</ion-label>\n        <div item-right><img src="assets/imgs/schedule.png" style="height: 30px;" (click)="checkSettings()" /></div>\n        <ion-toggle color="oceanBlue" [(ngModel)]="schedule" (ngModelChange)="setScheduler(schedule)"></ion-toggle>\n        <!-- <ion-toggle readonly color="oceanBlue" [(ngModel)]="schedule"></ion-toggle> -->\n    </ion-item>\n    <button (click)="openalarm()" ion-button>Alarm</button>\n</ion-content>\n<ion-footer no-header *ngIf="showScheduler">\n    <ion-toolbar>\n        <ion-row>\n            <ion-col col-2 style="text-align: center;">\n                <div class="dot">\n                    <p style="margin: 0px; font-weight: 600; color: white;">S</p>\n                </div>\n            </ion-col>\n            <ion-col col-2 style="margin-left: -11px;text-align: center;">\n                <div class="dot">\n                    <p style="margin: 0px; font-weight: 600; color: white;">M</p>\n                </div>\n            </ion-col>\n            <ion-col col-1 style="padding-left: 2px;text-align: center;">\n                <div class="dot">\n                    <p style="margin: 0px; font-weight: 600; color: white;">T</p>\n                </div>\n            </ion-col>\n            <ion-col col-2 style="padding-left: 18px;text-align: center;">\n                <div class="dot">\n                    <p style="margin: 0px; font-weight: 600; color: white;">W</p>\n                </div>\n            </ion-col>\n            <ion-col col-1 style="text-align: center;">\n                <div class="dot">\n                    <p style="margin: 0px; font-weight: 600; color: white;">T</p>\n                </div>\n            </ion-col>\n            <ion-col col-2 style="padding-left: 22px;text-align: center;">\n                <div class="dot">\n                    <p style="margin: 0px; font-weight: 600; color: white;">F</p>\n                </div>\n            </ion-col>\n            <ion-col col-2 style="text-align: center;">\n                <div class="dot">\n                    <p style="margin: 0px; font-weight: 600; color: white;">S</p>\n                </div>\n            </ion-col>\n        </ion-row>\n        <ion-row style="padding-bottom: 16px;">\n            <ion-col col-6 style="text-align: center;">\n                <p style="color: #ffffff; font-size: 1.5em; font-weight: 400;">{{"Start" | translate}}</p>\n            </ion-col>\n            <ion-col col-6 style="text-align: center;">\n                <p style="color: #ffffff; font-size: 1.5em; font-weight: 400;">{{"End" | translate}}</p>\n            </ion-col>\n        </ion-row>\n        <ion-row style="padding-bottom: 80px;">\n            <ion-col col-6 style="padding-right: 16px; color: white; border-right: 1px solid white;">\n                <ion-row>\n                    <ion-col col-1></ion-col>\n                    <ion-col col-3 style="text-align: center; font-size: 1.5em;" (click)="onHourClicked(\'start\',\'up\')">\n                        <ion-icon name="arrow-dropup"></ion-icon>\n                    </ion-col>\n                    <ion-col col-3></ion-col>\n                    <ion-col col-3 style="text-align: center; font-size: 1.5em;"\n                        (click)="onMinuteClicked(\'start\',\'up\')">\n                        <ion-icon name="arrow-dropup"></ion-icon>\n                    </ion-col>\n                    <ion-col col-2></ion-col>\n                </ion-row>\n                <ion-row>\n                    <ion-col col-1></ion-col>\n                    <ion-col col-3 style="text-align: center;">\n                        <p style="margin: 0px; font-size: 1.3em; font-weight: 600;">{{startHourNumber}}</p>\n                    </ion-col>\n                    <ion-col col-3 style="text-align: center;">\n                        <p style="margin: 0px; font-size: 1.5em; font-weight: 600;">:</p>\n                    </ion-col>\n                    <ion-col col-3 style="text-align: center;">\n                        <p style="margin: 0px; font-size: 1.3em;font-weight: 600;">{{startMinutesNumber}}</p>\n                    </ion-col>\n                    <ion-col col-2></ion-col>\n                    <!-- <ion-col col-3 style="text-align: center;">\n                        <ion-badge style="font-size: 1em;background: transparent;" (click)="ampmClicked(\'start\')">{{start_ampm}}</ion-badge>\n                    </ion-col> -->\n                </ion-row>\n                <ion-row>\n                    <ion-col col-1></ion-col>\n                    <ion-col col-3 style="text-align: center; font-size: 1.5em;"\n                        (click)="onHourClicked(\'start\',\'down\')">\n                        <ion-icon name="arrow-dropdown"></ion-icon>\n                    </ion-col>\n                    <ion-col col-3></ion-col>\n                    <ion-col col-3 style="text-align: center; font-size: 1.5em;"\n                        (click)="onMinuteClicked(\'start\',\'down\')">\n                        <ion-icon name="arrow-dropdown"></ion-icon>\n                    </ion-col>\n                    <ion-col col-2></ion-col>\n                </ion-row>\n            </ion-col>\n            <ion-col col-6 style="padding-left: 16px; color: white;">\n                <ion-row>\n                    <ion-col col-2></ion-col>\n                    <ion-col col-3 style="text-align: center; font-size: 1.5em;" (click)="onHourClicked(\'end\',\'up\')">\n                        <ion-icon name="arrow-dropup"></ion-icon>\n                    </ion-col>\n                    <ion-col col-3></ion-col>\n                    <ion-col col-3 style="text-align: center; font-size: 1.5em;" (click)="onMinuteClicked(\'end\',\'up\')">\n                        <ion-icon name="arrow-dropup"></ion-icon>\n                    </ion-col>\n                    <ion-col col-1></ion-col>\n                </ion-row>\n                <ion-row>\n                    <ion-col col-2></ion-col>\n                    <ion-col col-3 style="text-align: center;">\n                        <p style="margin: 0px; font-size: 1.3em;font-weight: 600;">{{endHourNumber}}</p>\n                    </ion-col>\n                    <ion-col col-3 style="text-align: center;">\n                        <p style="margin: 0px; font-size: 1.5em;font-weight: 600;">:</p>\n                    </ion-col>\n                    <ion-col col-3 style="text-align: center;">\n                        <p style="margin: 0px; font-size: 1.3em;font-weight: 600;">{{endMinutesNumber}}</p>\n                    </ion-col>\n                    <ion-col col-1></ion-col>\n                    <!-- <ion-col col-3 style="text-align: center;">\n                        <ion-badge style="font-size: 1em; background: transparent;" (click)="ampmClicked(\'end\')">{{end_ampm}}</ion-badge>\n                    </ion-col> -->\n                </ion-row>\n                <ion-row>\n                    <ion-col col-2></ion-col>\n                    <ion-col col-3 style="text-align: center; font-size: 1.5em;" (click)="onHourClicked(\'end\',\'down\')">\n                        <ion-icon name="arrow-dropdown"></ion-icon>\n                    </ion-col>\n                    <ion-col col-3></ion-col>\n                    <ion-col col-3 style="text-align: center; font-size: 1.5em;"\n                        (click)="onMinuteClicked(\'end\',\'down\')">\n                        <ion-icon name="arrow-dropdown"></ion-icon>\n                    </ion-col>\n                    <ion-col col-1></ion-col>\n                </ion-row>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col col-6 style="text-align: center;"><button ion-button round (click)="reset()">{{"Reset" | translate}}</button>\n            </ion-col>\n            <ion-col col-6 style="text-align: center;" *ngIf="showSubmitBtn"><button ion-button round (click)="submit()">{{"SUBMIT" | translate}}</button>\n            </ion-col>\n            <ion-col col-6 style="text-align: center;" *ngIf="showUpdateBtn"><button ion-button round (click)="submit()">{{"Update" | translate}}</button>\n            </ion-col>\n        </ion-row>\n    </ion-toolbar>\n</ion-footer>\n'/*ion-inline-end:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/profile/settings/notif-setting/time-picker/time-picker.html"*/,
            selector: 'app-time-picker-modal'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], TimePickerModal);
    return TimePickerModal;
}());

var ParkingAlarmPage = /** @class */ (function () {
    function ParkingAlarmPage(apiCall, navCtrl, navParams, nativeAudio) {
        this.apiCall = apiCall;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.nativeAudio = nativeAudio;
        this.paramData = {};
        this.ddata = [];
        this.paymentHistory = [];
        this.expDate = [];
        this.nativeAudio.preloadComplex('parkingViolation', 'assets/sound/notif.mp3', 1, 1, 0);
        if (navParams.get('vehName') !== null) {
            console.log("vehicle name is: ", navParams.get('vehName'));
        }
    }
    ParkingAlarmPage.prototype.ngOnInit = function () {
        this.getDetails();
    };
    ParkingAlarmPage.prototype.ionViewDidEnter = function () {
        console.log('ionViewDidEnter SirenAlertPage');
        // can optionally pass a callback to be called when the file is done playing
        // this.nativeAudio.play('parkingViolation', () => {
        //   this.dismiss();
        //   console.log('uniqueId1 is done playing')
        // });
        this.nativeAudio.loop('parkingViolation');
    };
    ParkingAlarmPage.prototype.getDetails = function () {
    };
    ParkingAlarmPage.prototype.dismiss = function () {
        this.navCtrl.pop();
        this.nativeAudio.stop('parkingViolation');
    };
    ParkingAlarmPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/profile/settings/notif-setting/time-picker/parking-alarm.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Parking Alarm</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content style="background-color: #a60033;">\n  <div style="    justify-content: center;\n  align-items: center;\n  display: flex;\n  width: 100%;\n  height: 100%;">\n    <img src="assets/imgs/alarmicon.png" style="width:18%;"/>\n    <div>\n      <button ion-button>Lock Engine</button>\n      <button ion-button (click)="dismiss()">Stop</button>\n    </div>\n    <!-- <button ion-button round block color="light" >Exit</button> -->\n  </div>\n\n\n</ion-content>\n\n'/*ion-inline-end:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/profile/settings/notif-setting/time-picker/parking-alarm.html"*/,
            styles: [
                "\n      .col {\n        padding: 0px;\n      }\n    ",
            ],
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_native_audio__["a" /* NativeAudio */]])
    ], ParkingAlarmPage);
    return ParkingAlarmPage;
}());

//# sourceMappingURL=time-picker.js.map

/***/ }),

/***/ 145:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return PaytmwalletloginPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return SuccessDetailPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaymentHistoryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__add_devices_add_devices__ = __webpack_require__(79);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PaytmwalletloginPage = /** @class */ (function () {
    function PaytmwalletloginPage(navCtrl, navParams, toastCtrl, apiCall, menu, toast, alertCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.apiCall = apiCall;
        this.menu = menu;
        this.toast = toast;
        this.alertCtrl = alertCtrl;
        this.successresponse = false;
        this.inputform = false;
        this.razor_key = "rzp_live_eZdWnBRmYRCLWo";
        this.currency = "INR";
        this.device_name = [];
        this.rechargeData = [];
        this.total = 0;
        this.isAlert = false;
        this.Duration_days = '90';
        // this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        // console.log("islogin devices => " + JSON.stringify(this.islogin));
        console.log("paytmwallet login====----->");
        this.islogin = localStorage.getItem("details")
            ? JSON.parse(localStorage.getItem("details"))
            : "";
        this.apiCall.getRechargePlanApi(this.islogin.supAdmin).subscribe(function (respData) {
            console.log("recharge data --------->", respData);
            if (respData.length == 0) {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__add_devices_add_devices__["c" /* PaymantgatweyPage */]);
            }
            _this.total = respData[0].Amount;
            _this.rechargeData = respData;
            _this.ingnitionStat = _this.rechargeData[0].Name;
            _this.Duration_days = _this.rechargeData[0].Duration_days;
            _this.total = _this.rechargeData[0].Amount;
            _this.paymentAmount = _this.total * _this.device_name.length;
            console.log("recharge -------->", _this.rechargeData);
        });
        console.log("user details", this.islogin);
        this.paytmnumber = this.islogin.phn;
        // this.parameters = navParams.get('param');
        console.log("parameters", this.parameters);
        this.device_name = navParams.get("param");
        // this.device_id.push(this.device_name._id)
        console.log("device name datas --->", this.device_name);
        //  let lenmult =this.device_name.length
        //     this.paymentAmount =lenmult;
        //console.log("paymentAmount ----->",lenmult);
        console.log("paymentAmount ---fcghf-->", this.paymentAmount);
        if (localStorage.getItem("default_curreny") !== null) {
            this.currency = localStorage.getItem("default_curreny");
        }
        if (this.islogin.isSuperAdmin == true) {
            var alert_1 = this.alertCtrl.create({
                message: "Do you want to Renew the vehicle?",
                buttons: [
                    {
                        text: "Yes",
                        handler: function () {
                            _this.updateExpDate();
                            (function (err) {
                                console.log(err);
                            });
                        },
                    },
                    {
                        text: "No",
                        handler: function () {
                            // this.menuCtrl.close();
                        },
                    },
                ],
            });
            alert_1.present();
        }
    }
    PaytmwalletloginPage.prototype.onItemChange = function (item, days) {
        console.log("ssssstype of ", typeof (days));
        this.isAlert = true;
        console.log("renew--id---->", this.islogin.Dealer_ID._id);
        this.Duration_days = days;
        console.log("Duration_days  -------days-->", this.Duration_days);
        this.total = item;
        this.paymentAmount = this.total * this.device_name.length;
        console.log("paymentAmount ---fcghf---apiiiii->", this.paymentAmount);
    };
    PaytmwalletloginPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.Amount = this.device_name.length * this.total;
        console.log("Amount data--->", this.Amount);
        this.menu.enable(true);
        this.apiCall.getRechargePlanApi(this.islogin.supAdmin).subscribe(function (respData) {
            console.log("recharge data --------->", respData);
            _this.rechargeData = respData;
            _this.ingnitionStat = _this.rechargeData[0].Name;
            _this.Duration_days = _this.rechargeData[0].Duration_days;
            _this.total = _this.rechargeData[0].Amount;
            _this.paymentAmount = _this.total * _this.device_name.length;
            if (_this.rechargeData.length == 0) {
                _this.navCtrl.push('PaymantgatweyPage');
            }
            console.log("recharge -------->", _this.rechargeData);
        });
    };
    PaytmwalletloginPage.prototype.payWithRazor = function () {
        var that = this;
        var options = {
            description: "Credits towards consultation",
            image: "https://i.imgur.com/GO0jiDP.jpg",
            currency: that.currency,
            key: that.razor_key,
            amount: that.paymentAmount * 100,
            prefill: {
                email: that.islogin.email,
                contact: that.paytmnumber,
                name: that.islogin.fn + " " + that.islogin.ln,
            },
            theme: {
                color: "#000000",
            },
            modal: {
                ondismiss: function () {
                    console.log("dismissed");
                },
            },
        };
        debugger;
        var successCallback = function (success) {
            that.isSuperAdmin = JSON.parse(localStorage.getItem("details"));
            console.log("dddddddd----dadtaaa", that.device_name);
            var VehicleNo = that.device_name.map(function (item) { return item.Device_Name; });
            console.log("vehicle numner---->", VehicleNo);
            var data = {
                Amount: that.paymentAmount,
                Customer_ID: that.isSuperAdmin._id,
                Status: "Paid",
                TransactionID: success.razorpay_payment_id,
                Remarks: "455",
                VehicleNo: VehicleNo,
                Items: "SIM + Software",
                PaymentMode: "UPI",
                supAdmin: that.isSuperAdmin.supAdmin,
                date: new Date()
            };
            that.apiCall.addCustomerPayment(data).subscribe(function () { });
            that.showAlert(success.razorpay_payment_id);
        };
        var cancelCallback = function (error) {
            console.log("razorpay error: ", error);
            that.onFailed(error);
            // alert(error.description + ' (Error ' + error.code + ')')
        };
        console.log("optionssss--->", options);
        // RazorpayCheckout.open(options, this.successCallback(), cancelCallback);
        RazorpayCheckout.on("payment.success", successCallback);
        RazorpayCheckout.on("payment.cancel", cancelCallback);
        RazorpayCheckout.open(options);
    };
    PaytmwalletloginPage.prototype.success = function () {
        var that = this;
        console.log("success function call ");
        var tempdate = new Date();
        var Duration_days = that.Duration_days;
        var tempExp = __WEBPACK_IMPORTED_MODULE_3_moment__(tempdate).add('days', Duration_days);
        var ISOtempdate = tempExp.toISOString();
        console.log("tempExp date--->", ISOtempdate);
        // let ISOtempdate = tempdate.toISOString();
        var device_id = that.device_name.map(function (item) { return item._id; });
        var data = {
            device: device_id,
            renew_by: that.islogin._id,
            expDate: ISOtempdate,
        };
        console.log("renew object---->", data);
        that.apiCall.addRenewDevices(data).subscribe(function (respData) {
            console.log("Renew ------Devicesss----->", respData);
        });
        that.navCtrl.push(SuccessDetailPage, {
            param: that.paymentAmount,
            vname: that.device_name,
            tdate: "datetimeEnd",
            superAdmin: that.islogin.isSuperAdmin,
        });
        console.log("success dataaaa---->");
        //this.updateExpDate();
    };
    PaytmwalletloginPage.prototype.showAlert = function (id) {
        var _this = this;
        var alert = this.alertCtrl.create({
            message: "Payment successfull...",
            buttons: [
                {
                    text: "Okay",
                    handler: function () {
                        // this.updateExpDate();
                        console.log("okk success-->");
                        _this.success();
                    },
                },
            ],
        });
        alert.present();
    };
    PaytmwalletloginPage.prototype.onFailed = function (err) {
        var _this = this;
        console.log(err);
        var alert = this.alertCtrl.create({
            message: "Transaction Failed! Please try again with valid credentials",
            buttons: [
                {
                    text: "Try Again",
                    handler: function () {
                        _this.goBack();
                    },
                },
            ],
        });
        alert.present();
    };
    PaytmwalletloginPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    PaytmwalletloginPage.prototype.updateExpDate = function () {
        var _this = this;
        debugger;
        var tempdate = new Date();
        tempdate.setDate(tempdate.getDate() + 365);
        var expDate = __WEBPACK_IMPORTED_MODULE_3_moment__(new Date(tempdate), "DD-MM-YYYY").format("YYYY-MM-DD");
        console.log("updated expiry date: ", expDate);
        var url = this.apiCall.mainUrl + "devices/editDevMaster";
        var payload = {
            // _id: this.parameters.Device_ID,
            _id: this.parameters._id,
            expiration_date: new Date(expDate).toISOString(),
        };
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdatatwo(url, payload).subscribe(function (respData) {
            _this.apiCall.stopLoading();
            console.log("respData of expiry date updation: ", respData);
            var toast = _this.toastCtrl.create({
                message: "The expired device is renewed successfully!!",
                duration: 2000,
                position: "middle",
            });
            toast.onDidDismiss(function () {
                _this.navCtrl.pop();
            });
            toast.present();
        }, function (err) {
            console.log("oops got error: ", err);
            _this.apiCall.stopLoading();
        });
    };
    PaytmwalletloginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: "page-paytmwalletlogin",template:/*ion-inline-start:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/add-devices/paytmwalletlogin/paytmwalletlogin.html"*/'<!--<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{ "Renewal Method" | translate }}</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content no-padding>\n  <ion-grid text-center *ngIf="this.islogin.isSuperAdmin == false">\n    <ion-row>\n      <ion-col>\n        ** For renewal please make the payment on 7507500582 via Gpay/ PhonePe. **\n          <p>Renewal Amount &nbsp;&nbsp; 2400</p>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <ion-grid text-center *ngIf="this.islogin.isSuperAdmin == true">\n    <ion-row>\n      <ion-col>\n        Yor are in admin mode.\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n   <ion-card class="welcome-card">\n    <img src="assets/imgs/dc-Cover-u0b349upqugfio195s4lpk8144-20190213120303.Medi.jpeg">\n    <ion-card-header>\n\n      <ion-card-title>Vehicle Name - {{parameters.Device_Name}}</ion-card-title>\n      <ion-row>\n        <ion-col>\n          Total Payment\n        </ion-col>\n        <ion-col>\n          {{currencyIcon}}{{paymentAmount/100}}\n        </ion-col>\n      </ion-row>\n    </ion-card-header>\n    <ion-card-content>\n\n      <button ion-button full color="gpsc" (click)="payWithRazor()">Pay with RazorPay</button>\n    </ion-card-content> -->\n  <!-- </ion-card> -->\n  <!-- <ion-list lines="none">\n    <ion-list-header>\n      <ion-label>Resources</ion-label>\n    </ion-list-header>\n    <ion-item href="https://github.com/razorpay/razorpay-cordova">\n      <ion-icon slot="start" color="medium" name="book"></ion-icon>\n      <ion-label>Razorpay Plugin Documentation</ion-label>\n    </ion-item>\n    <ion-item href="https://medium.com/enappd/how-to-integrate-razorpay-in-ionic-4-apps-and-pwa-612bb11482d9">\n      <ion-icon slot="start" color="medium" name="grid"></ion-icon>\n      <ion-label>Ionic 4 - Razorpay Integration</ion-label>\n    </ion-item>\n    <ion-item href="https://store.enappd.com">\n      <ion-icon slot="start" color="medium" name="color-fill"></ion-icon>\n      <ion-label>More Ionic 4 Starters</ion-label>\n    </ion-item>\n  </ion-list> -->\n\n  <!-- <div style="padding: 0% 10% 0% 10%;">\n\n    <h4 style="font-size: 1.8rem;letter-spacing: 2px;color: #777575;">{{ "Paytm" | translate }}</h4>\n    <ion-item style="padding-left: 0px;">\n      <ion-input [disabled]=\'inputform\' [(ngModel)]="paytmnumber" type="number" placeholder="Mobile Number*">\n      </ion-input>\n    </ion-item>\n    <p style="color:grey">{{ "Linking wallet is one time process. Next time, checkout will be a breeze!" | translate }}\n    </p>\n  </div>\n  <button [disabled]=\'inputform\' ion-button full color="gpsc"\n    style="height: 35px;width: 45%;margin-left: 30%;font-size: 15px;color: white;margin-top: 15px;color: rgb(255, 255, 255);"\n    (click)="paytmwallet()">{{ "Continue" | translate }}</button>\n  <ion-row style="padding: 2% 10% 0% 10%;" *ngIf="successresponse">\n    <h4 style="font-size: 1.8rem;letter-spacing: 2px;color: #777575;">{{ "Enter verification code" | translate }}</h4>\n    <p style="margin-top: 0px;color: grey;margin-bottom: 0;">{{ "Paytm has send verification code on" | translate }}\n      {{paytmnumber}} {{ "via sms Please enter it below, and you are done!" | translate }}</p>\n    <ion-item style="padding: 0px;">\n      <ion-input [(ngModel)]="paytmotp" type="number" placeholder="Enter OTP"></ion-input>\n    </ion-item>\n    <ion-col col-sm-6>\n      <button ion-button full\n        style="height: 35px;background: rgb(220, 220, 220);font-size: 15px;transition: none 0s ease 0s;margin-top: 15px;color: #4a4747;"\n        (click)="resndOTP()">{{ "Resend" | translate }}</button>\n    </ion-col>\n    <ion-col col-sm-6>\n      <button ion-button full [disabled]="paytmotp == undefined"\n        style="height: 35px;background: rgb(220, 220, 220);font-size: 15px;transition: none 0s ease 0s;margin-top: 15px;color: #4a4747;"\n        (click)="paytmAuthantication()">{{ "Proceed" | translate }}</button>\n    </ion-col>\n  </ion-row>\n</ion-content>-->\n\n\n\n<!-- new payment wallet code  -->\n\n\n\n<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{ "Recharge Method" | translate }}</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content no-padding>\n  <ion-grid text-center *ngIf="this.islogin.isSuperAdmin == false">\n    <ion-row>\n\n      <ion-col>\n        You have selected these vehicles for Recharge:\n        <span style="display: flex;padding: 0px 18px;"><p *ngFor="let n of device_name">{{n.Device_Name}}/</p></span>\n      </ion-col>\n\n    </ion-row>\n  </ion-grid>\n\n  <ion-grid  *ngFor="let r of rechargeData; let i = index"  style="display: flex;flex-direction: row;padding: 7px 20px;">\n    <ion-list style="width: 20%;justify-content: center; margin: 20px 0px;" radio-group [(ngModel)]="ingnitionStat" >\n\n        <ion-radio  [value]="r.Name" slot="start"  (click)="onItemChange(r.Amount,r.Duration_days)"  ></ion-radio>\n\n    </ion-list>\n\n    <ion-grid style="width: 100%; word-spacing: 2px;">\n      <p style="margin: 0px; font-weight: bold;">{{r.Name}}</p>\n      <p style="margin: 0px;">\n        Recharge your device and platform for {{r.Duration_days}} days\n      </p>\n    </ion-grid>\n    <ion-grid style="width: 20%;">\n      <p style="font-weight: bold;">{{r.Amount}}</p>\n    </ion-grid>\n  </ion-grid>\n  <ion-grid *ngIf="rechargeData.length > 0">\n    <p style="margin-left: 79%;font-weight: bold;">\n      ={{device_name.length}}*{{total}}\n    </p>\n   <p  style="margin-left: 53%;font-weight: bold;font-size: 16px;color: #00f5f5;">\n     Total Payable = {{device_name.length*total}}\n  </ion-grid>\n  <ion-grid>\n    <button ion-button (click)="payWithRazor()" style="color: white;background-color: black;padding: 12px;text-align: center;">\n      >  Pay with RazorPay {{device_name.length*total}}\n    </button>\n  </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/add-devices/paytmwalletlogin/paytmwalletlogin.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["MenuController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"]])
    ], PaytmwalletloginPage);
    return PaytmwalletloginPage;
}());

var SuccessDetailPage = /** @class */ (function () {
    function SuccessDetailPage(apiCall, navParam, navCtrl) {
        this.apiCall = apiCall;
        this.navParam = navParam;
        this.navCtrl = navCtrl;
        this.razor_key = "rzp_live_eZdWnBRmYRCLWo";
        this.paramData = {};
        this.ddata = [];
        this.vname = [];
        this.paymentHistory = [];
        // this.apiCall.addCustomerPayment("234")
        console.log("param parameters:------>paramdata ", this.navParam.get("param"));
        if (this.navParam.get("param")) {
            this.Amount = this.navParam.get("param");
        }
        if (this.navParam.get("vname")) {
            this.vname = this.navParam.get("vname");
        }
        console.log("vvvvname--->", this.vname);
        if (this.vname[0]) {
            this.customerId = this.vname[0].Dealer;
        }
        // this.tdate = this.navParam.get('tdate');
        // this.uid = this.navParam.get('uid');
    }
    SuccessDetailPage.prototype.ngOnInit = function () {
        this.getDetails();
    };
    SuccessDetailPage.prototype.paymenthistory = function () {
        var _this = this;
        var isSuperAdmin = JSON.parse(localStorage.getItem("details"));
        console.log("customer-----id-->", this.customerId);
        this.apiCall
            .getCustomerPayment(isSuperAdmin._id)
            .subscribe(function (respData) {
            _this.paymentHistory = respData;
            console.log("data response paymentHistory--->", respData);
            // this.rechargeData = respData;
            // console.log("recharge -------->",this.rechargeData);
        });
        this.navCtrl.push(PaymentHistoryPage, {
            param: "50",
            fdate: "datetimeStart",
            tdate: "datetimeEnd",
            uid: "islogin",
            customerId: this.customerId,
            paymentHistory: this.paymentHistory,
        });
    };
    SuccessDetailPage.prototype.getDetails = function () {
        // debugger
        // this.apiCall.startLoading().present();
        // this.apiCall.getDetailACReportAPI(new Date(this.fdate).toISOString(), new Date(this.tdate).toISOString(), this.uid, this.paramData._id.imei)
        //   .subscribe(data => {
        //     this.apiCall.stopLoading();
        //     console.log("detailed ac report data: ", data[0].s);
        //     this.ddata = data[0].s;
        //   })
    };
    SuccessDetailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/add-devices/paytmwalletlogin/success-Detail.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Payment</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <ion-card style="background-color: #f7f7f7;">\n    <img src="assets/imgs/righticon2.png" style="margin-left: 115px;width: 40%;"/>\n    <p style="margin:15px;font-size:25px;color: #2cad2a;text-align:center;font-weight: bold;">Payment Successful</p>\n    <ion-card-content>\n      <ion-row>\n        <ion-col col-6 style="font-size: 17px; text-align: justify;font-weight: bold;"\n          >Amount :</ion-col\n        >\n        <ion-col col-6 style="font-size: 15px; text-align: justify">\n          {{Amount}}</ion-col\n        >\n      </ion-row>\n      <!-- <ion-row>\n        <ion-col col-6 style="font-size: 17px; text-align: justify;font-weight: bold;"\n          >Customer_ID :</ion-col\n        >\n        <ion-col col-6 style="font-size: 15px; text-align: justify">\n          {{customerId}}</ion-col\n        >\n      </ion-row> -->\n      <ion-row>\n        <ion-col col-6 style="font-size: 17px; text-align: justify;font-weight: bold;"\n          >Items : </ion-col\n        >\n        <ion-col col-6 style="font-size: 15px; text-align: justify">\n          Software</ion-col\n        >\n      </ion-row>\n      <ion-row>\n        <ion-col col-6 style="font-size: 17px; text-align: justify;font-weight: bold;"\n          >PaymentMode :</ion-col\n        >\n        <ion-col col-6 style="font-size: 15px; text-align: justify">\n          UPI</ion-col\n        >\n      </ion-row>\n      <ion-row>\n        <ion-col col-6 style="font-size: 17px; text-align: justify;font-weight: bold;"\n          >Remarks :</ion-col\n        >\n        <ion-col col-6 style="font-size: 15px; text-align: justify">\n          test</ion-col\n        >\n      </ion-row>\n      <ion-row>\n        <ion-col col-6 style="font-size: 17px; text-align: justify;font-weight: bold;"\n          >Status :</ion-col\n        >\n        <ion-col col-6 style="font-size: 15px; text-align: justify">\n          Active</ion-col\n        >\n      </ion-row>\n      <ion-row>\n        <ion-col col-6 style="font-size: 17px; text-align: justify;font-weight: bold;"\n          >TransactionID :</ion-col\n        >\n        <ion-col col-6 style="font-size: 15px; text-align: justify">\n          {{razor_key}}</ion-col\n        >\n      </ion-row>\n      <ion-row>\n        <ion-col col-6 style="font-size: 17px; text-align: justify;font-weight: bold;"\n          >VehicleNo :</ion-col\n        >\n        <ion-col  style="font-size: 15px; text-align: justify" *ngFor="let n of vname">\n          {{n.Device_Name}}</ion-col\n        >\n      </ion-row>\n    </ion-card-content>\n    <button ion-button (click)="paymenthistory()" style="margin: 25px 90px;">\n      Check Payment History\n    </button>\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/add-devices/paytmwalletlogin/success-Detail.html"*/,
            styles: [
                "\n      .col {\n        padding: 0px;\n      }\n    ",
            ],
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"]])
    ], SuccessDetailPage);
    return SuccessDetailPage;
}());

var PaymentHistoryPage = /** @class */ (function () {
    function PaymentHistoryPage(apiCall, navParam) {
        this.apiCall = apiCall;
        this.navParam = navParam;
        this.paramData = {};
        this.ddata = [];
        this.paymentHistory = [];
        this.expDate = [];
    }
    PaymentHistoryPage.prototype.ngOnInit = function () {
        this.getDetails();
    };
    PaymentHistoryPage.prototype.getDetails = function () {
        var _this = this;
        var isSuperAdmin = JSON.parse(localStorage.getItem("details"));
        this.apiCall.getCustomerPayment(isSuperAdmin._id).subscribe(function (respData) {
            _this.paymentHistory = respData;
            _this.paymentHistory.map(function (item) {
                item.createdAt = __WEBPACK_IMPORTED_MODULE_3_moment__(new Date(item.createdAt), "DD-MM-YYYY").format("YYYY-MM-DD");
                return item;
            });
        });
    };
    PaymentHistoryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/add-devices/paytmwalletlogin/payment-History.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Payment History</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <ion-content\n    *ngIf="paymentHistory.length == 0"\n    style="text-align: center; margin: 15px; font-size: 20px"\n  >\n    History Not Found</ion-content\n  >\n  <ion-card *ngFor="let item of paymentHistory">\n    <ion-card-content>\n      <ion-row>\n        <ion-col col-6 style="font-size: 17px; font-weight: bold">\n          Status\n        </ion-col>\n        <ion-col style="font-size: 17px; font-weight: bold">\n          {{item?.Status}}\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6 style="font-size: 17px; font-weight: bold">\n          TransactionID\n        </ion-col>\n        <ion-col style="font-size: 17px; font-weight: bold">\n          {{item?.TransactionID}}\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6 style="font-size: 17px; font-weight: bold">\n          Amount\n        </ion-col>\n        <ion-col style="font-size: 17px; font-weight: bold">\n          {{item?.Amount}}\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6 style="font-size: 17px; font-weight: bold">\n          Date\n        </ion-col>\n        <ion-col style="font-size: 17px; font-weight: bold">\n          {{item?.createdAt}}\n        </ion-col>\n      </ion-row>\n    </ion-card-content>\n  </ion-card>\n</ion-content>\n<!-- [{"_id":"61b9cd046fff072a0c11c857","Customer_ID":"5cee7da97a38f414a4e5dfa3","Amount":1500,"Items":"GPS Renewal","PaymentMode":"Cheque","Remarks":"test","Status":"Paid","TransactionID":"4545454","supAdmin":"59cbbdbe508f164aa2fef3d8","__v":0,"VehicleNo":["615d910c4ef4e221a09e7549"]},{"_id":"61b9cd2d6fff072a0c11c85e","Customer_ID":"5cee7da97a38f414a4e5dfa3","Amount":2000,"Items":"GPS Renewal","PaymentMode":"Cash","Remarks":"renewal","Status":"Paid","TransactionID":"455544","supAdmin":"59cbbdbe508f164aa2fef3d8","__v":0,"VehicleNo":["60b5da4f8b50340d4e22c4f2"]},{"_id":"61ba0689dc5f282fae013b55","Customer_ID":"5cee7da97a38f414a4e5dfa3","Items":"SIM + Software","PaymentMode":"UPI","Remarks":"455","Status":"Paid","supAdmin":"59cbbdbe508f164aa2fef3d8","__v":0,"VehicleNo":[]},{"_id":"61ba11b2f521042fb8f60a56","Customer_ID":"5cee7da97a38f414a4e5dfa3","Amount":2,"Items":"SIM + Software","PaymentMode":"UPI","Remarks":"455","Status":"Paid","supAdmin":"59cbbdbe508f164aa2fef3d8","__v":0,"VehicleNo":["MH14HG8396","MH12KQ1871"]},{"_id":"61ba12b3f521042fb8f60a6a","Customer_ID":"5cee7da97a38f414a4e5dfa3","Amount":2,"Items":"SIM + Software","PaymentMode":"UPI","Remarks":"455","Status":"Paid","TransactionID":"pay_IXoKZzWLq2CQHr","supAdmin":"59cbbdbe508f164aa2fef3d8","__v":0,"VehicleNo":["MH14HG8396","MH12KQ1871"]}]-->\n'/*ion-inline-end:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/add-devices/paytmwalletlogin/payment-History.html"*/,
            styles: [
                "\n      .col {\n        padding: 0px;\n      }\n    ",
            ],
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], PaymentHistoryPage);
    return PaymentHistoryPage;
}());

//# sourceMappingURL=paytmwalletlogin.js.map

/***/ }),

/***/ 148:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DaywiseDistancePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_geocoder_geocoder__ = __webpack_require__(55);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DaywiseDistancePage = /** @class */ (function () {
    function DaywiseDistancePage(navCtrl, navParams, viewCtrl, apiCall, geocoderApi) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.apiCall = apiCall;
        this.geocoderApi = geocoderApi;
        this.limit = 28;
        this.page = 0;
        this.detailData = [];
        this.fdate = this.navParams.get('fdate');
        this.tdate = this.navParams.get('tdate');
        this.uid = this.navParams.get('uid');
        this.selectedVehicle = this.navParams.get('selectedVehicle');
        console.log("details", this.fdate, this.tdate, this.uid, this.selectedVehicle);
    }
    DaywiseDistancePage.prototype.ngOnInit = function () {
        this.getDetails();
    };
    DaywiseDistancePage.prototype.getDetails = function () {
        var _this = this;
        this.apiCall.startLoading().present();
        this.apiCall.distanceDaywiseData(new Date(this.fdate).toISOString(), new Date(this.tdate).toISOString(), this.uid, this.selectedVehicle, this.limit, this.page)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.detailData = data;
            var finalArr = [];
            var that = _this;
            var _loop_1 = function (j) {
                _this.clocation_1(_this.detailData[j], function (err, succ) {
                    if (err) {
                        console.log(err);
                        j++;
                    }
                    else {
                        finalArr.push(succ);
                        console.log(finalArr);
                        j++;
                        if (j === that.detailData.length) {
                            that.detailData = finalArr;
                            return that.detailData;
                        }
                    }
                });
                out_j_1 = j;
            };
            var out_j_1;
            for (var j = 0; j < _this.detailData.length; j++) {
                _loop_1(j);
                j = out_j_1;
            }
        });
    };
    DaywiseDistancePage.prototype.clocation_1 = function (latlngObj, cb) {
        var outerThis = this;
        var latLng = {
            lat: "0",
            long: "0"
        };
        latLng = {
            lat: latlngObj.startLat ? latlngObj.startLat : 0,
            long: latlngObj.startLng ? latlngObj.startLng : 0
        };
        outerThis.apiCall.getAddress(latLng).subscribe(function (res) {
            if (res.message == "Address not found in databse") {
                var adata = {
                    iden: 'noaddress',
                    latlng: latLng
                };
                latlngObj['startAddress'] = adata;
                // cb(null,latlngObj);
                var latLng_1_1 = {
                    lat: "0",
                    long: "0"
                };
                latLng_1_1 = {
                    lat: latlngObj.endLat ? latlngObj.endLat : 0,
                    long: latlngObj.endLng ? latlngObj.endLng : 0
                };
                outerThis.apiCall.getAddress(latLng_1_1).subscribe(function (res) {
                    if (res.message == "Address not found in databse") {
                        var bdata = {
                            iden: 'noaddress',
                            latlng: latLng_1_1
                        };
                        latlngObj['endAddress'] = bdata;
                        cb(null, latlngObj);
                    }
                    else {
                        latlngObj['endAddress'] = res.address;
                        cb(null, latlngObj);
                    }
                });
            }
            else {
                latlngObj['startAddress'] = res.address;
                var latLng_1_2 = {
                    lat: "0",
                    long: "0"
                };
                latLng_1_2 = {
                    lat: latlngObj.endLat ? latlngObj.endLat : 0,
                    long: latlngObj.endLng ? latlngObj.endLng : 0
                };
                outerThis.apiCall.getAddress(latLng_1_2).subscribe(function (res) {
                    if (res.message == "Address not found in databse") {
                        var cdata = {
                            iden: 'noaddress',
                            latlng: latLng_1_2
                        };
                        latlngObj['endAddress'] = cdata;
                        cb(null, latlngObj);
                    }
                    else {
                        latlngObj['endAddress'] = res.address;
                        cb(null, latlngObj);
                    }
                });
            }
        });
    };
    DaywiseDistancePage.prototype.start_address = function (d, index) {
        var _this = this;
        var that = this;
        var tempcord = {
            "lat": d.startLat,
            "long": d.startLng
        };
        this.apiCall.getAddress(tempcord)
            .subscribe(function (res) {
            if (res.message == "Address not found in databse") {
                _this.geocoderApi.reverseGeocode(d.startLat, d.startLng)
                    .then(function (res) {
                    var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
                    that.saveAddressToServer(str, d.startLat, d.startLng);
                    that.detailData[index].StartLocation = str;
                    // console.log("inside", that.address);
                });
            }
            else {
                that.detailData[index].StartLocation = res.address;
            }
        });
    };
    DaywiseDistancePage.prototype.end_address = function (item, index) {
        var _this = this;
        var that = this;
        if (!item.end_location) {
            that.detailData[index].EndLocation = "N/A";
            return;
        }
        var tempcord = {
            "lat": item.endLat,
            "long": item.endLng
        };
        this.apiCall.getAddress(tempcord)
            .subscribe(function (res) {
            console.log("test");
            console.log("endlocation result", res);
            console.log(res.address);
            // that.allDevices[index].address = res.address;
            if (res.message == "Address not found in databse") {
                _this.geocoderApi.reverseGeocode(item.endLat, item.endLng)
                    .then(function (res) {
                    var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
                    that.saveAddressToServer(str, item.endLat, item.endLng);
                    that.detailData[index].EndLocation = str;
                    // console.log("inside", that.address);
                });
            }
            else {
                console.log("enter in else");
                that.detailData[index].EndLocation = res.address;
            }
        });
    };
    DaywiseDistancePage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    DaywiseDistancePage.prototype.saveAddressToServer = function (address, lat, lng) {
        var payLoad = {
            "lat": lat,
            "long": lng,
            "address": address
        };
        this.apiCall.saveGoogleAddressAPI(payLoad)
            .subscribe(function (respData) {
            console.log("check if address is stored in db or not? ", respData);
        }, function (err) {
            console.log("getting err while trying to save the address: ", err);
        });
    };
    DaywiseDistancePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-daywise-distance',template:/*ion-inline-start:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/daywise-distance/daywise-distance.html"*/'<!--\n  Generated template for the DaywiseDistancePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>daywiseDistance</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="dismiss()">\n          <ion-icon name="close-circle"></ion-icon>\n      </button>\n  </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content style="padding: 0px;">\n  <ion-card *ngFor="let d of detailData; let i = index">\n\n    <ion-item style="border-bottom: 2px solid #dedede;">\n      <ion-avatar item-start>\n        <img src="assets/imgs/car_red_icon.png">\n      </ion-avatar>\n      <ion-label>{{d.device.Device_Name}}</ion-label>\n      <ion-badge item-end color="gpsc">{{ d.date }}</ion-badge>\n      <!-- <ion-buttons (click)="daywisedistance();">Details</ion-buttons> -->\n      <ion-badge item-end color="gpsc">{{ d.distance | number : \'1.0-2\'}}</ion-badge>\n\n    </ion-item>\n    <ion-card-content>\n      <ion-row style="padding-top: 5px;">\n        <ion-col col-1>\n          <ion-icon ios="ios-pin" md="md-pin" style="color:#33c45c;font-size:15px;"></ion-icon>\n        </ion-col>\n        <ion-col style="font-size: 0.8em;">\n          <div class="overme">\n            {{ d.startAddress }}\n          </div>\n        </ion-col>\n      </ion-row>\n      <ion-row style="padding-top: 5px;">\n        <ion-col col-1>\n          <ion-icon ios="ios-pin" md="md-pin" style="color:#e14444;font-size:15px;"></ion-icon>\n        </ion-col>\n        <ion-col style="font-size: 0.8em;">\n          <div class="overme">\n            {{ d.endAddress }}\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-card-content>\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/daywise-distance/daywise-distance.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_geocoder_geocoder__["a" /* GeocoderProvider */]])
    ], DaywiseDistancePage);
    return DaywiseDistancePage;
}());

//# sourceMappingURL=daywise-distance.js.map

/***/ }),

/***/ 149:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MsgUtilityPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MsgUtilityPage = /** @class */ (function () {
    // conditionParam: string;
    function MsgUtilityPage(navCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.obj = [];
        this.filterObj = [];
        this.isTrade = false;
        this.isNonTrade = false;
        this.checkAllNonTrades = false;
        this.checkAllTrades = false;
        this.selectedArray = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        debugger;
        if (navParams.get("param") != null) {
            this.obj = this.navParams.get("param");
            this.filterObj = JSON.parse(JSON.stringify(this.obj));
            // this.conditionParam = this.navParams.get("cond");
        }
        if (localStorage.getItem("AlreadyDimissed") !== null) {
            localStorage.removeItem("AlreadyDimissed");
        }
        if (localStorage.getItem('AlreadyClicked') !== null) {
            localStorage.removeItem("AlreadyClicked");
        }
    }
    MsgUtilityPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MsgUtilityPage');
    };
    MsgUtilityPage.prototype.done = function () {
        console.log("inside");
        console.log("chlist", this.selectedArray);
        this.dismiss();
    };
    MsgUtilityPage.prototype.getItems = function (event) {
        console.log(event.target.value);
        // set val to the value of the searchbar
        var val = event.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.filterObj = this.obj.filter(function (item) {
                // console.log("search result=> " + JSON.stringify(item))
                return (item.first_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
        else {
            this.filterObj = this.obj;
        }
        console.log("onj", this.obj);
    };
    MsgUtilityPage.prototype.back = function () {
        this.navCtrl.push("FastagListPage", {
            "selecteduser": this.selectedArray
        });
    };
    MsgUtilityPage.prototype.dismiss = function () {
        debugger;
        var data = this.selectedArray;
        this.viewCtrl.dismiss(data);
    };
    MsgUtilityPage.prototype.selectMember = function (data) {
        if (data.checked == true) {
            this.selectedArray.push({
                "phone": data.phone
            });
        }
        else {
            var newArray = this.selectedArray.filter(function (el) {
                return el.phone !== data.phone;
            });
            this.selectedArray = newArray;
        }
        console.log("st", this.selectedArray);
    };
    MsgUtilityPage.prototype.checkAll = function (key) {
        var that = this;
        if (that.filterObj.length > 0) {
            if (key == 'select') {
                that.filterObj.map(function (d) {
                    d.checked = true;
                    that.selectedArray.push({
                        "phone": d.phone
                    });
                    return d;
                });
            }
            else if (key == 'deselect') {
                that.filterObj.map(function (d) {
                    d.checked = false;
                    that.selectedArray = [];
                    return d;
                });
            }
        }
    };
    MsgUtilityPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-msg-utility',template:/*ion-inline-start:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/msg-utility/msg-utility.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Select User</ion-title>\n    <ion-buttons end>\n      <button (click)="dismiss()" icon-only ion-button>\n        <ion-icon name="close-circle"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n  <ion-toolbar>\n    <ion-searchbar (ionInput)="getItems($event)"></ion-searchbar>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-row>\n    <ion-col col-50>\n      <button ion-button color="gpsc"  round block (click)="checkAll(\'select\')">Select All</button>\n    </ion-col>\n    <ion-col col-50>\n      <button ion-button color="light"  round block (click)="checkAll(\'deselect\')">Deselect All</button>\n    </ion-col>\n  </ion-row>\n  \n  <ion-list *ngIf="obj.length > 0">\n    <ion-item *ngFor="let member of filterObj">\n      <ion-label>{{member.first_name}}</ion-label>\n      <ion-checkbox (click)="selectMember(member)"\n                    [(ngModel)]="member.checked"\n                    color="dark">\n      </ion-checkbox>\n    </ion-item>\n  </ion-list>\n</ion-content>\n\n<ion-footer class="myColor">\n  <ion-toolbar style="background-image: linear-gradient(to right top, #000000, #c0002d, #a60033, #8b0935, #6f1133);">\n    <ion-title (click)="done()">Done</ion-title>\n  </ion-toolbar>\n</ion-footer>\n'/*ion-inline-end:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/msg-utility/msg-utility.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], MsgUtilityPage);
    return MsgUtilityPage;
}());

//# sourceMappingURL=msg-utility.js.map

/***/ }),

/***/ 163:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 163;

/***/ }),

/***/ 204:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/ac-report/ac-report.module": [
		596,
		78
	],
	"../pages/add-devices/add-devices.module": [
		595,
		0
	],
	"../pages/add-devices/paytmwalletlogin/paytmwalletlogin.module": [
		597,
		77
	],
	"../pages/add-devices/update-device/update-device.module": [
		600,
		69
	],
	"../pages/add-devices/upload-doc/add-doc/add-doc.module": [
		598,
		68
	],
	"../pages/add-devices/upload-doc/upload-doc.module": [
		599,
		76
	],
	"../pages/add-devices/vehicle-details/vehicle-details.module": [
		601,
		24
	],
	"../pages/all-notifications/all-notifications.module": [
		607,
		67
	],
	"../pages/all-notifications/notif-map/notif-map.module": [
		602,
		66
	],
	"../pages/chat/chat.module": [
		603,
		23
	],
	"../pages/contact-us/contact-us.module": [
		604,
		75
	],
	"../pages/create-trip/create-trip.module": [
		605,
		65
	],
	"../pages/customers/customers.module": [
		606,
		64
	],
	"../pages/customers/modals/add-customer-modal/add-customer-modal.module": [
		608,
		63
	],
	"../pages/customers/modals/add-device-modal.module": [
		609,
		62
	],
	"../pages/customers/modals/group-modal/group-modal.module": [
		610,
		61
	],
	"../pages/customers/modals/update-cust/update-cust.module": [
		612,
		60
	],
	"../pages/daily-report-new/daily-report-new.module": [
		611,
		59
	],
	"../pages/daily-report/daily-report.module": [
		613,
		2
	],
	"../pages/dashboard/dashboard.module": [
		614,
		14
	],
	"../pages/daywise-distance/daywise-distance.module": [
		376
	],
	"../pages/daywise-report/daywise-report.module": [
		615,
		13
	],
	"../pages/dealers/add-dealer/add-dealer.module": [
		616,
		58
	],
	"../pages/dealers/dealers.module": [
		617,
		57
	],
	"../pages/dealers/edit-dealer/edit-dealer.module": [
		618,
		56
	],
	"../pages/device-summary-repo/device-summary-repo.module": [
		619,
		8
	],
	"../pages/distance-report/distance-report.module": [
		620,
		7
	],
	"../pages/expenses/add-expense/add-expense.module": [
		621,
		55
	],
	"../pages/expenses/expense-type-detail/expense-type-detail.module": [
		622,
		54
	],
	"../pages/expenses/expenses.module": [
		623,
		53
	],
	"../pages/fastag-list/fastag-list.module": [
		624,
		52
	],
	"../pages/fastag-list/fastag/fastag.module": [
		625,
		74
	],
	"../pages/feedback/feedback.module": [
		626,
		15
	],
	"../pages/fuel/fuel-chart/fuel-chart.module": [
		627,
		22
	],
	"../pages/fuel/fuel-consumption-report/fuel-consumption-report.module": [
		628,
		21
	],
	"../pages/fuel/fuel-consumption/fuel-consumption.module": [
		629,
		73
	],
	"../pages/fuel/fuel-events/fuel-events.module": [
		630,
		16
	],
	"../pages/gallery/gallery.module": [
		631,
		51
	],
	"../pages/geofence-report/geofence-report.module": [
		632,
		12
	],
	"../pages/geofence/add-geofence/add-geofence.module": [
		634,
		50
	],
	"../pages/geofence/geofence-show/geofence-show.module": [
		633,
		49
	],
	"../pages/geofence/geofence.module": [
		636,
		48
	],
	"../pages/groups/groups.module": [
		635,
		47
	],
	"../pages/history-device/history-device.module": [
		637,
		46
	],
	"../pages/idle-report/idle-report.module": [
		638,
		45
	],
	"../pages/ignition-report/ignition-report.module": [
		640,
		1
	],
	"../pages/live-single-device/device-settings/device-settings.module": [
		639,
		44
	],
	"../pages/live-single-device/live-single-device.module": [
		641,
		72
	],
	"../pages/live/expired/expired.module": [
		642,
		43
	],
	"../pages/live/live.module": [
		643,
		42
	],
	"../pages/loading-unloading-trip/loading-unloading-trip.module": [
		644,
		11
	],
	"../pages/login/login.module": [
		673,
		71
	],
	"../pages/maintenance-reminder/add-reminder/add-reminder.module": [
		645,
		41
	],
	"../pages/maintenance-reminder/maintenance-reminder.module": [
		646,
		40
	],
	"../pages/msg-utility/msg-utility.module": [
		379
	],
	"../pages/over-speed-repo/over-speed-repo.module": [
		647,
		6
	],
	"../pages/poi-list/add-poi/add-poi.module": [
		648,
		39
	],
	"../pages/poi-list/poi-list.module": [
		649,
		20
	],
	"../pages/poi-report/poi-report.module": [
		650,
		38
	],
	"../pages/profile/profile.module": [
		651,
		70
	],
	"../pages/profile/settings/notif-setting/notif-setting.module": [
		652,
		37
	],
	"../pages/profile/settings/settings.module": [
		653,
		36
	],
	"../pages/reports-collection/reports-collection.module": [
		654,
		35
	],
	"../pages/route-map-show/route-map-show.module": [
		655,
		34
	],
	"../pages/route-voilations/route-voilations.module": [
		656,
		33
	],
	"../pages/route/route.module": [
		657,
		32
	],
	"../pages/signup/signup-otp/signup-otp.module": [
		658,
		31
	],
	"../pages/signup/signup.module": [
		659,
		30
	],
	"../pages/siren-alert/siren-alert.module": [
		660,
		29
	],
	"../pages/sos-report/sos-report.module": [
		661,
		10
	],
	"../pages/speed-repo/speed-repo.module": [
		662,
		19
	],
	"../pages/stoppages-repo/stoppages-repo.module": [
		664,
		5
	],
	"../pages/supported-devices/supported-devices.module": [
		663,
		28
	],
	"../pages/toll-map/toll-map.module": [
		665,
		18
	],
	"../pages/toll-report/toll-report.module": [
		666,
		9
	],
	"../pages/toll/toll.module": [
		667,
		27
	],
	"../pages/trip-report/trip-report.module": [
		668,
		4
	],
	"../pages/trip-report/trip-review/trip-review.module": [
		669,
		26
	],
	"../pages/tt/tt.module": [
		670,
		25
	],
	"../pages/working-hours-report/working-hours-report.module": [
		671,
		3
	],
	"../pages/your-trips/your-trips.module": [
		672,
		17
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 204;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 348:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImmobilizeModelPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_speech_recognition__ = __webpack_require__(349);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_sms__ = __webpack_require__(140);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ImmobilizeModelPage = /** @class */ (function () {
    function ImmobilizeModelPage(navParams, viewCtrl, speechRecognition, translate, toastCtrl, apiCall, alertCtrl, events, sms, plt, cd) {
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.speechRecognition = speechRecognition;
        this.translate = translate;
        this.toastCtrl = toastCtrl;
        this.apiCall = apiCall;
        this.alertCtrl = alertCtrl;
        this.events = events;
        this.sms = sms;
        this.plt = plt;
        this.cd = cd;
        this.min_time = "10";
        this.timeoutSeconds = 60000;
        this.isRecording = false;
        this.engineCutStatus = '';
        this.immobilizeChk = false;
        this.immobilizeCount = 0;
        this.callLiveTracking = 0;
        this.showErr = false;
        this.unlocked = false;
        this.locked = false;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        if (navParams.get("param") != null) {
            this.obj = this.navParams.get("param");
        }
        if (localStorage.getItem("AlreadyDimissed") !== null) {
            localStorage.removeItem("AlreadyDimissed");
        }
        if (localStorage.getItem('AlreadyClicked') !== null) {
            localStorage.removeItem("AlreadyClicked");
        }
    }
    ImmobilizeModelPage.prototype.isIos = function () {
        return this.plt.is('ios');
    };
    ImmobilizeModelPage.prototype.stopListening = function () {
        var _this = this;
        this.speechRecognition.stopListening().then(function () {
            _this.isRecording = false;
        });
    };
    // ionicViewDidEnter() {
    //   localStorage.removeItem("AlreadyClicked");
    // }
    ImmobilizeModelPage.prototype.getPermission = function () {
        var _this = this;
        this.speechRecognition.hasPermission()
            .then(function (hasPermission) {
            if (!hasPermission) {
                _this.speechRecognition.requestPermission();
            }
        });
    };
    ImmobilizeModelPage.prototype.startListening = function () {
        var _this = this;
        this.getPermission();
        var options = {
            language: 'en-US'
        };
        this.speechRecognition.startListening(options).subscribe(function (matches) {
            _this.matches = matches;
            _this.cd.detectChanges();
            console.log("matech length: ", _this.matches.length);
            console.log("match found: ", _this.matches[0]);
            for (var i = 0; i < _this.matches.length; i++) {
                if (_this.matches[i] === 'Lock engine' || _this.matches[i] === 'Unlock Engine' || _this.matches[i] === 'lock engine' || _this.matches[i] === 'unlock engine') {
                    _this.IgnitionOnOff(_this.obj);
                }
            }
        });
        this.isRecording = true;
    };
    ImmobilizeModelPage.prototype.dismiss = function () {
        if (localStorage.getItem("AlreadyDimissed") !== null) {
            this.events.publish("Released:Dismiss");
            localStorage.removeItem("AlreadyDimissed");
        }
        console.log("Inside the function");
        this.viewCtrl.dismiss();
        localStorage.setItem("AlreadyDimissed", "true");
    };
    ImmobilizeModelPage.prototype.voiceToText = function () {
        var _this = this;
        // debugger
        // Check feature available
        this.speechRecognition.isRecognitionAvailable()
            .then(function (available) { return console.log(available); });
        // Check permission
        this.speechRecognition.hasPermission()
            .then(function (hasPermission) {
            console.log(hasPermission);
            if (!hasPermission) {
                // Request permissions
                _this.speechRecognition.requestPermission()
                    .then(function () {
                    console.log('Granted');
                    _this.startListning();
                }, function () { return console.log('Denied'); });
            }
            else {
                _this.startListning();
            }
        });
    };
    ImmobilizeModelPage.prototype.startListning = function () {
        var _this = this;
        // Start the recognition process
        this.speechRecognition.startListening()
            .subscribe(function (matches) {
            console.log(matches);
            for (var i = 0; i < matches.length; i++) {
                if (matches[i] === 'Lock Engine' || matches[i] === 'lock engine') {
                    _this.engineCut('1');
                }
                else if (matches[i] === 'Unlock Engine' || matches[i] === 'unlock engine') {
                    _this.engineCut('0');
                }
                // if (matches[i] === 'Lock Engine' || matches[i] === 'Unlock Engine' || matches[i] === 'lock engine' || matches[i] === 'unlock engine') {
                //   this.IgnitionOnOff(this.obj);
                // }
            }
        }, function (onerror) { return console.log('error:', onerror); });
    };
    // unlockEngine() {
    //   // debugger
    //   let that = this;
    //   if (this.obj.last_ACC != null || this.obj.last_ACC != undefined) {
    //     if (localStorage.getItem('AlreadyClicked') !== null) {
    //       let toast = this.toastCtrl.create({
    //         message: this.translate.instant('Process ongoing..'),
    //         duration: 1800,
    //         position: 'top'
    //       });
    //       toast.present();
    //     } else {
    //       this.checkImmobilizePassword();
    //       this.messages = undefined;
    //       this.dataEngine = this.obj;
    //       let baseURLp = this.apiCall.mainUrl + 'deviceModel/getDevModelByName?type=' + this.dataEngine.device_model.device_type;
    //       this.apiCall.startLoading().present();
    //       this.apiCall.ignitionoffCall(baseURLp)
    //         .subscribe(data => {
    //           this.apiCall.stopLoading();
    //           this.DeviceConfigStatus = data;
    //           this.immobType = data[0].imobliser_type;
    //           let key;
    //           console.log('ignition lock key: ', this.dataEngine.ignitionLock)
    //           if (this.dataEngine.ignitionLock == '1') {
    //             this.messages = this.translate.instant('Do you want to unlock the engine?')
    //             key = 1;
    //           } 
    //           else {
    //             if (this.dataEngine.ignitionLock == '0') {
    //               // this.messages = this.translate.instant('Do you want to lock the engine?')
    //               this.messages = this.translate.instant('The engine is already unlocked...!')
    //               key = 0;
    //             }
    //           }
    //           if (key === 1) {
    //             let alert = this.alertCtrl.create({
    //               message: this.messages,
    //               buttons: [{
    //                 text: this.translate.instant('YES'),
    //                 handler: () => {
    //                   debugger
    //                   if (this.immobType == 0 || this.immobType == undefined) {
    //                     // that.clicked = true;
    //                     localStorage.setItem('AlreadyClicked', 'true');
    //                     var devicedetail = {
    //                       "_id": this.dataEngine._id,
    //                       "engine_status": !this.dataEngine.engine_status
    //                     }
    //                     // this.apiCall.startLoading().present();
    //                     this.apiCall.deviceupdateCall(devicedetail)
    //                       .subscribe(response => {
    //                         // this.apiCall.stopLoading();
    //                         // this.editdata = response;
    //                         const toast = this.toastCtrl.create({
    //                           message: response.message,
    //                           duration: 2000,
    //                           position: 'top'
    //                         });
    //                         toast.present();
    //                         // this.responseMessage = "Edit successfully";
    //                         // this.getdevices();
    //                         var msg;
    //                         if (!this.dataEngine.engine_status) {
    //                           msg = this.DeviceConfigStatus[0].resume_command;
    //                         }
    //                         else {
    //                           msg = this.DeviceConfigStatus[0].immoblizer_command;
    //                         }
    //                         this.sms.send(this.obj.sim_number, msg);
    //                         const toast1 = this.toastCtrl.create({
    //                           message: this.translate.instant('SMS sent successfully'),
    //                           duration: 2000,
    //                           position: 'bottom'
    //                         });
    //                         toast1.present();
    //                         // that.clicked = false;
    //                         localStorage.removeItem("AlreadyClicked");
    //                       },
    //                         error => {
    //                           // that.clicked = false;
    //                           localStorage.removeItem("AlreadyClicked");
    //                           // this.apiCall.stopLoading();
    //                           console.log(error);
    //                         });
    //                   } else {
    //                     console.log("Call server code here!!")
    //                     if (that.checkedPass === 'PASSWORD_SET') {
    //                       this.askForPassword(this.obj);
    //                       return;
    //                     }
    //                     that.serverLevelOnOff(this.obj);
    //                   }
    //                 }
    //               },
    //               {
    //                 text: this.translate.instant('NO')
    //               }]
    //             });
    //             alert.present();
    //           } else {
    //             let alert = this.alertCtrl.create({
    //               message: this.messages,
    //               buttons: ['Okay']
    //             });
    //             alert.present();
    //           }
    //         },
    //           error => {
    //             this.apiCall.stopLoading();
    //             console.log("some error: ", error._body.message);
    //           });
    //     }
    //   }
    // }
    // lockEngine() {
    //   // debugger
    //   let that = this;
    //   if (this.obj.last_ACC != null || this.obj.last_ACC != undefined) {
    //     if (localStorage.getItem('AlreadyClicked') !== null) {
    //       let toast = this.toastCtrl.create({
    //         message: this.translate.instant('Process ongoing..'),
    //         duration: 1800,
    //         position: 'top'
    //       });
    //       toast.present();
    //     } else {
    //       this.checkImmobilizePassword();
    //       this.messages = undefined;
    //       this.dataEngine = this.obj;
    //       var baseURLp = this.apiCall.mainUrl + 'deviceModel/getDevModelByName?type=' + this.dataEngine.device_model.device_type;
    //       this.apiCall.startLoading().present();
    //       this.apiCall.ignitionoffCall(baseURLp)
    //         .subscribe(data => {
    //           this.apiCall.stopLoading();
    //           this.DeviceConfigStatus = data;
    //           this.immobType = data[0].imobliser_type;
    //           var key;
    //           console.log('ignition lock key: ', this.dataEngine.ignitionLock)
    //           if (this.dataEngine.ignitionLock == '1') {
    //             // this.messages = this.translate.instant('Do you want to unlock the engine?')
    //             this.messages = this.translate.instant('The engine is already locked...!')
    //             key = 0;
    //           } else {
    //             if (this.dataEngine.ignitionLock == '0') {
    //               this.messages = this.translate.instant('Do you want to lock the engine?')
    //               // this.messages = this.translate.instant('The engine is already unlocked...!')
    //               key = 1;
    //             }
    //           }
    //           if (key === 1) {
    //             let alert = this.alertCtrl.create({
    //               message: this.messages,
    //               buttons: [{
    //                 text: 'YES',
    //                 handler: () => {
    //                   if (this.immobType == 0 || this.immobType == undefined) {
    //                     // that.clicked = true;
    //                     localStorage.setItem('AlreadyClicked', 'true');
    //                     var devicedetail = {
    //                       "_id": this.dataEngine._id,
    //                       "engine_status": !this.dataEngine.engine_status
    //                     }
    //                     // this.apiCall.startLoading().present();
    //                     this.apiCall.deviceupdateCall(devicedetail)
    //                       .subscribe(response => {
    //                         // this.apiCall.stopLoading();
    //                         // this.editdata = response;
    //                         const toast = this.toastCtrl.create({
    //                           message: response.message,
    //                           duration: 2000,
    //                           position: 'top'
    //                         });
    //                         toast.present();
    //                         // this.responseMessage = "Edit successfully";
    //                         // this.getdevices();
    //                         var msg;
    //                         if (!this.dataEngine.engine_status) {
    //                           msg = this.DeviceConfigStatus[0].resume_command;
    //                         }
    //                         else {
    //                           msg = this.DeviceConfigStatus[0].immoblizer_command;
    //                         }
    //                         this.sms.send(this.obj.sim_number, msg);
    //                         const toast1 = this.toastCtrl.create({
    //                           message: this.translate.instant('SMS sent successfully'),
    //                           duration: 2000,
    //                           position: 'bottom'
    //                         });
    //                         toast1.present();
    //                         // that.clicked = false;
    //                         localStorage.removeItem("AlreadyClicked");
    //                       },
    //                         error => {
    //                           // that.clicked = false;
    //                           localStorage.removeItem("AlreadyClicked");
    //                           // this.apiCall.stopLoading();
    //                           console.log(error);
    //                         });
    //                   } else {
    //                     console.log("Call server code here!!")
    //                     if (that.checkedPass === 'PASSWORD_SET') {
    //                       this.askForPassword(this.obj);
    //                       return;
    //                     }
    //                     that.serverLevelOnOff(this.obj);
    //                   }
    //                 }
    //               },
    //               {
    //                 text: this.translate.instant('NO')
    //               }]
    //             });
    //             alert.present();
    //           } else {
    //             let alert = this.alertCtrl.create({
    //               message: this.messages,
    //               buttons: ['Okay']
    //             });
    //             alert.present();
    //           }
    //         },
    //           error => {
    //             this.apiCall.stopLoading();
    //             console.log("some error: ", error._body.message);
    //           });
    //     }
    //   }
    // }
    ImmobilizeModelPage.prototype.IgnitionOnOff = function (d) {
        var _this = this;
        var that = this;
        if (d.last_ACC != null || d.last_ACC != undefined) {
            if (localStorage.getItem('AlreadyClicked') !== null) {
                var toast = this.toastCtrl.create({
                    message: this.translate.instant('Process ongoing..'),
                    duration: 1800,
                    position: 'top'
                });
                toast.present();
            }
            else {
                this.checkImmobilizePassword();
                this.messages = undefined;
                this.dataEngine = d;
                var baseURLp = this.apiCall.mainUrl + 'deviceModel/getDevModelByName?type=' + this.dataEngine.device_model.device_type;
                this.apiCall.startLoading().present();
                this.apiCall.ignitionoffCall(baseURLp)
                    .subscribe(function (data) {
                    _this.apiCall.stopLoading();
                    _this.DeviceConfigStatus = data;
                    _this.immobType = data[0].imobliser_type;
                    if (_this.dataEngine.ignitionLock == '1') {
                        _this.messages = _this.translate.instant('Do you want to unlock the engine?');
                    }
                    else {
                        if (_this.dataEngine.ignitionLock == '0') {
                            _this.messages = _this.translate.instant('Do you want to lock the engine?');
                        }
                    }
                    var alert = _this.alertCtrl.create({
                        message: _this.messages,
                        buttons: [{
                                text: _this.translate.instant('YES'),
                                handler: function () {
                                    if (_this.immobType == 0 || _this.immobType == undefined) {
                                        // that.clicked = true;
                                        localStorage.setItem('AlreadyClicked', 'true');
                                        var devicedetail = {
                                            "_id": _this.dataEngine._id,
                                            "engine_status": !_this.dataEngine.engine_status
                                        };
                                        _this.apiCall.deviceupdateCall(devicedetail)
                                            .subscribe(function (response) {
                                            var toast = _this.toastCtrl.create({
                                                message: response.message,
                                                duration: 2000,
                                                position: 'top'
                                            });
                                            toast.present();
                                            var msg;
                                            if (!_this.dataEngine.engine_status) {
                                                msg = _this.DeviceConfigStatus[0].resume_command;
                                            }
                                            else {
                                                msg = _this.DeviceConfigStatus[0].immoblizer_command;
                                            }
                                            _this.sms.send(d.sim_number, msg);
                                            var toast1 = _this.toastCtrl.create({
                                                message: _this.translate.instant('SMS sent successfully'),
                                                duration: 2000,
                                                position: 'bottom'
                                            });
                                            toast1.present();
                                            // that.clicked = false;
                                            localStorage.removeItem("AlreadyClicked");
                                        }, function (error) {
                                            // that.clicked = false;
                                            localStorage.removeItem("AlreadyClicked");
                                            // this.apiCall.stopLoading();
                                            console.log(error);
                                        });
                                    }
                                    else {
                                        console.log("Call server code here!!");
                                        if (that.checkedPass === 'PASSWORD_SET') {
                                            _this.askForPassword(d, _this.dataEngine.ignitionLock);
                                            return;
                                        }
                                        that.serverLevelOnOff(d);
                                    }
                                }
                            },
                            {
                                text: _this.translate.instant('NO')
                            }]
                    });
                    alert.present();
                }, function (error) {
                    _this.apiCall.stopLoading();
                    console.log("some error: ", error._body.message);
                });
            }
        }
    };
    ;
    ImmobilizeModelPage.prototype.checkImmobilizePassword = function () {
        var _this = this;
        var rurl = this.apiCall.mainUrl + 'users/get_user_setting';
        var Var = { uid: this.islogin._id };
        this.apiCall.urlpasseswithdata(rurl, Var)
            .subscribe(function (data) {
            if (!data.engine_cut_psd) {
                _this.checkedPass = 'PASSWORD_NOT_SET';
            }
            else {
                _this.checkedPass = 'PASSWORD_SET';
            }
        });
    };
    ImmobilizeModelPage.prototype.askForPassword = function (d, key) {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Enter Password',
            message: "Enter password for engine cut",
            inputs: [
                {
                    name: 'password',
                    placeholder: 'Password'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Proceed',
                    handler: function (data) {
                        _this.verifyPassword(data, d, key);
                    }
                }
            ]
        });
        prompt.present();
    };
    ImmobilizeModelPage.prototype.verifyPassword = function (pass, d, key) {
        var _this = this;
        var ryurl = this.apiCall.mainUrl + "users/verify_EngineCut_Password";
        var payLd = {
            "uid": this.islogin._id,
            "psd": pass.password
        };
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(ryurl, payLd)
            .subscribe(function (resp) {
            _this.apiCall.stopLoading();
            console.log(resp);
            if (resp.message === 'password not matched') {
                _this.toastmsg(resp.message);
                return;
            }
            _this.immobilizeDevice(d, _this.engineCutStatus, key);
        }, function () {
            _this.apiCall.stopLoading();
        });
    };
    ImmobilizeModelPage.prototype.toastmsg = function (msg) {
        this.toastCtrl.create({
            message: msg,
            duration: 1500,
            position: 'bottom'
        }).present();
    };
    ImmobilizeModelPage.prototype.toastmsg2 = function (msg) {
        this.toastCtrl.create({
            message: msg,
            duration: 8000,
            position: 'top'
        }).present();
    };
    ImmobilizeModelPage.prototype.serverLevelOnOff = function (d) {
        var _this = this;
        // let that = this;
        // that.clicked = true;
        localStorage.setItem("AlreadyClicked", "true");
        var data = {
            "imei": d.Device_ID,
            "_id": this.dataEngine._id,
            "engine_status": d.ignitionLock,
            "protocol_type": d.device_model.device_type
        };
        // this.apiCall.startLoading().present();
        this.apiCall.serverLevelonoff(data)
            .subscribe(function (resp) {
            console.log("ignition on off=> ", resp);
            _this.respMsg = resp;
            _this.intervalID = setInterval(function () {
                _this.apiCall.callResponse(_this.respMsg._id)
                    .subscribe(function (data) {
                    debugger;
                    console.log("interval=> " + data);
                    _this.commandStatus = data.status;
                    if (_this.commandStatus == 'SUCCESS') {
                        clearTimeout(_this.timeoutId);
                        clearInterval(_this.intervalID);
                        // that.clicked = false;
                        localStorage.removeItem("AlreadyClicked");
                        // this.apiCall.stopLoadingnw();
                        debugger;
                        var toast1 = _this.toastCtrl.create({
                            // message: this.translate.instant('process has been completed successfully!'),
                            message: data.commandResp,
                            duration: 5000,
                            position: 'top'
                        });
                        toast1.present();
                        _this.dismiss();
                    }
                });
            }, 5000);
            var that = _this;
            that.timeoutId = setTimeout(function () {
                debugger;
                // if (that.commandStatus !== 'SUCCESS') {
                clearTimeout(that.timeoutId);
                clearInterval(that.intervalID);
                console.log("after removinf interval: ", that.intervalID);
                localStorage.removeItem("AlreadyClicked");
                // }
                // if (that.timeoutId) {
                // clearTimeout(that.timeoutId);
                console.log("after removinf timeout: ", that.timeoutId);
                // }
            }, that.timeoutSeconds);
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log("error in onoff=>", err);
            localStorage.removeItem("AlreadyClicked");
            // that.clicked = false;
        });
    };
    ImmobilizeModelPage.prototype.onInfo = function () {
        var toast = this.toastCtrl.create({
            message: this.translate.instant("Now you can use voice control to use immobilize. Say 'Lock Engine' to lock vehicle engine and say 'Unlock Engine' to unlock vehicle engine."),
            showCloseButton: true,
            closeButtonText: 'Dismiss'
        });
        toast.present();
    };
    ImmobilizeModelPage.prototype.engineCut = function (key) {
        var _this = this;
        if (localStorage.getItem('AlreadyClicked') !== null) {
            var toast = this.toastCtrl.create({
                message: this.translate.instant('Process ongoing..'),
                duration: 1800,
                position: 'top'
            });
            toast.present();
        }
        else {
            var device_1 = this.obj;
            // let msg;
            this.engineCutStatus = device_1.ignitionLock;
            // if (key === this.engineCutStatus) {
            //   if (key === '0') {
            //     msg = this.translate.instant('The engine is already unlocked...!')
            //     this.toastmsg2(msg);
            //   } else if (key === '1') {
            //     msg = this.translate.instant('The engine is already locked...!')
            //     this.toastmsg2(msg);
            //   }
            //   return;
            // }
            var rurl = this.apiCall.mainUrl + 'users/get_user_setting';
            var Var = { uid: this.islogin._id };
            this.apiCall.urlpasseswithdata(rurl, Var)
                .subscribe(function (res) {
                var engCutPass = res.engine_cut_psd;
                if (engCutPass == undefined) {
                    _this.immobilizeDevice(device_1, _this.engineCutStatus, key);
                }
                else {
                    _this.askForPassword(device_1, key);
                }
            });
        }
    };
    ImmobilizeModelPage.prototype.immobilizeDevice = function (deviceObj, onOFFcmd, key) {
        var _this = this;
        var deviceCommand = (onOFFcmd == '1') ? "OFF" : "ON";
        var devModelId;
        if (key == '1') {
            this.messages = this.translate.instant('Do you want to unlock the engine?');
        }
        else {
            this.messages = this.translate.instant('Do you want to lock the engine?');
        }
        // if (deviceCommand == 'ON') {
        //   this.messages = this.translate.instant('Do you want to unlock the engine?')
        //   // key = 1;
        // } else {
        //   if (deviceCommand == 'OFF') {
        //     // this.messages = this.translate.instant('Do you want to lock the engine?')
        //     this.messages = this.translate.instant('Do you want to lock the engine?')
        //     // key = 0;
        //   }
        // }
        var alert = this.alertCtrl.create({
            message: this.messages,
            buttons: [{
                    text: this.translate.instant('YES'),
                    handler: function () {
                        localStorage.setItem('AlreadyClicked', 'true');
                        var identifyModelType = typeof (deviceObj.device_model);
                        if (identifyModelType == 'string') {
                            devModelId = deviceObj.device_model;
                        }
                        else {
                            devModelId = deviceObj.device_model._id;
                        }
                        var baseURLp = _this.apiCall.mainUrl + 'deviceModel/getDevModelByName?id=' + devModelId;
                        _this.apiCall.startLoading().present();
                        _this.apiCall.ignitionoffCall(baseURLp)
                            .subscribe(function (resModel) {
                            _this.apiCall.stopLoading();
                            var deviceType = resModel[0].device_type;
                            //console.log('resModel', deviceType);
                            // let cmdqueObj = {
                            //   "imei": deviceObj.Device_ID,
                            //   "_id": deviceObj._id,
                            //   "engine_status": onOFFcmd,
                            //   "protocol_type": deviceType
                            // }
                            var cmdqueObj = {
                                "imei": deviceObj.Device_ID,
                                "_id": deviceObj._id,
                                "engine_status": key,
                                "protocol_type": deviceType
                            };
                            _this.triggerCmd(cmdqueObj, deviceCommand);
                        });
                    }
                },
                {
                    text: this.translate.instant('NO')
                }]
        });
        alert.present();
    };
    ImmobilizeModelPage.prototype.triggerCmd = function (cmdObj, deviceCommand) {
        var _this = this;
        var that = this;
        this.callLiveTracking = 0;
        that.immobilizeCount = 45;
        that.immobilizeChk = true;
        var timer = setInterval(function () {
            that.immobilizeCount--;
            if (that.immobilizeCount === 0) {
                clearInterval(timer);
                localStorage.removeItem('AlreadyClicked');
            }
        }, 1000);
        this.apiCall.serverLevelonoff(cmdObj)
            .subscribe(function (res) {
            var cmdQId = res._id;
            //console.log('cmdQId', cmdQId);
            _this.intervalTimeOut = setTimeout(function () {
                var message = "Please Try After Sometime";
                localStorage.removeItem('AlreadyClicked');
                that.toastmsg2(message);
                that.shoError = setTimeout(function () {
                    that.immobilizeChk = false;
                    that.showErr = false;
                    clearTimeout(that.shoError);
                }, 5000);
                clearInterval(that.status_cmdq);
                clearTimeout(that.intervalTimeOut);
                clearInterval(timer);
            }, 45000);
            that.status_cmdq = setInterval(function () {
                that.apiCall.callResponse(cmdQId)
                    .subscribe(function (res) {
                    that.locked = false;
                    that.unlocked = false;
                    if (res.status == "SUCCESS") {
                        // that.Load = false;
                        // var cc = ''
                        if (deviceCommand === 'ON') {
                            // cc = 'UNLOCKED';
                            that.unlocked = true;
                        }
                        if (deviceCommand === 'OFF') {
                            // cc = 'LOCKED';
                            that.locked = true;
                        }
                        // let message = 'Vehicle' + ' ' + cc;
                        that.showErr = true;
                        that.toastmsg2(res.commandResp);
                        ///////////////////////////
                        setTimeout(function () {
                            that.viewCtrl.dismiss();
                        }, 2000);
                        ///////////////////////////
                        that.shoError = setTimeout(function () {
                            that.immobilizeChk = false;
                            that.showErr = false;
                            clearTimeout(that.shoError);
                        }, 5000);
                        //console.log("Console after Getting success",that.callLiveTracking);
                        if (that.callLiveTracking === 0) {
                            that.callLiveTracking = 1;
                        }
                        localStorage.removeItem('AlreadyClicked');
                        clearInterval(timer);
                        clearTimeout(that.intervalTimeOut);
                        clearInterval(that.status_cmdq);
                    }
                }, function () {
                    // that.errIcon = true;
                    var message = "Please Try After Sometime";
                    localStorage.removeItem('AlreadyClicked');
                    that.showErr = true;
                    that.toastmsg2(message);
                    // that.ImmErrMsg = message;
                    that.shoError = setTimeout(function () {
                        that.immobilizeChk = false;
                        that.showErr = false;
                        clearTimeout(that.shoError);
                    }, 5000);
                    that.immobilizeChk = false;
                    clearInterval(timer);
                    clearInterval(that.status_cmdq);
                    clearTimeout(that.intervalTimeOut);
                });
            }, 3000);
        });
    };
    ImmobilizeModelPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-immobilize',template:/*ion-inline-start:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/add-devices/immobilize-modal.html"*/'<ion-icon name="close" style="font-size: 3em; color: white;float: right; padding-right: 11px;" (click)="dismiss()">\n\n</ion-icon>\n\n\n\n<div class="mainDiv">\n\n\n\n    <div class="secondDiv">\n\n        <ion-grid>\n\n            <ion-row no-padding>\n\n                <ion-col width-50>\n\n                    <div class="design">\n\n                        <span class="cont">\n\n                            <p>{{"ARM" | translate}}</p>\n\n                        </span>\n\n                        <div style="height: 60px; width: 70px; margin: auto; text-align: center;">\n\n                            <img src="assets/imgs/lock.png" />\n\n                        </div>\n\n                    </div>\n\n                </ion-col>\n\n                <ion-col width-50>\n\n                    <div class="design">\n\n                        <span class="cont">\n\n                            <p>{{"DISARM" | translate}}</p>\n\n                        </span>\n\n                        <div style="height: 60px; width: 70px; margin: auto; text-align: center;">\n\n                            <img src="assets/imgs/unlock.png" />\n\n                        </div>\n\n                    </div>\n\n                </ion-col>\n\n            </ion-row>\n\n            <ion-row>\n\n               \n\n                <ion-col width-50 (click)="engineCut(\'1\')">\n\n                    <div class="design">\n\n                        <div style="height: 60px; width: 70px; margin: auto; text-align: center; padding-top: 38px">\n\n                            <img src="assets/imgs/play.png" />\n\n                        </div>\n\n                        <span class="cont">\n\n                            <p>{{"RESUME ENGINE" | translate}}</p>\n\n                        </span>\n\n                    </div>\n\n                </ion-col>\n\n                <ion-col width-50 (click)="engineCut(\'0\')">\n\n                   \n\n                    <div class="design">\n\n                        <div style="height: 60px; width: 70px; margin: auto; text-align: center; padding-top: 38px">\n\n                            <img src="assets/imgs/stop.png" />\n\n                        </div>\n\n                        <span class="cont">\n\n                            <p>{{"STOP ENGINE" | translate}}</p>\n\n                        </span>\n\n                    </div>\n\n                </ion-col>\n\n            </ion-row>\n\n        </ion-grid>\n\n        <div *ngIf="immobilizeChk === true" >\n\n            <div class="final">\n\n                <img src="assets/imgs/clock.svg" class="manual" />\n\n                <p style="font-size: 20px;">{{immobilizeCount}} <span style="font-size: 12px;">Sec.</span></p>\n\n            </div>\n\n        </div>\n\n        <div *ngIf="immobilizeChk === false" >\n\n            <div class="final" (click)="voiceToText()" *ngIf="!isIos()">\n\n                <img src="assets/imgs/support.png" class="manual" />\n\n                <p>{{"LISTEN" | translate}}</p>\n\n            </div>\n\n            <div class="final" (click)="startListening()" *ngIf="isIos()">\n\n                <img src="assets/imgs/support.png" class="manual" />\n\n                <p>{{"LISTEN" | translate}}</p>\n\n            </div>\n\n        </div>\n\n        <div style="padding: 3px 11px;">\n\n            <button ion-button block color="light" (click)="stopListening()" *ngIf="isRecording">Stop Listening</button>\n\n\n\n            <button ion-button (click)="dismiss()" color="gpsc" block>\n\n                {{"SUBMIT" | translate}}\n\n            </button>\n\n\n\n        </div>\n\n    </div>\n\n</div>\n\n<!-- <ion-card>\n\n    <ion-card-header>This is what I understood...</ion-card-header>\n\n    <ion-card-content>\n\n        <ion-list>\n\n            <ion-item *ngFor="let match of matches">\n\n                {{ match }}\n\n            </ion-item>\n\n        </ion-list>\n\n    </ion-card-content>\n\n</ion-card> -->\n\n<ion-fab bottom right>\n\n    <button ion-fab (click)="onInfo()" color="gpsc">\n\n\n\n        <img class="imgSty" src="assets/imgs/info.png" />\n\n    </button>\n\n</ion-fab>'/*ion-inline-end:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/add-devices/immobilize-modal.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_speech_recognition__["a" /* SpeechRecognition */],
            __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_4__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_sms__["a" /* SMS */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"]])
    ], ImmobilizeModelPage);
    return ImmobilizeModelPage;
}());

//# sourceMappingURL=immobilize-modal.js.map

/***/ }),

/***/ 362:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ViewDoc; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ViewDoc = /** @class */ (function () {
    function ViewDoc(navparams, viewCtrl, apiCall) {
        this.navparams = navparams;
        this.viewCtrl = viewCtrl;
        this.apiCall = apiCall;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("param1: ", this.navparams.get("param1"));
        this._instData = this.navparams.get("param1");
        var str = this._instData.imageURL;
        var str1 = str.split('public/');
        this.imgUrl = this.apiCall.mainUrl + str1[1];
        console.log("img url: ", this._instData);
    }
    ViewDoc.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    ViewDoc = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
            selector: 'page-view-doc',template:/*ion-inline-start:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/add-devices/upload-doc/view-doc/view-doc.html"*/'<div>\n\n    <ion-row>\n\n        <ion-col col-11>\n\n\n\n        </ion-col>\n\n        <ion-col col-1>\n\n            <ion-icon style="font-size:1.5em" name="close-circle" (tap)="dismiss()"></ion-icon>\n\n        </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n        <ion-col style="text-align: center;">\n\n            <img src="{{imgUrl}}" width="300" height="450" />\n\n        </ion-col>\n\n    </ion-row>\n\n    <ion-row padding-left>\n\n        <ion-col col-3>\n\n            <b>{{ "Expiry Date:" | translate }}</b>\n\n        </ion-col>\n\n        <ion-col col-9>{{_instData.expDate | date: \'medium\'}}</ion-col>\n\n    </ion-row>\n\n</div>\n\n'/*ion-inline-end:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/add-devices/upload-doc/view-doc/view-doc.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */]])
    ], ViewDoc);
    return ViewDoc;
}());

//# sourceMappingURL=view-doc.js.map

/***/ }),

/***/ 376:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DaywiseDistancePageModule", function() { return DaywiseDistancePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__daywise_distance__ = __webpack_require__(148);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var DaywiseDistancePageModule = /** @class */ (function () {
    function DaywiseDistancePageModule() {
    }
    DaywiseDistancePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__daywise_distance__["a" /* DaywiseDistancePage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__daywise_distance__["a" /* DaywiseDistancePage */]),
            ],
            exports: []
        })
    ], DaywiseDistancePageModule);
    return DaywiseDistancePageModule;
}());

//# sourceMappingURL=daywise-distance.module.js.map

/***/ }),

/***/ 377:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NearbyVehicleModal; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NearbyVehicleModal = /** @class */ (function () {
    function NearbyVehicleModal(viewCtrl) {
        this.viewCtrl = viewCtrl;
    }
    NearbyVehicleModal.prototype.onClick = function (key) {
        this.dismiss(key);
    };
    NearbyVehicleModal.prototype.dismiss = function (key) {
        this.viewCtrl.dismiss(key);
    };
    NearbyVehicleModal = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-neraby-vehicle-modal',template:/*ion-inline-start:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/live-single-device/neraby-vehicle-modal/neraby-vehicle-modal.html"*/'<ion-icon name="close" style="font-size: 3em; color: white;float: right; padding-right: 11px;" (click)="dismiss()">\n</ion-icon>\n\n<div class="mainDiv">\n    <div class="secondDiv">\n        <ion-card>\n            <ion-card-header\n                style="text-align: center; background-color: tomato; color: white; font-weight: bold; font-size: 20spx;">\n                NearBy</ion-card-header>\n            <ion-list>\n                <ion-item (click)="onClick(\'atm\')">\n                    <img src="assets/imgs/nearBy/deposit.png" style="width: 25px;" item-start />\n                    <ion-label style="margin-left: 10px; margin-bottom: 8px;">ATMs</ion-label>\n                </ion-item>\n                <ion-item (click)="onClick(\'petrol pumps\')">\n                    <img src="assets/imgs/nearBy/petrol-pump.png" style="width: 25px;" item-start />\n                    <ion-label style="margin-left: 10px; margin-bottom: 8px;">Petrol Pumps</ion-label>\n                </ion-item>\n                <ion-item (click)="onClick(\'hospitals\')">\n                    <img src="assets/imgs/nearBy/hospital.png" style="width: 25px;" item-start />\n                    <ion-label style="margin-left: 10px; margin-bottom: 8px;">Hospitals</ion-label>\n                </ion-item>\n                <ion-item (click)="onClick(\'police stations\')">\n                    <img src="assets/imgs/nearBy/police-station.png" style="width: 25px;" item-start />\n                    <ion-label style="margin-left: 10px; margin-bottom: 8px;">Police Stations</ion-label>\n                </ion-item>\n                <ion-item (click)="onClick(\'service points\')">\n                    <img src="assets/imgs/nearBy/call-center-agent.png" style="width: 25px;" item-start />\n                    <ion-label style="margin-left: 10px; margin-bottom: 8px;">Service Points</ion-label>\n                </ion-item>\n                <ion-item (click)="onClick(\'restaurants\')">\n                    <img src="assets/imgs/nearBy/store.png" style="width: 25px;" item-start />\n                    <ion-label style="margin-left: 10px; margin-bottom: 8px;">Restaurants</ion-label>\n                </ion-item>\n                <ion-item (click)="onClick(\'parking\')">\n                  <img src="assets/imgs/nearBy/park.png" style="width: 25px;" item-start />\n                  <ion-label style="margin-left: 10px; margin-bottom: 8px;">Parking</ion-label>\n              </ion-item>\n                <!-- <ion-item (click)="onClick(\'Toll Plazas\')">\n                    <img src="assets/imgs/nearBy/store.png" style="width: 25px;" item-start />\n                    <ion-label style="margin-left: 10px; margin-bottom: 8px;">Toll Plazas</ion-label>\n                </ion-item> -->\n            </ion-list>\n        </ion-card>\n    </div>\n</div>\n'/*ion-inline-end:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/live-single-device/neraby-vehicle-modal/neraby-vehicle-modal.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], NearbyVehicleModal);
    return NearbyVehicleModal;
}());

//# sourceMappingURL=neraby-vehicle-modal.js.map

/***/ }),

/***/ 379:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MsgUtilityPageModule", function() { return MsgUtilityPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__msg_utility__ = __webpack_require__(149);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var MsgUtilityPageModule = /** @class */ (function () {
    function MsgUtilityPageModule() {
    }
    MsgUtilityPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__msg_utility__["a" /* MsgUtilityPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__msg_utility__["a" /* MsgUtilityPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"]
            ],
        })
    ], MsgUtilityPageModule);
    return MsgUtilityPageModule;
}());

//# sourceMappingURL=msg-utility.module.js.map

/***/ }),

/***/ 421:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export ConnectionStatus */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NetworkProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_network__ = __webpack_require__(422);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject__ = __webpack_require__(583);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var ConnectionStatus;
(function (ConnectionStatus) {
    ConnectionStatus[ConnectionStatus["Online"] = 0] = "Online";
    ConnectionStatus[ConnectionStatus["Offline"] = 1] = "Offline";
})(ConnectionStatus || (ConnectionStatus = {}));
var NetworkProvider = /** @class */ (function () {
    function NetworkProvider(network, toastController, plt) {
        var _this = this;
        this.network = network;
        this.toastController = toastController;
        this.plt = plt;
        this.status = new __WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject__["BehaviorSubject"](ConnectionStatus.Offline);
        this.plt.ready().then(function () {
            _this.initializeNetworkEvents();
            var status = _this.network.type !== 'none' ? ConnectionStatus.Online : ConnectionStatus.Offline;
            _this.status.next(status);
        });
    }
    NetworkProvider.prototype.initializeNetworkEvents = function () {
        var _this = this;
        this.network.onDisconnect().subscribe(function () {
            if (_this.status.getValue() === ConnectionStatus.Online) {
                console.log('WE ARE OFFLINE');
                _this.updateNetworkStatus(ConnectionStatus.Offline);
            }
        });
        this.network.onConnect().subscribe(function () {
            if (_this.status.getValue() === ConnectionStatus.Offline) {
                console.log('WE ARE ONLINE');
                _this.updateNetworkStatus(ConnectionStatus.Online);
            }
        });
    };
    // showToastMsg(msg) {
    //   let toast = this.toastCtrl.create({
    //     message: msg,
    //     duration: 3000,
    //     position: 'middle'
    //   });
    //   toast.present();
    // }
    NetworkProvider.prototype.updateNetworkStatus = function (status) {
        return __awaiter(this, void 0, void 0, function () {
            var connection, toast;
            return __generator(this, function (_a) {
                this.status.next(status);
                connection = status == ConnectionStatus.Offline ? 'Offline' : 'Online';
                toast = this.toastController.create({
                    message: "You are now " + connection,
                    duration: 3000,
                    position: 'top'
                });
                toast.present();
                return [2 /*return*/];
            });
        });
    };
    NetworkProvider.prototype.onNetworkChange = function () {
        return this.status.asObservable();
    };
    NetworkProvider.prototype.getCurrentNetworkStatus = function () {
        return this.status.getValue();
    };
    NetworkProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_native_network__["a" /* Network */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"]])
    ], NetworkProvider);
    return NetworkProvider;
}());

//# sourceMappingURL=network.js.map

/***/ }),

/***/ 423:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SideMenuContentComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_side_menu_option_select_event__ = __webpack_require__(584);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Angular
 // tslint:disable-line
// Ionic


// This class is defined in this file because
// we don't want to make it exportable
var InnerMenuOptionModel = /** @class */ (function () {
    function InnerMenuOptionModel() {
    }
    InnerMenuOptionModel.fromMenuOptionModel = function (option, parent) {
        var innerMenuOptionModel = new InnerMenuOptionModel();
        innerMenuOptionModel.id = this.counter++;
        innerMenuOptionModel.iconName = option.iconName;
        innerMenuOptionModel.displayText = option.displayText;
        innerMenuOptionModel.badge = option.badge;
        innerMenuOptionModel.targetOption = option;
        innerMenuOptionModel.parent = parent || null;
        innerMenuOptionModel.selected = option.selected;
        if (option.suboptions) {
            innerMenuOptionModel.expanded = false;
            innerMenuOptionModel.suboptionsCount = option.suboptions.length;
            innerMenuOptionModel.subOptions = [];
            option.suboptions.forEach(function (subItem) {
                var innerSubItem = InnerMenuOptionModel.fromMenuOptionModel(subItem, innerMenuOptionModel);
                innerMenuOptionModel.subOptions.push(innerSubItem);
                // Select the parent if any
                // child option is selected
                if (subItem.selected) {
                    innerSubItem.parent.selected = true;
                    innerSubItem.parent.expanded = true;
                }
            });
        }
        return innerMenuOptionModel;
    };
    InnerMenuOptionModel.counter = 1;
    return InnerMenuOptionModel;
}());
var SideMenuContentComponent = /** @class */ (function () {
    function SideMenuContentComponent(platform, eventsCtrl, cdRef) {
        var _this = this;
        this.platform = platform;
        this.eventsCtrl = eventsCtrl;
        this.cdRef = cdRef;
        this.collapsableItems = [];
        // Outputs: return the selected option to the caller
        this.change = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        // Handle the redirect event
        this.eventsCtrl.subscribe(__WEBPACK_IMPORTED_MODULE_2__models_side_menu_option_select_event__["a" /* SideMenuOptionSelect */], function (data) {
            _this.updateSelectedOption(data);
        });
    }
    Object.defineProperty(SideMenuContentComponent.prototype, "options", {
        set: function (value) {
            var _this = this;
            if (value) {
                // Keep a reference to the options
                // sent to this component
                this.menuOptions = value;
                this.collapsableItems = new Array();
                // Map the options to our internal models
                this.menuOptions.forEach(function (option) {
                    var innerMenuOption = InnerMenuOptionModel.fromMenuOptionModel(option);
                    _this.collapsableItems.push(innerMenuOption);
                    // Check if there's any option marked as selected
                    if (option.selected) {
                        _this.selectedOption = innerMenuOption;
                    }
                    else if (innerMenuOption.suboptionsCount) {
                        innerMenuOption.subOptions.forEach(function (subItem) {
                            if (subItem.selected) {
                                _this.selectedOption = subItem;
                            }
                        });
                    }
                });
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SideMenuContentComponent.prototype, "settings", {
        set: function (value) {
            if (value) {
                this.menuSettings = value;
                this.mergeSettings();
            }
        },
        enumerable: true,
        configurable: true
    });
    SideMenuContentComponent.prototype.ngOnDestroy = function () {
        this.eventsCtrl.unsubscribe(__WEBPACK_IMPORTED_MODULE_2__models_side_menu_option_select_event__["a" /* SideMenuOptionSelect */]);
    };
    // ---------------------------------------------------
    // PUBLIC methods
    // ---------------------------------------------------
    // Send the selected option to the caller component
    SideMenuContentComponent.prototype.select = function (option) {
        if (this.menuSettings.showSelectedOption) {
            this.setSelectedOption(option);
        }
        // Return the selected option (not our inner option)
        this.change.emit(option.targetOption);
    };
    // Toggle the sub options of the selected item
    SideMenuContentComponent.prototype.toggleItemOptions = function (targetOption) {
        if (!targetOption)
            return;
        // If the accordion mode is set to true, we need
        // to collapse all the other menu options
        if (this.menuSettings.accordionMode) {
            this.collapsableItems.forEach(function (option) {
                if (option.id !== targetOption.id) {
                    option.expanded = false;
                }
            });
        }
        // Toggle the selected option
        targetOption.expanded = !targetOption.expanded;
    };
    // Reset the entire menu
    SideMenuContentComponent.prototype.collapseAllOptions = function () {
        this.collapsableItems.forEach(function (option) {
            if (!option.selected) {
                option.expanded = false;
            }
            if (option.suboptionsCount) {
                option.subOptions.forEach(function (subItem) {
                    if (subItem.selected) {
                        // Expand the parent if any of
                        // its childs is selected
                        subItem.parent.expanded = true;
                    }
                });
            }
        });
        // Update the view since there wasn't
        // any user interaction with it
        this.cdRef.detectChanges();
    };
    Object.defineProperty(SideMenuContentComponent.prototype, "subOptionIndentation", {
        // Get the proper indentation of each option
        get: function () {
            if (this.platform.is('ios'))
                return this.menuSettings.subOptionIndentation.ios;
            if (this.platform.is('windows'))
                return this.menuSettings.subOptionIndentation.wp;
            return this.menuSettings.subOptionIndentation.md;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SideMenuContentComponent.prototype, "optionHeight", {
        // Get the proper height of each option
        get: function () {
            if (this.platform.is('ios'))
                return this.menuSettings.optionHeight.ios;
            if (this.platform.is('windows'))
                return this.menuSettings.optionHeight.wp;
            return this.menuSettings.optionHeight.md;
        },
        enumerable: true,
        configurable: true
    });
    // ---------------------------------------------------
    // PRIVATE methods
    // ---------------------------------------------------
    // Method that set the selected option and its parent
    SideMenuContentComponent.prototype.setSelectedOption = function (option) {
        if (!option.targetOption.component)
            return;
        // Clean the current selected option if any
        if (this.selectedOption) {
            this.selectedOption.selected = false;
            this.selectedOption.targetOption.selected = false;
            if (this.selectedOption.parent) {
                this.selectedOption.parent.selected = false;
                this.selectedOption.parent.expanded = false;
            }
            this.selectedOption = null;
        }
        // Set this option to be the selected
        option.selected = true;
        option.targetOption.selected = true;
        if (option.parent) {
            option.parent.selected = true;
            option.parent.expanded = true;
        }
        // Keep a reference to the selected option
        this.selectedOption = option;
        // Update the view if needed since we may have
        // expanded or collapsed some options
        this.cdRef.detectChanges();
    };
    // Update the selected option
    SideMenuContentComponent.prototype.updateSelectedOption = function (data) {
        if (!data.displayText)
            return;
        var targetOption;
        this.collapsableItems.forEach(function (option) {
            if (option.displayText.toLowerCase() === data.displayText.toLowerCase()) {
                targetOption = option;
            }
            else if (option.suboptionsCount) {
                option.subOptions.forEach(function (subOption) {
                    if (subOption.displayText.toLowerCase() === data.displayText.toLowerCase()) {
                        targetOption = subOption;
                    }
                });
            }
        });
        if (targetOption) {
            this.setSelectedOption(targetOption);
        }
    };
    // Merge the settings received with the default settings
    SideMenuContentComponent.prototype.mergeSettings = function () {
        var defaultSettings = {
            accordionMode: false,
            optionHeight: {
                ios: 50,
                md: 50,
                wp: 50
            },
            arrowIcon: 'ios-arrow-down',
            showSelectedOption: false,
            selectedOptionClass: 'selected-option',
            indentSubOptionsWithoutIcons: false,
            subOptionIndentation: {
                ios: 16,
                md: 16,
                wp: 16
            }
        };
        if (!this.menuSettings) {
            // Use the default values
            this.menuSettings = defaultSettings;
            return;
        }
        if (!this.menuSettings.optionHeight) {
            this.menuSettings.optionHeight = defaultSettings.optionHeight;
        }
        else {
            this.menuSettings.optionHeight.ios = this.isDefinedAndPositive(this.menuSettings.optionHeight.ios) ? this.menuSettings.optionHeight.ios : defaultSettings.optionHeight.ios;
            this.menuSettings.optionHeight.md = this.isDefinedAndPositive(this.menuSettings.optionHeight.md) ? this.menuSettings.optionHeight.md : defaultSettings.optionHeight.md;
            this.menuSettings.optionHeight.wp = this.isDefinedAndPositive(this.menuSettings.optionHeight.wp) ? this.menuSettings.optionHeight.wp : defaultSettings.optionHeight.wp;
        }
        this.menuSettings.showSelectedOption = this.isDefined(this.menuSettings.showSelectedOption) ? this.menuSettings.showSelectedOption : defaultSettings.showSelectedOption;
        this.menuSettings.accordionMode = this.isDefined(this.menuSettings.accordionMode) ? this.menuSettings.accordionMode : defaultSettings.accordionMode;
        this.menuSettings.arrowIcon = this.isDefined(this.menuSettings.arrowIcon) ? this.menuSettings.arrowIcon : defaultSettings.arrowIcon;
        this.menuSettings.selectedOptionClass = this.isDefined(this.menuSettings.selectedOptionClass) ? this.menuSettings.selectedOptionClass : defaultSettings.selectedOptionClass;
        this.menuSettings.indentSubOptionsWithoutIcons = this.isDefined(this.menuSettings.indentSubOptionsWithoutIcons) ? this.menuSettings.indentSubOptionsWithoutIcons : defaultSettings.indentSubOptionsWithoutIcons;
        if (!this.menuSettings.subOptionIndentation) {
            this.menuSettings.subOptionIndentation = defaultSettings.subOptionIndentation;
        }
        else {
            this.menuSettings.subOptionIndentation.ios = this.isDefinedAndPositive(this.menuSettings.subOptionIndentation.ios) ? this.menuSettings.subOptionIndentation.ios : defaultSettings.subOptionIndentation.ios;
            this.menuSettings.subOptionIndentation.md = this.isDefinedAndPositive(this.menuSettings.subOptionIndentation.md) ? this.menuSettings.subOptionIndentation.md : defaultSettings.subOptionIndentation.md;
            this.menuSettings.subOptionIndentation.wp = this.isDefinedAndPositive(this.menuSettings.subOptionIndentation.wp) ? this.menuSettings.subOptionIndentation.wp : defaultSettings.subOptionIndentation.wp;
        }
    };
    SideMenuContentComponent.prototype.isDefined = function (property) {
        return property !== null && property !== undefined;
    };
    SideMenuContentComponent.prototype.isDefinedAndPositive = function (property) {
        return this.isDefined(property) && !isNaN(property) && property > 0;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('options'),
        __metadata("design:type", Array),
        __metadata("design:paramtypes", [Array])
    ], SideMenuContentComponent.prototype, "options", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('settings'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], SideMenuContentComponent.prototype, "settings", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], SideMenuContentComponent.prototype, "change", void 0);
    SideMenuContentComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'side-menu-content',template:/*ion-inline-start:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/shared/side-menu-content/side-menu-content.component.html"*/'<ion-list no-margin no-lines>\n\n    <ng-template ngFor let-option [ngForOf]="collapsableItems" let-i="index">\n\n\n\n        <!-- It is a simple option -->\n\n        <ng-template [ngIf]="!option.suboptionsCount">\n\n            <ion-item class="option"\n\n                [ngClass]="menuSettings?.showSelectedOption && option.selected ? menuSettings.selectedOptionClass : null"\n\n                (tap)="select(option)" tappable>\n\n                <ion-icon *ngIf="option.iconName" [name]="option.iconName" item-left></ion-icon>\n\n                {{ option.displayText }}\n\n                <ion-badge item-right *ngIf="option.badge | async as badgeNo">{{ badgeNo }}</ion-badge>\n\n            </ion-item>\n\n        </ng-template>\n\n\n\n        <!-- It has nested options -->\n\n        <ng-template [ngIf]="option.suboptionsCount">\n\n\n\n            <ion-list no-margin class="accordion-menu">\n\n\n\n                <!-- Header -->\n\n                <!-- <ion-item class="header" [ngClass]="menuSettings?.showSelectedOption && option.selected ? menuSettings.selectedOptionClass : null"\n\n                    (tap)="toggleItemOptions(option)" tappable>\n\n                    <ion-icon [class.rotate]="option.expanded" class="header-icon" [name]="option.iconName || menuSettings.arrowIcon"\n\n                        item-left></ion-icon>\n\n                    {{ option.displayText }}\n\n                </ion-item> -->\n\n\n\n                <ion-item class="header"\n\n                    [ngClass]="menuSettings?.showSelectedOption && option.selected ? menuSettings.selectedOptionClass : null"\n\n                    (tap)="toggleItemOptions(option)" tappable>\n\n                    <ion-icon *ngIf="option.iconName" [name]="option.iconName" item-left></ion-icon>\n\n                    {{ option.displayText }}\n\n\n\n                    <ion-icon [class.rotate]="option.expanded" class="header-icon"\n\n                        [name]="option.iconName123 || menuSettings.arrowIcon" item-right></ion-icon>\n\n                </ion-item>\n\n\n\n                <!-- Sub items -->\n\n                <div [style.height]="option.expanded ? ((optionHeight + 1) * option.suboptionsCount) + \'px\' : \'0px\'" class="options">\n\n                    <ng-template ngFor let-item [ngForOf]="option.subOptions">\n\n                        <ion-item class="sub-option" [style.padding-left]="subOptionIndentation + \'px\'" [class.no-icon]="menuSettings?.indentSubOptionsWithoutIcons && !item.iconName"\n\n                            [ngClass]="menuSettings?.showSelectedOption && item.selected ? menuSettings.selectedOptionClass : null"\n\n                            tappable (tap)="select(item)">\n\n                            <ion-icon *ngIf="item.iconName" [name]="item.iconName" item-left></ion-icon>\n\n                            {{ item.displayText }}\n\n                            <ion-badge item-right *ngIf="item.badge | async as badgeNo">{{ badgeNo }}</ion-badge>\n\n                        </ion-item>\n\n                    </ng-template>\n\n                </div>\n\n            </ion-list>\n\n\n\n        </ng-template>\n\n\n\n    </ng-template>\n\n</ion-list>'/*ion-inline-end:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/shared/side-menu-content/side-menu-content.component.html"*/,
            changeDetection: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"]])
    ], SideMenuContentComponent);
    return SideMenuContentComponent;
}());

//# sourceMappingURL=side-menu-content.component.js.map

/***/ }),

/***/ 424:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AlertModelPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AlertModelPage = /** @class */ (function () {
    function AlertModelPage(navParams, viewCtrl, apiCall) {
        this.viewCtrl = viewCtrl;
        this.apiCall = apiCall;
        this.min_time = "10";
        this.page = 0;
        this.limit = 8;
        this.allDevices = [];
        this.exp_devices = [];
        this.final = [];
        this.checkIfStat = "ALL";
        this.obj = {};
        this.finalData = [];
        console.log("final sub", this.finalData);
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + JSON.stringify(this.islogin));
        if (navParams.get("label") && navParams.get("value")) {
            this.stausdevice = localStorage.getItem('status');
            this.checkIfStat = this.stausdevice;
        }
        else {
            this.stausdevice = undefined;
        }
        console.log("navparam test: " + navParams.get("params"));
        this.obj = navParams.get("params");
        this.datetime = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format('LLLL');
        console.log("current date =>", this.datetime);
        this.start = this.datetime;
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).add(6, 'days').format('LLLL');
        console.log("upcoming week date for expired device =>", this.datetimeStart);
        this.end = this.datetimeStart;
        console.log("start date", this.start);
        console.log("end date", this.end);
        this.popUpForExpiredDevices();
        this.setCookie('displayedPopupNewsletter', 'yes', 1);
    }
    AlertModelPage.prototype.setCookie = function (c_name, value, exdays) {
        var c_value = escape(value);
        if (exdays) {
            var exdate = new Date();
            exdate.setDate(exdate.getDate() + exdays);
            c_value += '; expires=' + exdate.toUTCString();
        }
        document.cookie = c_name + '=' + c_value;
    };
    AlertModelPage.prototype.getCookie = function (c_name) {
        var i, x, y, cookies = document.cookie.split(';');
        for (i = 0; i < cookies.length; i++) {
            x = cookies[i].substr(0, cookies[i].indexOf('='));
            y = cookies[i].substr(cookies[i].indexOf('=') + 1);
            x = x.replace(/^\s+|\s+$/g, '');
            if (x === c_name) {
                return unescape(y);
            }
        }
    };
    AlertModelPage.prototype.popUpForExpiredDevices = function () {
        var _this = this;
        var baseUrl = "https://www.oneqlik.in/reminder/getReminder?id=" + this.islogin._id;
        this.apiCall.getdevicesForAllVehiclesApi(baseUrl)
            .subscribe(function (resp) {
            console.log("chk reminders", resp);
            _this.reminders = resp;
        });
    };
    AlertModelPage.prototype.dismiss = function () {
        console.log("Inside the function");
        this.viewCtrl.dismiss();
    };
    AlertModelPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            template: "\n\n<ion-list>\n  <div class = \"mainDiv\">\n          <ion-buttons end>\n            <button ion-button icon-only (click)=\"dismiss()\">\n                <ion-icon name=\"close-circle\"></ion-icon>\n            </button>\n          </ion-buttons>\n      <div class=\"secondDiv\">\n      <!-- <img ng-src=\"../assets/imgs/tenor.gif\" alt=\"Description\" /> -->\n      <!-- <span style=\"text-align: center\">Upcoming Expired Devices</span> -->\n      <p style=\"text-align: center;\n    font-family: initial;\n    font-size: 0.85 em; color: black\">Reminders</p>\n      <ion-row *ngFor=\"let d of reminders\">\n        <ion-col style=\"font-size: 0.85em;color: black\">\n            {{d}}\n        </ion-col>\n        <!-- <ion-col style=\"font-size: 0.85em;\" >\n          {{d.device_name}}\n        </ion-col>\n        <ion-col style=\"font-size: 0.85em\">\n        {{d.expDate}}\n        </ion-col> -->\n       </ion-row>\n\n\n      </div>\n  </div>\n</ion-list>\n  ",
            styles: ["\n\n.mainDiv {\n    padding: 50px;\n    height: 100%;\n    margin-top: 17px;\n    /* background-color: rgba(0, 0, 0, 0.7); */\n\n    /* //background: url(\"../assets/imgs/don-t-forget.jpg\")\n      no-repeat center center;\n    background-repeat: no-repeat;\n    background-size: cover;\n    position: fixed;\n    top: 0;\n    left: 0;\n    width: 100%;\n    height: 100%;\n    text-align: center; */\n}\n .secondDiv {\n     padding-top: 20px;\n      border-radius: 18px;\n     /* border: 2px solid black; */\n     background: white;\n     height: 500px !important;\n     overflow: scroll;\n     /* width: max-content; */\n }\n\n\n     /* .scroll-content {\n       left: 0;\n     right: 0;\n     top: 0;\n      bottom: 0;\n      position: absolute;\n      z-index: 1;\n      display: block;\n      overflow-x: hidden;\n       overflow-y: hidden;\n      -webkit-overflow-scrolling: touch;\n      will-change: scroll-position;\n       contain: size style layout;\n   } */\n\n"]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */]])
    ], AlertModelPage);
    return AlertModelPage;
}());

//# sourceMappingURL=alertModelPage.js.map

/***/ }),

/***/ 425:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AnnounceModelPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AnnounceModelPage = /** @class */ (function () {
    function AnnounceModelPage(navParams, viewCtrl, apiCall) {
        this.viewCtrl = viewCtrl;
        this.apiCall = apiCall;
        this.min_time = "10";
        this.page = 0;
        this.limit = 8;
        this.allDevices = [];
        this.exp_devices = [];
        this.final = [];
        this.checkIfStat = "ALL";
        this.obj = {};
        this.finalData = [];
        console.log("final sub", this.finalData);
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + JSON.stringify(this.islogin));
        if (navParams.get("label") && navParams.get("value")) {
            this.stausdevice = localStorage.getItem('status');
            this.checkIfStat = this.stausdevice;
        }
        else {
            this.stausdevice = undefined;
        }
        console.log("navparam test: " + navParams.get("params"));
        this.obj = navParams.get("params");
        this.datetime = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format('LLLL');
        console.log("current date =>", this.datetime);
        this.start = this.datetime;
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).add(6, 'days').format('LLLL');
        console.log("upcoming week date for expired device =>", this.datetimeStart);
        this.end = this.datetimeStart;
        console.log("start date", this.start);
        console.log("end date", this.end);
        //this.popUpForExpiredDevices();
        this.presentPopover();
        //this.setCookie('displayedPopupNewsletter', 'yes', 1);
    }
    AnnounceModelPage.prototype.setCookie = function (c_name, value, exdays) {
        var c_value = escape(value);
        if (exdays) {
            var exdate = new Date();
            exdate.setDate(exdate.getDate() + exdays);
            c_value += '; expires=' + exdate.toUTCString();
        }
        document.cookie = c_name + '=' + c_value;
    };
    AnnounceModelPage.prototype.presentPopover = function () {
        var _this = this;
        var b_url = this.apiCall.mainUrl + "users/get_user_setting";
        var Var = { uid: this.islogin._id };
        this.apiCall.urlpasseswithdata(b_url, Var)
            .subscribe(function (resp) {
            console.log("check lang key: ", resp);
            _this.msgg = resp.announcement;
            console.log("announcement", _this.msgg);
        }, function (err) {
            console.log(err);
        });
    };
    AnnounceModelPage.prototype.popUpForExpiredDevices = function () {
        var _this = this;
        var baseURLp;
        var vehName = [];
        var final = [];
        if (this.stausdevice) {
            baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&statuss=' + this.stausdevice;
        }
        else {
            baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
        }
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        //this.allDevices = []
        this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            console.log("vechicle list for expired devices =>", data);
            _this.allDevices = data.devices;
            console.log("data", _this.allDevices);
            final = [], vehName = [];
            for (var t = 0; t < _this.allDevices.length; t++) {
                debugger;
                var vehExpDate = __WEBPACK_IMPORTED_MODULE_3_moment__(_this.allDevices[t].expiration_date).format('LLLL');
                //console.log("expf", vehExpDate)
                var sd = new Date(_this.start);
                var td = new Date(_this.end);
                var expDate = new Date(vehExpDate);
                // (vehExpDate <= this.end && vehExpDate >= this.start
                if ((expDate.getTime() >= sd.getTime()) && (expDate.getTime() <= td.getTime())) {
                    // count ++
                    // vehName.push(this.allDevices[t].Device_Name);
                    // final.push(vehExpDate);
                    var expDevices = {
                        device_name: _this.allDevices[t].Device_Name,
                        expDate: vehExpDate
                    };
                    _this.finalData.push(expDevices);
                }
            }
            console.log("exp_date", final);
            //console.log("exp_date2",final);
            console.log("dname", vehName);
        }, function (err) {
            console.log("error=> ", err);
        });
    };
    // doInfinite(infiniteScroll) {
    //   var baseURLp;
    //   var vehName = [];
    //   var final = [];
    //   if (this.stausdevice) {
    //     baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&statuss=' + this.stausdevice ;
    //   }
    //   else {
    //     baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
    //   }
    //   if (this.islogin.isSuperAdmin == true) {
    //     baseURLp += '&supAdmin=' + this.islogin._id;
    //   } else {
    //     if (this.islogin.isDealer == true) {
    //       baseURLp += '&dealer=' + this.islogin._id;
    //     }
    //   }
    //   //this.allDevices = []
    //   this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
    //   .subscribe(data => {
    //   console.log("vechicle list for expired devices =>", data);
    //   this.allDevices = data.devices
    //   console.log("data", this.allDevices)
    //   final  = [], vehName = [];
    //   for(var t = 0; t < this.allDevices.length; t++) {
    //     debugger;
    //     let vehExpDate = moment(this.allDevices[t].expiration_date).format('LLLL');
    //     //console.log("expf", vehExpDate)
    //     var sd = new Date(this.start);
    //     var td = new Date(this.end);
    //     var expDate = new Date(vehExpDate);
    //     // (vehExpDate <= this.end && vehExpDate >= this.start
    //     if ((expDate.getTime()>=sd.getTime())&&(expDate.getTime()<=td.getTime())) {
    //       // count ++
    //       // vehName.push(this.allDevices[t].Device_Name);
    //       // final.push(vehExpDate);
    //       var expDevices ={
    //         device_name : this.allDevices[t].Device_Name,
    //         expDate :vehExpDate
    //       }
    //       this.finalData.push(expDevices);
    //     }
    //   }
    //   infiniteScroll.complete();
    //   })
    // }
    // let that = this;
    // that.page = that.page + 1;
    // setTimeout(() => {
    //   var baseURLp;
    //   if (that.stausdevice) {
    //     baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + that.islogin._id + '&email=' + that.islogin.email + '&statuss=' + that.stausdevice + '&skip=' + that.page + '&limit=' + that.limit;
    //   }
    //   else {
    //     baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + that.islogin._id + '&email=' + that.islogin.email + '&skip=' + that.page + '&limit=' + that.limit;
    //   }
    //   if (this.islogin.isSuperAdmin == true) {
    //     baseURLp += '&supAdmin=' + this.islogin._id;
    //   } else {
    //     if (this.islogin.isDealer == true) {
    //       baseURLp += '&dealer=' + this.islogin._id;
    //     }
    //   }
    //   that.ndata = [];
    //   that.apiCall.getdevicesForAllVehiclesApi(baseURLp)
    //     .subscribe(
    //       res => {
    //         that.ndata = res.devices;
    //         for (let i = 0; i < that.ndata.length; i++) {
    //           that.allDevices.push(that.ndata[i]);
    //         }
    //         that.allDevicesSearch = that.allDevices;
    //         infiniteScroll.complete();
    //       },
    //       error => {
    //         console.log(error);
    //       });
    // }, 100);
    AnnounceModelPage.prototype.dismiss = function () {
        console.log("Inside the function");
        this.viewCtrl.dismiss();
    };
    AnnounceModelPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            template: "\n\n<ion-list>\n  <div class = \"mainDiv\">\n          <!-- <ion-buttons end>\n            <button ion-button icon-only (click)=\"dismiss()\">\n                <ion-icon name=\"close-circle\"></ion-icon>\n            </button>\n          </ion-buttons> -->\n          <!-- <div style=\"height: 50%; width:50%; border: 1px soild;\">\nkj\n          </div> -->\n      <div class=\"secondDiv\" style=\"\n          background: #ffffff;\n    /* height: 26px; */\n    /* width: 1100%; */\n    /* width: max-content;\n    margin-left: -6%; */\n    \">\n          <h3 style=\"text-align: center\">Announcement</h3>\n      <!-- <span style=\"text-align: center\">Upcoming Expired Devices</span> -->\n      <p style=\"text-align: center;\n    font-family: fangsong;\n    font-size: 0.85 em;\n    font-size: 20px;\">{{ this.msgg }}</p>\n\n    <p (click)=\"dismiss()\" style=\"text-align: center; color: red\"> Close </p>\n      <!-- <ion-row *ngFor=\"let d of finalData\">\n        <ion-col style=\"font-size: 0.85em;\" >\n          {{d.device_name}}\n        </ion-col>\n        <ion-col style=\"font-size: 0.85em\">\n        {{d.expDate}}\n        </ion-col>\n       </ion-row> -->\n\n\n      </div>\n  </div>\n</ion-list>\n  ",
            styles: ["\n\n.mainDiv {\n    padding: 50px;\n    height: 100%;\n    margin-top: 50%\n    /* background-color: rgba(0, 0, 0, 0.23); */\n}\n\n.secondDiv {\n    padding-top: 20px;\n    /* border-radius: 18px;\n    border: 2px solid black; */\n    background: white;\n    /* height: 500px !important; */\n    overflow: scroll;\n    background: url(\"../assets/imgs/tendencias-en-innovacion-tecnologica-y-proptechs-inmobiliarias-533x337xsmart-cities-opt.jpg.pagespeed.ic_.zsvuwxwqfv-2018-03-02_12-58-50_266848.jpg\")\n}\n\n     /* .scroll-content {\n       left: 0;\n     right: 0;\n     top: 0;\n      bottom: 0;\n      position: absolute;\n      z-index: 1;\n      display: block;\n      overflow-x: hidden;\n       overflow-y: hidden;\n      -webkit-overflow-scrolling: touch;\n      will-change: scroll-position;\n       contain: size style layout;\n   } */\n\n"]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */]])
    ], AnnounceModelPage);
    return AnnounceModelPage;
}());

//# sourceMappingURL=announceModel.js.map

/***/ }),

/***/ 429:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return LoginPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LanguagesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





// import jwt_decode from 'jwt-decode';
var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, formBuilder, alertCtrl, apiservice, toastCtrl, popoverCtrl, translate, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.alertCtrl = alertCtrl;
        this.apiservice = apiservice;
        this.toastCtrl = toastCtrl;
        this.popoverCtrl = popoverCtrl;
        this.translate = translate;
        this.events = events;
        this.data = {};
        this.showPassword = false;
        this.callBaseURL();
        localStorage.removeItem("ENTERED_BASE_URL");
        this.loginForm = formBuilder.group({
            username: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            password: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
        });
    }
    LoginPage_1 = LoginPage;
    LoginPage.prototype.ionViewWillEnter = function () {
        console.log("ionViewWillEnter");
        // if (localStorage.getItem("LANG_KEY") == null) {
        //   this.presentPopover();
        // }
    };
    LoginPage.prototype.ionViewDidEnter = function () {
        console.log("ionViewDidEnter");
    };
    LoginPage.prototype.toggleShowPassword = function () {
        this.showPassword = !this.showPassword;
    };
    LoginPage.prototype.callBaseURL = function () {
        var _this = this;
        // debugger
        if (localStorage.getItem("ENTERED_BASE_URL") === null) {
            var url = "https://www.oneqlik.in/pullData/getUrlnew";
            this.apiservice.getSOSReportAPI(url)
                .subscribe(function (data) {
                console.log("base url: ", data);
                if (data.url) {
                    localStorage.setItem("BASE_URL", JSON.stringify(data.url));
                }
                if (data.socket) {
                    localStorage.setItem("SOCKET_URL", JSON.stringify(data.socket));
                }
                _this.checkVersion();
            });
        }
    };
    LoginPage.prototype.checkVersion = function () {
        var _this = this;
        var that = this;
        that.apiservice.getDealerInfo()
            .subscribe(function (response) {
            console.log("response demo data ----->", response);
            if (response.app_version || response.ios_version) {
                _this.events.publish('latest:version', response);
            }
            if (response.supAdmin) {
                localStorage.setItem("DealerInfo", response.supAdmin);
            }
        });
    };
    LoginPage.prototype.userlogin = function () {
        var _this = this;
        this.submitAttempt = true;
        if (this.loginForm.value.username == "") {
            /*alert("invalid");*/
            return false;
        }
        else if (this.loginForm.value.password == "") {
            /*alert("invalid");*/
            return false;
        }
        function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        }
        var isEmail = validateEmail(this.loginForm.value.username);
        var isName = isNaN(this.loginForm.value.username);
        var strNum;
        if (isName == false) {
            strNum = this.loginForm.value.username.trim();
        }
        if (isEmail == false && isName == false && strNum.length == 10) {
            this.data = {
                "psd": this.loginForm.value.password,
                "ph_num": this.loginForm.value.username
            };
        }
        else if (isEmail) {
            this.data = {
                "psd": this.loginForm.value.password,
                "emailid": this.loginForm.value.username
            };
        }
        else {
            this.data = {
                "psd": this.loginForm.value.password,
                "user_id": this.loginForm.value.username
            };
        }
        if (localStorage.getItem("DealerInfo") != null) {
            var superAdmin = localStorage.getItem("DealerInfo");
            this.data.supAdmin = superAdmin;
        }
        this.apiservice.startLoading();
        this.apiservice.loginApi(this.data)
            .subscribe(function (response) {
            // this.logindata = response;
            // this.logindata = JSON.stringify(response);
            var logindetails = JSON.parse(JSON.stringify(response));
            _this.userDetails = window.atob(logindetails.token.split('.')[1]);
            // this.userDetails = jwt_decode(logindetails.token.split('.')[1]);
            _this.details = JSON.parse(_this.userDetails);
            // console.log("7y", this.details.email);
            // console.log("7yy", this.details._id);
            localStorage.setItem("loginflag", "loginflag");
            localStorage.setItem('details', JSON.stringify(_this.details));
            localStorage.setItem('condition_chk', _this.details.isDealer);
            localStorage.setItem('VeryFirstLoginUser', _this.details._id);
            localStorage.setItem('pwd', _this.loginForm.value.password);
            localStorage.setItem('_id', _this.details._id);
            localStorage.setItem("INTRO", "INTRO");
            ///////////////////////////////
            _this.events.publish('user:updated', _this.details);
            /////////////////////////////////
            _this.apiservice.stopLoading();
            var toast = _this.toastCtrl.create({
                message: _this.translate.instant("Welcome! You're logged In successfully."),
                duration: 3000,
                position: 'bottom'
            });
            toast.onDidDismiss(function () {
                _this.navCtrl.setRoot('DashboardPage');
            });
            toast.present();
        }, function (error) {
            localStorage.removeItem("ENTERED_BASE_URL");
            var body = error._body;
            var msg = JSON.parse(body);
            if (msg.message == "Mobile Phone Not Verified") {
                var confirmPopup = _this.alertCtrl.create({
                    title: 'Login failed!',
                    message: msg.message = !msg.message ? '  Do you Want to verify it?' : msg.message + '  Do you Want to verify it?',
                    buttons: [
                        { text: 'Cancel' },
                        {
                            text: _this.translate.instant('Okay'),
                            handler: function () { }
                        }
                    ]
                });
                confirmPopup.present();
            }
            else {
                var alertPopup = _this.alertCtrl.create({
                    title: 'Login failed!',
                    message: msg.message,
                    buttons: [_this.translate.instant('Okay')]
                });
                alertPopup.present();
            }
            _this.apiservice.stopLoading();
        });
    };
    LoginPage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 2500
        });
        toast.present();
    };
    LoginPage.prototype.gotosignuppage = function () {
        this.navCtrl.push('SignupPage');
    };
    LoginPage.prototype.forgotPassFunc = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: this.translate.instant('Forgot Password'),
            message: this.translate.instant('Enter your Registered Mobile Number and we will send you a confirmation code.'),
            inputs: [
                {
                    name: 'mobno',
                    placeholder: this.translate.instant('Mobile Number')
                },
            ],
            buttons: [
                {
                    text: this.translate.instant('SEND CONFIRMATION CODE'),
                    handler: function (data) {
                        var forgotdata = {
                            "cred": data.mobno
                        };
                        _this.apiservice.startLoading();
                        _this.apiservice.forgotPassApi(forgotdata)
                            .subscribe(function (data) {
                            _this.apiservice.stopLoading();
                            _this.presentToast(data.message);
                            _this.otpMess = data;
                            _this.PassWordConfirmPopup();
                        }, function (error) {
                            _this.apiservice.stopLoading();
                            var body = error._body;
                            var msg = JSON.parse(body);
                            var alert = _this.alertCtrl.create({
                                title: _this.translate.instant('Forgot Password Failed!'),
                                message: msg.message,
                                buttons: [_this.translate.instant('Okay')]
                            });
                            alert.present();
                        });
                    }
                }
            ]
        });
        prompt.present();
    };
    LoginPage.prototype.PassWordConfirmPopup = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: this.translate.instant('Reset Password'),
            inputs: [
                {
                    name: 'mobno',
                    placeholder: this.translate.instant('Mobile Number')
                },
                {
                    name: 'newpass',
                    placeholder: this.translate.instant('Password')
                },
                {
                    name: 'otp',
                    placeholder: this.translate.instant('OTP')
                }
            ],
            buttons: [
                {
                    text: this.translate.instant('Back'),
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: this.translate.instant('SAVE'),
                    handler: function (data) {
                        var Passwordset = {
                            "newpwd": data.newpass,
                            "otp": data.otp,
                            "phone": data.mobno,
                            "cred": data.mobno
                        };
                        _this.apiservice.startLoading();
                        _this.apiservice.forgotPassMobApi(Passwordset)
                            .subscribe(function (data) {
                            _this.apiservice.stopLoading();
                            _this.presentToast(data.message);
                            _this.navCtrl.setRoot(LoginPage_1);
                        }, function (error) {
                            _this.apiservice.stopLoading();
                            var body = error._body;
                            var msg = JSON.parse(body);
                            var alert = _this.alertCtrl.create({
                                title: _this.translate.instant('Forgot Password Failed!'),
                                message: msg.message,
                                buttons: [_this.translate.instant('Okay')]
                            });
                            alert.present();
                        });
                    }
                }
            ]
        });
        prompt.present();
    };
    LoginPage = LoginPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-login',template:/*ion-inline-start:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/login/login.html"*/'<ion-content id=outer>\n\n  <div class="warning">\n\n    <!-- <div id=inner> -->\n\n    <div class="logo">\n\n      <img src="assets/imgs/icon.png">\n\n    </div>\n\n    <form [formGroup]="loginForm">\n\n      <!-- <div class="temp">\n\n        <ion-item style="background-color: transparent; padding-left: 0px;" class="logitem">\n\n          <ion-input formControlName="url" type="text" placeholder="{{\'Server Url\' | translate}}"\n\n            class="text-input">\n\n          </ion-input>\n\n        </ion-item>\n\n      </div> -->\n\n      <div class="temp">\n\n        <ion-item style="background-color: transparent; padding-left: 0px;" class="logitem">\n\n          <ion-input formControlName="username" type="text" placeholder="{{\'Email/Mobile/User Id*\' | translate}}"\n\n            class="text-input">\n\n          </ion-input>\n\n        </ion-item>\n\n      </div>\n\n      <ion-item class="logitem1"\n\n        *ngIf="!loginForm.controls.username.valid && (loginForm.controls.username.dirty || submitAttempt)">\n\n        <p style="padding-left: 8px;">{{\'username required!\' | translate}}</p>\n\n      </ion-item>\n\n      <div class="temp">\n\n        <ion-item style="background-color: transparent; padding-left: 0px;" class="logitem">\n\n          <ion-input formControlName="password" *ngIf="!showPassword" type="password"\n\n            placeholder="{{\'Password*\' | translate}}" class="text-input"></ion-input>\n\n          <ion-input formControlName="password" *ngIf="showPassword" type="text"\n\n            placeholder="{{\'Password*\' | translate}}" class="text-input"></ion-input>\n\n\n\n          <button ion-button clear item-end (click)="toggleShowPassword()">\n\n            <ion-icon style="font-size: 1.6em;" *ngIf="showPassword" name="eye" color="light"></ion-icon>\n\n            <ion-icon style="font-size: 1.6em;" *ngIf="!showPassword" name="eye-off" color="light"></ion-icon>\n\n          </button>\n\n        </ion-item>\n\n      </div>\n\n      <ion-item class="logitem1"\n\n        *ngIf="!loginForm.controls.password.valid && (loginForm.controls.password.dirty || submitAttempt)">\n\n        <p style="padding-left: 8px;">{{\'Password required!\' | translate}}</p>\n\n      </ion-item>\n\n    </form>\n\n    <div class="btnDiv">\n\n      <button ion-button class="btnLog" color="gpsc" (tap)="userlogin()">{{\'SIGN IN\' | translate}}</button>\n\n      <!-- <ion-row>\n\n        <ion-col width-50 ion-text color="light" style="font-size: 1.1em;" (tap)="forgotPassFunc()">\n\n          {{\'Forgot Password\' | translate}} ?</ion-col>\n\n        <ion-col width-50 ion-text color="light" style="font-size: 1.1em;">{{\'New user?\' | translate}}&nbsp;&nbsp;\n\n          <span style="color:#000000;" (tap)="gotosignuppage()">{{\'SIGN UP\' | translate}}</span>\n\n        </ion-col>\n\n      </ion-row> -->\n\n    </div>\n\n    <!-- </div> -->\n\n    <!-- <ion-row style="margin-top: 50%;">\n\n      <ion-col col-12 style="text-align: center;">\n\n        <p style="margin: 0px; color: #fff; font-weight: 500; font-size: 10px;">\n\n          Call Support Time : 11 AM to 8 PM\n\n        </p>\n\n      </ion-col>\n\n      <ion-col col-12>\n\n        <p style="margin: 0px; color: #fff; font-weight: 500;font-size: 10px;">\n\n          <ion-icon name="mail"></ion-icon>&nbsp;&nbsp;\n\n          support@oneqlik.in &nbsp;&nbsp; <ion-icon name="call"></ion-icon>&nbsp;&nbsp;\n\n          +91 96737 77264\n\n        </p>\n\n      </ion-col>\n\n    </ion-row> -->\n\n  </div>\n\n</ion-content>'/*ion-inline-end:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["PopoverController"],
            __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"]])
    ], LoginPage);
    return LoginPage;
    var LoginPage_1;
}());

var LanguagesPage = /** @class */ (function () {
    function LanguagesPage(viewCtrl, translate, apiCall, toastCtrl, events) {
        this.viewCtrl = viewCtrl;
        this.translate = translate;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.events = events;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    }
    LanguagesPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    LanguagesPage.prototype.setLanguage = function (key) {
        var _this = this;
        var payload = {
            uid: this.islogin._id,
            lang: key
        };
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(this.apiCall.mainUrl + "users/set_user_setting", payload)
            .subscribe(function (resp) {
            localStorage.setItem("LanguageKey", key);
            _this.apiCall.stopLoading();
            var toast = _this.toastCtrl.create({
                message: resp.message,
                duration: 1500,
                position: 'bottom'
            });
            toast.present();
        }, function (err) {
            localStorage.setItem("LanguageKey", key);
        });
        this.events.publish('lang:key', key);
        localStorage.setItem('LanguageKey', key);
        this.viewCtrl.dismiss();
    };
    LanguagesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            template: "\n    <ion-list>\n      <ion-list-header style=\"text-align: center;\">{{'Select Language' | translate}}</ion-list-header>\n      <button ion-item (click)=\"setLanguage('en')\">{{'English' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('hi')\">{{'Hindi' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('bn')\">{{'Bangali' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('te')\">{{'Telugu' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('ta')\">{{'Tamil' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('gu')\">{{'Gujarati' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('kn')\">{{'Kannada' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('mr')\">{{'Marathi' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('ml')\">{{'Malayalam' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('sp')\">{{'Spanish' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('fa')\">{{'Persian' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('ar')\">{{'Arabic' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('ne')\">{{'Nepali' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('fr')\">{{'French' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('pn')\">{{'Punjabi' | translate}}</button>\n    </ion-list>\n  "
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"]])
    ], LanguagesPage);
    return LanguagesPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 430:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LiveSingleDevice; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return PoiPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_lodash__ = __webpack_require__(448);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ngx_translate_core__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_geocoder_geocoder__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ion_bottom_drawer__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_rxjs_Subscription__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_rxjs_Subscription___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_rxjs_Subscription__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__live_map_style_model__ = __webpack_require__(563);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_tinyurl__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_tinyurl___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_12_tinyurl__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__neraby_vehicle_modal_neraby_vehicle_modal__ = __webpack_require__(377);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_insomnia__ = __webpack_require__(378);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__providers_socket_connection_socket_connection__ = __webpack_require__(57);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








// import { Storage } from '@ionic/storage';









var LiveSingleDevice = /** @class */ (function () {
    function LiveSingleDevice(navCtrl, navParams, apiCall, actionSheetCtrl, elementRef, platform, event, modalCtrl, socialSharing, alertCtrl, toastCtrl, 
    // public storage: Storage,
    plt, insomnia, scoketConn, 
    // private launchNavigator: LaunchNavigator,
    // private nativePageTransitions: NativePageTransitions,
    translate, geocoderApi) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.actionSheetCtrl = actionSheetCtrl;
        this.elementRef = elementRef;
        this.platform = platform;
        this.event = event;
        this.modalCtrl = modalCtrl;
        this.socialSharing = socialSharing;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.plt = plt;
        this.insomnia = insomnia;
        this.scoketConn = scoketConn;
        this.translate = translate;
        this.geocoderApi = geocoderApi;
        this.showDistDuration = false;
        this.streetviewButtonColor = '#000000';
        this.streetColor = '#fff';
        this.navigateButtonColor = '#000000'; //Default Color
        this.navColor = '#fff';
        this.policeButtonColor = '#000000';
        this.policeColor = '#fff';
        this.eyeBtnColor = "#000000";
        this.eyeColor = "#fff";
        this.petrolButtonColor = "#000000";
        this.petrolColor = '#fff';
        this.flag = false;
        this.shouldBounce = true;
        this.dockedHeight = 80;
        // distanceTop = 200;
        this.distanceTop = 56;
        this.drawerState = __WEBPACK_IMPORTED_MODULE_9_ion_bottom_drawer__["a" /* DrawerState */].Docked;
        // drawerState = DrawerState.Top;
        this.states = __WEBPACK_IMPORTED_MODULE_9_ion_bottom_drawer__["a" /* DrawerState */];
        this.minimumHeight = 50;
        this.transition = '0.85s ease-in-out';
        this.drawerState1 = __WEBPACK_IMPORTED_MODULE_9_ion_bottom_drawer__["a" /* DrawerState */].Docked;
        this.dockedHeight1 = 150;
        this.distanceTop1 = 378;
        this.minimumHeight1 = 0;
        this.ongoingGoToPoint = {};
        this.ongoingMoveMarker = {};
        this.data = {};
        this.socketSwitch = {};
        this.socketChnl = [];
        this.socketData = {};
        this.selectedFlag = { _id: '', flag: false };
        this.allData = {};
        this.isEnabled = false;
        this.showShareBtn = false;
        this.mapHideTraffic = false;
        this.locateme = false;
        this.deviceDeatils = {};
        this.tempArray = [];
        this.mapTrail = false;
        this.condition = 'gpsc';
        this.condition1 = 'light';
        this.condition2 = 'light';
        this.showaddpoibtn = false;
        this.btnString = "Create Trip";
        this.showIcon = false;
        this.expectation = {};
        this.polyLines = [];
        this.marksArray = [];
        this.zoomLevel = 18;
        this.markersArray = [];
        this.devices = [];
        this.latLngArray = [];
        this.fraction = 0;
        this.direction = 1;
        this.navigateBtn = false;
        this.singleVehData = {};
        this.nearbyPolicesArray = [];
        this.nearbyPetrolArray = [];
        this.takeAction = "live";
        this.socketurl = "https://soc.oneqlik.in";
        this.showStreetMap = false;
        this.total_days = [];
        this.selectedDate = [];
        this.showSelectedTabArray = [];
        this.sevenDaysData = [];
        this.showString = 'start';
        this.car = 'M17.402,0H5.643C2.526,0,0,3.467,0,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z';
        this.bike = "M419 1754 l3 -44 -37 0 c-21 0 -34 4 -30 10 3 6 -1 10 -9 10 -19 0 -66 -21 -66 -30 0 -18 44 -44 71 -42 16 1 29 0 29 -2 0 -12 -24 -56 -30 -56 -4 0 -7 -6 -7 -12 0 -7 -8 -29 -16 -48 l-15 -35 -7 30 c-4 17 -8 36 -11 43 -6 19 -40 14 -69 -11 l-27 -23 16 -134 c12 -99 23 -149 41 -186 28 -57 25 -84 -10 -84 -34 0 -51 -25 -31 -45 9 -9 16 -33 16 -53 0 -30 -9 -46 -44 -84 -24 -27 -42 -48 -40 -48 2 0 -10 -21 -26 -47 -17 -27 -30 -61 -30 -79 0 -35 35 -189 52 -232 6 -15 10 -41 9 -59 -3 -34 19 -53 58 -53 12 0 21 -6 21 -14 0 -12 -7 -13 -32 -5 -44 12 -70 11 -66 -2 2 -6 21 -12 41 -14 21 -1 43 -7 49 -13 17 -17 23 -119 8 -137 -10 -12 -9 -19 3 -38 l15 -23 -46 23 c-29 15 -63 23 -94 23 -56 0 -61 -14 -17 -48 24 -20 42 -24 97 -24 63 -1 69 -3 109 -39 22 -21 38 -41 35 -44 -4 -4 -2 -5 3 -4 6 1 30 -3 55 -10 54 -16 142 -9 176 14 13 8 24 13 24 10 0 -3 16 13 36 34 35 39 36 40 103 39 59 -1 73 2 99 23 45 35 41 49 -17 49 -32 0 -64 -8 -97 -25 -27 -14 -42 -20 -33 -13 10 8 14 23 11 38 -10 54 -7 139 6 152 7 7 30 13 52 13 27 0 40 4 40 13 0 11 -9 13 -36 8 -19 -4 -42 -10 -50 -13 -10 -3 -12 0 -8 10 3 8 18 17 33 20 23 5 30 13 35 41 4 18 13 47 20 62 8 16 26 77 41 136 l26 106 -45 90 c-24 49 -56 102 -70 119 -14 17 -23 33 -19 37 3 3 1 6 -5 6 -15 0 -16 57 -1 62 22 7 -3 38 -31 38 -19 0 -31 6 -34 18 -7 20 38 114 49 104 5 -4 5 -2 2 4 -4 7 0 67 9 135 8 68 13 134 10 145 -8 29 -60 51 -81 34 -8 -7 -15 -22 -15 -35 0 -43 -13 -33 -46 36 -19 39 -34 73 -34 76 0 3 13 3 30 1 22 -3 35 2 50 17 27 29 17 49 -26 53 -22 1 -33 -2 -29 -8 4 -6 -8 -10 -29 -10 -34 0 -35 1 -38 43 -3 42 -3 42 -43 43 l-40 1 4 -43z m-140 -187 c13 -16 5 -27 -21 -27 -14 0 -18 -5 -14 -17 8 -29 26 -196 24 -220 -5 -48 -19 -2 -32 106 -16 127 -15 133 3 155 16 20 25 20 40 3z m402 -3 c7 -9 14 -22 15 -30 4 -26 -25 -237 -34 -246 -12 -12 -12 12 0 127 6 55 16 103 22 107 6 4 -2 12 -22 19 -26 9 -30 14 -21 25 15 18 24 18 40 -2z m-123 -226 c48 -23 54 -30 46 -51 -5 -13 -10 -14 -39 -1 -50 21 -163 18 -207 -5 -35 -17 -37 -17 -41 0 -7 24 23 49 85 70 58 20 89 18 156 -13z m28 -469 c39 -36 56 -76 56 -134 2 -169 -205 -230 -284 -85 -26 47 -21 155 7 176 10 8 15 14 10 14 -13 0 27 41 50 51 11 5 46 7 77 6 45 -3 64 -9 84 -28z m131 -37 c7 -8 10 -19 7 -23 -3 -5 -1 -9 4 -9 17 0 21 -41 10 -103 -10 -59 -28 -100 -28 -64 0 14 -44 137 -75 209 -6 15 -2 16 32 11 21 -3 43 -12 50 -21z m-455 -49 c-15 -38 -30 -71 -35 -74 -5 -3 -8 -9 -8 -13 3 -21 0 -36 -8 -36 -5 0 -11 30 -13 68 -5 61 -3 69 20 95 14 15 36 27 48 27 l22 0 -26 -67z m377 -317 c19 14 25 14 32 3 13 -21 12 -23 -21 -42 -16 -10 -30 -23 -30 -28 0 -6 -4 -8 -8 -5 -4 2 -9 -9 -10 -25 -2 -22 2 -29 16 -29 14 0 21 -12 29 -44 10 -43 9 -45 -13 -43 -13 1 -29 10 -37 21 -13 17 -15 14 -37 -40 -13 -33 -30 -65 -38 -72 -9 -6 -12 -12 -7 -12 4 0 0 -9 -10 -20 -35 -39 -107 -11 -117 45 -1 11 -9 26 -15 33 -12 12 -17 33 -15 54 1 3 -5 11 -13 18 -11 9 -16 7 -25 -9 -12 -23 -50 -29 -50 -7 0 44 14 76 32 76 17 0 19 4 13 23 -4 12 -7 30 -8 39 -1 9 -4 15 -8 12 -4 -2 -19 6 -33 18 -19 16 -23 27 -17 37 8 12 13 12 34 -2 l25 -16 7 39 c4 22 8 40 9 40 1 0 18 -9 39 -19 l37 -19 -28 -8 c-15 -4 -30 -7 -33 -6 -3 0 -6 -11 -5 -26 l1 -27 125 0 c120 0 125 1 128 21 2 16 -5 25 -27 34 l-31 12 33 22 32 22 10 -43 c10 -43 10 -44 34 -27z m-161 11 c2 -10 -3 -17 -12 -17 -18 0 -29 16 -21 31 9 14 29 6 33 -14z m-58 -11 c0 -16 -18 -31 -27 -22 -8 8 5 36 17 36 5 0 10 -6 10 -14z m108 -10 c3 -12 -1 -17 -10 -14 -7 3 -15 13 -16 22 -3 12 1 17 10 14 7 -3 15 -13 16 -22z m-148 -5 c0 -14 -18 -23 -31 -15 -8 4 -7 9 2 15 18 11 29 11 29 0z m198 -3 c-3 -7 -11 -13 -18 -13 -7 0 -15 6 -17 13 -3 7 4 12 17 12 13 0 20 -5 18 -12z m-430 -224 c37 -10 25 -23 -14 -14 -19 5 -38 11 -41 14 -8 8 26 7 55 0z m672 -3 c-8 -5 -35 -11 -60 -15 l-45 -6 35 14 c39 16 96 21 70 7z m-454 -113 l29 -30 -30 15 c-17 8 -38 24 -48 35 -18 20 -18 20 1 15 11 -2 33 -18 48 -35z m239 15 c-11 -8 -33 -21 -50 -30 l-30 -15 30 31 c16 17 35 29 42 27 7 -3 13 0 13 6 0 6 3 8 7 4 5 -4 -1 -14 -12 -23z";
        this.truck = 'M180 1725 c0 -8 -21 -25 -47 -37 -27 -12 -58 -31 -70 -42 l-23 -20 0 -495 0 -496 23 -18 c12 -10 31 -24 42 -32 20 -13 20 -14 -2 -37 -21 -22 -23 -34 -23 -131 0 -65 -4 -107 -10 -107 -5 0 -10 -7 -10 -15 0 -8 5 -15 10 -15 6 0 10 -31 10 -74 0 -44 5 -78 11 -82 8 -4 7 -9 -2 -15 -38 -23 96 -89 181 -89 85 0 219 66 181 89 -9 6 -10 11 -2 15 6 4 11 38 11 82 0 43 4 74 10 74 6 0 10 7 10 15 0 8 -4 15 -10 15 -6 0 -10 43 -10 108 0 96 -2 108 -20 120 -30 18 -24 38 20 69 l40 28 0 491 0 491 -22 24 c-22 24 -86 59 -106 59 -5 0 -12 9 -15 20 -5 18 -14 20 -91 20 -70 0 -86 -3 -86 -15z m115 -101 c21 -8 35 -45 28 -72 -7 -29 -47 -44 -78 -30 -29 13 -38 36 -29 72 7 30 43 44 79 30z m-148 -726 c-1 -134 -2 -25 -2 242 0 267 1 376 2 243 2 -134 2 -352 0 -485z m250 0 c-1 -134 -2 -25 -2 242 0 267 1 376 2 243 2 -134 2 -352 0 -485z m-102 326 c21 -9 35 -46 28 -74 -12 -44 -93 -46 -108 -1 -17 54 27 95 80 75z m20 -436 c2 -2 6 -15 10 -30 10 -38 -26 -72 -68 -64 -37 8 -35 6 -43 43 -8 42 18 67 64 59 19 -3 36 -6 37 -8z m18 -245 c-36 -16 -102 -16 -130 0 -18 10 -7 12 68 12 80 0 87 -1 62 -12z m-203 -69 c0 -21 -4 -33 -10 -29 -5 3 -10 19 -10 36 0 16 5 29 10 29 6 0 10 -16 10 -36z m300 1 c0 -24 -5 -35 -15 -35 -15 0 -22 50 -8 63 15 15 23 5 23 -28z m-300 -87 c0 -24 -5 -50 -10 -58 -7 -11 -10 2 -10 43 0 31 4 57 10 57 6 0 10 -19 10 -42z m300 -20 c0 -43 -3 -58 -10 -48 -13 20 -23 87 -15 100 15 25 25 5 25 -52z m-201 -44 c31 -4 80 -2 108 4 59 10 70 5 75 -43 3 -29 2 -30 -57 -38 -58 -8 -199 -2 -229 9 -12 5 -12 11 2 45 11 28 20 38 30 34 9 -3 40 -8 71 -11z m147 -222 c-14 -15 -15 -13 -3 17 8 23 13 28 15 16 2 -10 -3 -25 -12 -33z m-213 18 c3 -11 1 -20 -4 -20 -5 0 -9 9 -9 20 0 11 2 20 4 20 2 0 6 -9 9 -20z m267 -25 c0 -2 -10 -10 -22 -16 -21 -11 -22 -11 -9 4 13 16 31 23 31 12z';
        this.carIcon = {
            path: this.car,
            labelOrigin: [25, 50],
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            fillColor: 'blue',
            anchor: [10, 20],
        };
        this.busIcon = {
            path: this.car,
            labelOrigin: [25, 50],
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            fillColor: 'blue',
            anchor: [12.5, 12.5],
        };
        this.bikeIcon = {
            path: this.bike,
            labelOrigin: [25, 50],
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            anchor: [12.5, 12.5],
        };
        this.truckIcon = {
            path: this.truck,
            labelOrigin: [25, 50],
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            anchor: [10, 20],
        };
        this.userIcon = {
            path: this.truck,
            labelOrigin: [25, 50],
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            anchor: [12.5, 12.5],
        };
        this.ambulance = {
            path: this.car,
            labelOrigin: [25, 50],
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            fillColor: 'blue',
            anchor: [10, 20],
        };
        this.icons = {
            "car": this.carIcon,
            "bike": this.bikeIcon,
            "truck": this.truckIcon,
            "bus": this.busIcon,
            "user": this.userIcon,
            "jcb": this.carIcon,
            "tractor": this.carIcon,
            "ambulance": this.carIcon,
            "auto": this.carIcon
        };
        this.hideMe = false;
        this.resumeListener = new __WEBPACK_IMPORTED_MODULE_10_rxjs_Subscription__["Subscription"]();
        this.measurementUnit = 'MKS';
        this.showOtherBtns = false;
        this.afterClick = false;
        this.afterMapTypeClick = false;
        // Environment.setBackgroundColor("black");
        console.log("single check customer data: ", this.apiCall._customerData);
        if (this.apiCall._customerData != undefined) {
            this.zoomLevel = Number(this.apiCall._customerData.cust.user_settings.zoomLevel);
        }
        this.callBaseURL();
        this.calculate7Days();
        this.insomnia.keepAwake()
            .then(function () { return console.log('success'); }, function () { return console.log('error'); });
        var selectedMapKey;
        if (localStorage.getItem('MAP_KEY') != null) {
            selectedMapKey = localStorage.getItem('MAP_KEY');
            if (selectedMapKey == this.translate.instant('Hybrid')) {
                this.mapKey = 'MAP_TYPE_HYBRID';
            }
            else if (selectedMapKey == this.translate.instant('Normal')) {
                this.mapKey = 'MAP_TYPE_NORMAL';
            }
            else if (selectedMapKey == this.translate.instant('Terrain')) {
                this.mapKey = 'MAP_TYPE_TERRAIN';
            }
            else if (selectedMapKey == this.translate.instant('Satellite')) {
                this.mapKey = 'MAP_TYPE_HYBRID';
            }
        }
        else {
            this.mapKey = 'MAP_TYPE_NORMAL';
        }
        this.event.subscribe("tripstatUpdated", function (data) {
            if (data) {
                _this.checktripstat();
            }
        });
        this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
        console.log("user details=> " + JSON.stringify(this.userdetails));
        if (navParams.get("device") != null) {
            this.deviceDeatils = navParams.get("device");
        }
        this.motionActivity = 'Activity';
        this.menuActive = false;
        // console.log("check @Input() activity: ", this.deviceData);
    }
    LiveSingleDevice.prototype.callBaseURL = function () {
        var _this = this;
        // debugger
        if (localStorage.getItem("ENTERED_BASE_URL") === null) {
            var url = "https://www.oneqlik.in/pullData/getUrlnew";
            this.apiCall.getSOSReportAPI(url)
                .subscribe(function (data) {
                console.log("base url: ", data);
                if (data.url) {
                    localStorage.setItem("BASE_URL", JSON.stringify(data.url));
                }
                if (data.socket) {
                    localStorage.setItem("SOCKET_URL", JSON.stringify(data.socket));
                }
                _this.getSocketUrl();
            }, function (err) {
                _this.getSocketUrl();
            });
        }
    };
    LiveSingleDevice.prototype.calculate7Days = function () {
        var d0 = new Date().toISOString();
        this.total_days.push(d0);
        this.showSelectedTabArray.push({
            "day": d0,
            "showSelectedTab": true
        });
        console.log("date before 1 days: ", d0);
        var d1 = new Date(Date.now() - 1 * 24 * 60 * 60 * 1000).toISOString();
        this.total_days.push(d1);
        this.showSelectedTabArray.push({
            "day": d1,
            "showSelectedTab": false
        });
        console.log("date before 2 days: ", d1);
        var d2 = new Date(Date.now() - 2 * 24 * 60 * 60 * 1000).toISOString();
        this.total_days.push(d2);
        this.showSelectedTabArray.push({
            "day": d2,
            "showSelectedTab": false
        });
        console.log("date before 3 days: ", d2);
        var d3 = new Date(Date.now() - 3 * 24 * 60 * 60 * 1000).toISOString();
        this.total_days.push(d3);
        this.showSelectedTabArray.push({
            "day": d3,
            "showSelectedTab": false
        });
        console.log("date before 4 days: ", d3);
        var d4 = new Date(Date.now() - 4 * 24 * 60 * 60 * 1000).toISOString();
        this.total_days.push(d4);
        this.showSelectedTabArray.push({
            "day": d4,
            "showSelectedTab": false
        });
        console.log("date before 5 days: ", d4);
        var d5 = new Date(Date.now() - 5 * 24 * 60 * 60 * 1000).toISOString();
        this.total_days.push(d5);
        this.showSelectedTabArray.push({
            "day": d5,
            "showSelectedTab": false
        });
        console.log("date before 6 days: ", d5);
        var d6 = new Date(Date.now() - 6 * 24 * 60 * 60 * 1000).toISOString();
        this.total_days.push(d6);
        this.showSelectedTabArray.push({
            "day": d6,
            "showSelectedTab": false
        });
        console.log("date before 7 days: ", d6);
        console.log(this.total_days);
    };
    LiveSingleDevice.prototype.changeData = function (data, index) {
        console.log("chnages data: ", data);
        if (this.sevenDaysData.length > 0) {
        }
        else {
            this.showString = 'start';
        }
        for (var i = 0; i < this.showSelectedTabArray.length; i++) {
            if (this.showSelectedTabArray[i].showSelectedTab === true) {
                this.showSelectedTabArray[i].showSelectedTab = false;
            }
        }
        this.showSelectedTabArray[index].showSelectedTab = true;
        this.selectedDate = data;
        console.log("7days data", this.selectedDate);
        this.get7daysData();
    };
    LiveSingleDevice.prototype.get7daysData = function () {
        var _this = this;
        var _bUrl = this.apiCall.mainUrl + 'user_trip/StoppageLogs?date=' + this.selectedDate + '&device=' + this.deviceDeatils._id;
        this.apiCall.getSOSReportAPI(_bUrl)
            .subscribe(function (resp) {
            console.log("got response:- " + resp.result);
            _this.to_running = _this.parseMillisecondsIntoReadableTime(resp.today_running);
            _this.to_stopped = _this.parseMillisecondsIntoReadableTime(resp.today_stopped);
            _this.to_odo = _this.meterIntoKelometer(resp.todays_odo);
            //this.date = moment(result[i]).format('ddd MMM DD YYYY'),
            console.log("odo cheack", _this.to_odo);
            if (resp.result.length > 0) {
                _this.showString = "";
                _this.sevenDaysData = resp.result;
            }
            else {
                _this.sevenDaysData = [];
                _this.showString = 'No Data Found!';
            }
        }, function (err) {
            _this.showString = 'No Data Found!';
            console.log(" got error: ", err);
        });
    };
    LiveSingleDevice.prototype.meterIntoKelometer = function (value) {
        var km = value / 1000;
        return km.toFixed(1);
        // alert(km.toFixed(1) + " km"); // 1613.8 km
    };
    LiveSingleDevice.prototype.ngOnInit = function () {
        var that = this;
        that.tempArray = [];
        this.checktripstat();
    };
    // _liveGPS: any;
    LiveSingleDevice.prototype.getSocketUrl = function () {
        if (localStorage.getItem('SOCKET_URL') !== null) {
            this.socketurl = JSON.parse(localStorage.getItem('SOCKET_URL'));
        }
        // this._liveGPS = this.scoketConn.getSocket_gps();  
        // this._io = io.connect(this.socketurl + "/gps", { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
        // this._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });
    };
    LiveSingleDevice.prototype.ionViewDidEnter = function () {
        var _this = this;
        localStorage.removeItem("SocketFirstTime");
        this.getDefaultUserSettings();
        this.selectedDate = new Date().toISOString();
        ;
        this.get7daysData();
        this.navBar.backButtonClick = function (ev) {
            console.log('this will work in Ionic 3 +');
            if (_this.allData.map) {
                _this.allData.map.remove();
            }
            _this.hideMe = true;
            _this.navCtrl.pop({
                animate: true, animation: 'transition-ios', direction: 'back'
            });
        };
        // this._io = io.connect(this.socketurl + "/gps", { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
        // this._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });
        this.showShareBtn = true;
        var paramData = this.navParams.get("device");
        this.singleVehData = this.navParams.get("device");
        this.titleText = paramData.Device_Name;
        this.temp(paramData);
        this.showActionSheet = true;
    };
    LiveSingleDevice.prototype.initMap = function () {
        if (this.allData.map != undefined) {
            this.allData.map.remove();
        }
        this.allData = {};
        var mapOptions = {
            gestures: {
                rotate: false,
                tilt: false,
                compass: false
            },
            mapType: this.mapKey
        };
        this.allData.map = __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["b" /* GoogleMaps */].create('map_canvas_single_device', mapOptions);
    };
    LiveSingleDevice.prototype.getDefaultUserSettings = function () {
        var _this = this;
        var b_url = this.apiCall.mainUrl + "users/get_user_setting";
        var Var = { uid: this.userdetails._id };
        this.apiCall.urlpasseswithdata(b_url, Var)
            .subscribe(function (resp) {
            console.log("check lang key: ", resp);
            if (resp.unit_measurement !== undefined) {
                _this.measurementUnit = resp.unit_measurement;
            }
            else {
                if (localStorage.getItem('MeasurementType') !== null) {
                    var measureType = localStorage.getItem('MeasurementType');
                    _this.measurementUnit = measureType;
                }
                else {
                    _this.measurementUnit = 'MKS';
                }
            }
        }, function (err) {
            console.log(err);
            if (localStorage.getItem('MeasurementType') !== null) {
                var measureType = localStorage.getItem('MeasurementType');
                _this.measurementUnit = measureType;
            }
            else {
                _this.measurementUnit = 'MKS';
            }
        });
    };
    LiveSingleDevice.prototype.ionViewWillEnter = function () {
        var _this = this;
        // if (this.plt.is('ios')) {
        //   this.shwBckBtn = true;
        //   this.viewCtrl.showBackButton(false);
        // }
        this.initMap();
        console.log("check _gps: ", this.scoketConn._gps);
        if (this.scoketConn._gps == undefined || this.scoketConn._gps.disconnected == true) {
            this.scoketConn.gpsSocketConnect();
        }
        this.platform.ready().then(function () {
            _this.resumeListener = _this.platform.resume.subscribe(function () {
                var today, Christmas;
                today = new Date();
                Christmas = new Date(JSON.parse(localStorage.getItem("backgroundModeTime")));
                var diffMs = (today - Christmas); // milliseconds between now & Christmas
                var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
                if (diffMins >= 5) {
                    localStorage.removeItem("backgroundModeTime");
                    _this.alertCtrl.create({
                        message: _this.translate.instant('Its been 5 mins or more since the app was in background mode. Do you want to reload the screen?'),
                        buttons: [
                            {
                                text: _this.translate.instant('YES PROCEED'),
                                handler: function () {
                                    _this.refreshMe();
                                }
                            },
                            {
                                text: _this.translate.instant('Back'),
                                handler: function () {
                                    _this.navCtrl.setRoot('DashboardPage');
                                }
                            }
                        ]
                    }).present();
                }
            });
        });
    };
    LiveSingleDevice.prototype.ionViewWillLeave = function () {
        var _this = this;
        // this.allData.map.remove();
        this.platform.ready().then(function () {
            _this.resumeListener.unsubscribe();
        });
    };
    LiveSingleDevice.prototype.ionViewDidLeave = function () {
        var that = this;
        that.takeAction = "live";
        if (that.circleTimer) {
            clearInterval(that.circleTimer);
        }
        if (that.intevalId) {
            clearInterval(that.intevalId);
        }
        // for (var i = 0; i < that.socketChnl.length; i++)
        //   that._liveGPS.removeAllListeners(that.socketChnl[i]);
        // that._io.on('disconnect', () => {
        //   that._liveGPS.close();
        // });
        // that._liveGPS.close();
        that.scoketConn.gpsRemoveAllListners(that.socketChnl);
        if (localStorage.getItem("livepagetravelDetailsObject") != null) {
            localStorage.removeItem("livepagetravelDetailsObject");
        }
        localStorage.removeItem("gotPing");
        that.allData = {};
    };
    LiveSingleDevice.prototype.ngOnDestroy = function () { };
    LiveSingleDevice.prototype.setDocHeight = function () {
        var that = this;
        that.distanceTop = 200;
        that.drawerState = __WEBPACK_IMPORTED_MODULE_9_ion_bottom_drawer__["a" /* DrawerState */].Top;
    };
    LiveSingleDevice.prototype.setDocHeightAtFirst = function () {
        var that = this;
        that.distanceTop = 80;
        that.drawerState = __WEBPACK_IMPORTED_MODULE_9_ion_bottom_drawer__["a" /* DrawerState */].Top;
    };
    LiveSingleDevice.prototype.segmentChanged = function (ev) {
        console.log(ev);
        this.takeAction = ev._value;
        if (this.takeAction === 'history') {
            this.navCtrl.push('HistoryDevicePage', {
                device: this.singleVehData
            });
        }
        else if (this.takeAction === 'trip') {
            this.navCtrl.push('TripReportPage', {
                param: this.singleVehData
            });
        }
        else if (this.takeAction === 'notif') {
            this.navCtrl.push('AllNotificationsPage', {
                param: this.singleVehData
            });
        }
        else if (this.takeAction === 'car_service') {
            this.navCtrl.push('MaintenanceReminderPage', {
                param: this.singleVehData
            });
        }
    };
    LiveSingleDevice.prototype.streetView = function () {
        if (this.plt.is('ios')) {
            this.showOtherBtns = !this.showOtherBtns;
        }
        if (this.streetviewButtonColor == '#000000') {
            this.streetviewButtonColor = '#fff';
            this.streetColor = '#000000';
        }
        else if (this.streetviewButtonColor == '#fff') {
            this.streetviewButtonColor = '#000000';
            this.streetColor = '#fff';
        }
        var that = this;
        this.showStreetMap = !this.showStreetMap;
        if (this.showStreetMap) {
            document.getElementById("map_canvas_single_device").style.height = "50%";
            // debugger
            var fenway = {};
            if (this.singleVehData.last_location !== undefined) {
                fenway = { lat: this.singleVehData.last_location.lat, lng: this.singleVehData.last_location.long };
            }
            else if (this.singleVehData.last_lat !== undefined && this.singleVehData.last_lng !== undefined) {
                fenway = { lat: this.singleVehData.last_lat, lng: this.singleVehData.last_lng };
            }
            console.log("fenway: ", fenway);
            var panorama = new google.maps.StreetViewPanorama(document.getElementById("viewParanoma"), {
                position: fenway,
                pov: {
                    heading: 34,
                    pitch: 10
                }
            });
            that.allData.map.setStreetView(panorama);
        }
        else {
            // document.getElementById('map_canvas_single_device').setAttribute("style", "height:99%");
            document.getElementById("map_canvas_single_device").style.height = "99%";
        }
    };
    LiveSingleDevice.prototype.showNearby = function () {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_13__neraby_vehicle_modal_neraby_vehicle_modal__["a" /* NearbyVehicleModal */]);
        modal.onDidDismiss(function (ddata) {
            console.log("modal dissmissed!!");
            console.log("key is ", ddata);
            _this.openMap(ddata);
        });
        modal.present();
    };
    LiveSingleDevice.prototype.openMap = function (data) {
        var coordinates = {
            lat: this.recenterMeLat,
            long: this.recenterMeLng
        };
        this.getOnlyArea(coordinates, data);
    };
    LiveSingleDevice.prototype.addNearbyMarkers = function (key, url, icurl) {
        var _this = this;
        console.log("google maps activity: ", url);
        this.apiCall.getSOSReportAPI(url).subscribe(function (resp) {
            console.log(resp.status);
            if (resp.status === 'OK') {
                console.log(resp.results);
                console.log(resp);
                var mapData = resp.results.map(function (d) {
                    return { lat: d.geometry.location.lat, lng: d.geometry.location.lng };
                });
                var bounds = new __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["g" /* LatLngBounds */](mapData);
                var that_1 = _this;
                for (var t = 0; t < resp.results.length; t++) {
                    _this.allData.map.addMarker({
                        title: resp.results[t].name,
                        position: {
                            lat: resp.results[t].geometry.location.lat,
                            lng: resp.results[t].geometry.location.lng
                        },
                        icon: {
                            url: icurl,
                            size: {
                                height: 35,
                                width: 35
                            }
                        }
                    }).then(function (mark) {
                        if (key === 'police') {
                            that_1.nearbyPolicesArray.push(mark);
                        }
                        else if (key === 'petrol') {
                            that_1.nearbyPetrolArray.push(mark);
                        }
                    });
                }
                _this.allData.map.moveCamera({
                    target: bounds
                });
                _this.allData.map.setPadding(20, 20, 20, 20);
                _this.zoomLevel = 15;
            }
            else if (resp.status === 'ZERO_RESULTS') {
                if (key === 'police') {
                    _this.showToastMsg('No nearby police stations found.');
                }
                else if (key === 'petrol') {
                    _this.showToastMsg('No nearby petrol pump found.');
                }
            }
            else if (resp.status === 'OVER_QUERY_LIMIT') {
                _this.showToastMsg('Query limit exeed. Please try after some time.');
            }
            else if (resp.status === 'REQUEST_DENIED') {
                _this.showToastMsg('Please check your API key.');
            }
            else if (resp.status === 'INVALID_REQUEST') {
                _this.showToastMsg('Invalid request. Please try again.');
            }
            else if (resp.status === 'UNKNOWN_ERROR') {
                _this.showToastMsg('An unknown error occured. Please try again.');
            }
        });
    };
    LiveSingleDevice.prototype.showToastMsg = function (msg) {
        this.toastCtrl.create({
            message: msg,
            duration: 2000,
            position: 'bottom'
        }).present();
    };
    LiveSingleDevice.prototype.goBack = function () {
        // debugger
        if (this.navCtrl.canGoBack()) {
            this.navCtrl.pop({
                animate: true, animation: 'transition-ios', direction: 'forward'
            });
        }
    };
    LiveSingleDevice.prototype.refreshMe = function () {
        this.ngOnDestroy();
        this.ngOnInit();
        this.ionViewDidEnter();
    };
    LiveSingleDevice.prototype.refreshMe1 = function () {
        var that = this;
        if (that.circleTimer) {
            clearInterval(that.circleTimer);
        }
        if (that.intevalId) {
            clearInterval(that.intevalId);
        }
        that.scoketConn._gps.disconnect();
        that.scoketConn.gpsSocketConnect();
        var paramData = this.navParams.get("device");
        this.socketInit(paramData);
    };
    LiveSingleDevice.prototype.reCenterMe = function () {
        this.allData.map.moveCamera({
            target: { lat: this.recenterMeLat, lng: this.recenterMeLng },
            // zoom: 15,
            tilt: 30,
            // bearing: this.allData.map.getCameraBearing(),
            duration: 1800,
        }).then(function () {
        });
    };
    LiveSingleDevice.prototype.isNight = function () {
        //Returns true if the time is between
        //7pm to 5am
        var time = new Date().getHours();
        return (time > 5 && time < 19) ? false : true;
    };
    LiveSingleDevice.prototype.checktripstat = function () {
        var _this = this;
        var url = this.apiCall.mainUrl + "user_trip/getLastTrip?device=" + this.deviceDeatils._id + "&status=Started&tripInfo=last_trip";
        this.apiCall.getSOSReportAPI(url)
            .subscribe(function (data) {
            // debugger
            if (!data.message) {
                _this.tripStatus = data[0].trip_status;
                if (_this.tripStatus == 'Started') {
                    var sources = new __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["f" /* LatLng */](parseFloat(data[0].start_lat), parseFloat(data[0].start_long));
                    var dest = new __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["f" /* LatLng */](parseFloat(data[0].end_lat), parseFloat(data[0].end_long));
                    _this.calcRoute(sources, dest);
                }
            }
        });
    };
    LiveSingleDevice.prototype.navigateFromCurrentLoc = function () {
        if (this.plt.is('ios')) {
            window.open("https://www.google.com/maps/dir/?api=1&destination=" + this.recenterMeLat + "," + this.recenterMeLng + "&travelmode=driving", "_self");
        }
        else {
            if (this.plt.is('android')) {
                window.open("https://www.google.com/maps/dir/?api=1&destination=" + this.recenterMeLat + "," + this.recenterMeLng + "&travelmode=driving", "_blank");
            }
        }
    };
    LiveSingleDevice.prototype.calcRoute = function (start, end) {
        this.allData.AIR_PORTS = [];
        var directionsService = new google.maps.DirectionsService();
        var that = this;
        var request = {
            origin: start,
            destination: end,
            optimizeWaypoints: true,
            travelMode: google.maps.TravelMode.DRIVING
        };
        directionsService.route(request, function (response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                var path = new google.maps.MVCArray();
                for (var i = 0, len = response.routes[0].overview_path.length; i < len; i++) {
                    path.push(response.routes[0].overview_path[i]);
                    that.allData.AIR_PORTS.push({
                        lat: path.g[i].lat(), lng: path.g[i].lng()
                    });
                    // debugger
                    if (that.allData.AIR_PORTS.length > 1) {
                        if (!that.navigateBtn) {
                            that.allData.map.addMarker({
                                title: 'My Location',
                                position: start,
                                icon: 'green'
                            }).then(function (mark) {
                                that.marksArray.push(mark);
                            });
                            that.allData.map.addMarker({
                                title: 'Destination',
                                position: end,
                                icon: 'red'
                            }).then(function (mark) {
                                that.marksArray.push(mark);
                            });
                        }
                        // that.allData.map.setPadding(0, 30, 0, 55)
                        that.allData.map.addPolyline({
                            'points': that.allData.AIR_PORTS,
                            'color': '#3b3a3a',
                            'width': 4,
                            'geodesic': true,
                        }).then(function (poly) {
                            that.polyLines.push(poly);
                            that.getTravelDetails(start, end);
                        });
                    }
                }
            }
        });
    };
    LiveSingleDevice.prototype.getTravelDetails = function (source, dest) {
        var _this = this;
        this.service = new google.maps.DistanceMatrixService();
        var that = this;
        this._id = setInterval(function () {
            if (localStorage.getItem("livepagetravelDetailsObject") != null) {
                if (that.expectation.distance == undefined && that.expectation.duration == undefined) {
                    that.expectation = JSON.parse(localStorage.getItem("livepagetravelDetailsObject"));
                    console.log("expectation: ", that.expectation);
                    that.showDistDuration = true;
                }
                else {
                    clearInterval(_this._id);
                }
            }
        }, 3000);
        that.service.getDistanceMatrix({
            origins: [source],
            destinations: [dest],
            travelMode: 'DRIVING'
        }, that.callback);
        // that.apiCall.stopLoading();
    };
    LiveSingleDevice.prototype.callback = function (response, status) {
        var travelDetailsObject;
        if (status == 'OK') {
            var origins = response.originAddresses;
            for (var i = 0; i < origins.length; i++) {
                var results = response.rows[i].elements;
                for (var j = 0; j < results.length; j++) {
                    var element = results[j];
                    var distance = element.distance.text;
                    var duration = element.duration.text;
                    travelDetailsObject = {
                        distance: distance,
                        duration: duration
                    };
                }
            }
            localStorage.setItem("livepagetravelDetailsObject", JSON.stringify(travelDetailsObject));
        }
    };
    LiveSingleDevice.prototype.diff = function (start, end) {
        start = start.split(":");
        end = end.split(":");
        var startDate = new Date(0, 0, 0, start[0], start[1], 0);
        var endDate = new Date(0, 0, 0, end[0], end[1], 0);
        var diff = endDate.getTime() - startDate.getTime();
        var hours = Math.floor(diff / 1000 / 60 / 60);
        diff -= hours * 1000 * 60 * 60;
        var minutes = Math.floor(diff / 1000 / 60);
        // If using time pickers with 24 hours format, add the below line get exact hours
        if (hours < 0)
            hours = hours + 24;
        return (hours <= 9 ? "0" : "") + hours + " hours " + (minutes <= 9 ? "0" : "") + minutes + " mins";
    };
    LiveSingleDevice.prototype.callendtripfunc = function (res) {
        var _this = this;
        var endtime = __WEBPACK_IMPORTED_MODULE_4_moment__(new Date(), 'DD/MM/YYY').format('hh:mm');
        var starttime = __WEBPACK_IMPORTED_MODULE_4_moment__(new Date(res.doc.start_time), 'DD/MM/YYYY').format('hh:mm');
        var duration = this.diff(starttime, endtime);
        var dest = new __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["f" /* LatLng */](res.doc.start_lat, res.doc.start_long);
        var dest2 = new __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["f" /* LatLng */](res.doc.end_lat, res.doc.end_long);
        var distance = __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["h" /* Spherical */].computeDistanceBetween(dest, dest2); // in meters
        var bUrl = this.apiCall.mainUrl + "user_trip/updatePlantrip";
        var payload = {
            "user": res.doc.user,
            "device": res.doc.device,
            "start_loc": {
                "lat": res.doc.start_lat,
                "long": res.doc.start_long
            },
            "end_loc": {
                "lat": res.doc.end_lat,
                "long": res.doc.end_long,
            },
            "duration": duration,
            "distance": distance,
            "trip_status": "completed",
            "trip_id": res.doc._id,
            "end_time": new Date().toISOString()
        };
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(bUrl, payload)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            console.log("tri end data: ", data);
            // alert("trip ended succesfully");
            var toast = _this.toastCtrl.create({
                message: 'Trip has ended succesfully.',
                duration: 1500,
                position: 'bottom'
            });
            toast.present();
            var that = _this;
            // debugger
            that.tripStatus = 'completed';
            if (that.polyLines.length > 0) {
                for (var t = 0; t < that.polyLines.length; t++) {
                    that.polyLines[t].remove();
                }
            }
            if (that.marksArray.length > 0) {
                for (var e = 0; e < that.marksArray.length; e++) {
                    that.marksArray[e].remove();
                }
            }
            if (localStorage.getItem("livepagetravelDetailsObject") != null) {
                localStorage.removeItem("livepagetravelDetailsObject");
            }
            _this.checktripstat();
        });
    };
    LiveSingleDevice.prototype.endTrip = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: this.translate.instant('Confirm'),
            message: this.translate.instant('Do you want to end the trip?'),
            buttons: [
                {
                    text: this.translate.instant('YES PROCEED'),
                    handler: function () {
                        // this.storage.get("TRIPDATA").then((res) => {
                        //   if (res) {
                        //     this.callendtripfunc(res);
                        //   }
                        // })
                        var res = JSON.parse(localStorage.getItem("TRIPDATA"));
                        _this.callendtripfunc(res);
                    }
                },
                {
                    text: this.translate.instant('Back')
                }
            ]
        });
        alert.present();
    };
    LiveSingleDevice.prototype.onClickMap = function (maptype) {
        var that = this;
        if (maptype == 'SATELLITE' || maptype == 'HYBRID') {
            // this.ngOnDestroy();
            that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].HYBRID);
        }
        else {
            if (maptype == 'TERRAIN') {
                // this.ngOnDestroy();
                that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].TERRAIN);
            }
            else {
                if (maptype == 'NORMAL') {
                    // this.ngOnDestroy();
                    that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].NORMAL);
                }
            }
        }
    };
    LiveSingleDevice.prototype.createTrip = function () {
        console.log("create trip data: ", this.deviceDeatils);
        this.navCtrl.push('CreateTripPage', {
            'paramData': this.deviceDeatils,
        });
    };
    LiveSingleDevice.prototype.trafficFunc = function () {
        var that = this;
        that.isEnabled = !that.isEnabled;
        if (that.isEnabled == true) {
            that.allData.map.setTrafficEnabled(true);
        }
        else {
            that.allData.map.setTrafficEnabled(false);
        }
    };
    LiveSingleDevice.prototype.shareLive = function () {
        var that = this;
        that.showActionSheet = false;
        // that.drawerHidden = false;
        that.drawerState1 = __WEBPACK_IMPORTED_MODULE_9_ion_bottom_drawer__["a" /* DrawerState */].Docked;
        that.showFooter = true;
    };
    LiveSingleDevice.prototype.sharedevices = function (param) {
        var that = this;
        if (param == '15mins') {
            that.condition = 'gpsc';
            that.condition1 = 'light';
            that.condition2 = 'light';
            that.tttime = 15;
            // that.tttime  = (15 * 60000); //for miliseconds
        }
        else {
            if (param == '1hour') {
                that.condition1 = 'gpsc';
                that.condition = 'light';
                that.condition2 = 'light';
                that.tttime = 60;
                // that.tttime  = (1 * 3600000); //for miliseconds
            }
            else {
                if (param == '8hours') {
                    that.condition2 = 'gpsc';
                    that.condition = 'light';
                    that.condition1 = 'light';
                    that.tttime = (8 * 60);
                    // that.tttime  = (8 * 3600000);
                }
            }
        }
    };
    LiveSingleDevice.prototype.shareLivetemp = function () {
        var _this = this;
        var that = this;
        if (that.tttime == undefined) {
            that.tttime = 15;
        }
        var data = {
            id: that.liveDataShare._id,
            imei: that.liveDataShare.Device_ID,
            sh: this.userdetails._id,
            ttl: that.tttime // set to 1 hour by default
        };
        this.apiCall.startLoading().present();
        this.apiCall.shareLivetrackCall(data)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.resToken = data.t;
            _this.liveShare();
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    LiveSingleDevice.prototype.liveShare = function () {
        var that = this;
        var link = 'https://www.oneqlik.in/' + "share/liveShare?t=" + that.resToken;
        __WEBPACK_IMPORTED_MODULE_12_tinyurl__["shorten"](link).then(function (res) {
            // alert("tinyurl: " + res);
            that.socialSharing.share(that.userdetails.fn + " " + that.userdetails.ln + " has shared " + that.liveDataShare.Device_Name + " live trip with you. Please follow below link to track", "OneQlik- Live Trip", "", res);
        });
        // that.socialSharing.share(that.userdetails.fn + " " + that.userdetails.ln + " has shared " + that.liveDataShare.Device_Name + " live trip with you. Please follow below link to track", "OneQlik- Live Trip", "", link);
        that.showActionSheet = true;
        that.showFooter = false;
        that.tttime = undefined;
    };
    LiveSingleDevice.prototype.parseMillisecondsIntoReadableTime = function (milliseconds) {
        //Get hours from milliseconds
        var hours = milliseconds / (1000 * 60 * 60);
        var absoluteHours = Math.floor(hours);
        // var h = absoluteHours > 9 ? absoluteHours : '0' + absoluteHours;
        var h = absoluteHours;
        //Get remainder from hours and convert to minutes
        var minutes = (hours - absoluteHours) * 60;
        var absoluteMinutes = Math.floor(minutes);
        var m = absoluteMinutes > 9 ? absoluteMinutes : '0' + absoluteMinutes;
        if (h === 0) {
            return m + 'min';
        }
        else {
            if (m === '00') {
                return '0min';
            }
            else {
                return h + 'h ' + m + 'min';
            }
        }
    };
    LiveSingleDevice.prototype.callObjFunc = function (d) {
        var _this = this;
        var that = this;
        var _bUrl = that.apiCall.mainUrl + 'devices/getDevicebyId?deviceId=' + d.Device_ID;
        this.apiCall.startLoading().present();
        this.apiCall.getSOSReportAPI(_bUrl)
            .subscribe(function (resp) {
            _this.apiCall.stopLoading();
            console.log("updated device object=> " + resp);
            if (!resp) {
                return;
            }
            else {
                that.deviceDeatils = resp;
            }
        });
    };
    LiveSingleDevice.prototype.settings = function () {
        var that = this;
        var profileModal = this.modalCtrl.create('DeviceSettingsPage', {
            param: that.deviceDeatils
        });
        profileModal.present();
        profileModal.onDidDismiss(function () {
            that.callObjFunc(that.deviceDeatils);
        });
    };
    LiveSingleDevice.prototype.getOnlyArea = function (coordinates, data) {
        var that = this;
        if (!coordinates) {
            that.address = 'N/A';
            return;
        }
        this.geocoderApi.getAreaName(coordinates.lat, coordinates.long)
            .then(function (res) {
            var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
            that.saveAddressToServer(str, coordinates.lat, coordinates.long);
            that.areaName = str;
            var url = "https://www.google.com/maps/search/?api=1&query=" + data + "+" + that.areaName;
            // window.open("https://www.google.com/maps/search/?api=1&query=" + data, "_self");
            window.open(url, "_self");
        });
    };
    LiveSingleDevice.prototype.getAddress = function (coordinates) {
        var _this = this;
        var that = this;
        if (!coordinates) {
            that.address = 'N/A';
            return;
        }
        var tempcord = {
            "lat": coordinates.lat,
            "long": coordinates.long
        };
        this.apiCall.getAddress(tempcord)
            .subscribe(function (res) {
            // console.log("test");
            // console.log("address result", res);
            if (res.message == "Address not found in databse") {
                _this.geocoderApi.reverseGeocode(coordinates.lat, coordinates.long)
                    .then(function (res) {
                    var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
                    that.saveAddressToServer(str, coordinates.lat, coordinates.long);
                    that.address = str;
                    console.log("inside", that.address);
                });
            }
            else {
                that.address = res.address;
            }
            // console.log(that.address);
        }, function (err) {
            _this.geocoderApi.reverseGeocode(coordinates.lat, coordinates.long)
                .then(function (res) {
                var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
                that.saveAddressToServer(str, coordinates.lat, coordinates.long);
                that.address = str;
                console.log("inside", that.address);
            });
        });
        //this.getAddressinHindi()
    };
    LiveSingleDevice.prototype.getAddressinHindi = function () {
        var _this = this;
        this.flag = true;
        var data = {
            "text": this.address,
        };
        this.apiCall.startLoading().present();
        this.apiCall.announcementDataa(data)
            .subscribe(function (resp) {
            _this.apiCall.stopLoading();
            console.log("convertd hindi address", resp);
            _this.address = resp.translation;
            console.log("final", _this.address);
        });
    };
    LiveSingleDevice.prototype.saveAddressToServer = function (address, lat, lng) {
        var payLoad = {
            "lat": lat,
            "long": lng,
            "address": address
        };
        this.apiCall.saveGoogleAddressAPI(payLoad)
            .subscribe(function (respData) {
            // console.log("check if address is stored in db or not? ", respData)
        }, function (err) {
            console.log("getting err while trying to save the address: ", err);
        });
    };
    LiveSingleDevice.prototype.weDidntGetPing = function (data) {
        console.log("I came here in weDidntGetPing function");
        var that = this;
        if (data._id != undefined && data.last_location != undefined) {
            that.impkey = data._id;
            that.otherValues(data);
            if (data.last_location != undefined) {
                console.log("we did not get pung: ", that.getIconUrl(data));
                // debugger
                console.log("check lat: ", data.last_location.lat);
                console.log("check long: ", data.last_location.long);
                that.allData.map.addMarker({
                    title: data.Device_Name,
                    position: { lat: data.last_location.lat, lng: data.last_location.long },
                    icon: {
                        url: that.getIconUrl(data),
                        size: {
                            height: 40,
                            width: 20
                        }
                    },
                    rotation: Number(data.heading)
                }).then(function (marker) {
                    // debugger
                    that.allData[data._id] = {};
                    that.allData[data._id].poly = [];
                    that.allData[data._id].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
                    that.allData[data._id].mark = marker;
                    that.apiCall._lastPingOn = data.last_ping_on;
                    that.temporaryMarker = marker;
                    // that.allData[key].mark = marker;
                    marker.addEventListener(__WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK)
                        .subscribe(function (e) {
                        that.liveVehicleName = data.Device_Name;
                        that.showActionSheet = true;
                        that.showaddpoibtn = true;
                        that.getAddressTitle(marker);
                    });
                    that.getAddress(data.last_location);
                    that.socketInit(data);
                });
                // }
            }
        }
        else if (data._id != undefined && (data.last_lat !== undefined && data.last_lng !== undefined)) {
            that.impkey = data._id;
            that.otherValues(data);
            // if (data.last_location != undefined) {
            console.log("something went wrong: ", that.getIconUrl(data));
            // debugger
            // if (localStorage.getItem('gotPing') === null) {
            that.allData.map.addMarker({
                title: data.Device_Name,
                position: { lat: data.last_lat, lng: data.last_lng },
                icon: {
                    url: that.getIconUrl(data),
                    size: {
                        height: 40,
                        width: 20
                    }
                },
            }).then(function (marker) {
                // debugger
                that.temporaryMarker = marker;
                // that.allData[key].mark = marker;
                marker.addEventListener(__WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK)
                    .subscribe(function (e) {
                    that.liveVehicleName = data.Device_Name;
                    that.showActionSheet = true;
                    that.showaddpoibtn = true;
                    that.getAddressTitle(marker);
                });
                var last_location = {
                    lat: data.last_lat,
                    long: data.last_lng
                };
                that.getAddress(last_location);
                that.socketInit(data);
            });
            // }
            // }
        }
    };
    LiveSingleDevice.prototype.socketInit = function (pdata, center) {
        if (center === void 0) { center = false; }
        var that = this;
        that.event.subscribe("Reconnected", function () {
            console.log("Reconnected subscribed");
            that.scoketConn._gps.emit('acc', pdata.Device_ID);
        });
        that.scoketConn._gps.emit('acc', pdata.Device_ID);
        that.socketChnl.push(pdata.Device_ID + 'acc');
        that.scoketConn._gps.on(pdata.Device_ID + 'acc', function (d1, d2, d3, d4, d5) {
            if (d4 != undefined)
                console.log("ping data: => ", d4);
            (function (data) {
                if (data == undefined) {
                    return;
                }
                debugger;
                if (that.allData.map == undefined)
                    return;
                localStorage.setItem("pdata", JSON.stringify(data));
                that.socketData = data;
                var key = data._id;
                that.impkey = data._id;
                that.deviceDeatils = data;
                if (data._id != undefined && data.last_location != undefined) {
                    if (that.apiCall._lastPingOn != undefined) {
                        var date = __WEBPACK_IMPORTED_MODULE_4_moment__(that.apiCall._lastPingOn);
                        var now = __WEBPACK_IMPORTED_MODULE_4_moment__(data.last_ping_on);
                        if (now > date) {
                            console.log("status: " + data.status.toLowerCase());
                            console.log("last speed: " + data.last_speed);
                            console.log(that.getIconUrl(data));
                            that.otherValues(data);
                            if (that.allData[key]) {
                                // date is past
                                that.apiCall._lastPingOn = data.last_ping_on;
                                that.socketSwitch[key] = data;
                                console.log("check mark is avalible or not? ", that.allData[key].mark);
                                if (that.allData[key].mark !== undefined) {
                                    that.allData[key].mark.setIcon(that.getIconUrl(data));
                                    // that.allData[key].mark.setRotation(Number(data.heading));
                                    // that.allData[key].mark.setIcon(ic);
                                    that.allData[key].mark.setPosition(that.allData[key].poly[1]);
                                    var temp = __WEBPACK_IMPORTED_MODULE_5_lodash__["cloneDeep"](that.allData[key].poly[1]);
                                    that.allData[key].poly[0] = __WEBPACK_IMPORTED_MODULE_5_lodash__["cloneDeep"](temp);
                                    that.allData[key].poly[1] = { lat: data.last_location.lat, lng: data.last_location.long };
                                    var speed = data.status == "RUNNING" ? data.last_speed : 0;
                                    that.getAddress(data.last_location);
                                    that.liveTrack(that.allData.map, that.allData[key].mark, that.allData[key].poly, parseFloat(speed), 100, center, data._id, that);
                                }
                                else {
                                    return;
                                }
                            }
                            else {
                                return;
                            }
                        }
                    }
                    else {
                        that.apiCall._lastPingOn = data.last_ping_on;
                        that.allData[key] = {};
                        that.socketSwitch[key] = data;
                        that.allData[key].poly = [];
                        // if (data.status === 'STOPPED' || data.status === 'OUT OF REACH') {
                        // } else {
                        //   console.log("check status: ", data.status)
                        //   if (data.sec_last_location) {
                        //     that.allData[key].poly.push({ lat: data.sec_last_location.lat, lng: data.sec_last_location.long });
                        //   } else {
                        //     that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
                        //   }
                        // }
                        that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
                        if (data.last_location != undefined) {
                            //////////////////////
                            if (that.temporaryMarker !== undefined) {
                                that.temporaryMarker.setTitle(data.Device_Name);
                                that.temporaryMarker.setPosition(that.allData[key].poly[0]);
                                that.allData.map.animateCamera({
                                    target: {
                                        lat: that.allData[key].poly[0].lat,
                                        lng: that.allData[key].poly[0].lng
                                    },
                                    duration: 1800
                                });
                                that.allData[key].mark = that.temporaryMarker;
                                ///////////////////////////////
                                that.allData[key].mark.addEventListener(__WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK)
                                    .subscribe(function (e) {
                                    that.liveVehicleName = data.Device_Name;
                                    that.showActionSheet = true;
                                    that.showaddpoibtn = true;
                                    that.getAddressTitle(that.allData[key].mark);
                                });
                                that.getAddress(data.last_location);
                                var speed = data.status == "RUNNING" ? data.last_speed : 0;
                                // that.liveTrack(that.allData.map, that.allData[key].mark, ic.url, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
                                that.liveTrack(that.allData.map, that.allData[key].mark, that.allData[key].poly, parseFloat(speed), 100, center, data._id, that);
                            }
                            else {
                                that.allData.map.addMarker({
                                    title: data.Device_Name,
                                    position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
                                    icon: {
                                        url: that.getIconUrl(data),
                                        size: {
                                            width: 20,
                                            height: 40
                                        }
                                    },
                                }).then(function (marker) {
                                    that.allData[key].mark = marker;
                                    marker.addEventListener(__WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK)
                                        .subscribe(function (e) {
                                        that.liveVehicleName = data.Device_Name;
                                        that.showActionSheet = true;
                                        that.showaddpoibtn = true;
                                        that.getAddressTitle(marker);
                                    });
                                    that.getAddress(data.last_location);
                                    var speed = data.status == "RUNNING" ? data.last_speed : 0;
                                    that.liveTrack(that.allData.map, that.allData[key].mark, that.allData[key].poly, parseFloat(speed), 100, center, data._id, that);
                                });
                            }
                        }
                    }
                }
            })(d4);
        });
    };
    LiveSingleDevice.prototype.onTraffic = function () {
        // debugger
        this.afterClick = !this.afterClick;
        if (this.afterClick) {
            this.allData.map.setTrafficEnabled(true);
        }
        else {
            this.allData.map.setTrafficEnabled(false);
        }
    };
    LiveSingleDevice.prototype.onMapType = function () {
        // debugger
        this.afterMapTypeClick = !this.afterMapTypeClick;
        if (this.afterMapTypeClick) {
            this.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].HYBRID);
        }
        else {
            this.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].NORMAL);
        }
    };
    LiveSingleDevice.prototype.addCircleWithAnimation = function (lat, lng) {
        var that = this;
        var _radius = 500;
        var rMin = _radius * 1 / 9;
        var rMax = _radius;
        var direction = 1;
        var GOOGLE = {
            "lat": lat,
            "lng": lng
        };
        var circleOption = {
            'center': GOOGLE,
            'radius': 500,
            'fillColor': 'rgb(216, 6, 34)',
            'fillOpacity': 0.6,
            'strokeColor': '#950417',
            'strokeOpacity': 1,
            'strokeWidth': 0.5
        };
        // Add circle
        var circle = that.allData.map.addCircleSync(circleOption);
        that.circleObj = circle;
        that.circleTimer = setInterval(function () {
            var radius = circle.getRadius();
            if ((radius > rMax) || (radius < rMin)) {
                direction *= -1;
            }
            var _par = (radius / _radius) - 0.1;
            var opacity = 0.4 * _par;
            var colorString = that.RGBAToHexA(216, 6, 34, opacity);
            circle.setFillColor(colorString);
            circle.setRadius(radius + direction * 10);
        }, 30);
    };
    LiveSingleDevice.prototype.RGBAToHexA = function (r, g, b, a) {
        r = r.toString(16);
        g = g.toString(16);
        b = b.toString(16);
        a = Math.round(a * 255).toString(16);
        if (r.length == 1)
            r = "0" + r;
        if (g.length == 1)
            g = "0" + g;
        if (b.length == 1)
            b = "0" + b;
        if (a.length == 1)
            a = "0" + a;
        return "#" + r + g + b + a;
    };
    LiveSingleDevice.prototype.getAddressTitle = function (marker) {
        this.geocoderApi.reverseGeocode(marker.getPosition().lat, marker.getPosition().lng)
            .then(function (res) {
            var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
            marker.setSnippet(str);
        });
    };
    LiveSingleDevice.prototype.addPOI = function () {
        var _this = this;
        var that = this;
        var modal = this.modalCtrl.create(PoiPage, {
            param1: that.allData[that.impkey]
        });
        modal.onDidDismiss(function (data) {
            console.log(data);
            var that = _this;
            that.showaddpoibtn = false;
        });
        modal.present();
        console.log("param data", "param1");
    };
    /////////// Anjali code ////////
    LiveSingleDevice.prototype.getIconUrl = function (data) {
        var that = this;
        var iconUrl;
        // debugger
        if ((data.status.toLowerCase() === 'running' && Number(data.last_speed) > 0) || (data.status.toLowerCase() === 'idling' && Number(data.last_speed) > 0)) {
            if (that.plt.is('ios')) {
                iconUrl = 'www/assets/imgs/vehicles/running' + data.iconType + '.png';
            }
            else if (that.plt.is('android')) {
                iconUrl = './assets/imgs/vehicles/running' + data.iconType + '.png';
            }
        }
        else {
            if ((data.status.toLowerCase() === 'running' && Number(data.last_speed) === 0) || (data.status.toLowerCase() === 'idling' && Number(data.last_speed) === 0)) {
                if (that.plt.is('ios')) {
                    iconUrl = 'www/assets/imgs/vehicles/idling' + data.iconType + '.png';
                }
                else if (that.plt.is('android')) {
                    iconUrl = './assets/imgs/vehicles/idling' + data.iconType + '.png';
                }
            }
            else {
                if ((data.status.toLowerCase() === 'stopped') && Number(data.last_speed) > 0) {
                    if (that.plt.is('ios')) {
                        iconUrl = 'www/assets/imgs/vehicles/running' + data.iconType + '.png';
                    }
                    else if (that.plt.is('android')) {
                        iconUrl = './assets/imgs/vehicles/running' + data.iconType + '.png';
                    }
                }
                else {
                    var stricon;
                    if (data.status.toLowerCase() === 'out of reach') {
                        stricon = "outofreach";
                        if (that.plt.is('ios')) {
                            iconUrl = 'www/assets/imgs/vehicles/' + stricon + data.iconType + '.png';
                        }
                        else if (that.plt.is('android')) {
                            iconUrl = './assets/imgs/vehicles/' + stricon + data.iconType + '.png';
                        }
                    }
                    else {
                        if (that.plt.is('ios')) {
                            iconUrl = 'www/assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                        }
                        else if (that.plt.is('android')) {
                            iconUrl = './assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                        }
                    }
                }
            }
        }
        console.log("icon url: ", iconUrl);
        return iconUrl;
    };
    LiveSingleDevice.prototype.otherValues = function (data) {
        var that = this;
        that.todays_odo = 0;
        that.vehicle_speed = (data.last_speed ? data.last_speed : 0);
        that.battery_percent = (data.battery_percent ? data.battery_percent : 0);
        that.todays_odo = (data.today_odo ? data.today_odo : 0);
        that.total_odo = (data.total_odo ? data.total_odo : 0);
        that.todays_running = (data.today_running ? that.parseMillisecondsIntoReadableTime(data.today_running) : 0);
        console.log("chk running time from socket", that.todays_running);
        that.todays_stopped = (data.today_stopped ? that.parseMillisecondsIntoReadableTime(data.today_stopped) : 0);
        console.log("chk stop time from socket", that.todays_stopped);
        if (that.userdetails.fuel_unit == 'LITRE') {
            that.fuel = (data.currentFuel ? data.currentFuel : null);
        }
        else if (that.userdetails.fuel_unit == 'PERCENTAGE') {
            that.fuel = (data.fuel_percent ? data.fuel_percent : null);
        }
        else {
            that.fuel = (data.currentFuel ? data.currentFuel : null);
        }
        that.voltage = (data.battery ? data.battery : 0);
        if (data.last_location) {
            that.recenterMeLat = data.last_location.lat;
            that.recenterMeLng = data.last_location.long;
            that.getAddress(data.last_location);
        }
        else {
            if (data.last_lat !== undefined && data.last_lng !== undefined) {
                var location_1 = {
                    lat: data.last_lat,
                    long: data.last_lng
                };
                that.recenterMeLat = data.last_lat;
                that.recenterMeLng = data.last_lng;
                that.getAddress(location_1);
            }
        }
        that.last_ping_on = (data.last_ping_on ? data.last_ping_on : 'N/A');
        var tempvar = new Date(data.lastStoppedAt);
        if (data.lastStoppedAt != null) {
            var fd = tempvar.getTime();
            var td = new Date().getTime();
            var time_difference = td - fd;
            var total_min = time_difference / 60000;
            var hours = total_min / 60;
            var rhours = Math.floor(hours);
            var minutes = (hours - rhours) * 60;
            var rminutes = Math.round(minutes);
            that.lastStoppedAt = rhours + ':' + rminutes;
        }
        else {
            that.lastStoppedAt = '00' + ':' + '00';
        }
        that.distFromLastStop = data.distFromLastStop;
        if (!isNaN(data.timeAtLastStop)) {
            that.timeAtLastStop = that.parseMillisecondsIntoReadableTime(data.timeAtLastStop);
        }
        else {
            that.timeAtLastStop = '00:00:00';
        }
        that.today_stopped = that.parseMillisecondsIntoReadableTime(data.today_stopped);
        that.today_running = that.parseMillisecondsIntoReadableTime(data.today_running);
        that.last_ACC = data.last_ACC;
        that.acModel = data.ac;
        that.currentFuel = data.currentFuel;
        that.power = data.power;
        that.gpsTracking = data.gpsTracking;
        that.tempreture = data.temp;
        that.door = data.door;
    };
    LiveSingleDevice.prototype.createMarker = function (location, indice, iconUrl) {
        var _this = this;
        var latlng = new __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["f" /* LatLng */](location.last_location.lat, location.last_location.long);
        var markerOptions = {
            title: 'Ionic',
            animation: 'BOUNCE',
            position: latlng,
            icon: iconUrl
        };
        this.allData.map.addMarker(markerOptions).then(function (marker) {
            _this.markersArray[indice] = marker;
            console.log('merkers array list: ', _this.markersArray);
        });
    };
    LiveSingleDevice.prototype.liveTrack = function (map, mark, coords, speed, delay, center, id, that) {
        var target = 0;
        debugger;
        clearTimeout(that.ongoingGoToPoint[id]);
        clearTimeout(that.ongoingMoveMarker[id]);
        if (center) {
            that.allData.map.setCameraTarget(coords[0]);
        }
        function _goToPoint() {
            if (target > coords.length)
                return;
            var lat = mark.getPosition().lat;
            var lng = mark.getPosition().lng;
            var step = (speed * 1000 * delay) / 3600000; // in meters
            if (coords[target] == undefined)
                return;
            var dest = new __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["f" /* LatLng */](coords[target].lat, coords[target].lng);
            var distance = __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["h" /* Spherical */].computeDistanceBetween(dest, mark.getPosition()); // in meters
            var numStep = distance / step;
            var i = 0;
            var deltaLat = (coords[target].lat - lat) / numStep;
            var deltaLng = (coords[target].lng - lng) / numStep;
            function changeMarker(mark, deg) {
                mark.setRotation(deg);
                // mark.setIcon(icons);
            }
            function _moveMarker() {
                // debugger
                lat += deltaLat;
                lng += deltaLng;
                i += step;
                var head;
                if (i < distance) {
                    head = __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["h" /* Spherical */].computeHeading(mark.getPosition(), new __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["f" /* LatLng */](lat, lng));
                    head = head < 0 ? (360 + head) : head;
                    if ((head != 0) || (head == NaN)) {
                        changeMarker(mark, head);
                    }
                    if (that.mapTrail) {
                        that.showTrail(lat, lng, map);
                    }
                    else {
                        if (that.allData.polylineID) {
                            that.tempArray = [];
                            that.allData.polylineID.remove();
                        }
                    }
                    that.allData.map.setCameraTarget(new __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["f" /* LatLng */](lat, lng));
                    that.latlngCenter = new __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["f" /* LatLng */](lat, lng);
                    mark.setPosition(new __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["f" /* LatLng */](lat, lng));
                    that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
                }
                else {
                    head = __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["h" /* Spherical */].computeHeading(mark.getPosition(), dest);
                    head = head < 0 ? (360 + head) : head;
                    if ((head != 0) || (head == NaN)) {
                        changeMarker(mark, head);
                    }
                    if (that.mapTrail) {
                        that.showTrail(dest.lat, dest.lng, map);
                    }
                    else {
                        if (that.allData.polylineID) {
                            that.tempArray = [];
                            that.allData.polylineID.remove();
                        }
                    }
                    that.allData.map.setCameraTarget(dest);
                    that.latlngCenter = dest;
                    mark.setPosition(dest);
                    target++;
                    that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
                }
            }
            _moveMarker();
        }
        _goToPoint();
    };
    // liveTrack(map, mark, coords, speed, delay, center, id, that) {
    //   let target = 0;
    //   clearTimeout(that.ongoingGoToPoint[id]);
    //   clearTimeout(that.ongoingMoveMarker[id]);
    //   if (center) {
    //     map.setCameraTarget(coords[0]);
    //   }
    //   function _goToPoint() {
    //     if (target > coords.length)
    //       return;
    //     var lat = mark.getPosition().lat;
    //     var lng = mark.getPosition().lng;
    //     var step = (speed * 1000 * delay) / 3600000; // in meters
    //     if (coords[target] == undefined)
    //       return;
    //     var dest = new LatLng(coords[target].lat, coords[target].lng);
    //     var distance = Spherical.computeDistanceBetween(dest, mark.getPosition()); // in meters
    //     var numStep = distance / step;
    //     var i = 0;
    //     var deltaLat = (coords[target].lat - lat) / numStep;
    //     var deltaLng = (coords[target].lng - lng) / numStep;
    //     function changeMarker(mark, deg) {
    //       mark.setRotation(deg);
    //       // mark.setIcon(icons);
    //     }
    //     function _moveMarker() {
    //       // debugger
    //       lat += deltaLat;
    //       lng += deltaLng;
    //       i += step;
    //       var head;
    //       if (i < distance) {
    //         head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
    //         head = head < 0 ? (360 + head) : head;
    //         if ((head != 0) || (head == NaN)) {
    //           changeMarker(mark, head);
    //         }
    //         if (that.mapTrail) {
    //           that.showTrail(lat, lng, map);
    //         } else {
    //           if (that.allData.polylineID) {
    //             that.tempArray = [];
    //             that.allData.polylineID.remove();
    //           }
    //         }
    //         map.setCameraTarget(new LatLng(lat, lng));
    //         that.latlngCenter = new LatLng(lat, lng);
    //         mark.setPosition(new LatLng(lat, lng));
    //         that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
    //       }
    //       else {
    //         head = Spherical.computeHeading(mark.getPosition(), dest);
    //         head = head < 0 ? (360 + head) : head;
    //         if ((head != 0) || (head == NaN)) {
    //           changeMarker(mark, head);
    //         }
    //         if (that.mapTrail) {
    //           that.showTrail(dest.lat, dest.lng, map);
    //         }
    //         else {
    //           if (that.allData.polylineID) {
    //             that.tempArray = [];
    //             that.allData.polylineID.remove();
    //           }
    //         }
    //         map.setCameraTarget(dest);
    //         that.latlngCenter = dest;
    //         mark.setPosition(dest);
    //         target++;
    //         that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
    //       }
    //     }
    //     _moveMarker();
    //   }
    //   _goToPoint();
    // }
    LiveSingleDevice.prototype.zoomin = function () {
        var that = this;
        // that.ngOnDestroy();
        that.allData.map.moveCameraZoomIn();
    };
    LiveSingleDevice.prototype.zoomout = function () {
        var that = this;
        // that.ngOnDestroy();
        that.allData.map.moveCameraZoomOut();
    };
    LiveSingleDevice.prototype.temp = function (data) {
        var that = this;
        that.showShareBtn = true;
        that.liveDataShare = data;
        that.onClickShow = false;
        // if (that.allData.map != undefined) {
        //   that.allData.map.remove();
        // }
        // for (var i = 0; i < that.socketChnl.length; i++)
        //   that._io.removeAllListeners(that.socketChnl[i]);
        // that.allData = {};
        that.socketChnl = [];
        that.socketSwitch = {};
        var style = [];
        if (localStorage.getItem('NightMode') != null) {
            if (localStorage.getItem('NightMode') === 'ON') {
                //Change Style to night between 7pm to 5am
                if (this.isNight()) {
                    style = __WEBPACK_IMPORTED_MODULE_11__live_map_style_model__["a" /* mapStyle */];
                }
            }
        }
        if (data) {
            if (data.last_location) {
                if (that.plt.is('android')) {
                    // let map = GoogleMaps.create('map_canvas_single_device', mapOptions);
                    that.allData.map.moveCamera({
                        target: {
                            lat: data.last_location['lat'], lng: data.last_location['long']
                        },
                        zoom: this.zoomLevel
                    });
                    // that.allData.map = map;
                    // that.onButtonClick();
                }
                else {
                    if (that.plt.is('ios')) {
                        // let map = GoogleMaps.create('map_canvas_single_device', mapOptions);
                        that.allData.map.moveCamera({
                            target: {
                                lat: data.last_location['lat'], lng: data.last_location['long']
                            },
                            zoom: this.zoomLevel
                        });
                        // Environment.setBackgroundColor("black");
                        // that.allData.map = map;
                    }
                }
                that.weDidntGetPing(data);
            }
            else {
                if (data.last_lat !== undefined && data.last_lng !== undefined) {
                    if (that.plt.is('android')) {
                        // let map = GoogleMaps.create('map_canvas_single_device', mapOptions);
                        that.allData.map.moveCamera({
                            target: {
                                lat: data.last_lat, lng: data.last_lng
                            },
                            zoom: this.zoomLevel
                        });
                        // that.allData.map = map;
                    }
                    else {
                        if (that.plt.is('ios')) {
                            // let map = GoogleMaps.create('map_canvas_single_device', mapOptions);
                            that.allData.map.moveCamera({
                                target: {
                                    lat: data.last_lat, lng: data.last_lng
                                },
                                zoom: this.zoomLevel
                            });
                            // Environment.setBackgroundColor("black");
                            // that.allData.map = map;
                        }
                    }
                    that.weDidntGetPing(data);
                }
            }
            //** Check if traffice mode is on or off **//
            if (localStorage.getItem('TrafficMode') !== null) {
                if (localStorage.getItem('TrafficMode') === 'ON') {
                    this.mapHideTraffic = true;
                    this.allData.map.setTrafficEnabled(true);
                }
            }
            //** Check if traffice mode is on or off **//
        }
    };
    LiveSingleDevice.prototype.showTrail = function (lat, lng, map) {
        var that = this;
        that.tempArray.push({ lat: lat, lng: lng });
        if (that.tempArray.length === 2) {
            map.addPolyline({
                points: that.tempArray,
                color: '#0260f7',
                width: 5,
                geodesic: true
            }).then(function (polyline) {
                that.allData.polylineID = polyline;
                // that.polylineID.push(polyline);
            });
        }
        else if (that.tempArray.length > 2) {
            that.tempArray.shift();
            map.addPolyline({
                points: that.tempArray,
                color: '#0260f7',
                width: 5,
                geodesic: true
            }).then(function (polyline) {
                that.allData.polylineID = polyline;
                // that.allData.polylineID.push(polyline);
            });
        }
    };
    // _goBack() {
    //   this.navCtrl.setRoot('LivePage', {
    //     animate: true, animation: 'transition-ios'
    //   });
    //   this.navCtrl.popToRoot()
    // }
    LiveSingleDevice.prototype.onClickMainMenu = function (item) {
        this.menuActive = !this.menuActive;
    };
    LiveSingleDevice.prototype.onSelectMapOption = function (type) {
        var that = this;
        if (type == 'locateme') {
            that.allData.map.setCameraTarget(that.latlngCenter);
        }
        else {
            if (type == 'mapHideTraffic') {
                this.mapHideTraffic = !this.mapHideTraffic;
                if (this.mapHideTraffic) {
                    that.allData.map.setTrafficEnabled(true);
                }
                else {
                    that.allData.map.setTrafficEnabled(false);
                }
            }
            else {
                if (type == 'mapTrail') {
                    this.mapTrail = !this.mapTrail;
                    if (this.mapTrail) {
                        if (this.eyeBtnColor == '#000000') {
                            this.eyeBtnColor = '#f4f4f4';
                            this.eyeColor = '#000000';
                        }
                    }
                    else {
                        if (this.eyeBtnColor == '#f4f4f4') {
                            this.eyeBtnColor = '#000000';
                            this.eyeColor = '#fff';
                        }
                    }
                }
            }
        }
    };
    LiveSingleDevice.prototype.showGallery = function () {
        console.log("inside show gallery");
        this.navCtrl.push('GalleryPage');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Navbar"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Navbar"])
    ], LiveSingleDevice.prototype, "navBar", void 0);
    LiveSingleDevice = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-live',template:/*ion-inline-start:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/live-single-device/live-single-device.html"*/'<ion-header>\n  <ion-navbar>\n    <!-- <button start ion-button icon-only (click)="_goBack()">\n      <ion-icon name="arrow-back"></ion-icon>\n    </button> -->\n    <ion-title>{{ "Live For " | translate }}{{ titleText }}</ion-title>\n    <ion-buttons end>\n      <button *ngIf="tripStatus != \'Started\'" ion-button icon-only (click)="createTrip()">\n        <ion-icon name="play"></ion-icon>\n      </button>\n      <button *ngIf="tripStatus == \'Started\'" ion-button icon-only (click)="endTrip()">\n        <ion-icon name="radio-button-on"></ion-icon>\n      </button>\n      <!-- <button ion-button icon-only (click)="showGallery()">\n        <ion-icon name="camera"></ion-icon>\n      </button> -->\n    </ion-buttons>\n  </ion-navbar>\n  <ion-segment mode="md" color="gpsc" scrollable="true" (ionChange)="segmentChanged($event)" [(ngModel)]="takeAction"\n    style="background-color: white;">\n    <ion-segment-button class="seg" style="padding-top: 3px;" value="live">\n      <img src="assets/imgs/segment/map_active.png" style="width: 30px;" *ngIf="takeAction === \'live\'" />\n      <img src="assets/imgs/segment/map_inactive.png" style="width: 30px;" *ngIf="takeAction !== \'live\'" />\n    </ion-segment-button>\n    <ion-segment-button style="padding-top: 3px;" value="history">\n      <img src="assets/imgs/segment/history_active.png" style="width: 30px;" *ngIf="takeAction === \'history\'" />\n      <img src="assets/imgs/segment/history_inactive.png" style="width: 30px;" *ngIf="takeAction !== \'history\'" />\n    </ion-segment-button>\n    <ion-segment-button style="padding-top: 3px;" value="trip">\n      <img src="assets/imgs/segment/trip_active.png" style="width: 30px;" *ngIf="takeAction === \'trip\'" />\n      <img src="assets/imgs/segment/trip_inactive.png" style="width: 30px;" *ngIf="takeAction !== \'trip\'" />\n    </ion-segment-button>\n    <ion-segment-button style="padding-top: 3px;" value="notif">\n      <img src="assets/imgs/segment/bell_active.png" style="width: 30px;" *ngIf="takeAction === \'notif\'" />\n      <img src="assets/imgs/segment/bell_inactive.png" style="width: 30px;" *ngIf="takeAction !== \'notif\'" />\n    </ion-segment-button>\n    <ion-segment-button style="padding-top: 3px;" value="car_service">\n      <img src="assets/imgs/segment/car_service_active.png" style="width: 30px;" *ngIf="takeAction === \'car_service\'" />\n      <img src="assets/imgs/segment/car_service_inactive.png" style="width: 30px;"\n        *ngIf="takeAction !== \'car_service\'" />\n    </ion-segment-button>\n  </ion-segment>\n</ion-header>\n<ion-content *ngIf="!hideMe">\n  <div *ngIf="showOtherBtns">\n    <ion-fab right style="margin-top:72%" showWhen="ios">\n      <button ion-fab color="gpsc" mini (click)="reCenterMe()">\n        <ion-icon name="locate"></ion-icon>\n      </button>\n    </ion-fab>\n    <ion-fab right style="margin-top:85%" showWhen="ios">\n      <button color="gpsc" ion-fab (click)="zoomin()" mini>\n        <ion-icon name="add"></ion-icon>\n      </button>\n    </ion-fab>\n    <ion-fab right style="margin-top:98%" showWhen="ios">\n      <button color="gpsc" ion-fab (click)="zoomout()" mini>\n        <ion-icon name="remove"></ion-icon>\n      </button>\n    </ion-fab>\n    <ion-fab left style="margin-top:59%" showWhen="ios">\n      <button ion-fab [ngStyle]="{\'background-color\': policeButtonColor}" mini (click)="showNearby()">\n        <ion-icon name="custom-nearby" [ngStyle]="{\'color\': policeColor}"></ion-icon>\n      </button>\n    </ion-fab>\n\n    <!-- <ion-fab left style="margin-top:59%" showWhen="ios">\n      <button ion-fab [ngStyle]="{\'background-color\': policeButtonColor}" mini (click)="showNearby(\'police\')">\n        <ion-icon name="custom-station" [ngStyle]="{\'color\': policeColor}"></ion-icon>\n      </button>\n    </ion-fab>\n\n    <ion-fab left style="margin-top:72%" showWhen="ios">\n      <button ion-fab [ngStyle]="{\'background-color\': petrolButtonColor}" mini (click)="showNearby(\'petrol\')">\n        <ion-icon name="custom-fuel" [ngStyle]="{\'color\': petrolColor}"></ion-icon>\n      </button>\n    </ion-fab> -->\n\n    <ion-fab left style="margin-top:72%" showWhen="ios">\n      <button ion-fab color="gpsc" mini (click)="refreshMe1()">\n        <ion-icon name="refresh"></ion-icon>\n      </button>\n    </ion-fab>\n\n    <ion-fab left style="margin-top:85%" showWhen="ios">\n      <button ion-fab [ngStyle]="{\'background-color\': streetviewButtonColor}" mini (click)="streetView()">\n        <ion-icon name="custom-street" [ngStyle]="{\'color\': streetColor}"></ion-icon>\n      </button>\n    </ion-fab>\n    <ion-fab left style="margin-top:98%" showWhen="ios">\n      <button ion-fab [ngStyle]="{\'background-color\': navigateButtonColor}" mini (click)="navigateFromCurrentLoc()">\n        <ion-icon name="navigate" [ngStyle]="{\'color\': navColor}"></ion-icon>\n      </button>\n    </ion-fab>\n  </div>\n\n  <ion-fab right style="margin-top:72%" showWhen="android">\n    <button ion-fab color="gpsc" mini (click)="reCenterMe()">\n      <ion-icon name="locate"></ion-icon>\n    </button>\n  </ion-fab>\n  <ion-fab right style="margin-top:85%" showWhen="android">\n    <button color="gpsc" ion-fab (click)="zoomin()" mini>\n      <ion-icon name="add"></ion-icon>\n    </button>\n  </ion-fab>\n  <ion-fab right style="margin-top:98%" showWhen="android">\n    <button color="gpsc" ion-fab (click)="zoomout()" mini>\n      <ion-icon name="remove"></ion-icon>\n    </button>\n  </ion-fab>\n  <ion-fab left style="margin-top:59%" showWhen="android">\n    <button ion-fab [ngStyle]="{\'background-color\': policeButtonColor}" mini (click)="showNearby()">\n      <ion-icon name="custom-nearby" [ngStyle]="{\'color\': policeColor}"></ion-icon>\n    </button>\n  </ion-fab>\n\n  <ion-fab left style="margin-top:72%" showWhen="android">\n    <button ion-fab color="gpsc" mini (click)="refreshMe1()">\n      <ion-icon name="refresh"></ion-icon>\n    </button>\n  </ion-fab>\n\n  <ion-fab left style="margin-top:85%" showWhen="android">\n    <button ion-fab [ngStyle]="{\'background-color\': streetviewButtonColor}" mini (click)="streetView()">\n      <ion-icon name="custom-street" [ngStyle]="{\'color\': streetColor}"></ion-icon>\n    </button>\n  </ion-fab>\n  <ion-fab left style="margin-top:98%" showWhen="android">\n    <button ion-fab [ngStyle]="{\'background-color\': navigateButtonColor}" mini (click)="navigateFromCurrentLoc()">\n      <ion-icon name="navigate" [ngStyle]="{\'color\': navColor}"></ion-icon>\n    </button>\n  </ion-fab>\n\n\n  <ion-row *ngIf="showDistDuration"\n    style="margin-top:72%; background-color: rgb(0, 0, 0, 0.5); font-size: 0.8em; color: white;border-radius: 25px;width: 70%;margin: auto; padding:5px;">\n    <ion-col style="background-color: transparent; text-align: center;" col-6>\n      {{ "Distance" | translate }} {{ expectation.distance }}\n    </ion-col>\n    <ion-col style="background-color: transparent; text-align: center;" col-6>\n      {{ "Time" | translate }} {{ expectation.duration }}\n    </ion-col>\n  </ion-row>\n  <div id="map_canvas_single_device">\n    <ion-row>\n      <ion-col>\n        <ion-fab top left>\n            <button ion-fab color="gpsc" mini (click)="onMapType()">\n              <ion-icon *ngIf="afterMapTypeClick" name="logo-buffer"></ion-icon>\n              <img *ngIf="!afterMapTypeClick" src="assets/icon/stack-of-square-papers-svg.png">\n            </button>\n        </ion-fab>\n      </ion-col>\n      <ion-col>\n        <p class="blink" style="text-align: center;font-size:24px;color: green;font-weight: 600;"\n          *ngIf="vehicle_speed <= 60">\n          {{ vehicle_speed }}\n          <span\n            style="font-size:16px;">{{ (measurementUnit == \'MKS\') ? ("Km/hr" | translate) : ("Miles/hr" | translate)}}</span>\n        </p>\n        <p class="blink" style="text-align: center;font-size:24px;color: red;font-weight: 600;"\n          *ngIf="vehicle_speed > 60">\n          {{ vehicle_speed }}\n          <span\n            style="font-size:16px;">{{ (measurementUnit == \'MKS\') ? ("Km/hr" | translate) : ("Miles/hr" | translate)}}</span>\n        </p>\n      </ion-col>\n      <ion-col>\n        <ion-fab top right>\n          <button ion-fab color="gpsc" mini>\n            <ion-icon name="arrow-dropdown"></ion-icon>\n          </button>\n          <ion-fab-list side="bottom">\n            <!-- <button color="gpsc" style="margin: 1px 0px 1px 0px;" ion-fab *ngIf="showShareBtn"\n              (click)="shareLive($event)">\n              <ion-icon name="share"></ion-icon>\n            </button> -->\n            <button color="gpsc" style="margin: 1px 0px 1px 0px;" ion-fab (click)="onSelectMapOption(\'mapTrail\')"\n              [ngStyle]="{\'background-color\': eyeBtnColor}">\n              <ion-icon name="eye" [ngStyle]="{\'color\': eyeColor}"></ion-icon>\n            </button>\n            <button color="gpsc" style="margin: 1px 0px 1px 0px;" ion-fab (click)="onSelectMapOption(\'mapHideTraffic\')">\n              <img src="assets/icon/trafficON.png" *ngIf="mapHideTraffic" />\n              <img src="assets/icon/trafficOFF.png" *ngIf="!mapHideTraffic" />\n            </button>\n            <!-- <button color="gpsc" style="margin: 1px 0px 1px 0px;" ion-fab (click)="settings()">\n              <ion-icon name="cog"></ion-icon>\n            </button> -->\n            <button color="gpsc" style="margin: 1px 0px 1px 0px;" ion-fab (click)="addPOI()">\n              <ion-icon name="flag"></ion-icon>\n            </button>\n          </ion-fab-list>\n        </ion-fab>\n      </ion-col>\n    </ion-row>\n\n    <ion-fab right style="margin-top:85%" showWhen="ios">\n      <button ion-fab color="gpsc" mini (click)="reCenterMe()">\n        <ion-icon name="locate"></ion-icon>\n      </button>\n    </ion-fab>\n    <ion-fab right style="margin-top:98%" showWhen="ios">\n      <button color="gpsc" ion-fab (click)="zoomin()" mini>\n        <ion-icon name="add"></ion-icon>\n      </button>\n    </ion-fab>\n    <ion-fab right style="margin-top:111%" showWhen="ios">\n      <button color="gpsc" ion-fab (click)="zoomout()" mini>\n        <ion-icon name="remove"></ion-icon>\n      </button>\n    </ion-fab>\n\n    <ion-fab left style="margin-top:59%" showWhen="ios">\n      <button ion-fab [ngStyle]="{\'background-color\': policeButtonColor}" mini (click)="showNearby()">\n        <ion-icon name="custom-nearby" [ngStyle]="{\'color\': policeColor}"></ion-icon>\n      </button>\n    </ion-fab>\n \n\n    <ion-fab left style="margin-top:72%" showWhen="ios">\n      <button ion-fab color="gpsc" mini (click)="refreshMe1()">\n        <ion-icon name="refresh"></ion-icon>\n      </button>\n    </ion-fab>\n\n    <ion-fab left style="margin-top:85%" showWhen="ios">\n      <button ion-fab [ngStyle]="{\'background-color\': streetviewButtonColor}" mini (click)="streetView()">\n        <ion-icon name="custom-street" [ngStyle]="{\'color\': streetColor}"></ion-icon>\n      </button>\n    </ion-fab>\n    <ion-fab left style="margin-top:98%" showWhen="ios">\n      <button ion-fab [ngStyle]="{\'background-color\': navigateButtonColor}" mini (click)="navigateFromCurrentLoc()">\n        <ion-icon name="navigate" [ngStyle]="{\'color\': navColor}"></ion-icon>\n      </button>\n    </ion-fab>\n  </div>\n  <div id="viewParanoma" style="width: 100%;height: 300px;"></div>\n</ion-content>\n<div *ngIf="showActionSheet">\n  <!-- <div class="divPlan"> -->\n  <ion-bottom-drawer [(state)]="drawerState" [dockedHeight]="dockedHeight" [shouldBounce]="shouldBounce"\n    [distanceTop]="distanceTop" [minimumHeight]="minimumHeight" [transition]="transition" (click)="setDocHeight()">\n    <div class="drawer-content">\n      <ion-row style="margin-bottom: -10px;">\n        <ion-col col-9 no-padding>\n          <p padding-left style="font-size: 13px; color:cornflowerblue;">\n            {{ "Last Updated On" | translate }} &mdash;\n            {{ last_ping_on | date: "medium" }}\n          </p>\n        </ion-col>\n        <ion-col col-3 no-padding style="text-align: right; padding-right: 20px;" *ngIf="battery_percent">\n\n          <p style="font-size: 13px;">\n            <ion-icon name="battery-charging" style="color: #11a46e;"></ion-icon>\n            &nbsp;{{battery_percent}} %\n          </p>\n        </ion-col>\n      </ion-row>\n      <!-- <p padding-left style="font-size: 13px" *ngIf="!address">N/A</p>\n      <p padding-left style="font-size: 13px" *ngIf="address">{{ address }}</p> -->\n      <!-- <button ion-fab [ngStyle]="{\'background-color\': navigateButtonColor}" mini (click)="getAddressinHindi()">\n        <ion-icon name="navigate" [ngStyle]="{\'color\': navColor}"></ion-icon>\n      </button> -->\n      <ion-row>\n        <ion-col col-10>\n          <p padding-left style="font-size: 13px" *ngIf="!address">N/A</p>\n          <p padding-left style="font-size: 13px" *ngIf="address">{{ address }}</p>\n        </ion-col>\n        <ion-col col-2>\n          <img src="assets/imgs/language.png" (click)="getAddressinHindi()" style="height: 25px;margin-left: 10px;margin-top: 20px;">\n        </ion-col>\n      </ion-row>\n      <hr />\n      <ion-row>\n        <ion-col width-1 style="text-align:center">\n          <img src="assets/imgs/statusIcons/ign_red.png" *ngIf="last_ACC == \'0\'" width="20" height="20" />\n          <img src="assets/imgs/statusIcons/ign_green.png" *ngIf="last_ACC == \'1\'" width="20" height="20" />\n          <img src="assets/imgs/statusIcons/ign_idle.png" *ngIf="last_ACC == null" width="20" height="20" />\n          <p>{{ "IGN" | translate }}</p>\n        </ion-col>\n        <ion-col width-1 style="text-align:center">\n          <img src="assets/imgs/statusIcons/ac_na.png" *ngIf="acModel == null" width="20" height="20" />\n          <img src="assets/imgs/statusIcons/ac_present.png" *ngIf="acModel == \'1\'" width="20" height="20" />\n          <img src="assets/imgs/statusIcons/ac_empty.png" *ngIf="acModel == \'0\'" width="20" height="20" />\n          <p>{{ "AC" | translate }}</p>\n        </ion-col>\n        <ion-col width-1 style="text-align:center">\n          <img src="assets/imgs/statusIcons/fuel_available.png" *ngIf="currentFuel != null" width="20" height="20" />\n          <img src="assets/imgs/statusIcons/fuel_empty.png" *ngIf="currentFuel == null" width="20" height="20" />\n          <p>{{ "FUEL" | translate }}</p>\n        </ion-col>\n        <ion-col width-1 style="text-align:center">\n          <img src="assets/imgs/statusIcons/power_na.png" *ngIf="power == null" width="20" height="20" />\n          <img src="assets/imgs/statusIcons/pwer_disconnected.png" *ngIf="power == \'0\'" width="20" height="20" />\n          <img src="assets/imgs/statusIcons/power_connected.png" *ngIf="power == \'1\'" width="20" height="20" />\n          <p>{{ "POWER" | translate }}</p>\n        </ion-col>\n        <ion-col width-1 style="text-align:center">\n          <img src="assets/imgs/statusIcons/gps_na.png" *ngIf="gpsTracking == null" width="20" height="20" />\n          <img src="assets/imgs/statusIcons/gps_notPresent.png" *ngIf="gpsTracking == \'0\'" width="20" height="20" />\n          <img src="assets/imgs/statusIcons/gps_present.png" *ngIf="gpsTracking == \'1\'" width="20" height="20" />\n          <p>{{ "GPS" | translate }}</p>\n        </ion-col>\n        <ion-col width-1 style="text-align:center">\n          <ion-icon name="thermometer" *ngIf="tempreture == null" style="color: gray; font-size: 1.6em;"></ion-icon>\n          <ion-icon name="thermometer" *ngIf="tempreture == \'0\'" color="danger" style="font-size: 1.6em;"></ion-icon>\n          <ion-icon name="thermometer" *ngIf="tempreture == \'1\'" color="secondary" style="font-size: 1.6em;"></ion-icon>\n          <p>{{ "Temp" | translate }}</p>\n        </ion-col>\n        <ion-col width-1 style="text-align:center">\n          <img src="assets/imgs/statusIcons/car_no_data.png" *ngIf="door == null" width="20" height="20" />\n          <img src="assets/imgs/statusIcons/car_door_open.png" *ngIf="door == 0" width="20" height="20" />\n          <img src="assets/imgs/statusIcons/car_door_close.png" *ngIf="door == 1" width="20" height="20" />\n          <p>{{ "Door" | translate }}</p>\n        </ion-col>\n        <ion-col width-1 style="text-align:center">\n          <img src="assets/imgs/statusIcons/belt_no_data.png" *ngIf="seatbelt == null" width="20" height="20" />\n          <img src="assets/imgs/statusIcons/belt_open.png" *ngIf="seatbelt == 3" width="20" height="20" />\n          <img src="assets/imgs/statusIcons/belt_close.png" *ngIf="seatbelt == 4" width="20" height="20" />\n          <p>{{ "Seat Belt" | translate }}</p>\n        </ion-col>\n      </ion-row>\n      <hr />\n      <ion-row>\n        <ion-col no-padding width-33 style="text-align:center">\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!total_odo">\n            N/A\n          </p>\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="total_odo">\n            {{ total_odo | number: "1.0-2" }}\n          </p>\n          <p style="font-size: 13px">{{ "Odometer" | translate }}</p>\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!vehicle_speed">\n            0 {{ (measurementUnit == \'MKS\') ? ("Km/hr" | translate) : ("Miles/hr" | translate)}}\n          </p>\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="vehicle_speed">\n            {{ vehicle_speed }} {{ (measurementUnit == \'MKS\') ? ("Km/hr" | translate) : ("Miles/hr" | translate)}}\n          </p>\n          <p style="font-size: 13px">\n            {{ "Speed" | translate }}\n          </p>\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="fuel">\n            {{ fuel }}\n          </p>\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!fuel">N/A</p>\n          <p style="font-size: 13px">{{ "Fuel" | translate }}</p>\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="voltage">\n            {{ voltage | number: "1.0-2" }}\n          </p>\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!voltage">N/A</p>\n          <p style="font-size: 13px">{{ "Voltage" | translate }}</p>\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="tempreture">\n            {{ tempreture | number: "1.0-2" }}\n          </p>\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!tempreture">N/A</p>\n          <p style="font-size: 13px">{{ "Temp" | translate }}</p>\n        </ion-col>\n      </ion-row>\n      <hr />\n      <ion-scroll scrollX="true" style="width:100vw; height:45px">\n        <ion-row nowrap class="headerChip">\n          <div *ngFor="let tabName of total_days; let idx = index"\n            [ngClass]="showSelectedTabArray[idx].showSelectedTab ? \'headerChipGreen\' : \'headerChipGray\'">\n            <ion-chip (click)="changeData(tabName, idx)">\n              <ion-label><b><span style="font-size: 1.2em;">{{tabName | date:\'dd\'}}</span></b> {{tabName | date:\'MMM\'}}\n              </ion-label>\n              <div></div>\n            </ion-chip>\n          </div>\n        </ion-row>\n      </ion-scroll>\n      <ion-row\n        style="background-image: linear-gradient(to right top, #000000, #c0002d, #a60033, #8b0935, #6f1133); padding: 5px">\n        <ion-col col-4 style="padding-right: 5px;">\n          <ion-row>\n            <ion-col col-3 no-padding>\n              <img src="assets/imgs/car_circle.png" style="width: 35px;" />\n            </ion-col>\n            <ion-col col-9 style="margin: auto; padding: 0px 0px 0px 5px;border-right: 1px solid white;">\n              <p style="margin: 0px;font-size: 1em;color: white;font-weight: 500;">\n                {{ to_running ? to_running : \'0h 0min\' }}</p>\n            </ion-col>\n          </ion-row>\n        </ion-col>\n        <ion-col col-4 style="padding-right: 5px;">\n          <ion-row>\n            <ion-col col-3 no-padding>\n              <img src="assets/imgs/distance_circle.png" style="width: 35px;" />\n            </ion-col>\n            <ion-col col-9 style="margin: auto;padding: 0px 0px 0px 5px;border-right: 1px solid white;">\n              <p style="margin: 0px;font-size: 1em;color: white;font-weight: 500;">\n                {{ to_odo ? (to_odo | number: "1.0-2") : 0 }}\n                {{ (measurementUnit == \'MKS\') ? ("Kms" | translate) : ("Miles" | translate)}}</p>\n            </ion-col>\n          </ion-row>\n        </ion-col>\n        <ion-col col-4>\n          <ion-row>\n            <ion-col col-3 no-padding>\n              <img src="assets/imgs/parking_circle.png" style="width: 35px;" />\n            </ion-col>\n            <ion-col col-9 style="margin: auto;padding: 0px 0px 0px 5px;">\n              <p style="margin: 0px;font-size: 1em;color: white;font-weight: 500;">\n                {{ to_stopped ? to_stopped : \'0h 0min\' }}</p>\n            </ion-col>\n          </ion-row>\n        </ion-col>\n      </ion-row>\n      <div *ngIf="showString === \'start\'" style="text-align: center;">\n        <ion-spinner color="gpsc"></ion-spinner>\n        <p style="font-size: 1.2em;">Fetching data...</p>\n      </div>\n      <div *ngIf="showString === \'No Data Found!\'" style="text-align: center;">\n        <p style="font-size: 1.2em;">{{showString}}</p>\n      </div>\n      <ion-scroll scrollY="true" style="height: 300px;">\n        <ion-row>\n          <ion-col col-12 *ngFor="let item of sevenDaysData">\n            <ion-row style="background: white;" *ngIf="item.type === \'STOPPAGE\'">\n              <ion-col col-3 style="text-align: right; margin: auto;">\n                <p style="margin: 0px; color: black; font-weight: 500; font-size: 0.9em;">\n                  <span\n                    style="font-weight: 600; font-size: 0.8em;">{{item.start_time | date:\'MMM\'}} {{item.start_time | date:\'dd\'}},</span>\n                  {{item.start_time | date:\'hh\'}}:{{item.start_time | date:\'mm\'}}\n                  <span\n                     style="font-weight: 600; font-size: 0.7em;">{{item.start_time | date:\'aa\'}}</span>\n                  <!-- <span>  {{item.start_time | date:\'MMM\'}} {{item.start_time | date:\'dd\'}}</span> -->\n                </p>\n              </ion-col>\n              <ion-col col-1 class="content-block" style="border-right: 1px solid #BC3F30;">\n                <img src="assets/imgs/parking_circle.png" alt="parking">\n              </ion-col>\n              <ion-col col-8 style="padding: 10px 0px;">\n                <ion-row>\n                  <ion-col col-12 style="padding-left: 20px;">\n                    <ion-row>\n                      <ion-col col-1 style="margin: auto;">\n                        <ion-icon ios="ios-time" md="md-time" style="color: #6f1133;font-size: 1.5em;"></ion-icon>\n                      </ion-col>\n                      <ion-icon col-11>\n                        <p style="margin: 0px; font-size: 0.9em; font-weight: 500;">\n                          {{parseMillisecondsIntoReadableTime(item?.event_time)}}</p>\n                      </ion-icon>\n                    </ion-row>\n                  </ion-col>\n                  <!-- <ion-col col-12 style="padding-left: 20px;">\n                    <p style="margin: 0px; color: gray;font-size: 11px">N/A</p>\n                  </ion-col> -->\n                </ion-row>\n              </ion-col>\n            </ion-row>\n\n            <ion-row style="background: #ece3f6;" *ngIf="item.type === \'MOVING\'">\n              <ion-col col-3 style="text-align: right; margin: auto;">\n                <p style="margin: 0px; color: black; font-weight: 500; font-size: 0.9em;">\n                  <span\n                    style="font-weight: 600; font-size: 0.8em;">{{item.start_time | date:\'MMM\'}} {{item.start_time | date:\'dd\'}},</span>\n                  {{item.start_time | date:\'hh\'}}:{{item.start_time | date:\'mm\'}}\n                  <span\n                     style="font-weight: 600; font-size: 0.7em;">{{item.start_time | date:\'aa\'}}</span>\n                  <!-- <span>  {{item.start_time | date:\'MMM\'}} {{item.start_time | date:\'dd\'}}</span> -->\n                </p>\n              </ion-col>\n              <ion-col col-1 class="content-block" style="border-right: 1px solid #9a62c3;">\n                <img src="assets/imgs/car_circle.png" alt="parking">\n              </ion-col>\n              <ion-col col-8 style="padding-left: 13px;">\n                <ion-row>\n                  <ion-col col-1 style="margin: auto;margin-right: 0px;">\n                    <ion-icon ios="ios-time" md="md-time" style="color: #6f1133;font-size: 1.5em;"></ion-icon>\n                  </ion-col>\n                  <ion-icon col-4 style="margin: auto;">\n                    <p style="margin-left: -5px; font-size: 0.9em; font-weight: 500;">\n                      {{parseMillisecondsIntoReadableTime(item?.event_time)}}</p>\n                  </ion-icon>\n                  <ion-col col-1 style="margin: auto;">\n\n                      <img src="assets/imgs/wheel.png">\n\n                  </ion-col>\n                  <ion-icon col-4 style="margin: auto;">\n                    <p style="margin-left: 0px; font-size: 0.9em; font-weight: 500;">\n                      {{meterIntoKelometer(item?.distance) | number: "1.0-2"}}{{ (measurementUnit == \'MKS\') ? ("Kms" | translate) : ("Miles" | translate)}}\n                    </p>\n                  </ion-icon>\n                </ion-row>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n      </ion-scroll>\n    </div>\n  </ion-bottom-drawer>\n</div>\n\n<div *ngIf="showFooter">\n  <ion-bottom-drawer [(state)]="drawerState1" [dockedHeight]="dockedHeight1" [shouldBounce]="shouldBounce"\n    [distanceTop]="distanceTop1" [minimumHeight]="minimumHeight1">\n    <div class="drawer-content">\n      <p style="font-size:1.2em; color:black;">\n        {{ "Share Live Vehicle" | translate }}\n      </p>\n      <ion-row>\n        <ion-col col-4 style="padding: 5px">\n          <button ion-button block color="{{ condition }}" (click)="sharedevices(\'15mins\')">\n            {{ "15 mins" | translate }}\n          </button>\n        </ion-col>\n        <ion-col col-4 style="padding: 5px">\n          <button ion-button block color="{{ condition1 }}" (click)="sharedevices(\'1hour\')">\n            {{ "1 hour" | translate }}\n          </button>\n        </ion-col>\n        <ion-col col-4 style="padding: 5px">\n          <button ion-button block color="{{ condition2 }}" (click)="sharedevices(\'8hours\')">\n            {{ "8 hours" | translate }}\n          </button>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-4></ion-col>\n        <ion-col col-4></ion-col>\n        <ion-col col-4 style="text-align: right;">\n          <ion-fab style="right: calc(10px + env(safe-area-inset-right));">\n            <button ion-fab mini (click)="shareLivetemp()" color="gpsc">\n              <ion-icon name="send" color="black"></ion-icon>\n            </button>\n          </ion-fab>\n        </ion-col>\n      </ion-row>\n    </div>\n  </ion-bottom-drawer>\n</div>\n\n\n'/*ion-inline-end:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/live-single-device/live-single-device.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__["a" /* SocialSharing */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_14__ionic_native_insomnia__["a" /* Insomnia */],
            __WEBPACK_IMPORTED_MODULE_15__providers_socket_connection_socket_connection__["a" /* SocketConnectionProvider */],
            __WEBPACK_IMPORTED_MODULE_7__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_8__providers_geocoder_geocoder__["a" /* GeocoderProvider */]])
    ], LiveSingleDevice);
    return LiveSingleDevice;
}());

var PoiPage = /** @class */ (function () {
    function PoiPage(apicall, toastCtrl, navparam, viewCtrl) {
        this.apicall = apicall;
        this.toastCtrl = toastCtrl;
        this.navparam = navparam;
        this.viewCtrl = viewCtrl;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("cheack data", this.islogin);
        this.params = this.navparam.get("param1");
        console.log("cheacking", this.params);
        this.lat = this.params.mark.getPosition().lat;
        this.lng = this.params.mark.getPosition().lng;
    }
    PoiPage.prototype.ngOnInit = function () {
        var _this = this;
        var that = this;
        that.address = undefined;
        debugger;
        if (this.lat != undefined || this.lng != undefined) {
            var tempcord = {
                "lat": this.lat,
                "long": this.lng
            };
            this.apicall.getAddress(tempcord)
                .subscribe(function (res) {
                if (res.message == "Address not found in databse") {
                    var geocoder = new google.maps.Geocoder;
                    var latlng = new google.maps.LatLng(_this.lat, _this.lng);
                    var request = { "latLng": latlng };
                    geocoder.geocode(request, function (resp, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (resp[0] != null) {
                                that.address = resp[0].formatted_address;
                            }
                            else {
                                console.log("No address available");
                            }
                        }
                        else {
                            that.address = 'N/A';
                        }
                    });
                }
                else {
                    that.address = res.address;
                }
                // console.log(that.address);
            });
        }
    };
    PoiPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    PoiPage.prototype.save = function () {
        var _this = this;
        if (this.poi_name == undefined || this.address == undefined) {
            var toast = this.toastCtrl.create({
                message: "POI name is required!",
                duration: 1500,
                position: "bottom"
            });
            toast.present();
        }
        else {
            var payload = {
                "poi": [{
                        "location": {
                            "type": "Point",
                            "coordinates": [
                                this.lng,
                                this.lat
                            ]
                        },
                        "poiname": this.poi_name,
                        "status": "Active",
                        "address": this.address,
                        "user": this.islogin._id
                    }]
            };
            this.apicall.startLoading().present();
            this.apicall.addPOIAPI(payload)
                .subscribe(function (data) {
                _this.apicall.stopLoading();
                var toast = _this.toastCtrl.create({
                    message: "POI added successfully!",
                    duration: 2000,
                    position: 'top'
                });
                toast.present();
                _this.viewCtrl.dismiss();
            }, function (err) {
                _this.apicall.stopLoading();
            });
        }
    };
    PoiPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/live-single-device/poi.html"*/'<!-- <ion-content padding> -->\n\n<div>\n\n    <ion-row>\n\n        <ion-col col-12 style="text-align: center; font-size: 2rem;">\n\n            <b>Add POI</b>&nbsp;&nbsp;&nbsp;\n\n            <ion-icon class="close-button" id="close-button" name="close-circle" (tap)="dismiss()"></ion-icon>\n\n        </ion-col>\n\n        <!-- <ion-col col-2 style="text-align: right">\n\n                <ion-icon class="close-button" id="close-button" name="close-circle" (tap)="dismiss()"></ion-icon>\n\n            </ion-col> -->\n\n    </ion-row><br />\n\n    <!-- <p style="text-align: center; font-size: 2rem;">\n\n            <b>Add POI&nbsp;&nbsp;<ion-icon class="close-button" id="close-button" name="close-circle"\n\n                (tap)="dismiss()"></ion-icon></b>\n\n        </p><br /> -->\n\n    <ion-row>\n\n        <ion-col col-12><b>POI Name:</b></ion-col>\n\n        <ion-col col-12>\n\n            <ion-input type="text" [(ngModel)]="poi_name"></ion-input>\n\n        </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n        <ion-col col-12><b>POI Address:</b></ion-col>\n\n        <ion-col col-12>\n\n            <ion-input type="text" [(ngModel)]="address"></ion-input>\n\n        </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n        <ion-col col-6>\n\n            <button ion-button block (tap)="save()" color="gpsc">SAVE</button>\n\n        </ion-col>\n\n        <ion-col col-6>\n\n            <button ion-button block (tap)="dismiss()" color="gpsc">CANCEL</button>\n\n        </ion-col>\n\n    </ion-row>\n\n</div>\n\n<!-- </ion-content> -->'/*ion-inline-end:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/live-single-device/poi.html"*/,
            selector: 'page-device-settings'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], PoiPage);
    return PoiPage;
}());

//# sourceMappingURL=live-single-device.js.map

/***/ }),

/***/ 431:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return AcReportPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ACDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AcReportPage = /** @class */ (function () {
    function AcReportPage(navCtrl, navParams, apiCAll) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCAll = apiCAll;
        this.portstemp = [];
        this.reportData = [];
        this.twoMonthsLater = __WEBPACK_IMPORTED_MODULE_3_moment__().subtract(2, 'month').format("YYYY-MM-DD");
        this.today = __WEBPACK_IMPORTED_MODULE_3_moment__().format("YYYY-MM-DD");
        //   workingHoursArray=[]
        //  calculateACHours2(data){
        //   console.log("hhhh",data);
        //   var tempArry=[]
        //   var stopHr=0;
        //   var workinHr=0
        //   var result = data.reduce(function (r, a) {
        //         r[a.this.dev_id] = r[a.this.dev_id] || [];
        //         r[a.this.dev_id].push(a);
        //         return r;
        //     }, Object.create(null));
        //   console.log(result);
        //   for(var i=0;i<data.length;i++){
        //     for(var j=0;j<result[data[i]].length;j++){
        //       console.log("1");
        //       // if(j!==result[this.deviceArr[i]].length){
        //          console.log("11",result[data[i]][j]);
        //       if(result[data[i]][j].switch=="OFF"){
        //          console.log("111",(result[data[i]][j].timestamp).replace('Z', '').replace('T', ''));
        //          if(result[data[i]][j+1]){
        //         if((new Date(result[data[i]][j].timestamp).getTime())>(new Date(result[data[i]][j+1].timestamp).getTime())){
        //           workinHr=(new Date(result[data[i]][j].timestamp).getTime()-new Date(result[data[i]][j+1].timestamp).getTime())/1000;
        //           stopHr=(new Date(this.datetimeEnd).getTime()-new Date(this.datetimeStart).getTime())/1000;
        //         }
        //       }
        //       }
        //     // }
        //     }
        //     tempArry.push({deviceName:result[data[i]][j-1].vehicleName,Device_ID:data[i],stopHours:this.secondsToHms(stopHr),workingHours:this.secondsToHms(workinHr)})
        //     stopHr=0;
        //     workinHr=0;
        //   }
        //   this.totalWorkingHours=tempArry
        //   console.log("TEMP==>",tempArry);
        // }
        this.workingHoursArray1 = [];
        this.reportArray = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("email => " + this.islogin._id);
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format(); //new Date(a).toISOString();
    }
    AcReportPage.prototype.ionViewDidEnter = function () {
        console.log('ionViewDidEnter AcReportPage');
    };
    AcReportPage.prototype.ngOnInit = function () {
        this.getdevices();
    };
    AcReportPage.prototype.getAcReporID = function (key) {
        console.log("key: ", key.Device_ID);
        this.dev_id = key.Device_ID;
    };
    AcReportPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = this.apiCAll.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apiCAll.startLoading().present();
        this.apiCAll.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apiCAll.stopLoading();
            _this.portstemp = data.devices;
        }, function (err) {
            _this.apiCAll.stopLoading();
            console.log(err);
        });
    };
    AcReportPage.prototype.calculateHours = function (minutes) {
        var min = minutes / 60;
        var splitString = JSON.stringify(min).split('.');
        var hour = Number(splitString[0]);
        var temp = '0.' + splitString[1];
        var min1 = Number(temp) * 60;
        var a = JSON.stringify(min1).split('.');
        var min2 = Number(a[0]);
        return hour + 'hr ' + min2 + 'min';
    };
    AcReportPage.prototype.getAcReportData = function () {
        var _this = this;
        var that = this;
        this.reportData = [];
        if (that.dev_id == undefined) {
            that.dev_id = "";
        }
        this.apiCAll.startLoading().present();
        ;
        this.apiCAll.getACReportAPI(new Date(that.datetimeStart).toISOString(), new Date(that.datetimeEnd).toISOString(), that.islogin._id, that.dev_id)
            .subscribe(function (data) {
            _this.apiCAll.stopLoading();
            console.log("ac report data: ", data);
            _this.calculateACHours(data[0].s);
            //this.calculateACHours2(data)
            for (var key in data) {
                var payload = {
                    '1stAction': data[key]['1stAction'],
                    '1stTime': _this.calculateHours(data[key]['1stTime']),
                    '2stAction': data[key]['2stAction'],
                    '2stTime': _this.calculateHours(data[key]['2stTime']),
                    'VehicleName': data[key].VehicleName,
                    'imei': data[key].imei,
                    '_id': { 'imei': data[key]._id.imei }
                };
                _this.reportData.push(payload);
            }
            // this.reportData = data;
            // console.log("1stAction: ", data[0]['1stAction'])
        });
    };
    AcReportPage.prototype.calculateACHours = function (data) {
        //throw new Error('Method not implemented.');
        var total = 0;
        console.log(data);
        this.reportArray = [];
        for (var i = 0; i < data.length; i++) {
            if (i !== (data.length - 1)) {
                if (data[0].switch == "OFF") {
                    if (data[i].switch == "OFF" && data[i + 1].switch == "ON") {
                        var date = new Date((data[i].timestamp));
                        var date1 = new Date((data[i + 1].timestamp));
                        console.log("first", date, date1);
                        this.workingHoursArray1.push({ deviceName: data[i].vehicleName, timestamp: this.timeDifference(date, date1) });
                    }
                }
                else {
                    if (data[i].switch == "ON" && data[i + 1].switch == "OFF") {
                        var date = new Date((data[i].timestamp));
                        var date1 = new Date((data[i + 1].timestamp));
                        console.log("second", date, date1);
                        this.workingHoursArray1.push({ deviceName: data[i].vehicleName, timestamp: this.timeDifference(date, date1) });
                    }
                }
            }
            else {
                for (var j = 0; j < this.workingHoursArray1.length; j++) {
                    total = total + this.workingHoursArray1[j].timestamp;
                }
                var time = Math.abs(this.timeDifference(this.datetimeStart, this.datetimeEnd));
                var stopHrs = this.secondsToHms((time - total));
                this.reportArray.push({ deviceName: data[0].vehicleName, workingHours: this.secondsToHms(total), stopHours: stopHrs });
            }
        }
        console.log("JEDHJDH", this.workingHoursArray1, this.reportArray);
    };
    AcReportPage.prototype.timeDifference = function (date1, date2) {
        debugger;
        var seconds = (new Date(date1).getTime() - new Date(date2).getTime()) / 1000;
        return seconds;
    };
    AcReportPage.prototype.secondsToHms = function (d) {
        d = Number(d);
        var h = Math.floor(d / 3600);
        var m = Math.floor(d % 3600 / 60);
        var s = Math.floor(d % 3600 % 60);
        var hDisplay = h > 0 ? h + (h == 1 ? " hour, " : " hours, ") : "";
        var mDisplay = m > 0 ? m + (m == 1 ? " minute, " : " minutes, ") : "";
        var sDisplay = s > 0 ? s + (s == 1 ? " second" : " seconds") : "";
        return hDisplay + mDisplay + sDisplay;
    };
    // workingHoursArray=[]
    // async newMethod(data){
    //   console.log("hhhh",data);
    //   var tempArry=[]
    //   var stopHr=0;
    //   var workinHr=0
    //   var result = await data.reduce(function (r, a) {
    //         r[a.device.Device_ID] = r[a.device.Device_ID] || [];
    //         r[a.device.Device_ID].push(a);
    //         return r;
    //     }, Object.create(null));
    //   console.log(result);
    //   for(var i=0;i<data.length;i++){
    //     for(var j=0;j<result[this.deviceArr[i]].length;j++){
    //       console.log("1");
    //       // if(j!==result[this.deviceArr[i]].length){
    //          console.log("11",result[this.deviceArr[i]][j]);
    //       if(result[this.deviceArr[i]][j].switch=="OFF"){
    //          console.log("111",(result[this.deviceArr[i]][j].timestamp).replace('Z', '').replace('T', ''));
    //          if(result[this.deviceArr[i]][j+1]){
    //         if((new Date(result[this.deviceArr[i]][j].timestamp).getTime())>(new Date(result[this.deviceArr[i]][j+1].timestamp).getTime())){
    //           workinHr=(new Date(result[this.deviceArr[i]][j].timestamp).getTime()-new Date(result[this.deviceArr[i]][j+1].timestamp).getTime())/1000;
    //           stopHr=(new Date(this.todate).getTime()-new Date(this.Fromdate).getTime())/1000;
    //         }
    //       }
    //       }
    //     // }
    //     }
    //     tempArry.push({deviceName:result[this.deviceArr[i]][j-1].vehicleName,Device_ID:this.deviceArr[i],stopHours:this.secondsToHms(stopHr),workingHours:this.secondsToHms(workinHr)})
    //     stopHr=0;
    //     workinHr=0;
    //   }
    //   this.totalWorkingHours=tempArry
    //   console.log("TEMP==>",tempArry);
    // }
    AcReportPage.prototype.showDetail = function (pdata) {
        console.log("pdata: ", pdata);
        var that = this;
        this.navCtrl.push(ACDetailPage, {
            'param': pdata,
            'fdate': that.datetimeStart,
            'tdate': that.datetimeEnd,
            'uid': that.islogin._id
        });
    };
    AcReportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-ac-report',template:/*ion-inline-start:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/ac-report/ac-report.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{ "AC Report" | translate }}</ion-title>\n  </ion-navbar>\n  <ion-item style="background-color: #fafafa;">\n    <ion-label>{{ "Select Vehicle" | translate }}</ion-label>\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name"\n      itemTextField="Device_Name" [canSearch]="true" (onChange)="getAcReporID(selectedVehicle)">\n    </select-searchable>\n  </ion-item>\n\n  <ion-row padding-left padding-right style="background-color: #fafafa;">\n    <ion-col width-20>\n      <ion-label>\n        <span style="font-size: 13px">{{ "From Date" | translate }}</span>\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [min]="twoMonthsLater" [max]="today" [(ngModel)]="datetimeStart"\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;">\n        </ion-datetime>\n      </ion-label>\n    </ion-col>\n\n    <ion-col width-20>\n      <ion-label>\n        <span style="font-size: 13px">{{ "To Date" | translate }}</span>\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeEnd"\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;">\n        </ion-datetime>\n      </ion-label>\n    </ion-col>\n\n    <ion-col width-20>\n      <div style="margin-top: 9px; float: right">\n        <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;" (click)="getAcReportData();">\n        </ion-icon>\n      </div>\n    </ion-col>\n  </ion-row>\n</ion-header>\n\n<ion-content>\n  <ion-list *ngIf="reportData.length === 0">\n    <ion-item>\n      <p>Oops.. No record found..!</p>\n    </ion-item>\n  </ion-list>\n  <div *ngIf="reportData.length > 0">\n    <ion-card *ngFor="let rep of reportArray;" (click)="showDetail(rep)">\n      <ion-item style="border-bottom: 2px solid #dedede;">\n        <ion-avatar item-start>\n          <img src="assets/imgs/car_red_icon.png">\n        </ion-avatar>\n        <ion-label>{{rep.deviceName}}</ion-label>\n      </ion-item>\n\n      <ion-card-content>\n        <ion-row style="padding-top:5px;">\n          <ion-col col-4>Working Hours</ion-col>\n          <ion-col col-8 style="text-align: end;">{{rep.workingHours}}</ion-col>\n        </ion-row>\n        <!-- <ion-row>\n          <ion-col col-6 style="color:gray;font-size:11px;font-weight: 400;text-align:justify;">Action</ion-col>\n          <ion-col col-6 style="color:gray;font-size:11px;font-weight: 400;text-align:justify;">ON Time</ion-col>\n        </ion-row> -->\n        <ion-row>\n          <ion-col col-4>Stopping Hours</ion-col>\n          <ion-col col-8 style="text-align: end;">{{rep.stopHours}}</ion-col>\n        </ion-row>\n        <!-- <ion-row>\n          <ion-col col-6 style="color:gray;font-size:11px;font-weight: 400;text-align:justify;">Action</ion-col>\n          <ion-col col-6 style="color:gray;font-size:11px;font-weight: 400;text-align:justify;">OFF Time</ion-col>\n        </ion-row> -->\n      </ion-card-content>\n    </ion-card>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/ac-report/ac-report.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */]])
    ], AcReportPage);
    return AcReportPage;
}());

var ACDetailPage = /** @class */ (function () {
    function ACDetailPage(apiCall, navParam) {
        this.apiCall = apiCall;
        this.navParam = navParam;
        this.paramData = {};
        this.ddata = [];
        console.log("param parameters: ", this.navParam.get('param'));
        this.paramData = this.navParam.get('param');
        this.fdate = this.navParam.get('fdate');
        this.tdate = this.navParam.get('tdate');
        this.uid = this.navParam.get('uid');
    }
    ACDetailPage.prototype.ngOnInit = function () {
        this.getDetails();
    };
    ACDetailPage.prototype.getDetails = function () {
        var _this = this;
        debugger;
        this.apiCall.startLoading().present();
        this.apiCall.getDetailACReportAPI(new Date(this.fdate).toISOString(), new Date(this.tdate).toISOString(), this.uid, this.paramData._id.imei)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            console.log("detailed ac report data: ", data[0].s);
            _this.ddata = data[0].s;
        });
    };
    ACDetailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/ac-report/ac-detail.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>{{ "Details for" | translate }} {{paramData.VehicleName}}</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n    <ion-card *ngFor="let d of ddata;">\n\n        <ion-card-content>\n\n            <ion-row>\n\n                <ion-col col-6 style="font-size:13px;text-align:justify;">{{d.switch}}</ion-col>\n\n                <ion-col col-6 style="font-size:13px;text-align:justify;">\n\n                    {{d.timestamp | date:\'medium\'}}</ion-col>\n\n            </ion-row>\n\n            <ion-row>\n\n                <ion-col col-6 style="color:gray;font-size:11px;font-weight: 400;text-align:justify;">{{ "ON/OFF" | translate }}</ion-col>\n\n                <ion-col col-6 style="color:gray;font-size:11px;font-weight: 400;text-align:justify;">{{ "Time" | translate }}</ion-col>\n\n            </ion-row>\n\n        </ion-card-content>\n\n    </ion-card>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/ac-report/ac-detail.html"*/,
            styles: ["\n  .col {\n    padding: 0px;\n}"]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], ACDetailPage);
    return ACDetailPage;
}());

//# sourceMappingURL=ac-report.js.map

/***/ }),

/***/ 432:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return UploadDocPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DocPopoverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__view_doc_view_doc__ = __webpack_require__(362);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_social_sharing__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_tinyurl__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_tinyurl___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_tinyurl__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var UploadDocPage = /** @class */ (function () {
    function UploadDocPage(navCtrl, navParams, viewCtrl, platform, toastCtrl, loadingCtrl, apiCall, modalCtrl, popoverCtrl, event) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.platform = platform;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.apiCall = apiCall;
        this.modalCtrl = modalCtrl;
        this.popoverCtrl = popoverCtrl;
        this.event = event;
        this._docTypeList = [];
        this.imgData = {};
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.vehData = this.navParams.get("vehData");
        this.event.subscribe("reloaddoclist", function () {
            _this._docTypeList = [];
            _this.checkDevice();
        });
    }
    UploadDocPage.prototype.ngOnInit = function () {
        this.checkDevice();
    };
    UploadDocPage.prototype._addDocument = function () {
        this.navCtrl.push("AddDocPage", { vehData: this.vehData });
    };
    UploadDocPage.prototype.ionViewDidEnter = function () {
        console.log('ionViewDidEnter UploadDocPage');
    };
    UploadDocPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    UploadDocPage.prototype.onSelectChange = function (selectedValue) {
        console.log('Selected', selectedValue);
        this.docType = selectedValue;
    };
    UploadDocPage.prototype.brows = function () {
        this.docImge = true;
    };
    UploadDocPage.prototype.showToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 2000,
            position: 'bottom'
        });
        toast.present();
    };
    UploadDocPage.prototype.viewDocFunc = function (imgData) {
        // let that = this;
        console.log("imgData: ", imgData);
        if (imgData == undefined) {
            this.showToast('Data not found');
        }
        else {
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__view_doc_view_doc__["a" /* ViewDoc */], {
                param1: imgData
            });
            modal.present();
        }
    };
    UploadDocPage.prototype.checkDevice = function () {
        var _this = this;
        this.apiCall.startLoading();
        this.apiCall.getsingledevice(this.vehData.Device_ID)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            if (data.imageDoc.length > 0) {
                // console.log("imagedoc data: " + JSON.stringify(data.imageDoc))
                for (var i = 0; i < data.imageDoc.length; i++) {
                    _this._docTypeList.push({
                        docname: data.imageDoc[i].doctype,
                        expDate: data.imageDoc[i].docdate ? data.imageDoc[i].docdate : 'N/A',
                        imageURL: data.imageDoc[i].image
                    });
                }
            }
        }, function (err) {
            console.log(err);
            _this.apiCall.stopLoading();
        });
    };
    UploadDocPage.prototype.presentPopover = function (ev, data) {
        var _this = this;
        console.log("populated=> " + JSON.stringify(data));
        var that = this;
        var popover = this.popoverCtrl.create(DocPopoverPage, {
            vehData: that.vehData,
            imgData: data
        }, {
            cssClass: 'contact-popover'
        });
        popover.onDidDismiss(function () {
            _this._docTypeList = [];
            _this.checkDevice();
        });
        popover.present({
            ev: ev
        });
    };
    UploadDocPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-upload-doc',template:/*ion-inline-start:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/add-devices/upload-doc/upload-doc.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>{{ "Documents" | translate }}</ion-title>\n\n    <ion-buttons end>\n\n      <button ion-button icon-only (click)="_addDocument()">\n\n        <ion-icon name="add"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n  <ion-list *ngIf="_docTypeList.length > 0">\n\n    <ion-item *ngFor="let doc of _docTypeList" (click)="viewDocFunc(doc)">\n\n      <ion-avatar item-start>\n\n        <img *ngIf="doc.docname == \'RC\'" src="assets/imgs/rc.png">\n\n        <img *ngIf="doc.docname == \'PUC\'" src="assets/imgs/puc.png">\n\n        <img *ngIf="doc.docname == \'Insurance\'" src="assets/imgs/insurence.png">\n\n        <img *ngIf="doc.docname == \'Licence\'" src="assets/imgs/dl.png">\n\n        <img *ngIf="doc.docname == \'Fitment\'" src="assets/imgs/rc.png">\n\n        <img *ngIf="doc.docname == \'Permit\'" src="assets/imgs/rc.png">\n\n      </ion-avatar>\n\n      <h1>{{doc.docname}}</h1>\n\n      <p>{{ "Expire On*" | translate }} &nbsp;\n\n        <span *ngIf="doc.expDate != \'N/A\'">{{doc.expDate | date:\'mediumDate\'}}</span>\n\n        <span *ngIf="doc.expDate == \'N/A\'">{{doc.expDate}}</span>\n\n      </p>\n\n      <button ion-button item-end small icon-only color="gpsc" (click)="presentPopover($event, doc); $event.stopPropagation()">\n\n        <ion-icon name="more"></ion-icon>\n\n      </button>\n\n    </ion-item>\n\n  </ion-list>\n\n  <ion-card *ngIf="_docTypeList.length == 0">\n\n    <ion-card-content>\n\n      {{ " No doucments uploaded yet... Upload documents of your vehicle by pressing above + button" | translate }}\n\n    </ion-card-content>\n\n  </ion-card>\n\n</ion-content>\n\n\n\n'/*ion-inline-end:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/add-devices/upload-doc/upload-doc.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["PopoverController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"]])
    ], UploadDocPage);
    return UploadDocPage;
}());

var DocPopoverPage = /** @class */ (function () {
    function DocPopoverPage(navParams, modalCtrl, alertCtrl, apiCall, toastCtrl, navCtrl, viewCtrl, socialSharing) {
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.socialSharing = socialSharing;
        this.vehData = {};
        this.imgData = {};
        this.vehData = navParams.get("vehData");
        this.imgData = navParams.get("imgData");
        console.log("popover data=> ", this.imgData);
        var str = this.imgData.imageURL;
        var str1 = str.split('public/');
        this.link = this.apiCall.mainUrl + str1[1];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + this.islogin);
    }
    DocPopoverPage.prototype.ngOnInit = function () { };
    DocPopoverPage.prototype.editItem = function () {
        console.log("edit");
    };
    DocPopoverPage.prototype.deleteDoc = function () {
        var that = this;
        var alert = this.alertCtrl.create({
            message: 'Do you want to delete this Document?',
            buttons: [{
                    text: 'YES PROCEED',
                    handler: function () {
                        console.log(that.vehData.Device_ID);
                        that.deleteDevice(that.vehData.Device_ID);
                    }
                },
                {
                    text: 'NO'
                }]
        });
        alert.present();
    };
    DocPopoverPage.prototype.deleteDevice = function (d_id) {
        var _this = this;
        var that = this;
        var payload = {
            devId: d_id,
            doctype: that.imgData.docname
        };
        this.apiCall.startLoading().present();
        var baseUrl = this.apiCall.mainUrl + "devices/deleteDocuments";
        this.apiCall.urlpasseswithdata(baseUrl, payload)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            var DeletedDevice = data;
            console.log(DeletedDevice);
            var toast = _this.toastCtrl.create({
                message: 'Document deleted successfully!',
                position: 'bottom',
                duration: 2000
            });
            toast.onDidDismiss(function () {
                console.log('Dismissed toast');
                _this.viewCtrl.dismiss();
            });
            toast.present();
        }, function (err) {
            _this.apiCall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = _this.alertCtrl.create({
                title: 'Oops!',
                message: msg.message,
                buttons: ['OK']
            });
            alert.present();
        });
    };
    DocPopoverPage.prototype.shareItem = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_5_tinyurl__["shorten"](this.link).then(function (res) {
            // alert("tinyurl: " + res);
            // that.socialSharing.share(that.islogin.fn + " " + that.islogin.ln + " has shared " + that.vehicleData.Device_Name + " live trip with you. Please follow below link to track", "OneQlik- Live Trip", "", res);
            _this.socialSharing.share(_this.islogin.fn + " " + _this.islogin.ln + " has shared the document with you.", "OneQlik- Vehicle Document", res, "");
        });
    };
    DocPopoverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            template: "\n    <ion-list>\n      <ion-item class=\"text-palatino\" (click)=\"editItem()\">\n        <ion-icon name=\"create\"></ion-icon>&nbsp;&nbsp;Edit\n      </ion-item>\n      <ion-item class=\"text-san-francisco\" (click)=\"deleteDoc()\">\n        <ion-icon name=\"trash\"></ion-icon>&nbsp;&nbsp;Delete\n      </ion-item>\n      <ion-item class=\"text-seravek\" (click)=\"shareItem()\">\n        <ion-icon name=\"share\"></ion-icon>&nbsp;&nbsp;Share\n      </ion-item>\n      \n    </ion-list>\n  "
        })
        // <ion-item id="step1" class="text-seravek" (click)="upload()">
        //        <ion-icon name="cloud-upload"></ion-icon>&nbsp;&nbsp;Docs
        //        </ion-item>
        ,
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_social_sharing__["a" /* SocialSharing */]])
    ], DocPopoverPage);
    return DocPopoverPage;
}());

//# sourceMappingURL=upload-doc.js.map

/***/ }),

/***/ 435:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactUsPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return ShowSupportContactsModal; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_pullup__ = __webpack_require__(78);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_call_number__ = __webpack_require__(112);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ContactUsPage = /** @class */ (function () {
    function ContactUsPage(navCtrl, formBuilder, api, toastCtrl, modalCtrl) {
        this.navCtrl = navCtrl;
        this.formBuilder = formBuilder;
        this.api = api;
        this.toastCtrl = toastCtrl;
        this.modalCtrl = modalCtrl;
        this.contact_data = {};
        this.tickets = [];
        this.showDatePanel = false;
        this.openNum = 0;
        this.closeNum = 0;
        this.inprogressNum = 0;
        this.tempTickets = [];
        this.device_id = [];
        this.device_name = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        //this.datetimeStart = moment({ hours: 0 }).format();
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_4_moment__().startOf('month').format();
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_4_moment__({ hours: 0 }).add(1, 'days').format();
        //this.datetimeEnd = moment().format();
        //new Date(a).toISOString();
        this.contactusForm = formBuilder.group({
            name: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            mail: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].email],
            mobno: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            note: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required]
        });
    }
    ContactUsPage_1 = ContactUsPage;
    ContactUsPage.prototype.ionViewDidEnter = function () {
        console.log('ionViewDidEnter ContactUsPage');
    };
    ContactUsPage.prototype.ngOnInit = function () {
        this.viewTicket();
        this.getdevices();
    };
    ContactUsPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = this.api.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        //this.api.startLoading().present();
        this.api.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            //this.api.stopLoading();
            _this.devices = data;
            _this.portstemp = data.devices;
        }, function (err) {
            _this.api.stopLoading();
            console.log(err);
        });
    };
    ContactUsPage.prototype.call = function (num) {
        // this.callNumber.callNumber(num, true)
        //   .then(res => console.log('Launched dialer!', res))
        //   .catch(err => console.log('Error launching dialer', err));
    };
    ContactUsPage.prototype.footerExpanded = function () {
        console.log('Footer expanded!');
    };
    ContactUsPage.prototype.footerCollapsed = function () {
        console.log('Footer collapsed!');
    };
    ContactUsPage.prototype.contactUs = function () {
        var _this = this;
        this.submitAttempt = true;
        if (this.contactusForm.valid) {
            var payData = {
                "user": this.islogin._id,
                // "email": this.contactusForm.value.mail,
                "msg": this.contactusForm.value.note,
                "fileName": "sanskarvts.json",
                //"fileName":"sanskarvts.json",
                "DeviceIMEI": this.device_id,
                "VehicleNo": this.device_name,
                "mobile": this.contactusForm.value.mobno,
                "email": this.contactusForm.value.mail
            };
            this.api.startLoading().present();
            // this.api.contactusApi(this.contact_data)
            this.api.createTicketApi(payData)
                .subscribe(function (data) {
                _this.api.stopLoading();
                console.log(data.message);
                if (data.message == 'saved response') {
                    var toast = _this.toastCtrl.create({
                        // message: 'Your request has been submitted successfully. We will get back to you soon.',
                        message: 'Your ticket has been submitted successfully. We will get back to you soon.',
                        position: 'bottom',
                        duration: 3000
                    });
                    toast.present();
                    _this.navCtrl.setRoot(ContactUsPage_1);
                }
                else {
                    var toast = _this.toastCtrl.create({
                        message: 'Something went wrong. Please try after some time.',
                        position: 'bottom',
                        duration: 3000
                    });
                    toast.onDidDismiss(function () {
                        console.log('Dismissed toast');
                        _this.navCtrl.setRoot(ContactUsPage_1);
                    });
                    toast.present();
                }
            }, function (error) {
                _this.api.stopLoading();
                console.log(error);
            });
        }
    };
    ContactUsPage.prototype.getSummaarydevice = function (selectedVehicle) {
        this.device_id = [];
        this.device_name = [];
        if (selectedVehicle.length > 0) {
            if (selectedVehicle.length > 1) {
                for (var t = 0; t < selectedVehicle.length; t++) {
                    this.device_id.push(selectedVehicle[t].Device_ID);
                    this.device_name.push(selectedVehicle[t].Device_Name);
                }
            }
            else {
                this.device_id.push(selectedVehicle[0].Device_ID);
                this.device_name.push(selectedVehicle[0].Device_Name);
            }
        }
        else
            return;
        console.log("selectedVehicle=> ", this.device_id);
    };
    ContactUsPage.prototype.addTicket = function () {
        this.footerState = this.footerState == __WEBPACK_IMPORTED_MODULE_5_ionic_pullup__["a" /* IonPullUpFooterState */].Collapsed ? __WEBPACK_IMPORTED_MODULE_5_ionic_pullup__["a" /* IonPullUpFooterState */].Expanded : __WEBPACK_IMPORTED_MODULE_5_ionic_pullup__["a" /* IonPullUpFooterState */].Collapsed;
    };
    ContactUsPage.prototype.viewTicket = function () {
        var _this = this;
        var _bUrl = this.api.mainUrl + "customer_support/getCustomerQuery";
        var payload = {
            "draw": 1,
            "columns": [
                {
                    "data": "_id"
                },
                {
                    "data": "superAdmin"
                },
                {
                    "data": "dealer"
                },
                {
                    "data": "ticketId"
                },
                {
                    "data": "message"
                },
                {
                    "data": "assigned_to"
                },
                {
                    "data": "assigned_to.first_name"
                },
                {
                    "data": "assigned_to.last_name"
                },
                {
                    "data": "support_status"
                },
                {
                    "data": "posted_on"
                },
                {
                    "data": "posted_by"
                },
                {
                    "data": "posted_by.first_name"
                },
                {
                    "data": "posted_by.last_name"
                },
                {
                    "data": "posted_by.phone"
                },
                {
                    "data": "posted_by.email"
                },
                {
                    "data": "role"
                },
                {
                    "data": "user"
                }
            ],
            "order": [
                {
                    "column": 3,
                    "dir": "desc",
                }
            ],
            "start": 0,
            "length": 50,
            "search": {
                "value": "",
                "regex": false
            },
            "op": {},
            "select": [],
            "find": {
                "posted_by": this.islogin._id,
                "posted_on": {
                    "$gte": {
                        "_eval": "date",
                        "value": new Date(this.datetimeStart).toISOString()
                    },
                    "$lte": {
                        "_eval": "date",
                        "value": new Date(this.datetimeEnd).toISOString()
                    }
                }
            }
        };
        this.tickets = [];
        this.openNum = 0;
        this.closeNum = 0;
        this.inprogressNum = 0;
        this.api.startLoading().present();
        this.api.urlpasseswithdata(_bUrl, payload)
            .subscribe(function (data) {
            _this.api.stopLoading();
            console.log("tickets: ", data);
            _this.tickets = data.data;
            _this.tempTickets = data.data;
            for (var i = 0; i < data.data.length; i++) {
                if (data.data[i].support_status == 'OPEN') {
                    _this.openNum += 1;
                }
                else if (data.data[i].support_status == 'CLOSE') {
                    _this.closeNum += 1;
                }
                else if (data.data[i].support_status == 'IN PROGRESS') {
                    _this.inprogressNum += 1;
                }
            }
        }, function (err) {
            _this.api.stopLoading();
            console.log("getting err while getting data: ", err);
        });
    };
    ContactUsPage.prototype.loadContent = function (key) {
        if (key === 'PROGRESS') {
            this.tickets = [];
            for (var i = 0; i < this.tempTickets.length; i++) {
                if (this.tempTickets[i].support_status === 'IN PROGRESS') {
                    this.tickets.push(this.tempTickets[i]);
                }
            }
        }
        else if (key === 'OPEN') {
            this.tickets = [];
            for (var r = 0; r < this.tempTickets.length; r++) {
                if (this.tempTickets[r].support_status === 'OPEN') {
                    this.tickets.push(this.tempTickets[r]);
                }
            }
        }
        else if (key === 'CLOSE') {
            this.tickets = [];
            for (var v = 0; v < this.tempTickets.length; v++) {
                if (this.tempTickets[v].support_status === 'CLOSE') {
                    this.tickets.push(this.tempTickets[v]);
                }
            }
        }
    };
    ContactUsPage.prototype.onClickChat = function () {
        var customTModal = this.modalCtrl.create(ShowSupportContactsModal);
        customTModal.present();
        customTModal.onDidDismiss(function (param) {
        });
    };
    ContactUsPage = ContactUsPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-contact-us',template:/*ion-inline-start:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/contact-us/contact-us.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{ "Contact Us" | translate }}</ion-title>\n  </ion-navbar>\n  <ion-row padding-left padding-right style="background-color: #fafafa;">\n    <ion-col width-20>\n      <ion-label>\n        <span style="font-size: 13px">{{ "From Date" | translate }}</span>\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeStart"\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;">\n        </ion-datetime>\n      </ion-label>\n    </ion-col>\n\n    <ion-col width-20>\n      <ion-label>\n        <span style="font-size: 13px">{{ "To Date" | translate }}</span>\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeEnd"\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;">\n        </ion-datetime>\n      </ion-label>\n    </ion-col>\n\n    <ion-col width-20>\n      <div style="margin-top: 9px; float: right">\n        <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;" (click)="viewTicket()">\n        </ion-icon>\n      </div>\n    </ion-col>\n  </ion-row>\n</ion-header>\n\n<ion-content padding-bottom>\n  <ion-row style="margin-top: 33%">\n    <ion-col col-4 tappable (click)="loadContent(\'OPEN\')">\n      <ion-row>\n        <ion-col>\n          <div\n            style="width:75px; height: 75px; border-radius: 48%; background: rgb(238, 100, 69); text-align: center;margin: auto; padding: 25px; font-size: 1.5em; color: white;">\n            <b>{{ openNum }}</b>\n          </div>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col style="text-align: center;">\n          {{ "OPEN" | translate }}\n        </ion-col>\n      </ion-row>\n    </ion-col>\n\n    <ion-col col-4 tappable (click)="loadContent(\'PROGRESS\')">\n      <ion-row>\n        <ion-col>\n          <div\n            style="width:75px; height: 75px; border-radius: 48%; background: rgb(233, 182, 16); text-align: center;margin: auto; padding: 25px; font-size: 1.5em; color: white;">\n            <b>{{ inprogressNum }}</b>\n          </div>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col style="text-align: center;">\n          {{ "IN PROGRESS" | translate }}\n        </ion-col>\n      </ion-row>\n    </ion-col>\n\n    <ion-col col-4 tappable (click)="loadContent(\'CLOSE\')">\n      <ion-row>\n        <ion-col>\n          <div\n            style="width:75px; height: 75px; border-radius: 48%; background: rgb(132, 231, 45); text-align: center;margin: auto; padding: 25px; font-size: 1.5em; color: white;">\n            <b>{{ closeNum }}</b>\n          </div>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col style="text-align: center;">\n          {{ "CLOSE" | translate }}\n        </ion-col>\n      </ion-row>\n    </ion-col>\n\n  </ion-row>\n  <div *ngIf="tickets.length > 0">\n    <ion-card *ngFor="let t of tickets">\n      <ion-card-content style="background: transparent;">\n        <p style="margin: 0px; padding: 0px; color: black">\n          <b>{{ \'Ticket ID\' | translate}}: </b>{{ t.ticketId ?t.ticketId : \'NA\'  }}\n        </p>\n        <ion-row>\n          <ion-col col-12 style="padding-left: 0px">\n            <ion-icon name="person"></ion-icon>&nbsp;\n            {{ t.posted_by.first_name }}\n            {{ t.posted_by.last_name }}&nbsp;&nbsp;<ion-icon name="call" color="gpsc"></ion-icon>\n          </ion-col>\n\n        </ion-row>\n        <ion-row>\n\n          <ion-col col-6 style="padding-left: 0px">\n            <ion-icon name="calendar"></ion-icon>&nbsp;&nbsp;{{\n              t.posted_on | date: "short"\n            }}\n          </ion-col>\n          <ion-col col-6 style="text-align: right">\n            <ion-badge [ngClass]="{\n                myClass: t.support_status == \'IN PROGRESS\',\n                myClass2: t.support_status == \'OPEN\',\n                myClass3: t.support_status == \'CLOSE\'\n              }">{{ t.support_status }}</ion-badge>\n          </ion-col>\n        </ion-row>\n\n        <p style="margin: 0px; padding: 0px; color: black">\n          <b>{{ \'Issue\' | translate}}: </b>{{ t.message }}\n        </p>\n      </ion-card-content>\n    </ion-card>\n  </div>\n\n</ion-content>\n\n<ion-fab bottom right *ngIf="!islogin.isSuperAdmin">\n  <button ion-fab (click)="addTicket()" color="gpsc">\n    <ion-icon name="add" *ngIf="footerState == 0"></ion-icon>\n    <ion-icon name="close" *ngIf="footerState == 1"></ion-icon>\n  </button>\n</ion-fab>\n<ion-fab bottom right style="bottom: 82px;" *ngIf="!islogin.isSuperAdmin">\n  <button ion-fab (click)="onClickChat()" color="light">\n    <img src="assets/imgs/telemarketer.png" />\n    <!-- <ion-icon name="add" *ngIf="footerState == 0"></ion-icon> -->\n    <!-- <ion-icon name="close" *ngIf="footerState == 1"></ion-icon> -->\n  </button>\n</ion-fab>\n<ion-pullup #pullup (onExpand)="footerExpanded()" (onCollapse)="footerCollapsed()" [(state)]="footerState">\n  <ion-content>\n\n    <ion-item style="height: 45px; background-color: #fafafa;">\n      <ion-label>{{ "Select Vehicle" | translate }}</ion-label>\n      <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name"\n        itemTextField="Device_Name" [isMultiple]="true" [canSearch]="true"\n        (onChange)="getSummaarydevice(selectedVehicle)">\n      </select-searchable>\n    </ion-item>\n\n    <form class="form" [formGroup]="contactusForm" style="margin-top: -15px;">\n      <p class="para" type="Name:">\n        <input formControlName="name" type="text" placeholder="Write your name here.." />\n      </p>\n      <span class="span"\n        *ngIf="!contactusForm.controls.name.valid && submitAttempt">{{ \'Name is required and should be in valid format!\' | translate}}</span>\n      <p class="para" type="Email:">\n        <input formControlName="mail" type="text" placeholder="Let us know how to contact you back.." />\n      </p>\n      <span class="span"\n        *ngIf="!contactusForm.controls.mail.valid && submitAttempt">{{ \'Email id is required and should be in valid format!\' | translate }}</span>\n      <p class="para" type="Mobile Num.:">\n        <input formControlName="mobno" type="number" maxlength="10" minlength="10"\n          placeholder="{{ \'Let us know how to contact you back via mobile number..\' | translate}}" />\n      </p>\n      <p class="para" type="Message:">\n        <textarea rows="2" cols="50" formControlName="note"\n          placeholder="{{ \'What would you like to tell us..\' | translate }}"></textarea>\n      </p>\n      <span class="span"\n        *ngIf="!contactusForm.controls.note.valid && submitAttempt">{{ \'Please write your message!\' | translate}}</span>\n      <ion-row>\n        <ion-col col-12><button (tap)="contactUs()" class="buttonclass">\n            {{ \'Raise Ticket\' | translate}}\n          </button></ion-col>\n      </ion-row>\n    </form>\n  </ion-content>\n</ion-pullup>\n'/*ion-inline-end:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/contact-us/contact-us.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"]])
    ], ContactUsPage);
    return ContactUsPage;
    var ContactUsPage_1;
}());

var ShowSupportContactsModal = /** @class */ (function () {
    function ShowSupportContactsModal(viewCtrl, apiCall, callNumber) {
        var _this = this;
        this.viewCtrl = viewCtrl;
        this.apiCall = apiCall;
        this.callNumber = callNumber;
        this.islogin = {};
        this.supportArray = [];
        this.serviceArray = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        var b_url = this.apiCall.mainUrl + "users/get_user_setting";
        var Var = { uid: this.islogin._id };
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(b_url, Var)
            .subscribe(function (resp) {
            _this.apiCall.stopLoading();
            console.log("check lang key: ", resp);
            if (resp.Service != undefined) {
                if (resp.Service.length > 0) {
                    _this.serviceArray = resp.Service;
                }
            }
            if (resp.Support != undefined) {
                if (resp.Support.length > 0) {
                    _this.supportArray = resp.Support;
                }
            }
        }, function (err) {
            console.log(err);
            _this.apiCall.stopLoading();
        });
    }
    ShowSupportContactsModal.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    ShowSupportContactsModal.prototype.call_num = function (number) {
        this.callNumber.callNumber(number.toString(), true)
            .then(function (res) { return console.log('Launched dialer!', res); })
            .catch(function (err) { return console.log('Error launching dialer', err); });
    };
    ShowSupportContactsModal = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-show-support-contacts',template:/*ion-inline-start:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/contact-us/show-support-contacts.html"*/'\n\n    <div padding style="background: white;height: auto; width: 90%; margin: auto; border-radius: 5px;">\n\n        <ion-row>\n\n          <ion-col col-12>\n\n            <p style="text-align: center; font-size: 2rem;">\n\n              <b>{{\'Contacts\' | translate}}</b>\n\n             &nbsp;&nbsp;&nbsp; <ion-icon class="close-button" id="close-button" name="close-circle" (tap)="dismiss()"></ion-icon>\n\n            </p>\n\n          </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-list>\n\n            <ion-list-header>\n\n                Support\n\n              </ion-list-header>\n\n            <ion-item *ngFor="let sup of supportArray" (click)="call_num(sup)">\n\n              <ion-icon name="call" item-start></ion-icon>\n\n                {{sup}}\n\n            </ion-item>\n\n            \n\n            <ion-list-header>\n\n                Sales & Services\n\n            </ion-list-header>\n\n            <ion-item *ngFor="let item of serviceArray" (click)="call_num(item)">\n\n              <ion-icon name="call" item-start></ion-icon>\n\n              {{item}}\n\n            </ion-item>\n\n          </ion-list>\n\n      </div>\n\n    '/*ion-inline-end:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/contact-us/show-support-contacts.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"], __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_call_number__["a" /* CallNumber */]])
    ], ShowSupportContactsModal);
    return ShowSupportContactsModal;
}());

//# sourceMappingURL=contact-us.js.map

/***/ }),

/***/ 436:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReportSettingModal; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ReportSettingModal = /** @class */ (function () {
    function ReportSettingModal(viewCtrl, apiCall, toastCtrl, navParams) {
        this.viewCtrl = viewCtrl;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.reportArray = [
            { name: "Daily Report", Rstatus: true, Astatus: false, value: "daily_report" },
            { name: "Daywise Report", Rstatus: true, Astatus: false, value: "daywise_report" },
            { name: "Speed Variation", Rstatus: true, Astatus: false, value: "speed_variation" },
            { name: "Fuel Report", Rstatus: true, Astatus: false, value: "fuel_report" },
            { name: "Idle Report", Rstatus: true, Astatus: false, value: "idle_report" },
            { name: "Fuel Consumption Report", Rstatus: true, Astatus: false, value: "fuel_consumption_report" },
            { name: "Trip Report", Rstatus: true, Astatus: false, value: "trip_report" },
            { name: "Travel Path Report", Rstatus: true, Astatus: false, value: "travel_path_report" },
            { name: "Summary Report", Rstatus: true, Astatus: false, value: "summary_report" },
            { name: "Geofence Report", Rstatus: true, Astatus: false, value: "geofence_report" },
            { name: "Overspeed Report", Rstatus: true, Astatus: false, value: "overspeed_report" },
            { name: "Route Violation Report", Rstatus: true, Astatus: false, value: "route_violation_report" },
            { name: "Stoppage Report", Rstatus: true, Astatus: false, value: "stoppage_report" },
            { name: "Ignition Report", Rstatus: true, Astatus: false, value: "ignition_report" },
            { name: "Distance Report", Rstatus: true, Astatus: false, value: "distance_report" },
            { name: "POI Report", Rstatus: true, Astatus: false, value: "poi_report" },
            { name: "SOS Report", Rstatus: true, Astatus: false, value: "sos_report" },
            { name: "AC Report", Rstatus: true, Astatus: false, value: "ac_report" },
            { name: "Driver Performance Report", Rstatus: true, Astatus: false, value: "driver_performance_report" },
            { name: "User Trip Report", Rstatus: true, Astatus: false, value: "user_trip_report" },
            { name: "Alert Report", Rstatus: true, Astatus: false, value: "alert_report" }
        ];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.params = navParams.get('param');
        console.log("navparams: ", this.params);
        this.getCostumerDetail();
    }
    ReportSettingModal.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    ReportSettingModal.prototype.submit = function () {
        var _this = this;
        var report_preference_payload = [];
        report_preference_payload = JSON.parse(JSON.stringify(this.reportArray));
        var tempsemp = {};
        console.log(report_preference_payload);
        for (var index in report_preference_payload) {
            var k = report_preference_payload[index].value;
            if (report_preference_payload[index].Rstatus == false) {
                report_preference_payload[index].Astatus = false;
            }
            tempsemp[k] = report_preference_payload[index];
            delete tempsemp[k].name;
            delete tempsemp[k].value;
        }
        console.log(tempsemp);
        var reportPref = {
            reportsArr: tempsemp,
            id: this.params._id
        };
        var url = this.apiCall.mainUrl + "users/reportPrefrence";
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(url, reportPref).subscribe(function (res) {
            console.log("check status: ", res);
            _this.apiCall.stopLoading();
            _this.toastCtrl.create({
                message: 'Report preference updated',
                duration: 1000,
                position: 'top'
            }).present();
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log("error", err);
        });
    };
    ReportSettingModal.prototype.selectedStat = function (ind, ev, flag) {
        if (flag == 'rVisibility') {
            this.reportArray[ind].Rstatus = ev.checked;
        }
        // console.log('this.reportArray', this.reportArray);
    };
    ReportSettingModal.prototype.getCostumerDetail = function () {
        var that = this;
        this.apiCall.getcustToken(this.params._id).subscribe(function (res) {
            // console.log("resresresresresresresresres", res.cust.report_preference);
            var tempReport = res.cust.report_preference;
            var _loop_1 = function (dt) {
                var arr = that.reportArray.filter(function (d) {
                    return d.value == dt;
                });
                arr[0].Astatus = tempReport[dt].Astatus;
                arr[0].Rstatus = tempReport[dt].Rstatus;
            };
            for (var dt in tempReport) {
                _loop_1(dt);
            }
            // console.log("that.reportArray=>", that.reportArray);
        });
    };
    ReportSettingModal = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({template:/*ion-inline-start:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/customers/modals/report-setting/report-setting.html"*/'<ion-content style="background-color: rgba(0, 0, 0, 0.7);">\n\n    <div class="mainDiv">\n\n        <div class="secondDiv">\n\n            <ion-row>\n\n                <ion-col col-12 text-center>\n\n                    <h5 style="margin-top: -10px;">REPORT PREFERENCE</h5>\n\n                </ion-col>\n\n            </ion-row>\n\n            <ion-list radio-group [(ngModel)]="min_time">\n\n                <ion-item *ngFor="let item of reportArray; let i = index">\n\n                    <ion-label>{{item.name}}</ion-label>\n\n                    <!-- <md-checkbox type="checkbox" [checked]="devStatus.Rstatus"\n\n                        (change)="selectedStatus(i,$event,\'rVisibility\')"></md-checkbox> -->\n\n                    <ion-checkbox item-end color="gpsc" [checked]="item.Rstatus"\n\n                        (ionChange)="selectedStat(i,$event,\'rVisibility\')"></ion-checkbox>\n\n                </ion-item>\n\n            </ion-list>\n\n            <ion-row>\n\n                <ion-col col-6>\n\n                    <div style="padding: 5px;"><button ion-button (click)="submit()" color="gpsc" block>Submit</button>\n\n                    </div>\n\n\n\n                </ion-col>\n\n                <ion-col col-6>\n\n                    <div style="padding: 5px;"><button ion-button (click)="dismiss()" color="gpsc" block>Dismiss</button>\n\n                    </div>\n\n                </ion-col>\n\n            </ion-row>\n\n        </div>\n\n    </div>\n\n</ion-content>'/*ion-inline-end:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/customers/modals/report-setting/report-setting.html"*/,
            styles: ["\n    .mainDiv {\n        padding: 50px;\n        \n    }\n\n    .secondDiv {\n        padding-top: 20px;\n        border-radius: 18px;\n        border: 2px solid black;\n        background: white;\n    }"]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["NavParams"]])
    ], ReportSettingModal);
    return ReportSettingModal;
}());

//# sourceMappingURL=report-setting.js.map

/***/ }),

/***/ 437:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FilterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var FilterPage = /** @class */ (function () {
    function FilterPage(apiCall, viewCtrl, translate
    // private datePicker: DatePicker
    ) {
        this.apiCall = apiCall;
        this.viewCtrl = viewCtrl;
        this.translate = translate;
        this.modal = {};
        this.filterList = [
            { filterID: 1, filterName: this.translate.instant('Overspeeding'), filterValue: 'overspeed', checked: false },
            { filterID: 2, filterName: this.translate.instant('Ignition'), filterValue: 'IGN', checked: false },
            { filterID: 3, filterName: "Route", filterValue: 'Route', checked: false },
            { filterID: 4, filterName: this.translate.instant('GEOFENCE'), filterValue: 'Geo-Fence', checked: false },
            { filterID: 5, filterName: this.translate.instant('Stoppage'), filterValue: 'MAXSTOPPAGE', checked: false },
            { filterID: 6, filterName: this.translate.instant('Fuel'), filterValue: 'Fuel', checked: false },
            { filterID: 7, filterName: this.translate.instant("AC"), filterValue: 'AC', checked: false },
            { filterID: 8, filterName: "Route POI", filterValue: 'route-poi', checked: false },
            { filterID: 9, filterName: this.translate.instant('Power'), filterValue: 'power', checked: false },
            { filterID: 10, filterName: this.translate.instant('Immobilize'), filterValue: 'immo', checked: false },
            { filterID: 11, filterName: "Parking", filterValue: 'theft', checked: false },
            { filterID: 12, filterName: this.translate.instant('SOS'), filterValue: 'SOS', checked: false },
            { filterID: 13, filterName: 'Reminder', filterValue: 'Reminder', checked: false },
            { filterID: 14, filterName: 'Toll', filterValue: 'toll', checked: false },
            { filterID: 15, filterName: 'MAX Idle', filterValue: 'MAXIDLE', checked: false },
            { filterID: 16, filterName: 'MAX Stoppage', filterValue: 'MAXSTOPPAGE', checked: false },
            { filterID: 17, filterName: 'Vibration', filterValue: 'Vibration', checked: false },
            { filterID: 18, filterName: 'Harsh Break', filterValue: 'harshbreak', checked: false },
            { filterID: 19, filterName: 'Harsh ACC', filterValue: 'harshacc', checked: false },
            { filterID: 20, filterName: 'Low Battery', filterValue: 'lowbattery', checked: false },
            { filterID: 21, filterName: 'ACC Alarm', filterValue: 'accalarm', checked: false },
            { filterID: 22, filterName: 'Info', filterValue: 'Info', checked: false },
        ];
        this.selectedArray = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + this.islogin);
        if (localStorage.getItem("checkedList") != null) {
            this.checkedData = JSON.parse(localStorage.getItem("checkedList"));
            console.log("checked data=> " + this.checkedData);
        }
    }
    FilterPage.prototype.ngOnInit = function () {
        // this.getNotifTypes()
    };
    FilterPage.prototype.getNotifTypes = function () {
        var _this = this;
        this.apiCall.startLoading();
        this.apiCall.getVehicleListCall(this.apiCall.mainUrl + "notifs/getTypes")
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            console.log("notif type list: ", data);
            _this.filterList = data;
        });
    };
    FilterPage.prototype.applyFilter = function () {
        localStorage.setItem("checkedList", JSON.stringify(this.selectedArray));
        if (this.selectedArray.length == 0) {
            localStorage.setItem("dates", "dates");
            this.viewCtrl.dismiss(this.modal);
        }
        else {
            localStorage.setItem("types", "types");
            this.viewCtrl.dismiss(this.selectedArray);
        }
    };
    FilterPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    FilterPage.prototype.selectMember = function (data) {
        console.log("selected=> " + JSON.stringify(data));
        if (data.checked == true) {
            this.selectedArray.push(data);
        }
        else {
            var newArray = this.selectedArray.filter(function (el) {
                return el.filterID !== data.filterID;
            });
            this.selectedArray = newArray;
        }
    };
    FilterPage.prototype.cancel = function () {
        console.log("do nothing");
        this.viewCtrl.dismiss();
    };
    FilterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-filter',template:/*ion-inline-start:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/all-notifications/filter/filter.html"*/'<ion-list>\n\n    <ion-item-group>\n\n        <ion-item-divider color="light">{{ "Filter Type" | translate }}</ion-item-divider>\n\n        <div *ngIf="filterList.length > 0">\n\n            <ion-item *ngFor="let member of filterList">\n\n                <ion-label>{{member?.filterName || \'N/A\'}}</ion-label>\n\n                <ion-checkbox color="danger" (click)="selectMember(member)" [(ngModel)]="member.checked"></ion-checkbox>\n\n            </ion-item>\n\n        </div>\n\n    </ion-item-group>\n\n</ion-list>\n\n<ion-row style="background-color: darkgray">\n\n    <ion-col width-50 style="text-align: center; background-color: #f53d3d;">\n\n        <button ion-button clear small color="light" (click)="cancel()">{{ "Cancel" | translate }}</button>\n\n    </ion-col>\n\n    <ion-col width-50 style="text-align: center; background-color: #11b700;">\n\n        <button ion-button clear small color="light" (click)="applyFilter()">{{ "Apply Filter" | translate }}</button>\n\n    </ion-col>\n\n</ion-row>\n\n'/*ion-inline-end:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/all-notifications/filter/filter.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["c" /* TranslateService */]
            // private datePicker: DatePicker
        ])
    ], FilterPage);
    return FilterPage;
}());

//# sourceMappingURL=filter.js.map

/***/ }),

/***/ 440:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DealerPermModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DealerPermModalPage = /** @class */ (function () {
    function DealerPermModalPage(navParams, viewCtrl, apiCall, toastCtrl) {
        this.viewCtrl = viewCtrl;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.dev_permission = true;
        this.cust_permission = true;
        this.dealerData = navParams.get('param');
        console.log("dealer's data: ", this.dealerData);
        if (this.dealerData.device_add_permission != undefined && this.dealerData.cust_add_permission != undefined) {
            this.dev_permission = this.dealerData.device_add_permission;
            this.cust_permission = this.dealerData.cust_add_permission;
        }
    }
    DealerPermModalPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    DealerPermModalPage.prototype.submit = function () {
        var _this = this;
        console.log("changed values: ", this.dev_permission);
        console.log("changed values: ", this.cust_permission);
        var url = this.apiCall.mainUrl + "users/setDealerPermission";
        var payload = {
            "device_add_permission": this.dev_permission,
            "cust_add_permission": this.cust_permission,
            "uid": this.dealerData._id
        };
        this.apiCall.urlpasseswithdata(url, payload).subscribe(function (respData) {
            console.log("response data check: ", respData);
            if (respData.message) {
                _this.toastCtrl.create({
                    message: respData.message,
                    duration: 1500,
                    position: 'middle'
                }).present();
                _this.dismiss();
            }
            else {
                _this.toastCtrl.create({
                    message: 'Something went wrong.. Please try after some time.',
                    duration: 1500,
                    position: 'middle'
                }).present();
                _this.dismiss();
            }
        });
    };
    DealerPermModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            // selector: 'page-modal',
            template: "\n    <div class=\"mainDiv\">\n        <div class=\"secondDiv\">\n        <div text-center>\n            <h5>Dealer Permission</h5>\n            </div>\n            <ion-list>\n                <ion-item>\n                    <ion-label>Add Vehicle</ion-label>\n                    <ion-toggle [(ngModel)]=\"dev_permission\"></ion-toggle>\n                </ion-item>\n                <ion-item>\n                    <ion-label>Add Customer</ion-label>\n                    <ion-toggle [(ngModel)]=\"cust_permission\"></ion-toggle>\n                </ion-item>\n            </ion-list>\n            <ion-row>\n                <ion-col col-6>\n                    <button ion-button (click)=\"submit()\" color=\"gpsc\" block>Submit</button>\n                </ion-col>\n                <ion-col col-6>\n                    <button ion-button (click)=\"dismiss()\" color=\"grey\" block>Dismiss</button>\n                </ion-col>\n            </ion-row>\n        </div>\n    </div>\n    ",
            styles: ["\n      \n            .mainDiv {\n                padding: 50px;\n                height: 100%;\n                background-color: rgba(0, 0, 0, 0.7);\n            }\n\n            .secondDiv {\n                padding-top: 20px;\n                border-radius: 18px;\n                border: 2px solid black;\n                background: white;\n            }\n        \n    "]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], DealerPermModalPage);
    return DealerPermModalPage;
}());

//# sourceMappingURL=dealer-perm.js.map

/***/ }),

/***/ 441:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddPointsModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AddPointsModalPage = /** @class */ (function () {
    function AddPointsModalPage(navParams, viewCtrl, apiCall, toastCtrl) {
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.min_time = "10";
        this.perunitprice = 0;
        this.pointShareCount = 0;
        this.params = {};
        this.totAllocatedPoints = 0;
        this.totAvailablePoints = 0;
        this.totalAmount = 0;
        this.params = this.navParams.get("params");
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        // debugger
        if (this.params.point_Allocated) {
            this.totAllocatedPoints = this.params.point_Allocated;
        }
    }
    AddPointsModalPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss(this.min_time);
    };
    AddPointsModalPage.prototype.ionViewDidEnter = function () {
        this.checkPointsAvailability();
    };
    AddPointsModalPage.prototype.checkPointsAvailability = function () {
        var _this = this;
        var url = this.apiCall.mainUrl + "users/checkPointAvailablity?u_id=" + this.islogin._id + "&demanded_points=0";
        this.apiCall.getSOSReportAPI(url)
            .subscribe(function (res) {
            // console.log("points availability: ", res);
            if ((res.available_points == undefined) || (res.available_points < 0)) {
                _this.totAvailablePoints = 0;
            }
            else {
                _this.totAvailablePoints = res.available_points ? res.available_points : 0;
            }
        }, function (err) {
            console.log("error ", err);
        });
    };
    AddPointsModalPage.prototype.calculateTotal = function () {
        this.totalAmount = this.pointShareCount * this.perunitprice;
        // console.log("calculated amount: ", this.totalAmount)
    };
    AddPointsModalPage.prototype.proceed = function () {
        var _this = this;
        var _bUrl = this.apiCall.mainUrl + 'points/addpointAllocation';
        var insertPoint = {
            by: this.islogin._id,
            to: this.params._id,
            points: this.pointShareCount,
            rate: this.perunitprice,
            totalAmount: (this.pointShareCount * this.perunitprice),
            Payment_type: this.payment_stauts,
            created_date: new Date().toISOString(),
        };
        if (this.pointShareCount > this.totAvailablePoints) {
            this.errmsg = "Insufficient Points";
            this.showToast(this.errmsg);
            return;
        }
        else {
            if ((this.pointShareCount == undefined) || (this.payment_stauts == undefined)) {
                this.errmsg = "All Fields are mendatory";
                this.showToast(this.errmsg);
                return;
            }
            else {
                this.apiCall.startLoading().present();
                this.apiCall.urlpasseswithdata(_bUrl, insertPoint).subscribe(function (resp) {
                    _this.apiCall.stopLoading();
                    console.log("get response: ", resp);
                    if (resp.message) {
                        _this.showToast(resp.message);
                        _this.dismiss();
                    }
                }, function (err) {
                    _this.apiCall.stopLoading();
                    console.log("error ", err);
                });
            }
        }
    };
    AddPointsModalPage.prototype.showToast = function (msg) {
        this.toastCtrl.create({
            message: msg,
            duration: 1500,
            position: 'middle'
        }).present();
    };
    AddPointsModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            // selector: 'page-modal',
            //template:/*ion-inline-start:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/dealers/add-points-modal.html"*/'<div class="mainDiv">\n\n    <div class="secondDiv">\n\n        <ion-row>\n\n            <ion-col style="padding: 0px; text-align: center;">\n\n                <p style="margin: 0px; font-size:20px;">Add Points</p>\n\n            </ion-col>\n\n        </ion-row>\n\n        <div class="temp">\n\n            <ion-item class="logitem">\n\n                <ion-input [(ngModel)]="username" type="text">\n\n                </ion-input>\n\n            </ion-item>\n\n        </div>\n\n        <!-- <ion-item class="logitem1"\n\n            *ngIf="!loginForm.controls.username.valid && (loginForm.controls.username.dirty || submitAttempt)">\n\n            <p>{{\'username required!\' | translate}}</p>\n\n        </ion-item> -->\n\n        <div class="temp">\n\n            <ion-item class="logitem">\n\n                <ion-input [(ngModel)]="password" type="password"></ion-input>\n\n\n\n            </ion-item>\n\n        </div>\n\n        <!-- <ion-item class="logitem1"\n\n            *ngIf="!loginForm.controls.password.valid && (loginForm.controls.password.dirty || submitAttempt)">\n\n            <p>{{\'Password required!\' | translate}}</p>\n\n        </ion-item> -->\n\n        <div style="padding: 5px;"><button ion-button (click)="dismiss()" color="gpsc" block>Submit</button></div>\n\n    </div>\n\n</div>'/*ion-inline-end:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/dealers/add-points-modal.html"*/,
            template: "\n    <div class=\"mainDiv\">\n    <div class=\"secondDiv\">\n        <ion-row>\n            <ion-col style=\"padding: 0px; text-align: center;\">\n                <p style=\"margin: 0px; font-size:20px;\">Add Points</p>\n            </ion-col>\n        </ion-row>\n        <div class=\"temp\">  \n            <ion-label style=\"font-size: 1.2em;\">Points</ion-label>\n            <ion-item class=\"logitem\" style=\"border: 1px solid black;\">\n                <ion-input [(ngModel)]=\"pointShareCount\" type=\"number\" (ngModelChange) =\"calculateTotal()\">\n                </ion-input>\n            </ion-item>\n        </div>\n        <p style=\"margin: 0px;color: #666; padding-left: 10px;padding-bottom: 2px;\">\n            Please type No. of points to be shared\n        </p>\n           \n        <div class=\"temp\">\n            <ion-label style=\"font-size: 1.2em;\">Price/Unit</ion-label>\n            <ion-item class=\"logitem\" style=\"border: 1px solid black;\">\n                <ion-input [(ngModel)]=\"perunitprice\" type=\"number\" (ngModelChange) =\"calculateTotal()\"></ion-input>\n            </ion-item>\n        </div>\n        <p style=\"margin: 0px;color: #666; padding-left: 10px;padding-bottom: 10px;\">\n            Please type per unit price of points\n        </p>\n        <div class=\"temp\">\n            <ion-item>\n                <ion-label>Payment Status</ion-label>\n                <ion-select [(ngModel)]=\"payment_stauts\">\n                    <ion-option value=\"paid\">Paid</ion-option>\n                    <ion-option value=\"credit\">Credit</ion-option>\n                    <ion-option value=\"link\">Send Payment Link</ion-option>\n                </ion-select>\n            </ion-item>\n        </div>\n\n        <div class=\"temp\" *ngIf=\"payment_stauts === 'paid'\">\n            <ion-label style=\"font-size: 1.2em;\">Remarks</ion-label>\n            <ion-item class=\"logitem\" style=\"border: 1px solid black;\">\n                <ion-input [(ngModel)]=\"remarks\" type=\"text\"></ion-input>\n            </ion-item>\n        </div>\n\n        <div class=\"temp\" *ngIf=\"payment_stauts === 'credit'\">\n            <ion-item class=\"logitem\" style=\"border: 1px solid black;\">\n                <ion-datetime displayFormat=\"DD/MM/YYYY hh:mm a\" pickerFormat=\"DD/MM/YY hh:mm a\" [(ngModel)]=\"credit_date\"></ion-datetime>\n            </ion-item>\n        </div>\n        <p *ngIf=\"payment_stauts === 'credit'\" style=\"margin: 0px;color: #666; padding-left: 10px; padding-bottom: 10px;\">\n            Please Select Credit Date\n        </p>\n        <div class=\"temp\" *ngIf=\"payment_stauts === 'link'\">\n           <ion-row>\n                <ion-col col-8></ion-col>\n                <ion-col col-2 style=\"padding: 0px;text-align: right;\">\n                    <ion-icon color=\"gpsc\" style=\"font-size: 1.5em;\" name=\"mail\"></ion-icon>\n                </ion-col>\n                <ion-col col-2 style=\"padding: 0px;text-align: right;\">\n                    <ion-icon color=\"secondary\" style=\"font-size: 1.8em;\" name=\"text\"></ion-icon>\n                </ion-col>\n           </ion-row>\n        </div>\n        <div class=\"temp\">\n            <p><span style=\"font-weight: bold;\">Total Allocated Points</span> <span style=\"float: right;\">{{totAllocatedPoints}}</span></p>\n            <p><span style=\"font-weight: bold;\">Total Available Points</span> <span style=\"float: right;\">{{totAvailablePoints}}</span></p>\n            <p><span style=\"font-weight: bold;\">Total Amount</span> <span style=\"float: right;\">{{totalAmount}}</span></p>\n        </div>\n      \n        <div style=\"padding: 5px;\">\n            <ion-row>\n                <ion-col col-5>\n                    <button ion-button (click)=\"dismiss()\" color=\"light\" block>Cancle</button>\n                </ion-col>\n                <ion-col col-7>\n                    <button ion-button (click)=\"proceed()\" color=\"gpsc\" block>Proceed</button>\n                </ion-col>\n            </ion-row>\n        </div>\n    </div>\n</div>\n    ",
            styles: ["\n    .logitem {\n        background-color: transparent;\n        padding-bottom: 5px;\n\n        ion-input {\n            color: #000;\n            ::placeholder {\n                color: #000;\n            }\n        }\n        border: 1px solid black;\n        border-radius: 5px;\n    }\n  \n    .mainDiv {\n        padding: 15px;\n        height: 100%;\n        background-color: rgba(0, 0, 0, 0.7);\n    }\n\n    .secondDiv {\n        padding-top: 20px;\n        border-radius: 18px;\n        border: 2px solid black;\n        background: white;\n    }\n    .temp {\n        padding-left: 10px;\n        padding-right: 10px;\n        padding-top: 5px;\n        padding-bottom: 5px;\n    }\n    ion-input {\n        background-color: transparent;\n    }\n    .item-md.item-block .item-inner,\n    .item-ios.item-block .item-inner {\n        padding-right: 8px;\n        border: none;\n    }\n        \n    "]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], AddPointsModalPage);
    return AddPointsModalPage;
}());

//# sourceMappingURL=AddPointsModalPage.js.map

/***/ }),

/***/ 442:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FastagPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return FasttagPayNow; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var FastagPage = /** @class */ (function () {
    function FastagPage(navCtrl, navParams, apiCall, toastCtrl, translate) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.translate = translate;
        this.buttonColor = '#fff'; //Default Color
        this.buttonColor1 = '#fff';
        this.mainCost = 200;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    }
    FastagPage.prototype.ionViewDidEnter = function () {
        console.log('ionViewDidEnter FastagPage');
    };
    FastagPage.prototype.calc = function () {
        this.totalPayable = this.mainCost * this.numFastag;
    };
    FastagPage.prototype.addEvent = function () {
        this.vehicleType = 'Truck';
        this.buttonColor1 = '#fff';
        this.buttonColor2 = '#fff';
        this.buttonColor3 = '#fff';
        this.buttonColor4 = '#fff';
        this.buttonColor5 = '#fff';
        this.buttonColor = '#FBCCD2'; //desired Color
    };
    FastagPage.prototype.addEvent1 = function () {
        this.vehicleType = 'Bus';
        this.buttonColor = '#fff';
        this.buttonColor2 = '#fff';
        this.buttonColor3 = '#fff';
        this.buttonColor4 = '#fff';
        this.buttonColor5 = '#fff';
        this.buttonColor1 = '#FBCCD2'; //desired Color
    };
    FastagPage.prototype.addEvent2 = function () {
        this.vehicleType = 'Taxi';
        this.buttonColor1 = '#fff';
        this.buttonColor = '#fff';
        this.buttonColor3 = '#fff';
        this.buttonColor4 = '#fff';
        this.buttonColor5 = '#fff';
        this.buttonColor2 = '#FBCCD2'; //desired Color
    };
    FastagPage.prototype.addEvent3 = function () {
        this.vehicleType = 'Car';
        this.buttonColor2 = '#fff';
        this.buttonColor1 = '#fff';
        this.buttonColor = '#fff';
        this.buttonColor4 = '#fff';
        this.buttonColor5 = '#fff';
        this.buttonColor3 = '#FBCCD2'; //desired Color
    };
    FastagPage.prototype.addEvent4 = function () {
        this.vehicleType = 'Bike';
        this.buttonColor2 = '#fff';
        this.buttonColor1 = '#fff';
        this.buttonColor = '#fff';
        this.buttonColor3 = '#fff';
        this.buttonColor5 = '#fff';
        this.buttonColor4 = '#FBCCD2'; //desired Color
    };
    FastagPage.prototype.addEvent5 = function () {
        this.vehicleType = 'Other';
        this.buttonColor4 = '#fff';
        this.buttonColor2 = '#fff';
        this.buttonColor1 = '#fff';
        this.buttonColor = '#fff';
        this.buttonColor3 = '#fff';
        this.buttonColor5 = '#FBCCD2'; //desired Color
    };
    FastagPage.prototype.fastagReq = function () {
        var _this = this;
        if (this.vehicleType === undefined || this.numFastag === undefined) {
            this.toastCtrl.create({
                message: this.translate.instant('Please select the vehicle type and add number of requests.'),
                duration: 2000,
                position: 'bottom'
            }).present();
            return;
        }
        var url = this.apiCall.mainUrl + "fastTag/addRequest";
        var payload = {};
        // debugger
        if (this.islogin.Dealer_ID === undefined) {
            payload = {
                user: this.islogin._id,
                Dealer: this.islogin.supAdmin,
                supAdmin: this.islogin.supAdmin,
                date: new Date().toISOString(),
                quantity: this.numFastag,
                vehicle_type: this.vehicleType
            };
        }
        else {
            payload = {
                user: this.islogin._id,
                Dealer: this.islogin.Dealer_ID._id,
                supAdmin: this.islogin.supAdmin,
                date: new Date().toISOString(),
                quantity: this.numFastag,
                vehicle_type: this.vehicleType
            };
        }
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(url, payload)
            .subscribe(function (respData) {
            _this.apiCall.stopLoading();
            console.log('response data: ', respData);
            if (respData.message === 'saved succesfully') {
                _this.toastCtrl.create({
                    message: 'Fastag added successfully.',
                    duration: 2000,
                    position: 'bottom'
                }).present();
                _this.navCtrl.pop();
            }
        }, function (err) {
            _this.apiCall.stopLoading();
        });
    };
    FastagPage.prototype.payNow = function () {
        this.navCtrl.push(FasttagPayNow, {
            param: {
                amount: this.totalPayable,
                fastagType: this.vehicleType,
                quantity: this.numFastag
            }
        });
    };
    FastagPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-fastag',template:/*ion-inline-start:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/fastag-list/fastag/fastag.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{"Add Fastag" | translate}}</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <input type="radio" id="male" name="gender" value="male">\n  <label for="male">Male</label><br>\n  <input type="radio" id="female" name="gender" value="female">\n  <label for="female">Female</label><br>\n  <input type="radio" id="other" name="gender" value="other">\n  <label for="other">Other</label>\n</ion-content>\n'/*ion-inline-end:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/fastag-list/fastag/fastag.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["c" /* TranslateService */]])
    ], FastagPage);
    return FastagPage;
}());

var FasttagPayNow = /** @class */ (function () {
    function FasttagPayNow(navParams) {
        this.navParams = navParams;
        this.razor_key = 'rzp_live_eZdWnBRmYRCLWo';
        this.currency = 'INR';
        this.data = {};
        this.islogin = localStorage.getItem('details') ? JSON.parse(localStorage.getItem('details')) : "";
        this.data = this.navParams.get('param');
        this.paymentAmount = this.data.amount * 100;
        console.log(this.navParams.get('param'));
        if (localStorage.getItem('default_curreny') !== null) {
            this.currency = localStorage.getItem('default_curreny');
        }
    }
    FasttagPayNow.prototype.payWithRazor = function () {
        var options = {
            description: 'Credits towards consultation',
            image: 'https://i.imgur.com/GO0jiDP.jpg',
            currency: this.currency,
            key: this.razor_key,
            amount: this.paymentAmount,
            name: this.data.fastagType,
            prefill: {
                email: this.islogin.email,
                contact: this.islogin.phn,
                name: this.islogin.fn + ' ' + this.islogin.ln
            },
            theme: {
                color: '#000000'
            },
            modal: {
                ondismiss: function () {
                    console.log('dismissed');
                }
            }
        };
        var successCallback = function (payment_id) {
            alert('payment_id: ' + payment_id);
        };
        var cancelCallback = function (error) {
            alert(error.description + ' (Error ' + error.code + ')');
        };
        RazorpayCheckout.open(options, successCallback, cancelCallback);
    };
    FasttagPayNow = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/fastag-list/fastag/paynow.html"*/'<ion-header>\n    <ion-navbar>\n        <button ion-button menuToggle>\n            <ion-icon name="menu"></ion-icon>\n        </button>\n        <ion-title>{{ "Payment Method" | translate }}</ion-title>\n    </ion-navbar>\n</ion-header>\n<ion-content no-padding>\n    <ion-grid text-center>\n        <ion-row>\n            <ion-col>\n                ** Pay with razorpay **\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n    <ion-card class="welcome-card">\n        <img src="/assets/imgs/dc-Cover-u0b349upqugfio195s4lpk8144-20190213120303.Medi.jpeg">\n        <ion-card-header>\n            <!-- <ion-card-subtitle>Get Started</ion-card-subtitle> -->\n            <ion-card-title>{{"Fastag for vehicle type" | translate}} - {{data.fastagType}}</ion-card-title>\n            <!-- <p>{{"Total Quantity" | translate}} - {{data.quantity}}</p> -->\n            <ion-row>\n                <ion-col>\n                    {{"Total Quantity" | translate}}\n                </ion-col>\n                <ion-col>\n                    {{data.quantity}}\n                </ion-col>\n            </ion-row>\n            <ion-row>\n                <ion-col>\n                    {{"Total Payment Amount" | translate}}\n                </ion-col>\n                <ion-col>\n                    {{currencyIcon}}{{paymentAmount/100}}\n                </ion-col>\n            </ion-row>\n        </ion-card-header>\n        <ion-card-content>\n            <!-- <ion-button expand="full" color="success" (click)="payWithRazor()">Pay with RazorPay</ion-button> -->\n            <button ion-button full color="gpsc" (click)="payWithRazor()">Pay with RazorPay</button>\n        </ion-card-content>\n    </ion-card>\n</ion-content>'/*ion-inline-end:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/fastag-list/fastag/paynow.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], FasttagPayNow);
    return FasttagPayNow;
}());

//# sourceMappingURL=fastag.js.map

/***/ }),

/***/ 444:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PagerProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PagerProvider = /** @class */ (function () {
    function PagerProvider(http) {
        this.http = http;
        console.log('Hello PagerProvider Provider');
    }
    PagerProvider.prototype.getPager = function (totalItems, currentPage, pageSize) {
        if (currentPage === void 0) { currentPage = 1; }
        if (pageSize === void 0) { pageSize = 10; }
        //calculate total pages
        var totalPages = Math.ceil(totalItems / pageSize);
        //ensure current page isn't out of range
        if (currentPage < 1) {
            currentPage = 1;
        }
        else if (currentPage > totalPages) {
            currentPage = totalPages;
        }
        var startPage, endPage;
        if (totalPages <= 10) {
            //less than 10 total pages so show all
            startPage = 1;
            endPage = totalPages;
        }
        else {
            //more that 10 total pages so calculate start and end pages
            if (currentPage <= 6) {
                startPage = 1;
                endPage = 10;
            }
            else if (currentPage + 4 >= totalPages) {
                startPage = totalPages - 9;
                endPage = totalPages;
            }
            else {
                startPage = currentPage - 5;
                endPage = currentPage + 4;
            }
        }
        // calculate start and end item indexes
        var startIndex = (currentPage - 1) * pageSize;
        var endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);
        // create an array of pages to ng-repeat in the pager control
        var pages = Array.from(Array((endPage + 1) - startPage).keys()).map(function (i) { return startPage + i; });
        // return object with all pager properties required by the view
        return {
            totalItems: totalItems,
            currentPage: currentPage,
            pageSize: pageSize,
            totalPages: totalPages,
            startPage: startPage,
            endPage: endPage,
            startIndex: startIndex,
            endIndex: endIndex,
            pages: pages
        };
    };
    PagerProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], PagerProvider);
    return PagerProvider;
}());

//# sourceMappingURL=pager.js.map

/***/ }),

/***/ 445:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FuelConsumptionPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return FuelEntryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_pullup__ = __webpack_require__(78);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var FuelConsumptionPage = /** @class */ (function () {
    function FuelConsumptionPage(apiCall, navCtrl, navParams, toastCtrl, event, translate) {
        var _this = this;
        this.apiCall = apiCall;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.event = event;
        this.translate = translate;
        this.portstemp = [];
        this.fuelList = [];
        this.measurementUnit = 'MKS';
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("user details=> ", JSON.stringify(this.islogin));
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_4_moment__({ hours: 0 }).format();
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_4_moment__().format();
        if (localStorage.getItem('default_curreny') !== null) {
            this.currency_code = localStorage.getItem('default_curreny');
        }
        this.event.subscribe('reloadFuellist', function () {
            if (_this._vehId != undefined) {
                _this.getFuelList();
            }
        });
    }
    FuelConsumptionPage.prototype.ionViewDidEnter = function () {
        console.log('ionViewDidEnter FuelConsumptionPage');
        this.getDefaultUserSettings();
    };
    FuelConsumptionPage.prototype.getDefaultUserSettings = function () {
        var _this = this;
        var b_url = this.apiCall.mainUrl + "users/get_user_setting";
        var Var = { uid: this.islogin._id };
        this.apiCall.urlpasseswithdata(b_url, Var)
            .subscribe(function (resp) {
            console.log("check lang key: ", resp);
            if (resp.unit_measurement !== undefined) {
                _this.measurementUnit = resp.unit_measurement;
            }
            else {
                if (localStorage.getItem('MeasurementType') !== null) {
                    var measureType = localStorage.getItem('MeasurementType');
                    _this.measurementUnit = measureType;
                }
                else {
                    _this.measurementUnit = 'MKS';
                }
            }
        }, function (err) {
            console.log(err);
            if (localStorage.getItem('MeasurementType') !== null) {
                var measureType = localStorage.getItem('MeasurementType');
                _this.measurementUnit = measureType;
            }
            else {
                _this.measurementUnit = 'MKS';
            }
        });
    };
    FuelConsumptionPage.prototype.ngOnInit = function () {
        this.getdevices();
    };
    FuelConsumptionPage.prototype.toggleFooter = function () {
        this.footerState = this.footerState == __WEBPACK_IMPORTED_MODULE_2_ionic_pullup__["a" /* IonPullUpFooterState */].Collapsed ? __WEBPACK_IMPORTED_MODULE_2_ionic_pullup__["a" /* IonPullUpFooterState */].Expanded : __WEBPACK_IMPORTED_MODULE_2_ionic_pullup__["a" /* IonPullUpFooterState */].Collapsed;
    };
    FuelConsumptionPage.prototype.addFuelEntry = function () {
        this.navCtrl.push(FuelEntryPage, {
            portstemp: this.portstemp
        });
    };
    FuelConsumptionPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apiCall.startLoading().present();
        this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.portstemp = data.devices;
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    FuelConsumptionPage.prototype.getFuelList = function () {
        var _this = this;
        this.fuelList = [];
        if (this._vehId == undefined) {
            var toast = this.toastCtrl.create({
                message: this.translate.instant('Please select the vehicle first..'),
                position: 'bottom',
                duration: 1500
            });
            toast.present();
        }
        else {
            var _baseURL = this.apiCall.mainUrl + "fuel/getFuels?vehicle=" + this._vehId + "&user=" + this.islogin._id;
            this.apiCall.startLoading().present();
            this.apiCall.getSOSReportAPI(_baseURL)
                .subscribe(function (data) {
                _this.apiCall.stopLoading();
                console.log("fule entry list: ", data);
                if (!data.message) {
                    _this.fuelList = data;
                }
            }, function (err) {
                _this.apiCall.stopLoading();
                var toast = _this.toastCtrl.create({
                    message: _this.translate.instant("Report not found!"),
                    duration: 1500,
                    position: "bottom"
                });
                toast.present();
            });
        }
    };
    FuelConsumptionPage.prototype.getid = function (veh) {
        this._vehId = veh._id;
    };
    FuelConsumptionPage.prototype.footerExpanded = function () {
        console.log('Footer expanded!');
    };
    FuelConsumptionPage.prototype.footerCollapsed = function () {
        console.log('Footer collapsed!');
    };
    FuelConsumptionPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-fuel-consumption',template:/*ion-inline-start:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/fuel/fuel-consumption/fuel-consumption.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>{{ "Fuel Entry" | translate }}</ion-title>\n\n  </ion-navbar>\n\n  <ion-item style="background-color: #fafafa;">\n\n    <ion-label style="margin-top: 15px;">{{\n\n      "Select Vehicle" | translate\n\n    }}</ion-label>\n\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name"\n\n      itemTextField="Device_Name" [canSearch]="true" (onChange)="getid(selectedVehicle)">\n\n    </select-searchable>\n\n  </ion-item>\n\n\n\n  <ion-row style="background-color: #fafafa;" padding-left padding-right>\n\n    <ion-col width-20 class="cust">\n\n      <ion-label>\n\n        <span style="font-size: 13px">{{ "From Date" | translate }}</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeStart"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;">\n\n        </ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20 class="cust">\n\n      <ion-label>\n\n        <span style="font-size: 13px">{{ "To Date" | translate }}</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeEnd"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;">\n\n        </ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <div style="margin-top: 9px; float: right">\n\n        <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;" (click)="getFuelList()">\n\n        </ion-icon>\n\n      </div>\n\n    </ion-col>\n\n  </ion-row>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <ion-card *ngFor="let f of fuelList">\n\n    <ion-card-content>\n\n      <ion-row>\n\n        <ion-col col-6 style="font-size: 1.2em;" no-padding>\n\n          <ion-row>\n\n            <ion-col col-2>\n\n              <ion-icon name="car" color="gpsc"></ion-icon>\n\n            </ion-col>\n\n            <ion-col col-10>\n\n              {{ f.vehicle.Device_Name }}\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-col>\n\n        <ion-col col-6 style="text-align: right;" no-padding>\n\n          <ion-row>\n\n            <ion-col col-12>\n\n              <ion-badge color="gpsc" style="font-size: 1.2em;">{{ f.quantity }}\n\n                {{ (measurementUnit == \'MKS\') ? ("Litres" | translate) : ("Gallon" | translate)}}</ion-badge>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row>\n\n        <ion-col col-6 no-padding>\n\n          <ion-row>\n\n            <ion-col col-2>\n\n              <ion-icon style="color:gray;" name="calendar"></ion-icon>\n\n            </ion-col>\n\n            <ion-col col-10 style="color:gray;font-size:14px;font-weight: 400;">{{ f.Date | date: "mediumDate" }}\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-col>\n\n        <ion-col col-6 no-padding>\n\n          <ion-row style="text-align: right;">\n\n            <ion-col col-12 style="color:gray;font-size:14px;font-weight: 400;">\n\n              <ion-icon style="color:gray;" name="cash"></ion-icon>&nbsp;&nbsp;{{ f.price }} {{ currency_code }}\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-card-content>\n\n  </ion-card>\n\n</ion-content>\n\n<ion-fab right bottom>\n\n  <button ion-fab color="gpsc" (click)="addFuelEntry()">\n\n    <ion-icon name="add"></ion-icon>\n\n  </button>\n\n</ion-fab>'/*ion-inline-end:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/fuel/fuel-consumption/fuel-consumption.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["c" /* TranslateService */]])
    ], FuelConsumptionPage);
    return FuelConsumptionPage;
}());

var FuelEntryPage = /** @class */ (function () {
    function FuelEntryPage(navParams, apiCall, toastCtrl, event, translate) {
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.event = event;
        this.translate = translate;
        this.portstemp = [];
        this.fuelType = 'CNG';
        this.liter = 0;
        this.amt = 0;
        this.ddate = new Date().toISOString();
        this.measurementUnit = 'MKS';
        this.portstemp = this.navParams.get("portstemp");
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("user details=> ", JSON.stringify(this.islogin));
    }
    FuelEntryPage.prototype.ionViewDidEnter = function () {
        this.getDefaultUserSettings();
    };
    FuelEntryPage.prototype.getDefaultUserSettings = function () {
        var _this = this;
        var b_url = this.apiCall.mainUrl + "users/get_user_setting";
        var Var = { uid: this.islogin._id };
        this.apiCall.urlpasseswithdata(b_url, Var)
            .subscribe(function (resp) {
            console.log("check lang key: ", resp);
            if (resp.unit_measurement !== undefined) {
                _this.measurementUnit = resp.unit_measurement;
            }
            else {
                if (localStorage.getItem('MeasurementType') !== null) {
                    var measureType = localStorage.getItem('MeasurementType');
                    _this.measurementUnit = measureType;
                }
                else {
                    _this.measurementUnit = 'MKS';
                }
            }
        }, function (err) {
            console.log(err);
            if (localStorage.getItem('MeasurementType') !== null) {
                var measureType = localStorage.getItem('MeasurementType');
                _this.measurementUnit = measureType;
            }
            else {
                _this.measurementUnit = 'MKS';
            }
        });
    };
    FuelEntryPage.prototype.onChnageEvent = function (veh) {
        console.log("vehicle info:", veh);
        debugger;
        var that = this;
        that.veh_id = veh._id;
        var sb = veh.total_odo;
        that.tot_odo = sb.toFixed(2);
    };
    FuelEntryPage.prototype.fixDecimals = function (value) {
        value = "" + value;
        value = value.trim();
        value = parseFloat(value).toFixed(2);
        return value;
    };
    FuelEntryPage.prototype.submit = function () {
        var _this = this;
        var that = this;
        debugger;
        if (this.liter == undefined || this.amt == undefined || this.tot_odo == undefined || this.fuelType == undefined || this.ddate == undefined || this.vehName == undefined) {
            var toast = this.toastCtrl.create({
                message: this.translate.instant("Please fill all the mandatory fields.."),
                position: 'bottom',
                duration: 2000
            });
            toast.present();
        }
        else {
            var payload = {};
            var _baseURL = this.apiCall.mainUrl + "fuel/addFuel";
            payload = {
                "vehicle": that.veh_id,
                "user": that.islogin._id,
                "date": new Date(that.ddate).toISOString(),
                "quantity": that.liter,
                "odometer": that.tot_odo,
                "price": that.amt,
                "fuel_type": that.fuelType,
                "comment": "First fill"
            };
            this.apiCall.startLoading().present();
            this.apiCall.urlpasseswithdata(_baseURL, payload)
                .subscribe(function (data) {
                _this.apiCall.stopLoading();
                console.log("fuel entry added: " + data);
                _this.liter = null;
                _this.amt = null;
                _this.tot_odo = null;
                _this.fuelType = null;
                _this.ddate = null;
                _this.vehName = null;
                _this.veh_id = null;
                var toast = _this.toastCtrl.create({
                    message: _this.translate.instant("Fuel entry added successfully! Add another entry.."),
                    position: 'bottom',
                    duration: 2000
                });
                toast.present();
                _this.event.publish('reloadFuellist');
            });
        }
    };
    FuelEntryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/fuel/fuel-consumption/fuel-entry.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>{{"Add Fuel Entry" | translate}}</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n    <ion-list>\n\n        <ion-item>\n\n            <ion-label>{{"Select Vehicle" | translate}}</ion-label>\n\n            <ion-select [(ngModel)]="vehName">\n\n                <ion-option *ngFor="let veh of portstemp" [value]="veh.Device_Name" (ionSelect)="onChnageEvent(veh)">\n\n                    {{veh.Device_Name}}</ion-option>\n\n            </ion-select>\n\n        </ion-item>\n\n        <ion-item *ngIf="tot_odo != undefined">\n\n            <ion-label>{{"Total Odo" | translate}}</ion-label>\n\n            <ion-input type="number" [(ngModel)]="tot_odo" readonly></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-label>{{"Fuel Type" | translate}}</ion-label>\n\n            <ion-select [(ngModel)]="fuelType">\n\n                <ion-option [value]="CNG" selected>{{"CNG" | translate}}</ion-option>\n\n                <ion-option [value]="Petrol">{{"PETROL" | translate}}</ion-option>\n\n                <ion-option [value]="Diesel">{{"DIESEL" | translate}}</ion-option>\n\n                <ion-option [value]="Other">{{"OTHER" | translate}}</ion-option>\n\n            </ion-select>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label>{{ (measurementUnit == \'MKS\') ? ("Litre" | translate) : ("Gallon" | translate)}}</ion-label>\n\n            <ion-input type="number" [(ngModel)]="liter"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-label>{{"Amount" | translate}}</ion-label>\n\n            <ion-input type="number" [(ngModel)]="amt"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-label>{{"Date" | translate}}</ion-label>\n\n            <ion-datetime displayFormat="DD/MM/YYYY" pickerFormat="DD/MM/YYYY" [(ngModel)]="ddate" name="ddate">\n\n            </ion-datetime>\n\n        </ion-item>\n\n    </ion-list>\n\n</ion-content>\n\n\n\n<ion-footer class="footSty">\n\n    <ion-toolbar>\n\n        <ion-row no-padding>\n\n            <ion-col width-50 style="text-align: center;">\n\n                <button ion-button clear color="light" (click)="submit()">{{"SUBMIT" | translate}}</button>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-toolbar>\n\n</ion-footer>'/*ion-inline-end:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/fuel/fuel-consumption/fuel-entry.html"*/,
            selector: 'page-fuel-consumption'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["c" /* TranslateService */]])
    ], FuelEntryPage);
    return FuelEntryPage;
}());

//# sourceMappingURL=fuel-consumption.js.map

/***/ }),

/***/ 446:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UpdateGroup; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var UpdateGroup = /** @class */ (function () {
    function UpdateGroup(navCtrl, navParams, apigroupupdatecall, alertCtrl, modalCtrl, formBuilder, viewCtrl, apiCall, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apigroupupdatecall = apigroupupdatecall;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.formBuilder = formBuilder;
        this.viewCtrl = viewCtrl;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.GroupStatus = [];
        this.groupDetails = {};
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        if (navParams.get('params') !== null) {
            this.groupDetails = navParams.get('params');
        }
        console.log("_id=> " + this.islogin._id);
        this.GroupStatus = [
            {
                name: "Active",
                value: true
            },
            {
                name: "InActive",
                value: false
            }
        ];
        this.groupForm = formBuilder.group({
            group_name: [this.groupDetails.groupname, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            status: [this.groupDetails.status, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
        });
    }
    UpdateGroup.prototype.ngOnInit = function () {
    };
    UpdateGroup.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    UpdateGroup.prototype.UpdateGroup = function () {
        var _this = this;
        var url = this.apiCall.mainUrl + "groups/updateGroup";
        var payload = {
            "name": this.groupForm.value.group_name,
            "status": this.groupForm.value.status,
            "_id": this.groupDetails._id
        };
        this.apiCall.urlpasseswithdata(url, payload).subscribe(function (resp) {
            console.log(resp);
            _this.toastCtrl.create({
                message: 'Group details updated..!',
                duration: 2000,
                position: 'top'
            }).present();
            setTimeout(function () {
                _this.viewCtrl.dismiss();
            }, 2000);
        });
    };
    UpdateGroup = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-update-model',template:/*ion-inline-start:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/groups/update-group/update-group.html"*/'<ion-header>\n\n        <ion-navbar>\n\n            <ion-title>Update Group</ion-title>\n\n            <ion-buttons end>\n\n                <button ion-button icon-only (click)="dismiss()">\n\n                    <ion-icon name="close-circle"></ion-icon>\n\n                </button>\n\n            </ion-buttons>\n\n        </ion-navbar>\n\n    </ion-header>\n\n    <ion-content>\n\n     \n\n        <form [formGroup]="groupForm">\n\n\n\n            <ion-item>\n\n                    <ion-label fixed fixed style="min-width: 50% !important;">Group Name</ion-label>\n\n                    <ion-input formControlName="group_name" type="text"></ion-input>\n\n           </ion-item>\n\n           \n\n\n\n           <ion-item>\n\n                <ion-label >Group Status*</ion-label>\n\n                <ion-select formControlName="status" style="min-width:50%;">\n\n                    <ion-option *ngFor="let statusname of GroupStatus" [value]="statusname.value">{{statusname.name}}</ion-option>\n\n                </ion-select>\n\n            </ion-item>\n\n    \n\n      \n\n    </form>\n\n    <!-- <button ion-button block (click)="AddGroup()">ADD</button> -->\n\n        \n\n    </ion-content>\n\n    <ion-footer class="footSty">\n\n    \n\n            <ion-toolbar>\n\n                <ion-row no-padding>\n\n                    <ion-col width-50 style="text-align: center;">\n\n                        <button ion-button clear color="light" (click)="UpdateGroup()">UPDATE</button>\n\n                    </ion-col>\n\n                </ion-row>\n\n            </ion-toolbar>\n\n        </ion-footer>'/*ion-inline-end:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/groups/update-group/update-group.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], UpdateGroup);
    return UpdateGroup;
}());

//# sourceMappingURL=update-group.js.map

/***/ }),

/***/ 447:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ModalPage = /** @class */ (function () {
    function ModalPage(
    // private navParams: NavParams,
    viewCtrl) {
        this.viewCtrl = viewCtrl;
        this.min_time = "10";
    }
    ModalPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss(this.min_time);
    };
    ModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            // selector: 'page-modal',
            template: "\n        <div class=\"mainDiv\">\n            <div class=\"secondDiv\">\n                <ion-list radio-group [(ngModel)]=\"min_time\">\n                    <ion-item>\n                        <ion-label>10 Mins</ion-label>\n                        <ion-radio value=\"10\" checked></ion-radio>\n                    </ion-item>\n                    <ion-item>\n                        <ion-label>20 Mins</ion-label>\n                        <ion-radio value=\"20\"></ion-radio>\n                    </ion-item>\n                    <ion-item>\n                        <ion-label>30 Mins</ion-label>\n                        <ion-radio value=\"30\"></ion-radio>\n                    </ion-item>\n                    <ion-item>\n                        <ion-label>40 Mins</ion-label>\n                        <ion-radio value=\"40\"></ion-radio>\n                    </ion-item>\n                    <ion-item>\n                        <ion-label>50 Mins</ion-label>\n                        <ion-radio value=\"50\"></ion-radio>\n                    </ion-item>\n                    <ion-item>\n                        <ion-label>60 Mins</ion-label>\n                        <ion-radio value=\"60\"></ion-radio>\n                    </ion-item>\n                    \n                </ion-list>\n                <div style=\"padding: 5px;\"><button ion-button (click)=\"dismiss()\" color=\"gpsc\" block>Submit</button></div>\n            </div>\n        </div>\n    ",
            styles: ["\n      \n            .mainDiv {\n                padding: 50px;\n                height: 100%;\n                background-color: rgba(0, 0, 0, 0.7);\n            }\n\n            .secondDiv {\n                padding-top: 20px;\n                border-radius: 18px;\n                border: 2px solid black;\n                background: white;\n            }\n        \n    "]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], ModalPage);
    return ModalPage;
}());

//# sourceMappingURL=modal.js.map

/***/ }),

/***/ 449:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return ServiceProviderPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return UpdatePasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_pullup__ = __webpack_require__(78);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_app_version__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_camera__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_crop__ = __webpack_require__(450);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_file_transfer__ = __webpack_require__(146);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_socket_connection_socket_connection__ = __webpack_require__(57);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






// import { Storage } from '@ionic/storage';




// import * as io from 'socket.io-client';
// import { TextToSpeech } from '@ionic-native/text-to-speech';
var ProfilePage = /** @class */ (function () {
    function ProfilePage(appVersion, apiCall, alertCtrl, navCtrl, navParams, events, formBuilder, 
    // private storage: Storage,
    platform, actionSheetCtrl, crop, camera, transfer, toastCtrl, socketConn) {
        var _this = this;
        this.appVersion = appVersion;
        this.apiCall = apiCall;
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.events = events;
        this.formBuilder = formBuilder;
        this.platform = platform;
        this.actionSheetCtrl = actionSheetCtrl;
        this.crop = crop;
        this.camera = camera;
        this.transfer = transfer;
        this.toastCtrl = toastCtrl;
        this.socketConn = socketConn;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.appVersion.getVersionNumber().then(function (version) {
            _this.aVer = version;
        });
        this.isDealer = this.islogin.isDealer;
        this.footerState = __WEBPACK_IMPORTED_MODULE_3_ionic_pullup__["a" /* IonPullUpFooterState */].Collapsed;
        this.credentialsForm = this.formBuilder.group({
            fname: [this.islogin.fn],
            lname: [this.islogin.ln],
            email: [this.islogin.email],
            phonenum: [this.islogin.phn],
            org: [this.islogin._orgName],
        });
        this.getImgUrl();
    }
    ProfilePage.prototype.ionViewDidEnter = function () {
        console.log('ionViewDidEnter ProfilePage');
    };
    ProfilePage.prototype.footerExpanded = function () {
        console.log('Footer expanded!');
    };
    ProfilePage.prototype.footerCollapsed = function () {
        console.log('Footer collapsed!');
    };
    ProfilePage.prototype.getImgUrl = function () {
        var _this = this;
        var url = "https://www.oneqlik.in/users/shareProfileImage?uid=" + this.islogin._id;
        this.apiCall.getdevicesForAllVehiclesApi(url)
            .subscribe(function (resp) {
            if (JSON.parse(JSON.stringify(resp)).imageDoc.length > 0) {
                var imgUrl = JSON.parse(JSON.stringify(resp)).imageDoc[0];
                var str1 = imgUrl.split('public/');
                _this.fileUrl = "https://www.oneqlik.in/" + str1[1];
            }
        });
    };
    ProfilePage.prototype.selectImage = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Select Image Source',
            buttons: [
                {
                    text: 'Load from Library',
                    handler: function () {
                        _this.pickImage(_this.camera.PictureSourceType.SAVEDPHOTOALBUM);
                    }
                },
                {
                    text: 'Use Camera',
                    handler: function () {
                        _this.pickImage(_this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }
            ]
        });
        actionSheet.present();
    };
    ProfilePage.prototype.pickImage = function (sourceType) {
        var _this = this;
        var url = "https://www.oneqlik.in/users/uploadProfilePicture";
        var options = {
            quality: 100,
            sourceType: sourceType,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        };
        this.camera.getPicture(options).then(function (imageData) {
            _this.crop.crop(imageData, { quality: 100 })
                .then(function (newImage) {
                var dlink123 = newImage.split('?');
                var wear = dlink123[0];
                var fileTransfer = _this.transfer.create();
                var uploadOpts = {
                    fileKey: 'photo',
                    fileName: wear.substr(wear.lastIndexOf('/') + 1)
                };
                _this.toggleFooter();
                _this.apiCall.startLoading().present();
                fileTransfer.upload(wear, url, uploadOpts)
                    .then(function (data) {
                    _this.apiCall.stopLoading();
                    var respData = data.response;
                    _this.dlUpdate(respData);
                }, function (err) {
                    _this.apiCall.stopLoading();
                    console.log(err);
                    _this.toastCtrl.create({
                        message: 'Something went wrong while uploading file... Please try after some time..',
                        duration: 2000,
                        position: 'bottom'
                    }).present();
                });
            });
        }, function (err) {
            console.log("imageData err: ", err);
            // Handle error
        });
    };
    ProfilePage.prototype.dlUpdate = function (dllink) {
        var _this = this;
        // let dlink123 = dllink.split('?');
        // let wear = dlink123[0];
        var _burl = "https://www.oneqlik.in/users/updateImagePath";
        var payload = {
            imageDoc: [dllink],
            _id: this.islogin._id
        };
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(_burl, payload)
            .subscribe(function () {
            _this.apiCall.stopLoading();
            _this.getImgUrl();
        }, function (err) {
            _this.apiCall.stopLoading();
        });
    };
    ProfilePage.prototype.reports = function () {
        this.navCtrl.push('ReportsCollectionPage');
    };
    ProfilePage.prototype.toggleFooter = function () {
        this.footerState = this.footerState == __WEBPACK_IMPORTED_MODULE_3_ionic_pullup__["a" /* IonPullUpFooterState */].Collapsed ? __WEBPACK_IMPORTED_MODULE_3_ionic_pullup__["a" /* IonPullUpFooterState */].Expanded : __WEBPACK_IMPORTED_MODULE_3_ionic_pullup__["a" /* IonPullUpFooterState */].Collapsed;
    };
    ProfilePage.prototype.settings = function () {
        this.navCtrl.push('SettingsPage');
    };
    ProfilePage.prototype.service = function () {
        this.navCtrl.push(ServiceProviderPage, {
            param: this.islogin
        });
    };
    ProfilePage.prototype.password = function () {
        this.navCtrl.push(UpdatePasswordPage, {
            param: this.islogin
        });
    };
    ProfilePage.prototype.onSignIn = function () {
        var _this = this;
        var data = {
            "fname": this.credentialsForm.value.fname,
            "lname": this.credentialsForm.value.lname,
            "org": this.credentialsForm.value.org,
            "noti": true,
            "uid": this.islogin._id,
            "fuel_unit": "LITRE"
        };
        this.apiCall.startLoading().present();
        this.apiCall.updateprofile(data)
            .subscribe(function (resdata) {
            _this.apiCall.stopLoading();
            if (resdata.token) {
                var alert_1 = _this.alertCtrl.create({
                    message: 'Profile updated succesfully!',
                    buttons: [{
                            text: 'OK',
                            handler: function () {
                                var logindata = JSON.stringify(resdata);
                                var logindetails = JSON.parse(logindata);
                                var userDetails = window.atob(logindetails.token.split('.')[1]);
                                var details = JSON.parse(userDetails);
                                localStorage.setItem("loginflag", "loginflag");
                                localStorage.setItem('details', JSON.stringify(details));
                                localStorage.setItem('condition_chk', details.isDealer);
                                _this.islogin = JSON.parse(localStorage.getItem('details')) || {};
                                _this.footerState = 1;
                                _this.toggleFooter();
                            }
                        }]
                });
                alert_1.present();
            }
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log("error from service=> ", err);
        });
    };
    ProfilePage.prototype.logout = function () {
        var _this = this;
        if (localStorage.getItem('getDevicesInterval_ID')) {
            var intervalid = localStorage.getItem('getDevicesInterval_ID');
            clearInterval(JSON.parse(intervalid));
        }
        this.token = localStorage.getItem("DEVICE_TOKEN");
        var pushdata = {};
        if (this.platform.is('android')) {
            pushdata = {
                "uid": this.islogin._id,
                "token": this.token,
                "os": "android"
            };
        }
        else {
            pushdata = {
                "uid": this.islogin._id,
                "token": this.token,
                "os": "ios"
            };
        }
        var alert = this.alertCtrl.create({
            message: 'Do you want to logout from the application?',
            buttons: [{
                    text: 'Yes',
                    handler: function () {
                        _this.apiCall.startLoading().present();
                        _this.apiCall.pullnotifyCall(pushdata)
                            .subscribe(function () {
                            _this.apiCall.stopLoading();
                            localStorage.clear();
                            localStorage.setItem('count', null);
                            _this.socketConn.socketDisconnect();
                            _this.socketConn.notifSocketDisconnect();
                            // this.storage.clear().then(() => {
                            //   console.log("ionic storage cleared!")
                            // })
                            _this.navCtrl.setRoot('LoginPage');
                        }, function (err) {
                            _this.apiCall.stopLoading();
                            console.log(err);
                        });
                    }
                },
                {
                    text: 'No',
                    handler: function () {
                        // this.menuCtrl.close();
                    }
                }]
        });
        alert.present();
    };
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-profile',template:/*ion-inline-start:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/profile/profile.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{\'Me\' | translate}}</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (tap)="toggleFooter()">\n        <ion-icon color="light" name="create" *ngIf="footerState == 0"></ion-icon>\n        <ion-icon color="light" name="done-all" *ngIf="footerState == 1"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <ion-item-group>\n    <ion-item-divider color="light"></ion-item-divider>\n  </ion-item-group>\n  <ion-item-group>\n    <ion-item>\n      <ion-avatar item-start>\n        <img *ngIf="!fileUrl" src="assets/imgs/dummy-user-img.png">\n        <img *ngIf="fileUrl" src="{{fileUrl}}">\n      </ion-avatar>\n      <h2>{{islogin.fn}}&nbsp;{{islogin.ln}}</h2>\n      <p>{{\'Account:\' | translate}}{{islogin.account}}</p>\n    </ion-item>\n  </ion-item-group>\n  <ion-item-group>\n    <ion-item-divider color="light"></ion-item-divider>\n  </ion-item-group>\n  <ion-item-group>\n    <!-- <ion-item (tap)="service()" *ngIf="!isDealer"> -->\n    <ion-item (tap)="service()">\n      <ion-icon name="person" item-start></ion-icon>\n      {{\'Service Provider\' | translate}}\n    </ion-item>\n    <ion-item (tap)="password()">\n      <ion-icon name="lock" item-start></ion-icon>\n      {{\'Password\' | translate}}\n    </ion-item>\n  </ion-item-group>\n  <ion-item-group>\n    <ion-item-divider color="light"></ion-item-divider>\n  </ion-item-group>\n  <ion-item-group>\n    <ion-item (tap)="settings()">\n      <ion-icon name="cog" item-start></ion-icon>\n      {{\'Settings\' | translate}}\n    </ion-item>\n  </ion-item-group>\n  <ion-item-group>\n    <ion-item-divider color="light"></ion-item-divider>\n  </ion-item-group>\n  <ion-item-group (tap)="logout()">\n    <ion-item ion-text text-center>\n      <ion-icon name="log-out"></ion-icon>&nbsp;&nbsp;{{\'Logout\' | translate}}\n    </ion-item>\n  </ion-item-group>\n\n</ion-content>\n<ion-footer padding-bottom padding-top>\n  <div style="text-align: center;">{{\'Version\' | translate}} {{aVer}}</div>\n  <br />\n  <div id="photo" style="text-align: center">\n    <span style="vertical-align:middle">{{\'Powered by\' | translate}} </span>&nbsp;\n    <a href="http://www.oneqlik.in/telematics/">\n      <img style="vertical-align:middle" src="assets/imgs/sign.jpg" width="55" height="19">\n    </a>\n  </div>\n</ion-footer>\n\n<ion-pullup #pullup (onExpand)="footerExpanded()" (onCollapse)="footerCollapsed()" [(state)]="footerState">\n  <ion-content>\n    <ion-row padding>\n      <ion-col style="text-align: center; font-size: 15px">{{\'Update Profile\' | translate}}</ion-col>\n    </ion-row>\n    <div style="text-align: center;">\n      <img *ngIf="!fileUrl" class="circular--square" src="assets/imgs/dummy-user-img.png" />\n      <img *ngIf="fileUrl" class="circular--square" src="{{fileUrl}}" />\n      <ion-fab>\n        <button ion-fab mini style="margin-left: -45px; margin-top: 80px;" color="medium" (click)="selectImage()">\n          <ion-icon name="camera"></ion-icon>\n        </button>\n      </ion-fab>\n    </div>\n    <form [formGroup]="credentialsForm">\n      <ion-item>\n        <ion-label floating>{{\'First Name\' | translate}}</ion-label>\n        <ion-input [formControl]="credentialsForm.controls[\'fname\']" type="text"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating>{{\'Last Name\' | translate}}</ion-label>\n        <ion-input [formControl]="credentialsForm.controls[\'lname\']" type="text"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating>{{\'Email Id\' | translate}}</ion-label>\n        <ion-input [formControl]="credentialsForm.controls[\'email\']" type="email"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating>{{\'Phone Number\' | translate}}</ion-label>\n        <ion-input [formControl]="credentialsForm.controls[\'phonenum\']" type="number"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating>{{\'Organisation\' | translate}}</ion-label>\n        <ion-input [formControl]="credentialsForm.controls[\'org\']" type="text"></ion-input>\n      </ion-item>\n\n\n      <ion-row>\n        <ion-col text-center>\n          <button ion-button block color="gpsc" (click)="onSignIn()">\n            {{\' Update Account\' | translate}}\n          </button>\n        </ion-col>\n      </ion-row>\n\n    </form>\n  </ion-content>\n</ion-pullup>\n'/*ion-inline-end:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/profile/profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__ionic_native_app_version__["a" /* AppVersion */],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_5__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_crop__["a" /* Crop */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_9__providers_socket_connection_socket_connection__["a" /* SocketConnectionProvider */]])
    ], ProfilePage);
    return ProfilePage;
}());

var ServiceProviderPage = /** @class */ (function () {
    function ServiceProviderPage(navParam) {
        this.navParam = navParam;
        this.uData = {};
        this.sorted = [];
        this.uData = this.navParam.get("param");
        if (this.uData.Dealer_ID != undefined) {
            this.sorted = this.uData.Dealer_ID;
        }
        else {
            this.sorted.first_name = this.uData.fn;
            this.sorted.last_name = this.uData.ln;
            this.sorted.phone = this.uData.phn;
            this.sorted.email = this.uData.email;
        }
        console.log("udata=> ", this.uData);
        console.log("udata=> ", JSON.stringify(this.uData));
    }
    ServiceProviderPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/profile/service-provider.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>{{\'Provider Info\' | translate}}</ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n<ion-content>\n\n    <div class="div">\n\n        <img src="assets/imgs/dummy_user.jpg">\n\n    </div>\n\n    <hr>\n\n    <ion-row padding-left padding-right style="padding-bottom: 6px; padding-top: 6px">\n\n        <ion-col>{{\'Name\' | translate}}</ion-col>\n\n        <ion-col>\n\n            <p style="float: right; margin:0px; padding:0px">{{sorted.first_name}}&nbsp;{{sorted.last_name}}</p>\n\n        </ion-col>\n\n    </ion-row>\n\n    <hr>\n\n    <ion-row padding-left padding-right style="padding-bottom: 6px; padding-top: 6px">\n\n        <ion-col>{{\'Contacts\' | translate}}</ion-col>\n\n        <ion-col>\n\n            <p style="float: right; margin:0px; padding:0px">{{sorted.phone}}</p>\n\n        </ion-col>\n\n    </ion-row>\n\n    <hr>\n\n    <ion-row padding-left padding-right style="padding-bottom: 6px; padding-top: 6px">\n\n        <ion-col>{{"Mobile" | translate}}</ion-col>\n\n        <ion-col>\n\n            <p style="float: right; margin:0px; padding:0px">{{sorted.phone}}</p>\n\n        </ion-col>\n\n    </ion-row>\n\n    <hr>\n\n    <ion-row padding-left padding-right style="padding-bottom: 6px; padding-top: 6px">\n\n        <ion-col>{{"E-mail" | translate}}</ion-col>\n\n        <ion-col>\n\n            <p style="float: right; margin:0px; padding:0px">{{sorted.email}}</p>\n\n        </ion-col>\n\n    </ion-row>\n\n    <hr>\n\n    <ion-row padding-left padding-right style="padding-bottom: 6px; padding-top: 6px">\n\n        <ion-col>{{\'Address\' | translate}}</ion-col>\n\n        <ion-col>\n\n            <p style="float: right; margin:0px; padding:0px">N/A</p>\n\n        </ion-col>\n\n    </ion-row>\n\n    <hr>\n\n</ion-content>\n\n'/*ion-inline-end:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/profile/service-provider.html"*/,
            selector: 'page-profile'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], ServiceProviderPage);
    return ServiceProviderPage;
}());

var UpdatePasswordPage = /** @class */ (function () {
    function UpdatePasswordPage(navParam, alertCtrl, toastCtrl, apiSrv, navCtrl) {
        this.navParam = navParam;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.apiSrv = apiSrv;
        this.navCtrl = navCtrl;
        this.passData = this.navParam.get("param");
        console.log("passData=> " + JSON.stringify(this.passData));
    }
    UpdatePasswordPage.prototype.savePass = function () {
        var _this = this;
        if (this.oldP == undefined || this.newP == undefined || this.cnewP == undefined) {
            var alert_2 = this.alertCtrl.create({
                message: 'Fields should not be empty!',
                buttons: ['OK']
            });
            alert_2.present();
        }
        else {
            if (this.newP != this.cnewP) {
                var alert_3 = this.alertCtrl.create({
                    message: 'Password Missmatched!!',
                    buttons: ['Try Again']
                });
                alert_3.present();
            }
            else {
                var data = {
                    "ID": this.passData._id,
                    "OLD_PASS": this.oldP,
                    "NEW_PASS": this.newP
                };
                this.apiSrv.startLoading().present();
                this.apiSrv.updatePassword(data)
                    .subscribe(function (respData) {
                    _this.apiSrv.stopLoading();
                    console.log("respData=> ", respData);
                    var toast = _this.toastCtrl.create({
                        message: 'Password Updated successfully',
                        position: "bottom",
                        duration: 2000
                    });
                    toast.onDidDismiss(function () {
                        _this.oldP = "";
                        _this.newP = "";
                        _this.cnewP = "";
                    });
                    toast.present();
                }, function (err) {
                    _this.apiSrv.stopLoading();
                    console.log("error in update password=> ", err);
                    // debugger
                    if (err.message == "Timeout has occurred") {
                        // alert("the server is taking much time to respond. Please try in some time.")
                        var alerttemp = _this.alertCtrl.create({
                            message: "the server is taking much time to respond. Please try in some time.",
                            buttons: [{
                                    text: 'Okay',
                                    handler: function () {
                                        _this.navCtrl.setRoot("DashboardPage");
                                    }
                                }]
                        });
                        alerttemp.present();
                    }
                    else {
                        var toast = _this.toastCtrl.create({
                            message: err._body.message,
                            position: "bottom",
                            duration: 2000
                        });
                        toast.onDidDismiss(function () {
                            _this.oldP = "";
                            _this.newP = "";
                            _this.cnewP = "";
                            _this.navCtrl.setRoot("DashboardPage");
                        });
                        toast.present();
                    }
                });
            }
        }
    };
    UpdatePasswordPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-profile',template:/*ion-inline-start:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/profile/update-password.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>{{\'Change Password\' | translate}}</ion-title>\n\n        <ion-buttons end>\n\n            <button ion-button icon-only (click)="savePass()">\n\n                {{\'SAVE\' | translate}}\n\n            </button>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n    <ion-list>\n\n        <ion-item>\n\n            <ion-icon class="ic" name="lock" item-start></ion-icon>\n\n            <ion-input placeholder="{{\'Old\' | translate}} {{\'Password\' | translate}}" [(ngModel)]="oldP"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-icon class="ic" name="lock" item-start></ion-icon>\n\n            <ion-input placeholder="{{\'New\' | translate}} {{\'Password\' | translate}}" [(ngModel)]="newP"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-icon class="ic" name="lock" item-start></ion-icon>\n\n            <ion-input placeholder="{{\'Confirm\' | translate}} {{\'New\' | translate}} {{\'Password\' | translate}}" [(ngModel)]="cnewP"></ion-input>\n\n        </ion-item>\n\n    </ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/profile/update-password.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"]])
    ], UpdatePasswordPage);
    return UpdatePasswordPage;
}());

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 451:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotifModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var NotifModalPage = /** @class */ (function () {
    function NotifModalPage(navParams, viewCtrl, apiCall, toastCtrl) {
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.isAddEmail = false;
        this.isAddPhone = false;
        this.emailList = [];
        this.phonelist = [];
        this.femail = '';
        this.fphone = 0;
        this.model = {};
        this.newArray = [];
        this.fData = {};
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.notifType = navParams.get('notifData').notifType;
        this.data = navParams.get('notifData').compData;
        if (navParams.get('notifData').buttonClick == 'email') {
            this.isAddEmail = true;
        }
        if (navParams.get('notifData').buttonClick == 'phone') {
            this.isAddPhone = true;
        }
        if (navParams.get('notifData').newArray !== undefined) {
            console.log("check all data first: ", navParams.get('notifData').newArray);
            this.newArray = navParams.get('notifData').newArray;
        }
    }
    NotifModalPage.prototype.ngOnInit = function () {
        this.emailList = this.data[this.notifType].emails;
        this.phonelist = this.data[this.notifType].phones;
    };
    NotifModalPage.prototype.onSubmit = function () {
        console.log('Form submitted!!');
    };
    NotifModalPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    NotifModalPage.prototype.addEmail = function (id, index) {
        var _this = this;
        if (id == 'add') {
            if (!this.data[this.notifType].emails) {
                this.data[this.notifType].emails = [];
            }
            var arrayList = [];
            this.data[this.notifType].emails.push(this.model.email);
            console.log(this.data);
            var that_1 = this;
            arrayList = this.newArray.map(function (r) {
                if (r.key === that_1.notifType) {
                    return r;
                }
            });
            console.log(arrayList);
            var filtered_1 = arrayList.filter(function (x) {
                return x !== undefined;
            });
            console.log(filtered_1);
            var res123 = this.newArray.map(function (obj) { return filtered_1.find(function (o) { return o.key === obj.key; }) || obj; });
            console.log(res123);
            var temp = [];
            for (var e = 0; e < res123.length; e++) {
                temp.push((_a = {},
                    _a[res123[e].key] = res123[e].keys,
                    _a.key = res123[e].key,
                    _a));
            }
            /* covert an array into object */
            var res_1 = {};
            temp.forEach(function (a) { res_1[a.key] = a[a.key]; });
            this.fData.contactid = this.islogin._id;
            this.fData.alert = res_1;
            var url = this.apiCall.mainUrl + 'users/editUserDetails';
            this.apiCall.startLoading().present();
            this.apiCall.urlpasseswithdata(url, this.fData)
                .subscribe(function (respData) {
                _this.apiCall.stopLoading();
                console.log("Please check: ", respData);
                _this.model.email = null;
                if (respData) {
                    _this.toastCtrl.create({
                        message: 'Email Id added!!',
                        duration: 1000,
                        position: 'middle'
                    }).present();
                }
            }, function (err) {
                _this.apiCall.stopLoading();
                _this.toastCtrl.create({
                    message: 'Internal server error, please try again!!!',
                    duration: 1500,
                    position: 'bottom'
                }).present();
            });
        }
        if (id == 'delete') {
            var arrayList_1 = [];
            var splicedmail = this.data[this.notifType].emails[index];
            console.log("spliceNumber =>", splicedmail);
            this.data[this.notifType].emails.splice(index, 1);
            ///////////////////////////////////////
            var that_2 = this;
            arrayList_1 = this.newArray.map(function (r) {
                if (r.key === that_2.notifType) {
                    return r;
                }
            });
            console.log(arrayList_1);
            var filtered_2 = arrayList_1.filter(function (x) {
                return x !== undefined;
            });
            console.log(filtered_2);
            var res123 = void 0;
            // filtered[0][this.notifType].emails.splice(index, 1);
            if (filtered_2.length > 0) {
                res123 = this.newArray.map(function (obj) { return filtered_2.find(function (o) { return o.key === obj.key; }) || obj; });
                console.log(res123);
            }
            else {
                res123 = this.newArray.map(function (obj) { return arrayList_1.find(function (o) { return o.key === obj.key; }) || obj; });
                console.log(res123);
            }
            var temp = [];
            for (var e = 0; e < res123.length; e++) {
                temp.push((_b = {},
                    _b[res123[e].key] = res123[e].keys,
                    _b.key = res123[e].key,
                    _b));
            }
            /* covert an array into object */
            var res_2 = {};
            temp.forEach(function (a) { res_2[a.key] = a[a.key]; });
            ///////////////////////////////////////
            this.fData.contactid = this.islogin._id;
            // this.fData.alert = this.data;
            this.fData.alert = res_2;
            var url = this.apiCall.mainUrl + 'users/editUserDetails';
            this.apiCall.startLoading().present();
            this.apiCall.urlpasseswithdata(url, this.fData)
                .subscribe(function (respData) {
                _this.apiCall.stopLoading();
                _this.model.email = null;
                console.log("Please check: ", respData);
                if (respData) {
                    _this.toastCtrl.create({
                        message: 'Email Id deleted!!',
                        duration: 1000,
                        position: 'middle'
                    }).present();
                }
            }, function (err) {
                _this.apiCall.stopLoading();
                _this.toastCtrl.create({
                    message: 'Internal server error, please try again!!!',
                    duration: 1500,
                    position: 'bottom'
                }).present();
            });
        }
        var _a, _b;
    };
    NotifModalPage.prototype.addPhone = function (id, index) {
        var _this = this;
        if (id == 'add') {
            if (!this.data[this.notifType].phones) {
                this.data[this.notifType].phones = [];
            }
            var arrayList = [];
            this.data[this.notifType].phones.push(this.model.phone);
            var that_3 = this;
            arrayList = this.newArray.map(function (r) {
                if (r.key === that_3.notifType) {
                    return r;
                }
            });
            // console.log(arrayList)
            var filtered_3 = arrayList.filter(function (x) {
                return x !== undefined;
            });
            console.log(filtered_3);
            var res123 = this.newArray.map(function (obj) { return filtered_3.find(function (o) { return o.key === obj.key; }) || obj; });
            // console.log(res123);
            var temp = [];
            for (var e = 0; e < res123.length; e++) {
                temp.push((_a = {},
                    _a[res123[e].key] = res123[e].keys,
                    _a.key = res123[e].key,
                    _a));
            }
            /* covert an array into object */
            var res_3 = {};
            temp.forEach(function (a) { res_3[a.key] = a[a.key]; });
            this.fData.contactid = this.islogin._id;
            this.fData.alert = res_3;
            var url = this.apiCall.mainUrl + 'users/editUserDetails';
            this.apiCall.startLoading().present();
            this.apiCall.urlpasseswithdata(url, this.fData)
                .subscribe(function (respData) {
                _this.apiCall.stopLoading();
                console.log("Please check: ", respData);
                _this.model.phone = null;
                if (respData) {
                    _this.toastCtrl.create({
                        message: 'Phone number added!!',
                        duration: 1000,
                        position: 'middle'
                    }).present();
                }
            }, function (err) {
                _this.apiCall.stopLoading();
                _this.toastCtrl.create({
                    message: 'Internal server error, please try again!!!',
                    duration: 1500,
                    position: 'bottom'
                }).present();
            });
        }
        if (id == 'delete') {
            var splicedNum = this.data[this.notifType].phones[index];
            console.log("spliceNumber =>", splicedNum);
            this.data[this.notifType].phones.splice(index, 1);
            ///////////////////////////////////////
            var arrayList_2 = [];
            var that_4 = this;
            arrayList_2 = this.newArray.map(function (r) {
                if (r.key === that_4.notifType) {
                    return r;
                }
            });
            console.log(arrayList_2);
            var filtered_4 = arrayList_2.filter(function (x) {
                return x !== undefined;
            });
            console.log(filtered_4);
            var res123 = void 0;
            // filtered[0][this.notifType].emails.splice(index, 1);
            if (filtered_4.length > 0) {
                res123 = this.newArray.map(function (obj) { return filtered_4.find(function (o) { return o.key === obj.key; }) || obj; });
                console.log(res123);
            }
            else {
                res123 = this.newArray.map(function (obj) { return arrayList_2.find(function (o) { return o.key === obj.key; }) || obj; });
                console.log(res123);
            }
            var temp_1 = [];
            for (var e = 0; e < res123.length; e++) {
                temp_1.push((_b = {},
                    _b[res123[e].key] = res123[e].keys,
                    _b.key = res123[e].key,
                    _b));
            }
            /* covert an array into object */
            var res_4 = {};
            temp_1.forEach(function (a) { res_4[a.key] = a[a.key]; });
            ///////////////////////////////////////
            this.fData.contactid = this.islogin._id;
            this.fData.alert = res_4;
            var url = this.apiCall.mainUrl + 'users/editUserDetails';
            this.apiCall.startLoading().present();
            this.apiCall.urlpasseswithdata(url, this.fData)
                .subscribe(function (respData) {
                _this.apiCall.stopLoading();
                console.log("Please check: ", respData);
                _this.model.phone = null;
                if (respData) {
                    _this.toastCtrl.create({
                        message: 'Phone number deleted!!',
                        duration: 1000,
                        position: 'middle'
                    }).present();
                }
            }, function (err) {
                _this.apiCall.stopLoading();
                _this.toastCtrl.create({
                    message: 'Internal server error, please try again!!!',
                    duration: 1500,
                    position: 'bottom'
                }).present();
            });
        }
        var _a, _b;
    };
    NotifModalPage.prototype.Close = function () {
        // this.dialogRef.close();
    };
    NotifModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-notif-modal',template:/*ion-inline-start:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/profile/settings/notif-setting/notif-modal.html"*/'<div class="mainDiv">\n\n    <div class="secondDiv">\n\n        <form novalidate #f="ngForm" *ngIf="isAddEmail">\n\n            <ion-list>\n\n                <ion-item>\n\n                    <ion-label floating>{{"Add Email" | translate}}</ion-label>\n\n                    <ion-input type="email" name="femail" [(ngModel)]="model.email" required pattern="[^ @]*@[^ @]*"\n\n                        #femail="ngModel"></ion-input>\n\n                </ion-item>\n\n                <div style="padding: 0px 16px 0px; color: red"\n\n                    *ngIf="femail.errors && (femail.dirty || femail.touched)">\n\n                    <p *ngIf="femail.errors.required">{{"Email is required" | translate}}</p>\n\n                    <p *ngIf="femail.errors.pattern">{{"Email must contain at least the @ character" | translate}}</p>\n\n                </div>\n\n                <!-- <pre>Valid? {{f.form.controls.email?.valid}}</pre>\n\n                <pre>Dirty? {{f.form.controls.email?.dirty}}</pre>\n\n                <pre>Touched? {{f.form.controls.email?.touched}}</pre> -->\n\n            </ion-list>\n\n            <ion-row>\n\n                <ion-col col-6>\n\n                    <div style="padding: 5px;"><button ion-button (click)="addEmail(\'add\',null)" color="gpsc" round\n\n                            small style="width: 100px;" [disabled]="f.invalid">{{"Add" | translate}}</button></div>\n\n                </ion-col>\n\n                <ion-col col-6>\n\n                    <div style="padding: 5px;"><button ion-button (click)="dismiss()" color="grey" round small\n\n                            style="width: 100px;">{{"Close" | translate}}</button>\n\n                    </div>\n\n                </ion-col>\n\n            </ion-row>\n\n        </form>\n\n        <form novalidate #f1="ngForm" *ngIf="isAddPhone">\n\n            <ion-list>\n\n                <ion-item>\n\n                    <ion-label floating>{{"Add Phone Number" | translate}}</ion-label>\n\n                    <ion-input type="tel" name="fphone" [(ngModel)]="model.phone" required minlength="10" maxlength="10"\n\n                        #fphone="ngModel" pattern="[0-9]*"></ion-input>\n\n                </ion-item>\n\n                <div style="padding: 0px 16px 0px; color: red"\n\n                    *ngIf="fphone.errors && (fphone.dirty || fphone.touched)">\n\n                    <p *ngIf="fphone.errors.required">{{"Phone number is required" | translate}}</p>\n\n                    <p *ngIf="fphone.errors.pattern">{{"Phone number must contain 10 digits." | translate}}</p>\n\n                </div>\n\n            </ion-list>\n\n            <ion-row>\n\n                <ion-col col-6>\n\n                    <div style="padding: 5px;"><button ion-button (click)="addPhone(\'add\',null)" color="gpsc" round\n\n                            small style="width: 100px;" [disabled]="f1.invalid">{{"Add" | translate}}</button></div>\n\n                </ion-col>\n\n                <ion-col col-6>\n\n                    <div style="padding: 5px;"><button ion-button (click)="dismiss()" color="grey" round small\n\n                            style="width: 100px;">{{"Close" | translate}}</button>\n\n                    </div>\n\n                </ion-col>\n\n            </ion-row>\n\n        </form>\n\n        <ion-list *ngIf="isAddEmail">\n\n            <ion-item *ngFor=\'let email of emailList; let i = index\'>\n\n                <ion-grid item-content>\n\n                    <ion-row>\n\n                        <ion-col col-2>\n\n                            <ion-checkbox></ion-checkbox>\n\n                        </ion-col>\n\n                        <ion-col col-8>\n\n                            <p style="margin-top: 8px;">{{email}}</p>\n\n                        </ion-col>\n\n                        <ion-col col-2 style="margin: auto;">\n\n                            <ion-icon style="font-size: 1em;" color="danger" name="trash"\n\n                                (click)="addEmail(\'delete\',i)"></ion-icon>\n\n                        </ion-col>\n\n                    </ion-row>\n\n                </ion-grid>\n\n            </ion-item>\n\n        </ion-list>\n\n        <ion-list *ngIf="isAddPhone">\n\n            <ion-item *ngFor=\'let phone of phonelist; let i = index\'>\n\n                <ion-grid item-content>\n\n                    <ion-row>\n\n                        <ion-col col-2>\n\n                            <ion-checkbox></ion-checkbox>\n\n                        </ion-col>\n\n                        <ion-col col-8>\n\n                            <p style="margin-top: 8px;">{{phone}}</p>\n\n                        </ion-col>\n\n                        <ion-col col-2 style="margin: auto;">\n\n                            <ion-icon style="font-size: 1em;" color="danger" name="trash"\n\n                                (click)="addPhone(\'delete\',i)"></ion-icon>\n\n                        </ion-col>\n\n                    </ion-row>\n\n                </ion-grid>\n\n            </ion-item>\n\n        </ion-list>\n\n        <!-- <pre>{{f.value | json }}</pre> -->\n\n    </div>\n\n</div>'/*ion-inline-end:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/profile/settings/notif-setting/notif-modal.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], NotifModalPage);
    return NotifModalPage;
}());

//# sourceMappingURL=notif-modal.js.map

/***/ }),

/***/ 452:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppLanguages; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_model__ = __webpack_require__(564);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AppLanguages = /** @class */ (function () {
    function AppLanguages(translate) {
        this.translate = translate;
        this._lang = [
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('English'), 'en'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Hindi'), 'hi'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Marathi'), 'mr'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Bangali'), 'bn'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Tamil'), 'ta'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Telugu'), 'te'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Gujarati'), 'gu'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Kannada'), 'kn'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Malayalam'), 'ml'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Spanish'), 'sp'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Persian'), 'fa'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Arabic'), 'ar'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Nepali'), 'ne'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('French'), 'fr'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Punjabi'), 'pn'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Portuguese'), 'pt'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Pashto'), 'ps'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Italian'), 'it'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Dutch'), 'nl'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Indonesian'), 'id')
        ];
    }
    Object.defineProperty(AppLanguages.prototype, "Languages", {
        get: function () {
            return this._lang.slice();
        },
        enumerable: true,
        configurable: true
    });
    AppLanguages = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__["c" /* TranslateService */]])
    ], AppLanguages);
    return AppLanguages;
}());

//# sourceMappingURL=app-languages.js.map

/***/ }),

/***/ 453:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailedPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DetailedPage = /** @class */ (function () {
    function DetailedPage(navParams, alertCtrl) {
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.slideData = [
            {
                image: "../../assets/slides/img1.jpg"
            },
            {
                image: "../../assets/slides/img2.jpg"
            },
            {
                image: "../../assets/slides/img3.jpg"
            }
        ];
        this.deviceDetail = {};
        console.log("check parameters: ", navParams.get("param"));
        this.deviceDetail = navParams.get("param");
    }
    DetailedPage.prototype.ionViewDidEnter = function () {
        this.slides.autoplayDisableOnInteraction = false;
    };
    DetailedPage.prototype.purchase = function () {
        this.alertCtrl.create({
            message: 'Do you want to proceed to payment?',
            buttons: [{
                    text: 'Proceed',
                    handler: function () {
                    }
                },
                {
                    text: 'Back'
                }]
        }).present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Slides"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Slides"])
    ], DetailedPage.prototype, "slides", void 0);
    DetailedPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-supported-devices',template:/*ion-inline-start:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/supported-devices/detailed/detailed.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>Device Details</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n    <!-- <ion-slides pager>\n\n\n\n        <ion-slide style="background-color: green">\n\n          <h2>Slide 1</h2>\n\n        </ion-slide>\n\n      \n\n        <ion-slide style="background-color: blue">\n\n          <h2>Slide 2</h2>\n\n        </ion-slide>\n\n      \n\n        <ion-slide style="background-color: red">\n\n          <h2>Slide 3</h2>\n\n        </ion-slide>\n\n      \n\n      </ion-slides> -->\n\n      <!-- <ion-slides  class="slide-css" autoplay="100" loop="true" speed="3000" pager="true" autoplayDisableOnInteraction = "false"> -->\n\n        <ion-slides  class="slide-css" pager="true" >\n\n        <ion-slide *ngFor="let slide of slideData">\n\n        <img src="{{slide.image}}" />\n\n        </ion-slide>\n\n        </ion-slides>\n\n   <ion-list>\n\n    <ion-item>\n\n      <ion-avatar item-start>\n\n        <img src="assets/slides/img1.jpg" />\n\n      </ion-avatar>\n\n      <h2>{{deviceDetail.Product_name}}</h2>\n\n      <h3>Don\'t Know What To Do!</h3>\n\n      <p>I\'ve had a pretty messed up day. If we just...</p>\n\n    </ion-item>\n\n    </ion-list>\n\n\n\n    <p padding-left padding-right style="text-align: justify;">\n\n      This Tk103 GPS Vehicle Tracker is based on the GSM/GPRS network and GPS satellite positioning system, which set multiple functions of security, positioning, monitoring surveillance, emergency alarms and tracking in its entirety. It can track and monitor remote target by SMS or internet.\n\n    </p>\n\n    <ion-row>  <ion-col col-9>\n\n        <p padding-left padding-right><span style="font-size: 20px; color: cadetblue;">Current Stock - {{deviceDetail.current_stock}}</span></p>\n\n      </ion-col>\n\n\n\n      <ion-col col-3>\n\n        <p padding-left padding-right><ion-icon name="custom-rupee"></ion-icon> <span style="font-size: 20px;">{{deviceDetail.discount_price}}</span></p>\n\n      </ion-col>\n\n    \n\n    </ion-row>\n\n    <ion-row>\n\n      <ion-col>\n\n        <button ion-button block color="gpsc" (click)="purchase()">Purchase</button>\n\n      </ion-col>\n\n    </ion-row>\n\n</ion-content>'/*ion-inline-end:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/supported-devices/detailed/detailed.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"]])
    ], DetailedPage);
    return DetailedPage;
}());

//# sourceMappingURL=detailed.js.map

/***/ }),

/***/ 454:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(455);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__polyfills__ = __webpack_require__(470);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_module__ = __webpack_require__(472);



Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 470:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_web_animations_js_web_animations_min__ = __webpack_require__(471);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_web_animations_js_web_animations_min___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_web_animations_js_web_animations_min__);

//# sourceMappingURL=polyfills.js.map

/***/ }),

/***/ 472:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export createTranslateLoader */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(582);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_http__ = __webpack_require__(340);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_network__ = __webpack_require__(422);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_app_languages__ = __webpack_require__(452);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_status_bar__ = __webpack_require__(419);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_add_devices_paytmwalletlogin_paytmwalletlogin__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_add_devices_add_devices__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_splash_screen__ = __webpack_require__(420);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__providers_network_network__ = __webpack_require__(421);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_native_google_maps__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__node_modules_ion_bottom_drawer__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_push__ = __webpack_require__(147);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_groups_update_group_update_group__ = __webpack_require__(446);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_all_notifications_filter_filter__ = __webpack_require__(437);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_android_permissions__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__shared_side_menu_content_side_menu_content_component__ = __webpack_require__(423);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_profile_profile__ = __webpack_require__(449);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_native_app_version__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__ionic_native_social_sharing__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__ionic_native_text_to_speech__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__ionic_native_speech_recognition__ = __webpack_require__(349);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__ionic_native_camera__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__ionic_native_transfer__ = __webpack_require__(428);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__ionic_native_file_path__ = __webpack_require__(427);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__ionic_native_file__ = __webpack_require__(426);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__ionic_native_file_transfer__ = __webpack_require__(146);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_live_single_device_live_single_device__ = __webpack_require__(430);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__ionic_native_native_geocoder__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_add_devices_upload_doc_view_doc_view_doc__ = __webpack_require__(362);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_add_devices_upload_doc_upload_doc__ = __webpack_require__(432);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__pages_ac_report_ac_report__ = __webpack_require__(431);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__pages_fuel_fuel_consumption_fuel_consumption__ = __webpack_require__(445);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__ionic_native_screen_orientation__ = __webpack_require__(443);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__ngx_translate_core__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__ngx_translate_http_loader__ = __webpack_require__(591);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__angular_common_http__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__pages_login_login__ = __webpack_require__(429);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__providers_geocoder_geocoder__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__providers_pager_pager__ = __webpack_require__(444);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__pages_history_device_modal__ = __webpack_require__(447);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__pages_profile_settings_notif_setting_notif_modal__ = __webpack_require__(451);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__pages_customers_modals_report_setting_report_setting__ = __webpack_require__(436);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__pages_fastag_list_fastag_fastag__ = __webpack_require__(442);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__pages_dealers_dealer_perm_dealer_perm__ = __webpack_require__(440);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__ionic_native_sms__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_51__ionic_native_barcode_scanner__ = __webpack_require__(438);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_52__pages_supported_devices_detailed_detailed__ = __webpack_require__(453);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_53__pages_add_devices_immobilize_modal__ = __webpack_require__(348);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_54__pages_dealers_AddPointsModalPage__ = __webpack_require__(441);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_55__providers_chat_service__ = __webpack_require__(593);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_56__pages_profile_settings_notif_setting_time_picker_time_picker__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_57__angular_common__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_58__ionic_native_native_audio__ = __webpack_require__(144);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_59__ionic_native_geolocation__ = __webpack_require__(439);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_60__ionic_native_call_number__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_61__pages_live_single_device_neraby_vehicle_modal_neraby_vehicle_modal__ = __webpack_require__(377);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_62__pages_msg_utility_msg_utility__ = __webpack_require__(149);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_63__pages_daywise_distance_daywise_distance__ = __webpack_require__(148);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_64__pages_msg_utility_msg_utility_module__ = __webpack_require__(379);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_65__ionic_native_insomnia__ = __webpack_require__(378);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_66__pages_daywise_distance_daywise_distance_module__ = __webpack_require__(376);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_67__pages_dashboard_alertModelPage__ = __webpack_require__(424);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_68__pages_dashboard_announceModel__ = __webpack_require__(425);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_69__ionic_native_in_app_browser__ = __webpack_require__(594);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_70__pages_contact_us_contact_us__ = __webpack_require__(435);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_71__ionic_native_clipboard__ = __webpack_require__(433);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_72__providers_socket_connection_socket_connection__ = __webpack_require__(57);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};























// Custom components






// import { IonicStorageModule } from '@ionic/storage';












































// import { ThemeableBrowser } from '@ionic-native/themeable-browser';



function createTranslateLoader(http) {
    return new __WEBPACK_IMPORTED_MODULE_40__ngx_translate_http_loader__["a" /* TranslateHttpLoader */](http, 'assets/i18n/', '.json');
}
// export function createTranslateLoader(http: Http) {
//   return new TranslateHttpLoader(http, './assets/i18n/', '.json');
// }
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_21__shared_side_menu_content_side_menu_content_component__["a" /* SideMenuContentComponent */],
                __WEBPACK_IMPORTED_MODULE_11__pages_add_devices_add_devices__["d" /* PopoverPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_groups_update_group_update_group__["a" /* UpdateGroup */],
                // OnCreate,
                __WEBPACK_IMPORTED_MODULE_19__pages_all_notifications_filter_filter__["a" /* FilterPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_profile_profile__["b" /* ServiceProviderPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_profile_profile__["c" /* UpdatePasswordPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_live_single_device_live_single_device__["b" /* PoiPage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_add_devices_upload_doc_view_doc_view_doc__["a" /* ViewDoc */],
                __WEBPACK_IMPORTED_MODULE_35__pages_add_devices_upload_doc_upload_doc__["a" /* DocPopoverPage */],
                __WEBPACK_IMPORTED_MODULE_36__pages_ac_report_ac_report__["a" /* ACDetailPage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_fuel_fuel_consumption_fuel_consumption__["b" /* FuelEntryPage */],
                __WEBPACK_IMPORTED_MODULE_42__pages_login_login__["a" /* LanguagesPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_add_devices_paytmwalletlogin_paytmwalletlogin__["c" /* SuccessDetailPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_add_devices_paytmwalletlogin_paytmwalletlogin__["a" /* PaymentHistoryPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_add_devices_add_devices__["c" /* PaymantgatweyPage */],
                __WEBPACK_IMPORTED_MODULE_45__pages_history_device_modal__["a" /* ModalPage */],
                __WEBPACK_IMPORTED_MODULE_46__pages_profile_settings_notif_setting_notif_modal__["a" /* NotifModalPage */],
                __WEBPACK_IMPORTED_MODULE_47__pages_customers_modals_report_setting_report_setting__["a" /* ReportSettingModal */],
                __WEBPACK_IMPORTED_MODULE_48__pages_fastag_list_fastag_fastag__["b" /* FasttagPayNow */],
                __WEBPACK_IMPORTED_MODULE_49__pages_dealers_dealer_perm_dealer_perm__["a" /* DealerPermModalPage */],
                __WEBPACK_IMPORTED_MODULE_67__pages_dashboard_alertModelPage__["a" /* AlertModelPage */],
                __WEBPACK_IMPORTED_MODULE_68__pages_dashboard_announceModel__["a" /* AnnounceModelPage */],
                __WEBPACK_IMPORTED_MODULE_52__pages_supported_devices_detailed_detailed__["a" /* DetailedPage */],
                __WEBPACK_IMPORTED_MODULE_53__pages_add_devices_immobilize_modal__["a" /* ImmobilizeModelPage */],
                //DaywiseDistancePage,
                // MsgUtilityPage,
                __WEBPACK_IMPORTED_MODULE_54__pages_dealers_AddPointsModalPage__["a" /* AddPointsModalPage */],
                __WEBPACK_IMPORTED_MODULE_56__pages_profile_settings_notif_setting_time_picker_time_picker__["b" /* TimePickerModal */],
                __WEBPACK_IMPORTED_MODULE_56__pages_profile_settings_notif_setting_time_picker_time_picker__["a" /* ParkingAlarmPage */],
                // AddBookingModal,
                __WEBPACK_IMPORTED_MODULE_61__pages_live_single_device_neraby_vehicle_modal_neraby_vehicle_modal__["a" /* NearbyVehicleModal */],
                __WEBPACK_IMPORTED_MODULE_11__pages_add_devices_add_devices__["b" /* CustomDurationPage */],
                __WEBPACK_IMPORTED_MODULE_70__pages_contact_us_contact_us__["b" /* ShowSupportContactsModal */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_forms__["ReactiveFormsModule"],
                __WEBPACK_IMPORTED_MODULE_6__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_41__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_64__pages_msg_utility_msg_utility_module__["MsgUtilityPageModule"],
                __WEBPACK_IMPORTED_MODULE_66__pages_daywise_distance_daywise_distance_module__["DaywiseDistancePageModule"],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicModule"].forRoot(__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */], {
                // scrollPadding: false,
                // scrollAssist: true,
                // autoFocusAssist: true
                // scrollAssist: false,
                // autoFocusAssist: false
                }, {
                    links: [
                        { loadChildren: '../pages/add-devices/add-devices.module#AddDevicesPageModule', name: 'AddDevicesPage', segment: 'add-devices', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/ac-report/ac-report.module#AcReportPageModule', name: 'AcReportPage', segment: 'ac-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/paytmwalletlogin/paytmwalletlogin.module#PaytmwalletloginPageModule', name: 'PaytmwalletloginPage', segment: 'paytmwalletlogin', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/upload-doc/add-doc/add-doc.module#AddDocPageModule', name: 'AddDocPage', segment: 'add-doc', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/upload-doc/upload-doc.module#UploadDocPageModule', name: 'UploadDocPage', segment: 'upload-doc', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/update-device/update-device.module#UpdateDevicePageModule', name: 'UpdateDevicePage', segment: 'update-device', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/vehicle-details/vehicle-details.module#VehicleDetailsPageModule', name: 'VehicleDetailsPage', segment: 'vehicle-details', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/all-notifications/notif-map/notif-map.module#NotifMapPageModule', name: 'NotifMapPage', segment: 'notif-map', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/chat/chat.module#ChatPageModule', name: 'ChatPage', segment: 'chat', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/contact-us/contact-us.module#ContactUsPageModule', name: 'ContactUsPage', segment: 'contact-us', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/create-trip/create-trip.module#CreateTripPageModule', name: 'CreateTripPage', segment: 'create-trip', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/customers/customers.module#CustomersPageModule', name: 'CustomersPage', segment: 'customers', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/all-notifications/all-notifications.module#AllNotificationsPageModule', name: 'AllNotificationsPage', segment: 'all-notifications', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/customers/modals/add-customer-modal/add-customer-modal.module#AddCustomerModalModule', name: 'AddCustomerModal', segment: 'add-customer-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/customers/modals/add-device-modal.module#AddDeviceModalPageModule', name: 'AddDeviceModalPage', segment: 'add-device-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/customers/modals/group-modal/group-modal.module#GroupModalPageModule', name: 'GroupModalPage', segment: 'group-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/daily-report-new/daily-report-new.module#DailyReportNewPageModule', name: 'DailyReportNewPage', segment: 'daily-report-new', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/customers/modals/update-cust/update-cust.module#UpdateCustModalPageModule', name: 'UpdateCustModalPage', segment: 'update-cust', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/daily-report/daily-report.module#DailyReportPageModule', name: 'DailyReportPage', segment: 'daily-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dashboard/dashboard.module#DashboardPageModule', name: 'DashboardPage', segment: 'dashboard', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/daywise-distance/daywise-distance.module#DaywiseDistancePageModule', name: 'DaywiseDistancePage', segment: 'daywise-distance', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/daywise-report/daywise-report.module#DaywiseReportPageModule', name: 'DaywiseReportPage', segment: 'daywise-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dealers/add-dealer/add-dealer.module#AddDealerPageModule', name: 'AddDealerPage', segment: 'add-dealer', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dealers/dealers.module#DealerPageModule', name: 'DealerPage', segment: 'dealers', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dealers/edit-dealer/edit-dealer.module#EditDealerPageModule', name: 'EditDealerPage', segment: 'edit-dealer', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/device-summary-repo/device-summary-repo.module#DeviceSummaryRepoPageModule', name: 'DeviceSummaryRepoPage', segment: 'device-summary-repo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/distance-report/distance-report.module#DistanceReportPageModule', name: 'DistanceReportPage', segment: 'distance-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/expenses/add-expense/add-expense.module#AddExpensePageModule', name: 'AddExpensePage', segment: 'add-expense', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/expenses/expense-type-detail/expense-type-detail.module#ExpenseTypeDetailPageModule', name: 'ExpenseTypeDetailPage', segment: 'expense-type-detail', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/expenses/expenses.module#ExpensesPageModule', name: 'ExpensesPage', segment: 'expenses', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/fastag-list/fastag-list.module#FastagListPageModule', name: 'FastagListPage', segment: 'fastag-list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/fastag-list/fastag/fastag.module#FastagPageModule', name: 'FastagPage', segment: 'fastag', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/feedback/feedback.module#FeedbackPageModule', name: 'FeedbackPage', segment: 'feedback', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/fuel/fuel-chart/fuel-chart.module#FuelChartPageModule', name: 'FuelChartPage', segment: 'fuel-chart', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/fuel/fuel-consumption-report/fuel-consumption-report.module#FuelConsumptionReportPageModule', name: 'FuelConsumptionReportPage', segment: 'fuel-consumption-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/fuel/fuel-consumption/fuel-consumption.module#FuelConsumptionPageModule', name: 'FuelConsumptionPage', segment: 'fuel-consumption', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/fuel/fuel-events/fuel-events.module#FuelEventsComponentModule', name: 'FuelEventsComponent', segment: 'fuel-events', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/gallery/gallery.module#GalleryPageModule', name: 'GalleryPage', segment: 'gallery', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/geofence-report/geofence-report.module#GeofenceReportPageModule', name: 'GeofenceReportPage', segment: 'geofence-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/geofence/geofence-show/geofence-show.module#GeofenceShowPageModule', name: 'GeofenceShowPage', segment: 'geofence-show', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/geofence/add-geofence/add-geofence.module#AddGeofencePageModule', name: 'AddGeofencePage', segment: 'add-geofence', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/groups/groups.module#GroupsPageModule', name: 'GroupsPage', segment: 'groups', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/geofence/geofence.module#GeofencePageModule', name: 'GeofencePage', segment: 'geofence', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/history-device/history-device.module#HistoryDevicePageModule', name: 'HistoryDevicePage', segment: 'history-device', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/idle-report/idle-report.module#IdleReportPageModule', name: 'IdleReportPage', segment: 'idle-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/live-single-device/device-settings/device-settings.module#DeviceSettingsPageModule', name: 'DeviceSettingsPage', segment: 'device-settings', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/ignition-report/ignition-report.module#IgnitionReportPageModule', name: 'IgnitionReportPage', segment: 'ignition-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/live-single-device/live-single-device.module#LiveSingleDeviceModule', name: 'LiveSingleDevice', segment: 'live-single-device', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/live/expired/expired.module#ExpiredPageModule', name: 'ExpiredPage', segment: 'expired', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/live/live.module#LivePageModule', name: 'LivePage', segment: 'live', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/loading-unloading-trip/loading-unloading-trip.module#LoadingUnloadingTripPageModule', name: 'LoadingUnloadingTripPage', segment: 'loading-unloading-trip', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/maintenance-reminder/add-reminder/add-reminder.module#AddReminderPageModule', name: 'AddReminderPage', segment: 'add-reminder', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/maintenance-reminder/maintenance-reminder.module#MaintenanceReminderPageModule', name: 'MaintenanceReminderPage', segment: 'maintenance-reminder', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/msg-utility/msg-utility.module#MsgUtilityPageModule', name: 'MsgUtilityPage', segment: 'msg-utility', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/over-speed-repo/over-speed-repo.module#OverSpeedRepoPageModule', name: 'OverSpeedRepoPage', segment: 'over-speed-repo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/poi-list/add-poi/add-poi.module#AddPoiPageModule', name: 'AddPoiPage', segment: 'add-poi', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/poi-list/poi-list.module#PoiListPageModule', name: 'PoiListPage', segment: 'poi-list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/poi-report/poi-report.module#POIReportPageModule', name: 'POIReportPage', segment: 'poi-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile/profile.module#ProfilePageModule', name: 'ProfilePage', segment: 'profile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile/settings/notif-setting/notif-setting.module#NotifSettingPageModule', name: 'NotifSettingPage', segment: 'notif-setting', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile/settings/settings.module#SettingsPageModule', name: 'SettingsPage', segment: 'settings', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/reports-collection/reports-collection.module#ReportsCollectionPageModule', name: 'ReportsCollectionPage', segment: 'reports-collection', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/route-map-show/route-map-show.module#RouteMapShowPageModule', name: 'RouteMapShowPage', segment: 'route-map-show', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/route-voilations/route-voilations.module#RouteVoilationsPageModule', name: 'RouteVoilationsPage', segment: 'route-voilations', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/route/route.module#RoutePageModule', name: 'RoutePage', segment: 'route', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/signup/signup-otp/signup-otp.module#SignupOtpPageModule', name: 'SignupOtpPage', segment: 'signup-otp', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/signup/signup.module#SignupPageModule', name: 'SignupPage', segment: 'signup', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/siren-alert/siren-alert.module#SirenAlertPageModule', name: 'SirenAlertPage', segment: 'siren-alert', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/sos-report/sos-report.module#SosReportPageModule', name: 'SosReportPage', segment: 'sos-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/speed-repo/speed-repo.module#SpeedRepoPageModule', name: 'SpeedRepoPage', segment: 'speed-repo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/supported-devices/supported-devices.module#SupportedDevicesPageModule', name: 'SupportedDevicesPage', segment: 'supported-devices', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/stoppages-repo/stoppages-repo.module#StoppagesRepoPageModule', name: 'StoppagesRepoPage', segment: 'stoppages-repo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/toll-map/toll-map.module#TollMapPageModule', name: 'TollMapPage', segment: 'toll-map', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/toll-report/toll-report.module#TollReportPageModule', name: 'TollReportPage', segment: 'toll-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/toll/toll.module#TollPageModule', name: 'TollPage', segment: 'toll', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/trip-report/trip-report.module#TripReportPageModule', name: 'TripReportPage', segment: 'trip-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/trip-report/trip-review/trip-review.module#TripReviewPageModule', name: 'TripReviewPage', segment: 'trip-review', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/tt/tt.module#TtPageModule', name: 'TtPage', segment: 'tt', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/working-hours-report/working-hours-report.module#WorkingHoursReportPageModule', name: 'WorkingHoursReportPage', segment: 'working-hours-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/your-trips/your-trips.module#YourTripsPageModule', name: 'YourTripsPage', segment: 'your-trips', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] }
                    ]
                }),
                // SignaturePadModule,
                // IonicStorageModule.forRoot(),
                __WEBPACK_IMPORTED_MODULE_39__ngx_translate_core__["b" /* TranslateModule */].forRoot({
                    loader: {
                        provide: __WEBPACK_IMPORTED_MODULE_39__ngx_translate_core__["a" /* TranslateLoader */],
                        useFactory: (createTranslateLoader),
                        deps: [__WEBPACK_IMPORTED_MODULE_41__angular_common_http__["a" /* HttpClient */]],
                    }
                }),
                __WEBPACK_IMPORTED_MODULE_16__node_modules_ion_bottom_drawer__["b" /* IonBottomDrawerModule */],
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"],
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicApp"]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_11__pages_add_devices_add_devices__["d" /* PopoverPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_groups_update_group_update_group__["a" /* UpdateGroup */],
                __WEBPACK_IMPORTED_MODULE_19__pages_all_notifications_filter_filter__["a" /* FilterPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_profile_profile__["b" /* ServiceProviderPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_profile_profile__["c" /* UpdatePasswordPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_live_single_device_live_single_device__["b" /* PoiPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_add_devices_paytmwalletlogin_paytmwalletlogin__["c" /* SuccessDetailPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_add_devices_paytmwalletlogin_paytmwalletlogin__["a" /* PaymentHistoryPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_add_devices_add_devices__["c" /* PaymantgatweyPage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_add_devices_upload_doc_view_doc_view_doc__["a" /* ViewDoc */],
                __WEBPACK_IMPORTED_MODULE_35__pages_add_devices_upload_doc_upload_doc__["a" /* DocPopoverPage */],
                __WEBPACK_IMPORTED_MODULE_36__pages_ac_report_ac_report__["a" /* ACDetailPage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_fuel_fuel_consumption_fuel_consumption__["b" /* FuelEntryPage */],
                __WEBPACK_IMPORTED_MODULE_42__pages_login_login__["a" /* LanguagesPage */],
                __WEBPACK_IMPORTED_MODULE_45__pages_history_device_modal__["a" /* ModalPage */],
                __WEBPACK_IMPORTED_MODULE_46__pages_profile_settings_notif_setting_notif_modal__["a" /* NotifModalPage */],
                __WEBPACK_IMPORTED_MODULE_67__pages_dashboard_alertModelPage__["a" /* AlertModelPage */],
                __WEBPACK_IMPORTED_MODULE_68__pages_dashboard_announceModel__["a" /* AnnounceModelPage */],
                __WEBPACK_IMPORTED_MODULE_47__pages_customers_modals_report_setting_report_setting__["a" /* ReportSettingModal */],
                __WEBPACK_IMPORTED_MODULE_48__pages_fastag_list_fastag_fastag__["b" /* FasttagPayNow */],
                __WEBPACK_IMPORTED_MODULE_49__pages_dealers_dealer_perm_dealer_perm__["a" /* DealerPermModalPage */],
                __WEBPACK_IMPORTED_MODULE_52__pages_supported_devices_detailed_detailed__["a" /* DetailedPage */],
                __WEBPACK_IMPORTED_MODULE_53__pages_add_devices_immobilize_modal__["a" /* ImmobilizeModelPage */],
                __WEBPACK_IMPORTED_MODULE_63__pages_daywise_distance_daywise_distance__["a" /* DaywiseDistancePage */],
                __WEBPACK_IMPORTED_MODULE_62__pages_msg_utility_msg_utility__["a" /* MsgUtilityPage */],
                __WEBPACK_IMPORTED_MODULE_54__pages_dealers_AddPointsModalPage__["a" /* AddPointsModalPage */],
                __WEBPACK_IMPORTED_MODULE_56__pages_profile_settings_notif_setting_time_picker_time_picker__["b" /* TimePickerModal */],
                __WEBPACK_IMPORTED_MODULE_56__pages_profile_settings_notif_setting_time_picker_time_picker__["a" /* ParkingAlarmPage */],
                // AddBookingModal,
                __WEBPACK_IMPORTED_MODULE_61__pages_live_single_device_neraby_vehicle_modal_neraby_vehicle_modal__["a" /* NearbyVehicleModal */],
                __WEBPACK_IMPORTED_MODULE_11__pages_add_devices_add_devices__["b" /* CustomDurationPage */],
                __WEBPACK_IMPORTED_MODULE_70__pages_contact_us_contact_us__["b" /* ShowSupportContactsModal */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_12__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicErrorHandler"],
                // NativePageTransitions,
                // [{ provide: ErrorHandler, useClass: MyErrorHandler }],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicErrorHandler"] },
                __WEBPACK_IMPORTED_MODULE_13__providers_api_service_api_service__["a" /* ApiServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_network__["a" /* Network */],
                __WEBPACK_IMPORTED_MODULE_14__providers_network_network__["a" /* NetworkProvider */],
                __WEBPACK_IMPORTED_MODULE_15__ionic_native_google_maps__["b" /* GoogleMaps */],
                __WEBPACK_IMPORTED_MODULE_15__ionic_native_google_maps__["h" /* Spherical */],
                __WEBPACK_IMPORTED_MODULE_17__ionic_native_push__["a" /* Push */],
                __WEBPACK_IMPORTED_MODULE_23__ionic_native_app_version__["a" /* AppVersion */],
                __WEBPACK_IMPORTED_MODULE_24__ionic_native_social_sharing__["a" /* SocialSharing */],
                __WEBPACK_IMPORTED_MODULE_25__ionic_native_text_to_speech__["a" /* TextToSpeech */],
                __WEBPACK_IMPORTED_MODULE_28__ionic_native_transfer__["a" /* Transfer */],
                __WEBPACK_IMPORTED_MODULE_28__ionic_native_transfer__["b" /* TransferObject */],
                __WEBPACK_IMPORTED_MODULE_30__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_31__ionic_native_file_transfer__["a" /* FileTransfer */],
                __WEBPACK_IMPORTED_MODULE_31__ionic_native_file_transfer__["b" /* FileTransferObject */],
                __WEBPACK_IMPORTED_MODULE_27__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_29__ionic_native_file_path__["a" /* FilePath */],
                __WEBPACK_IMPORTED_MODULE_33__ionic_native_native_geocoder__["a" /* NativeGeocoder */],
                __WEBPACK_IMPORTED_MODULE_38__ionic_native_screen_orientation__["a" /* ScreenOrientation */],
                __WEBPACK_IMPORTED_MODULE_8__providers_app_languages__["a" /* AppLanguages */],
                __WEBPACK_IMPORTED_MODULE_43__providers_geocoder_geocoder__["a" /* GeocoderProvider */],
                __WEBPACK_IMPORTED_MODULE_43__providers_geocoder_geocoder__["a" /* GeocoderProvider */],
                __WEBPACK_IMPORTED_MODULE_44__providers_pager_pager__["a" /* PagerProvider */],
                __WEBPACK_IMPORTED_MODULE_20__ionic_native_android_permissions__["a" /* AndroidPermissions */],
                __WEBPACK_IMPORTED_MODULE_50__ionic_native_sms__["a" /* SMS */],
                __WEBPACK_IMPORTED_MODULE_51__ionic_native_barcode_scanner__["a" /* BarcodeScanner */],
                __WEBPACK_IMPORTED_MODULE_65__ionic_native_insomnia__["a" /* Insomnia */],
                __WEBPACK_IMPORTED_MODULE_26__ionic_native_speech_recognition__["a" /* SpeechRecognition */],
                __WEBPACK_IMPORTED_MODULE_55__providers_chat_service__["a" /* ChatService */],
                __WEBPACK_IMPORTED_MODULE_57__angular_common__["DatePipe"],
                __WEBPACK_IMPORTED_MODULE_58__ionic_native_native_audio__["a" /* NativeAudio */],
                __WEBPACK_IMPORTED_MODULE_59__ionic_native_geolocation__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_69__ionic_native_in_app_browser__["a" /* InAppBrowser */],
                __WEBPACK_IMPORTED_MODULE_60__ionic_native_call_number__["a" /* CallNumber */],
                // ThemeableBrowser,
                __WEBPACK_IMPORTED_MODULE_71__ionic_native_clipboard__["a" /* Clipboard */],
                __WEBPACK_IMPORTED_MODULE_72__providers_socket_connection_socket_connection__["a" /* SocketConnectionProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 496:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 205,
	"./af.js": 205,
	"./ar": 206,
	"./ar-dz": 207,
	"./ar-dz.js": 207,
	"./ar-kw": 208,
	"./ar-kw.js": 208,
	"./ar-ly": 209,
	"./ar-ly.js": 209,
	"./ar-ma": 210,
	"./ar-ma.js": 210,
	"./ar-sa": 211,
	"./ar-sa.js": 211,
	"./ar-tn": 212,
	"./ar-tn.js": 212,
	"./ar.js": 206,
	"./az": 213,
	"./az.js": 213,
	"./be": 214,
	"./be.js": 214,
	"./bg": 215,
	"./bg.js": 215,
	"./bm": 216,
	"./bm.js": 216,
	"./bn": 217,
	"./bn-bd": 218,
	"./bn-bd.js": 218,
	"./bn.js": 217,
	"./bo": 219,
	"./bo.js": 219,
	"./br": 220,
	"./br.js": 220,
	"./bs": 221,
	"./bs.js": 221,
	"./ca": 222,
	"./ca.js": 222,
	"./cs": 223,
	"./cs.js": 223,
	"./cv": 224,
	"./cv.js": 224,
	"./cy": 225,
	"./cy.js": 225,
	"./da": 226,
	"./da.js": 226,
	"./de": 227,
	"./de-at": 228,
	"./de-at.js": 228,
	"./de-ch": 229,
	"./de-ch.js": 229,
	"./de.js": 227,
	"./dv": 230,
	"./dv.js": 230,
	"./el": 231,
	"./el.js": 231,
	"./en-au": 232,
	"./en-au.js": 232,
	"./en-ca": 233,
	"./en-ca.js": 233,
	"./en-gb": 234,
	"./en-gb.js": 234,
	"./en-ie": 235,
	"./en-ie.js": 235,
	"./en-il": 236,
	"./en-il.js": 236,
	"./en-in": 237,
	"./en-in.js": 237,
	"./en-nz": 238,
	"./en-nz.js": 238,
	"./en-sg": 239,
	"./en-sg.js": 239,
	"./eo": 240,
	"./eo.js": 240,
	"./es": 241,
	"./es-do": 242,
	"./es-do.js": 242,
	"./es-mx": 243,
	"./es-mx.js": 243,
	"./es-us": 244,
	"./es-us.js": 244,
	"./es.js": 241,
	"./et": 245,
	"./et.js": 245,
	"./eu": 246,
	"./eu.js": 246,
	"./fa": 247,
	"./fa.js": 247,
	"./fi": 248,
	"./fi.js": 248,
	"./fil": 249,
	"./fil.js": 249,
	"./fo": 250,
	"./fo.js": 250,
	"./fr": 251,
	"./fr-ca": 252,
	"./fr-ca.js": 252,
	"./fr-ch": 253,
	"./fr-ch.js": 253,
	"./fr.js": 251,
	"./fy": 254,
	"./fy.js": 254,
	"./ga": 255,
	"./ga.js": 255,
	"./gd": 256,
	"./gd.js": 256,
	"./gl": 257,
	"./gl.js": 257,
	"./gom-deva": 258,
	"./gom-deva.js": 258,
	"./gom-latn": 259,
	"./gom-latn.js": 259,
	"./gu": 260,
	"./gu.js": 260,
	"./he": 261,
	"./he.js": 261,
	"./hi": 262,
	"./hi.js": 262,
	"./hr": 263,
	"./hr.js": 263,
	"./hu": 264,
	"./hu.js": 264,
	"./hy-am": 265,
	"./hy-am.js": 265,
	"./id": 266,
	"./id.js": 266,
	"./is": 267,
	"./is.js": 267,
	"./it": 268,
	"./it-ch": 269,
	"./it-ch.js": 269,
	"./it.js": 268,
	"./ja": 270,
	"./ja.js": 270,
	"./jv": 271,
	"./jv.js": 271,
	"./ka": 272,
	"./ka.js": 272,
	"./kk": 273,
	"./kk.js": 273,
	"./km": 274,
	"./km.js": 274,
	"./kn": 275,
	"./kn.js": 275,
	"./ko": 276,
	"./ko.js": 276,
	"./ku": 277,
	"./ku.js": 277,
	"./ky": 278,
	"./ky.js": 278,
	"./lb": 279,
	"./lb.js": 279,
	"./lo": 280,
	"./lo.js": 280,
	"./lt": 281,
	"./lt.js": 281,
	"./lv": 282,
	"./lv.js": 282,
	"./me": 283,
	"./me.js": 283,
	"./mi": 284,
	"./mi.js": 284,
	"./mk": 285,
	"./mk.js": 285,
	"./ml": 286,
	"./ml.js": 286,
	"./mn": 287,
	"./mn.js": 287,
	"./mr": 288,
	"./mr.js": 288,
	"./ms": 289,
	"./ms-my": 290,
	"./ms-my.js": 290,
	"./ms.js": 289,
	"./mt": 291,
	"./mt.js": 291,
	"./my": 292,
	"./my.js": 292,
	"./nb": 293,
	"./nb.js": 293,
	"./ne": 294,
	"./ne.js": 294,
	"./nl": 295,
	"./nl-be": 296,
	"./nl-be.js": 296,
	"./nl.js": 295,
	"./nn": 297,
	"./nn.js": 297,
	"./oc-lnc": 298,
	"./oc-lnc.js": 298,
	"./pa-in": 299,
	"./pa-in.js": 299,
	"./pl": 300,
	"./pl.js": 300,
	"./pt": 301,
	"./pt-br": 302,
	"./pt-br.js": 302,
	"./pt.js": 301,
	"./ro": 303,
	"./ro.js": 303,
	"./ru": 304,
	"./ru.js": 304,
	"./sd": 305,
	"./sd.js": 305,
	"./se": 306,
	"./se.js": 306,
	"./si": 307,
	"./si.js": 307,
	"./sk": 308,
	"./sk.js": 308,
	"./sl": 309,
	"./sl.js": 309,
	"./sq": 310,
	"./sq.js": 310,
	"./sr": 311,
	"./sr-cyrl": 312,
	"./sr-cyrl.js": 312,
	"./sr.js": 311,
	"./ss": 313,
	"./ss.js": 313,
	"./sv": 314,
	"./sv.js": 314,
	"./sw": 315,
	"./sw.js": 315,
	"./ta": 316,
	"./ta.js": 316,
	"./te": 317,
	"./te.js": 317,
	"./tet": 318,
	"./tet.js": 318,
	"./tg": 319,
	"./tg.js": 319,
	"./th": 320,
	"./th.js": 320,
	"./tk": 321,
	"./tk.js": 321,
	"./tl-ph": 322,
	"./tl-ph.js": 322,
	"./tlh": 323,
	"./tlh.js": 323,
	"./tr": 324,
	"./tr.js": 324,
	"./tzl": 325,
	"./tzl.js": 325,
	"./tzm": 326,
	"./tzm-latn": 327,
	"./tzm-latn.js": 327,
	"./tzm.js": 326,
	"./ug-cn": 328,
	"./ug-cn.js": 328,
	"./uk": 329,
	"./uk.js": 329,
	"./ur": 330,
	"./ur.js": 330,
	"./uz": 331,
	"./uz-latn": 332,
	"./uz-latn.js": 332,
	"./uz.js": 331,
	"./vi": 333,
	"./vi.js": 333,
	"./x-pseudo": 334,
	"./x-pseudo.js": 334,
	"./yo": 335,
	"./yo.js": 335,
	"./zh-cn": 336,
	"./zh-cn.js": 336,
	"./zh-hk": 337,
	"./zh-hk.js": 337,
	"./zh-mo": 338,
	"./zh-mo.js": 338,
	"./zh-tw": 339,
	"./zh-tw.js": 339
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 496;

/***/ }),

/***/ 523:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 525:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 55:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeocoderProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_native_geocoder__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__ = __webpack_require__(76);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var GeocoderProvider = /** @class */ (function () {
    function GeocoderProvider(_GEOCODE) {
        this._GEOCODE = _GEOCODE;
        console.log('Hello GeocoderProvider Provider');
    }
    GeocoderProvider.prototype.reverseGeocode = function (lat, lng) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._GEOCODE.reverseGeocode(lat, lng)
                .then(function (result) {
                var a = result[0].thoroughfare ? result[0].thoroughfare : null;
                var b = result[0].subThoroughfare ? result[0].subThoroughfare : null;
                var c = result[0].subLocality ? result[0].subLocality : null;
                var d = result[0].subAdministrativeArea ? result[0].subAdministrativeArea : null;
                var e = result[0].postalCode ? result[0].postalCode : null;
                var f = result[0].locality ? result[0].locality : null;
                var g = result[0].countryName ? result[0].countryName : null;
                var h = result[0].administrativeArea ? result[0].administrativeArea : null;
                var str = '';
                if (a != null && a != 'Unnamed Road')
                    str = a + ', ';
                if (b != null && b != 'Unnamed Road')
                    str = str + b + ', ';
                if (c != null && c != 'Unnamed Road')
                    str = str + c + ', ';
                if (d != null && d != 'Unnamed Road')
                    str = str + d + ', ';
                if (e != null && e != 'Unnamed Road')
                    str = str + e + ', ';
                if (f != null && f != 'Unnamed Road')
                    str = str + f + ', ';
                if (g != null && g != 'Unnamed Road')
                    str = str + g + ', ';
                if (h != null && h != 'Unnamed Road')
                    str = str + h + ', ';
                resolve(str);
            })
                .catch(function (error) {
                reject(error);
            });
        });
    };
    GeocoderProvider.prototype.getAreaName = function (lat, lng) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._GEOCODE.reverseGeocode(lat, lng)
                .then(function (result) {
                var c = result[0].subLocality ? result[0].subLocality : null;
                var f = result[0].locality ? result[0].locality : null;
                var str = '';
                if (c != null && c != 'Unnamed Road')
                    str = str + c + ', ';
                if (f != null && f != 'Unnamed Road')
                    str = str + f + ', ';
                resolve(str);
            })
                .catch(function (error) {
                reject(error);
            });
        });
    };
    GeocoderProvider.prototype.geocoderResult = function (lat, lng) {
        return new Promise(function (resolve, reject) {
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__["a" /* Geocoder */].geocode({
                "position": {
                    lat: lat,
                    lng: lng
                }
            }).then(function (results) {
                var addr;
                if (results.length == 0) {
                    addr = 'N/A';
                    resolve(addr);
                }
                else {
                    addr = results[0].extra.lines[0];
                    resolve(addr);
                }
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    GeocoderProvider.prototype.getStateName = function (lat, lng) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._GEOCODE.reverseGeocode(lat, lng)
                .then(function (result) {
                var d = result[0].administrativeArea ? result[0].administrativeArea : null;
                var str = '';
                if (d != null && d != 'Unnamed Road')
                    str = str + d;
                resolve(str);
            })
                .catch(function (error) {
                reject(error);
            });
        });
    };
    GeocoderProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_native_geocoder__["a" /* NativeGeocoder */]])
    ], GeocoderProvider);
    return GeocoderProvider;
}());

//# sourceMappingURL=geocoder.js.map

/***/ }),

/***/ 555:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 563:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return mapStyle; });
var mapStyle = [
    {
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#242f3e"
            }
        ]
    },
    {
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#746855"
            }
        ]
    },
    {
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "color": "#242f3e"
            }
        ]
    },
    {
        "featureType": "administrative.locality",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#d59563"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#d59563"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#263c3f"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#6b9a76"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#38414e"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#212a37"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#9ca5b3"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#746855"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#1f2835"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#f3d19c"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#2f3948"
            }
        ]
    },
    {
        "featureType": "transit.station",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#d59563"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#17263c"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#515c6d"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "color": "#17263c"
            }
        ]
    }
];
//# sourceMappingURL=map-style.model.js.map

/***/ }),

/***/ 564:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Language; });
var Language = /** @class */ (function () {
    function Language(name, key) {
        this.name = name;
        this.key = key;
    }
    return Language;
}());

//# sourceMappingURL=app.model.js.map

/***/ }),

/***/ 57:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SocketConnectionProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_socket_io_client__ = __webpack_require__(434);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_socket_io_client___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_socket_io_client__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__api_service_api_service__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SocketConnectionProvider = /** @class */ (function () {
    // public multipleLiveData: any = {};
    function SocketConnectionProvider(http, apiCall, events) {
        var _this = this;
        this.http = http;
        this.apiCall = apiCall;
        this.events = events;
        this.userdetails = {};
        this.initialMapLoad = true;
        console.log('Hello SocketConnectionProvider Provider');
        this.events.subscribe('user:updated', function (udata) {
            _this.userdetails = udata;
        });
        // this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
        this.getSocketUrl();
    }
    SocketConnectionProvider.prototype.getSocketUrl = function () {
        // if (localStorage.getItem('SOCKET_URL') != null) {
        //   this.socketurl = JSON.parse(localStorage.getItem('SOCKET_URL')) + '/';
        // } else {
        //   this.socketurl = this.apiCall.mainUrl;
        // }
        this.socketurl = "https://soc.oneqlik.in/";
    };
    // getSocket_gps(){
    //   return  this._gps;
    // }
    SocketConnectionProvider.prototype.gpsSocketConnect = function () {
        var that = this;
        that._gps = __WEBPACK_IMPORTED_MODULE_3_socket_io_client___default.a.connect(this.socketurl + 'gps?userId=' + this.userdetails._id, { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
        that._gps.on('connect', function () {
            console.log('GPS IO Connected: ' + that._gps.connected);
            that.events.subscribe("_socketReconnected", function () {
                that.events.publish("Reconnected");
            });
        });
        that._gps.on('connect_failed', function () {
            console.log("Sorry, there seems to be an issue with the socket connection!");
        });
        that._gps.on('error', function () {
            console.log("socket io encountered an error!");
        });
        that._gps.on('reconnecting', function () {
            console.log("socket io trying to reconnect!");
            that.events.publish("_socketReconnected");
        });
        that._gps.on('reconnect_failed', function () {
            console.log("reconnecting failed!");
        });
        that._gps.on('disconnect', function () {
            console.log('socket is disconnect!');
        });
    };
    SocketConnectionProvider.prototype.userACCSocketConnect = function () {
        var that = this;
        that._userACC = __WEBPACK_IMPORTED_MODULE_3_socket_io_client___default.a.connect(this.socketurl + 'userACC?userId=' + this.userdetails._id, { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false, forceNew: false });
        that._userACC.on('connect', function () {
            console.log('userACC IO Connected: ', that._userACC.connected);
        });
    };
    SocketConnectionProvider.prototype.userACCSocketDisconnect = function () {
        this._userACC.disconnect();
        delete this._userACC.io.nsps[this._userACC.nsp];
        delete this._userACC;
        // return new Promise((resolve, reject) => {
        //   this._userACC.on('disconnect', function (result) {
        //     console.log('userACC socket-io disconnect!')
        //     resolve(result);
        //   });
        // });
    };
    SocketConnectionProvider.prototype.gpsSocketDisconnect = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._gps.on('disconnect', function (result) {
                console.log('GPS socket-io disconnect!');
                resolve(result);
            });
        });
    };
    SocketConnectionProvider.prototype.disconnectUserACC = function (imeiList) {
        var that = this;
        imeiList.forEach(function (imei) {
            that._userACC.off(imei + 'acc');
        });
        that._userACC.emit('leaveRoom', imeiList);
        that.leavePromise = that.onceFunc();
    };
    SocketConnectionProvider.prototype.gpsRemoveAllListners = function (imeiList) {
        var that = this;
        imeiList.forEach(function (imei) {
            that._gps.off(imei + 'acc');
        });
        that._gps.emit('leaveRoom', imeiList);
        that.leavePromise = that.onceFunc();
    };
    SocketConnectionProvider.prototype.onceFunc = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._gps.once('leaveRoomConfirm', function (result) {
                console.log('GPS socket-io leaveRoomConfirm!');
                resolve(result);
            });
        });
    };
    SocketConnectionProvider.prototype.socketDisconnect = function () {
        var that = this;
        debugger;
        if (that._gps != undefined)
            that._gps.disconnect();
        // console.log("connected ", that._gps.connected);
        // console.log("disconnected ", that._gps.disconnected);
        // that._gps.on("disconnect", () => {
        //   console.log(that._gps.connected); // false
        // });
        // that._gps.on("disconnect", (reason) => {
        // //   debugger
        //   // console.log('GPS IO disconnected: ', reason);
        // that.events.publish("SocketDisconnected");
        // console.log("event publish executed")
        //   // if (reason === "io server disconnect") {
        //   //   // the disconnection was initiated by the server, you need to reconnect manually
        //   //   that.gpsSocketConnect();
        //   // }
        // });
    };
    SocketConnectionProvider.prototype.notifSocketDisconnect = function () {
        var that = this;
        if (that._notifIO != undefined)
            that._notifIO.disconnect();
    };
    SocketConnectionProvider.prototype.notifIOSocketConnect = function () {
        var _this = this;
        this._notifIO = __WEBPACK_IMPORTED_MODULE_3_socket_io_client___default.a.connect(this.socketurl + 'notifIOV2?userId=' + this.userdetails._id, { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
        this._notifIO.on('connect', function () {
            console.log('NotifIO Connected');
        });
        this._notifIO.on(this.userdetails._id, function (msg) {
            _this.events.publish("cart:addmore", msg);
        });
    };
    SocketConnectionProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_4__api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["Events"]])
    ], SocketConnectionProvider);
    return SocketConnectionProvider;
}());

//# sourceMappingURL=socket-connection.js.map

/***/ }),

/***/ 582:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(419);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(420);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_network_network__ = __webpack_require__(421);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_push__ = __webpack_require__(147);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__shared_side_menu_content_side_menu_content_component__ = __webpack_require__(423);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_ReplaySubject__ = __webpack_require__(585);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_ReplaySubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs_ReplaySubject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_text_to_speech__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_app_version__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ngx_translate_core__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_android_permissions__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_dashboard_alertModelPage__ = __webpack_require__(424);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_dashboard_announceModel__ = __webpack_require__(425);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__providers_socket_connection_socket_connection__ = __webpack_require__(57);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
















var MyApp = /** @class */ (function () {
    function MyApp(translate, platform, statusBar, splashScreen, events, networkProvider, menuCtrl, modalCtrl, push, alertCtrl, app, apiCall, toastCtrl, tts, appVersion, androidPermissions, socketConn) {
        var _this = this;
        this.translate = translate;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.events = events;
        this.networkProvider = networkProvider;
        this.menuCtrl = menuCtrl;
        this.modalCtrl = modalCtrl;
        this.push = push;
        this.alertCtrl = alertCtrl;
        this.app = app;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.tts = tts;
        this.appVersion = appVersion;
        this.androidPermissions = androidPermissions;
        this.socketConn = socketConn;
        this.islogin = {};
        // Settings for the SideMenuContentComponent
        this.sideMenuSettings = {
            accordionMode: true,
            showSelectedOption: true,
            selectedOptionClass: 'active-side-menu-option'
        };
        this.unreadCountObservable = new __WEBPACK_IMPORTED_MODULE_7_rxjs_ReplaySubject__["ReplaySubject"](0);
        this.textDir = "ltr";
        this.pageRoot = "DashboardPage";
        this.reportPref = {};
        if (navigator.onLine) {
        }
        else {
            var alert_1 = this.alertCtrl.create({
                message: 'No internet connection. Make sure that Wi-Fi or mobile data turned on, then try again.',
                buttons: [
                    {
                        text: 'Okay',
                        handler: function () {
                        }
                    }
                ]
            });
            alert_1.present();
        }
        this.callBaseURL();
        this.headerContent = "header";
        translate.setDefaultLang('en');
        this.events.subscribe('lang:key', function (key) {
            translate.setDefaultLang(key);
            translate.use(key);
        });
        this.events.subscribe('latest:version', (function (data) {
            var applink;
            var ver;
            if (_this.platform.is('android')) {
                applink = data.androidApp;
                ver = data.app_version;
            }
            else {
                applink = data.appleApp;
                ver = data.ios_version;
            }
            _this.appVersion.getVersionNumber().then(function (version) {
                if (version < ver) {
                    var alert_2 = _this.alertCtrl.create({
                        message: 'New version is available. Please update application. Latest Version - ' + ver,
                        buttons: [
                            {
                                text: 'Proceed',
                                handler: function () {
                                    window.open(applink, "_blank");
                                }
                            }
                        ],
                        enableBackdropDismiss: false
                    });
                    alert_2.present();
                }
            });
        }));
        this.events.subscribe('user:updated', function (udata) {
            _this.islogin = udata;
            _this.isDealer = udata.isDealer;
            if (localStorage.getItem('FirstGreetDone') === null) {
                _this.greetings();
            }
            if ((localStorage.getItem("custumer_status") == 'OFF' || localStorage.getItem("custumer_status") == null) && (localStorage.getItem("dealer_status") == null || localStorage.getItem("dealer_status") == "OFF")) {
                _this.getCustData(_this.islogin._id);
            }
            _this.initializeOptions();
            _this.getImgUrl();
            _this.reminderChk();
            _this.announceChk();
            _this.checkReportStatus();
        });
        this.events.subscribe('Screen:Changed', function (screenData) {
            if (screenData === 'vehiclelist') {
                _this.rootPage = 'AddDevicesPage';
                _this.pageRoot = 'AddDevicesPage';
            }
            else if (screenData === 'live') {
                _this.rootPage = 'LivePage';
                _this.pageRoot = 'LivePage';
            }
            else {
                _this.rootPage = 'DashboardPage';
                _this.pageRoot = 'DashboardPage';
            }
        });
        platform.ready().then(function () {
            statusBar.styleDefault();
            statusBar.hide();
            _this.splashScreen.hide();
            _this.checkLocationPermission();
            platform.registerBackButtonAction(function () {
                var nav = _this.app.getActiveNavs()[0];
                var activeView = nav.getActive();
                if (activeView.name == "DashboardPage") {
                    var alert_3 = _this.alertCtrl.create({
                        title: 'App termination',
                        message: 'Do you want to close the app?',
                        buttons: [{
                                text: 'Cancel',
                                role: 'cancel',
                                handler: function () {
                                    console.log('Application exit prevented!');
                                }
                            }, {
                                text: 'Close App',
                                handler: function () {
                                    _this.platform.exitApp(); // Close this application
                                }
                            }]
                    });
                    alert_3.present();
                }
                else {
                    if (nav.canGoBack()) { // CHECK IF THE USER IS IN THE ROOT PAGE.
                        nav.pop(); // IF IT'S NOT THE ROOT, POP A PAGE.
                    }
                    else {
                        platform.exitApp(); // IF IT'S THE ROOT, EXIT THE APP.
                    }
                }
            });
        });
        // this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        // console.log("islogin=> " + JSON.stringify(this.islogin));
        this.setsmsforotp = localStorage.getItem('setsms');
        // this.DealerDetails = JSON.stringify(localStorage.getItem('dealer')) || {};
        // this.dealerStatus = this.islogin.isDealer;
        this.initializeApp();
        if (localStorage.getItem("loginflag")) {
            if (localStorage.getItem("SCREEN") != null) {
                if (localStorage.getItem("SCREEN") === 'live') {
                    this.rootPage = 'LivePage';
                }
                else if (localStorage.getItem("SCREEN") === 'dashboard') {
                    this.rootPage = 'DashboardPage';
                }
                else if (localStorage.getItem("SCREEN") === 'vehiclelist') {
                    this.rootPage = 'AddDevicesPage';
                }
            }
            else {
                this.rootPage = 'DashboardPage';
            }
        }
        else {
            this.rootPage = 'LoginPage';
        }
    }
    MyApp.prototype.getCustData = function (user_id) {
        var _this = this;
        var that = this;
        that.apiCall.getcustToken(user_id).subscribe(function (res) {
            that.reportPref = res.cust.report_preference;
            if (localStorage.getItem("pwd") !== null) {
                var passwrd = localStorage.getItem("pwd");
                if (passwrd !== res.cust.pass) {
                    _this.logout();
                }
            }
        }, function (err) { return console.log(err); });
    };
    MyApp.prototype.openModel = function () {
        var pModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_14__pages_dashboard_announceModel__["a" /* AnnounceModelPage */], {});
        pModal.present();
        pModal.onDidDismiss(function () {
        });
    };
    MyApp.prototype.reminderChk = function () {
        var _this = this;
        var baseUrl = "https://www.oneqlik.in/reminder/getReminder?id=" + this.islogin._id;
        this.apiCall.getdevicesForAllVehiclesApi(baseUrl)
            .subscribe(function (resp) {
            if (resp.length != 0) {
                _this.openExpireddeviceModal();
            }
        });
    };
    MyApp.prototype.announceChk = function () {
        var _this = this;
        var b_url = this.apiCall.mainUrl + "users/get_user_setting";
        var Var = { uid: this.islogin._id };
        this.apiCall.urlpasseswithdata(b_url, Var)
            .subscribe(function (resp) {
            if (resp.announcement) {
                _this.openModel();
            }
        }, function (err) {
            console.log(err);
        });
    };
    MyApp.prototype.openExpireddeviceModal = function () {
        var profileModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_13__pages_dashboard_alertModelPage__["a" /* AlertModelPage */]);
        profileModal.onDidDismiss(function () {
        });
        profileModal.present();
    };
    MyApp.prototype.getImgUrl = function () {
        var _this = this;
        var url = "https://www.oneqlik.in/users/shareProfileImage?uid=" + this.islogin._id;
        this.apiCall.getdevicesForAllVehiclesApi(url)
            .subscribe(function (resp) {
            _this.pssss = resp.pass;
            if (_this.pssss != localStorage.getItem("pwd") && resp._id == localStorage.getItem("_id")) {
                _this.logout();
            }
            if (JSON.parse(JSON.stringify(resp)).imageDoc.length > 0) {
                var imgUrl = JSON.parse(JSON.stringify(resp)).imageDoc[0];
                var str1 = imgUrl.split('public/');
                _this.fileUrl = "https://www.oneqlik.in/" + str1[1];
            }
        });
    };
    MyApp.prototype.ionViewWillEnter = function () {
        this.chkCondition();
    };
    MyApp.prototype.checkLocationPermission = function () {
        var _this = this;
        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION).then(function (result) { return console.log('Has permission?', result.hasPermission); }, function (err) { return _this.androidPermissions.requestPermission(_this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION); });
        this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION, this.androidPermissions.PERMISSION.REQUEST_PRIORITY_HIGH_ACCURACY]);
    };
    MyApp.prototype.ngOnInit = function () { };
    MyApp.prototype.greetings = function () {
        var welcome;
        var date = new Date();
        var hour = date.getHours();
        if (hour < 12) {
            welcome = "good morning ";
        }
        else if (hour < 17) {
            welcome = "good afternoon ";
        }
        else {
            welcome = "good evening ";
        }
        this.tts.speak({
            text: welcome + this.islogin.fn + " " + this.islogin.ln,
            locale: 'en-UK'
        })
            .then(function () {
            localStorage.setItem('FirstGreetDone', 'true');
        })
            .catch(function (reason) { return console.log(reason); });
    };
    MyApp.prototype.callBaseURL = function () {
        var _this = this;
        if (localStorage.getItem("ENTERED_BASE_URL") === null) {
            var url = "https://www.oneqlik.in/pullData/getUrlnew";
            this.apiCall.getSOSReportAPI(url)
                .subscribe(function (data) {
                if (data.url) {
                    localStorage.setItem("BASE_URL", JSON.stringify(data.url));
                }
                if (data.socket) {
                    localStorage.setItem("SOCKET_URL", JSON.stringify(data.socket));
                }
                _this.checkVersion();
            });
        }
    };
    MyApp.prototype.checkVersion = function () {
        var _this = this;
        var that = this;
        that.apiCall.getDealerInfo()
            .subscribe(function (response) {
            if (response.app_version || response.ios_version) {
                _this.events.publish('latest:version', response);
            }
            if (response.supAdmin) {
                localStorage.setItem("DealerInfo", response.supAdmin);
            }
        });
    };
    MyApp.prototype.logout = function () {
        var _this = this;
        if (localStorage.getItem('getDevicesInterval_ID')) {
            var intervalid = localStorage.getItem('getDevicesInterval_ID');
            clearInterval(JSON.parse(intervalid));
        }
        this.token = localStorage.getItem("DEVICE_TOKEN");
        var pushdata = {};
        if (this.platform.is('android')) {
            pushdata = {
                "uid": this.islogin._id,
                "token": this.token,
                "os": "android"
            };
        }
        else {
            pushdata = {
                "uid": this.islogin._id,
                "token": this.token,
                "os": "ios"
            };
        }
        this.apiCall.startLoading().present();
        this.apiCall.pullnotifyCall(pushdata)
            .subscribe(function () {
            _this.apiCall.stopLoading();
            localStorage.clear();
            localStorage.setItem('count', null);
            _this.socketConn.socketDisconnect();
            _this.socketConn.notifSocketDisconnect();
            _this.nav.setRoot('LoginPage');
        });
    };
    MyApp.prototype.pushNotify = function () {
        var that = this;
        that.push.hasPermission() // to check if we have permission
            .then(function (res) {
            if (res.isEnabled) {
                console.log('We have permission to send push notifications');
                that.pushSetup();
            }
            else {
                console.log('We do not have permission to send push notifications');
                that.pushSetup();
            }
        });
    };
    MyApp.prototype.pushSetup = function () {
        var _this = this;
        // Create a channel (Android O and above). You'll need to provide the id, description and importance properties.
        this.push.createChannel({
            id: "ignitionon",
            description: "ignition on description",
            sound: 'ignitionon',
            // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
            importance: 4
        }).then(function () { });
        this.push.createChannel({
            id: "ignitionoff",
            description: "ignition off description",
            sound: 'ignitionoff',
            // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
            importance: 4
        }).then(function () { });
        this.push.createChannel({
            id: "devicepowerdisconnected",
            description: "devicepowerdisconnected description",
            sound: 'devicepowerdisconnected',
            // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
            importance: 4
        }).then(function () { });
        this.push.createChannel({
            id: "default",
            description: "default description",
            sound: 'default',
            // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
            importance: 4
        }).then(function () { });
        this.push.createChannel({
            id: "sos",
            description: "default description",
            sound: 'notif',
            // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
            importance: 4
        }).then(function () { });
        this.push.createChannel({
            id: "theft",
            description: "default description",
            sound: 'theft',
            // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
            importance: 4
        }).then(function () { });
        this.push.createChannel({
            id: "tow",
            description: "default description",
            sound: 'tow',
            // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
            importance: 4
        }).then(function () { });
        // Return a list of currently configured channels
        this.push.listChannels().then(function () { });
        // to initialize push notifications
        var that = this;
        var options = {
            android: {
                senderID: '644983599736',
                icon: 'icicon',
                iconColor: 'red'
            },
            ios: {
                alert: 'true',
                badge: true,
                sound: 'true'
            },
            windows: {},
            browser: {
                pushServiceURL: 'http://push.api.phonegap.com/v1/push'
            }
        };
        var pushObject = that.push.init(options);
        pushObject.on('notification').subscribe(function (notification) {
            if (localStorage.getItem("notifValue") != null) {
                if (localStorage.getItem("notifValue") == 'true') {
                }
            }
            if (notification.additionalData.coldstart) {
                var str = notification.message;
                var strArray = str.split(' ');
                var vehName = void 0;
                if (strArray[0] === 'tow' || strArray[0] === 'theft') {
                    if (strArray[3] === 'Vehicle') {
                        var totLength = strArray.length - 4;
                        if (totLength === 1) {
                            vehName = strArray[4];
                        }
                        else {
                            vehName = strArray[4] + (strArray[5] ? (' ' + strArray[5]) : '') + (strArray[6] ? (' ' + strArray[6]) : '') + (strArray[7] ? (' ' + strArray[7]) : '') + (strArray[8] ? (' ' + strArray[8]) : '') + (strArray[9] ? (' ' + strArray[9]) : '');
                        }
                    }
                    _this.nav.push('ParkingAlarmPage', {
                        "vehName": vehName
                    });
                }
            }
            else if (notification.additionalData.foreground) {
                var toast = _this.toastCtrl.create({
                    message: notification.message,
                    duration: 2000
                });
                toast.present();
                var str = notification.message;
                var strArray = str.split(' ');
                var vehName = void 0;
                if (strArray[0] === 'Tow' || strArray[0] === 'Theft') {
                    if (strArray[3] === 'Vehicle') {
                        var totLength = strArray.length - 4;
                        if (totLength === 1) {
                            vehName = strArray[4];
                        }
                        else {
                            vehName = strArray[4] + (strArray[5] ? (' ' + strArray[5]) : '') + (strArray[6] ? (' ' + strArray[6]) : '') + (strArray[7] ? (' ' + strArray[7]) : '') + (strArray[8] ? (' ' + strArray[8]) : '') + (strArray[9] ? (' ' + strArray[9]) : '');
                        }
                    }
                    _this.nav.push('ParkingAlarmPage', {
                        "vehName": vehName
                    });
                }
            }
        });
        pushObject.on('registration')
            .subscribe(function (registration) {
            console.log("device token => " + registration.registrationId);
            localStorage.setItem("DEVICE_TOKEN", registration.registrationId);
        });
        pushObject.on('error').subscribe(function (error) {
            console.error('Error with Push plugin', error);
        });
    };
    MyApp.prototype.showToastMsg = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'middle'
        });
        toast.present();
    };
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        var that = this;
        that.platform.ready().then(function () {
            that.platform.pause.subscribe(function (ev) {
                localStorage.setItem("backgroundModeTime", JSON.stringify(new Date()));
            });
            that.pushNotify();
        });
        // Initialize some options
        // that.initializeOptions();
        // Change the value for the batch every 5 seconds
        setInterval(function () {
            _this.unreadCountObservable.next(Math.floor(Math.random() * 10) + 1);
        }, 5000);
    };
    MyApp.prototype.checkReportStatus = function () {
        this.suboptions = [];
        this.fuelSuboptions = [];
        if (this.islogin.report_preference.ac_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('AC Report'),
                component: 'AcReportPage'
            });
        }
        if (this.islogin.report_preference.daily_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('Daily Report'),
                component: 'DailyReportPage'
            });
        }
        if (this.islogin.report_preference.daywise_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('Daywise Report'),
                component: 'DaywiseReportPage'
            });
        }
        if (this.islogin.report_preference.distance_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('Distance Report'),
                component: 'DistanceReportPage'
            });
        }
        if (this.islogin.report_preference.geofence_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('Geofence Report'),
                component: 'GeofenceReportPage'
            });
        }
        if (this.islogin.report_preference.idle_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('Idle Report'),
                component: 'IdleReportPage'
            });
        }
        if (this.islogin.report_preference.ignition_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('Ignition Report'),
                component: 'IgnitionReportPage'
            });
        }
        this.suboptions.push({
            iconName: 'clipboard',
            displayText: this.translate.instant('Loading Unloading Trip'),
            component: 'LoadingUnloadingTripPage'
        });
        if (this.islogin.report_preference.overspeed_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('Over Speed Report'),
                component: 'OverSpeedRepoPage'
            });
        }
        if (this.islogin.report_preference.poi_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('POI Report'),
                component: 'POIReportPage'
            });
        }
        if (this.islogin.report_preference.route_violation_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('Route Violation Report'),
                component: 'RouteVoilationsPage'
            });
        }
        if (this.islogin.report_preference.sos_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('SOS'),
                component: 'SosReportPage'
            });
        }
        if (this.islogin.report_preference.speed_variation.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('Speed Variation Report'),
                component: 'SpeedRepoPage'
            });
        }
        if (this.islogin.report_preference.stoppage_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('Stoppages Report'),
                component: 'StoppagesRepoPage'
            });
        }
        if (this.islogin.report_preference.summary_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('Summary Report'),
                component: 'DeviceSummaryRepoPage'
            });
        }
        if (this.islogin.report_preference.trip_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('Trip Report'),
                component: 'TripReportPage'
            });
        }
        if (this.islogin.report_preference.user_trip_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('User Trip report'),
                component: 'YourTripsPage'
            });
        }
        this.suboptions.push({
            iconName: 'clipboard',
            displayText: this.translate.instant('Value Screen'),
            component: 'DailyReportNewPage'
        });
        if (this.islogin.report_preference.user_trip_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('Working Hours Report'),
                component: 'WorkingHoursReportPage'
            });
        }
        this.fuelSuboptions.push({
            iconName: 'custom-fuel',
            displayText: this.translate.instant('Fuel Entry'),
            component: 'FuelConsumptionPage'
        });
        if (this.islogin.report_preference.fuel_consumption_report.Rstatus) {
            this.fuelSuboptions.push({
                iconName: 'custom-fuel',
                displayText: this.translate.instant('Fuel Consumption'),
                component: 'FuelConsumptionReportPage'
            });
        }
        this.fuelSuboptions.push({
            iconName: 'custom-fuel',
            displayText: this.translate.instant('Fuel Chart'),
            component: 'FuelChartPage'
        });
    };
    MyApp.prototype.onOptionSelected = function (option) {
        var _this = this;
        this.menuCtrl.close().then(function () {
            if (option.custom && option.custom.isLogin) {
                _this.presentAlert('You\'ve clicked the login option!');
            }
            else if (option.custom && option.custom.isLogout) {
                _this.presentAlert('You\'ve clicked the logout option!');
            }
            else if (option.custom && option.custom.isExternalLink) {
                var url = option.custom.externalUrl;
                window.open(url, '_blank');
            }
            else {
                var params = option.custom && option.custom.param;
                if (option.displayText == _this.translate.instant('Admin') && option.component == 'DashboardPage') {
                    localStorage.setItem("dealer_status", 'OFF');
                    localStorage.setItem('details', localStorage.getItem("superAdminData"));
                    localStorage.removeItem('superAdminData');
                }
                if (option.displayText == _this.translate.instant('Dealers') && option.component == 'DashboardPage') {
                    if (localStorage.getItem('custumer_status') == 'ON') {
                        var _dealdata = JSON.parse(localStorage.getItem("dealer"));
                        if (localStorage.getItem("superAdminData") != null || _this.islogin.isSuperAdmin == true) {
                            localStorage.setItem("dealer_status", 'ON');
                        }
                        else {
                            if (_dealdata.isSuperAdmin == true) {
                                localStorage.setItem("dealer_status", 'OFF');
                            }
                            else {
                                localStorage.setItem("OnlyDealer", "true");
                            }
                        }
                        localStorage.setItem("custumer_status", 'OFF');
                        localStorage.setItem('details', localStorage.getItem("dealer"));
                    }
                    else {
                        console.log("something wrong!!");
                    }
                }
                _this.nav.setRoot(option.component, params);
            }
        });
    };
    MyApp.prototype.collapseMenuOptions = function () {
        this.sideMenu.collapseAllOptions();
    };
    MyApp.prototype.presentAlert = function (message) {
        var alert = this.alertCtrl.create({
            title: 'Information',
            message: message,
            buttons: ['Ok']
        });
        alert.present();
    };
    MyApp.prototype.openPage = function (page, index) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        if (page.component) {
            this.nav.setRoot(page.component);
            this.menuCtrl.close();
        }
        else {
            if (this.selectedMenu) {
                this.selectedMenu = 0;
            }
            else {
                this.selectedMenu = index;
            }
        }
    };
    MyApp.prototype.resize = function () {
        var offset = this.headerTag.nativeElement.offsetHeight;
        this.scrollableTag.nativeElement.style.marginTop = offset + 'px';
    };
    MyApp.prototype.chkCondition = function () {
        var _this = this;
        // this.resize();
        this.events.subscribe("event_sidemenu", function (data) {
            _this.islogin = JSON.parse(data);
            _this.options[2].displayText = _this.translate.instant('Dealers');
            _this.options[2].iconName = 'person';
            _this.options[2].component = 'DashboardPage';
            _this.initializeOptions();
        });
        this.initializeOptions();
    };
    MyApp.prototype.initializeOptions = function () {
        var _this = this;
        this.options = new Array();
        // if ((this.islogin.role == undefined || this.islogin.role == 'supAdm' || this.islogin.role == 'adm') && this.islogin.isSuperAdmin == true && (this.islogin.isDealer == false || this.islogin.isDealer == undefined)) {
        if (this.islogin.isSuperAdmin === true && (this.islogin.isDealer === false || this.islogin.isDealer === undefined)) {
            this.options.push({
                iconName: 'home',
                displayText: this.translate.instant('Home'),
                component: this.pageRoot,
            });
            this.options.push({
                iconName: 'people',
                displayText: this.translate.instant('Groups'),
                component: 'GroupsPage'
            });
            this.options.push({
                iconName: 'people',
                displayText: this.translate.instant('Dealers'),
                component: 'DealerPage'
            });
            this.options.push({
                iconName: 'contacts',
                displayText: this.translate.instant('Customers'),
                component: 'CustomersPage'
            });
            this.options.push({
                iconName: 'list',
                displayText: this.translate.instant('POI List'),
                component: 'PoiListPage'
            });
            this.options.push({
                iconName: 'cash',
                displayText: this.translate.instant('Expenses'),
                component: 'ExpensesPage'
            });
            this.options.push({
                iconName: 'chatboxes',
                displayText: this.translate.instant('Message Utility'),
                component: 'FastagListPage'
            });
            this.options.push({
                displayText: this.translate.instant('Fuel'),
                iconName: 'custom-fuel',
                iconName123: 'arrow-left',
                // iconName: 'arrow-dropright',
                suboptions: [
                    {
                        iconName: 'custom-fuel',
                        displayText: this.translate.instant('Fuel Entry'),
                        component: 'FuelConsumptionPage'
                    },
                    {
                        iconName: 'custom-fuel',
                        displayText: this.translate.instant('Fuel Consumption'),
                        component: 'FuelConsumptionReportPage'
                    },
                    {
                        iconName: 'custom-fuel',
                        displayText: this.translate.instant('Fuel Chart'),
                        component: 'FuelChartPage'
                    }
                ]
            });
            this.options.push({
                displayText: this.translate.instant('Vehicle Maintenance'),
                // iconName: 'arrow-dropright',
                iconName123: 'arrow-left',
                iconName: 'cog',
                suboptions: [
                    {
                        iconName: 'notifications',
                        displayText: this.translate.instant('Reminders'),
                        component: 'MaintenanceReminderPage'
                    }
                ]
            });
            this.options.push({
                displayText: this.translate.instant(this.translate.instant('Reports')),
                // iconName: 'arrow-dropright',
                iconName123: 'arrow-left',
                iconName: 'clipboard',
                suboptions: this.suboptions
            });
            //==== no reports for superadmin and dealer
            // Load special options
            // -----------------------------------------------
            // this.options.push({
            //   displayText: this.translate.instant('Tolls'),
            //   iconName: 'custom-toll',
            //   suboptions: [
            //     {
            //       iconName: 'custom-toll',
            //       displayText: this.translate.instant('Toll List'),
            //       component: 'TollPage'
            //     },
            //     {
            //       iconName: 'custom-toll',
            //       displayText: this.translate.instant('Toll Report'),
            //       component: 'TollReportPage'
            //     }
            //   ]
            // });
            this.options.push({
                displayText: this.translate.instant('Customer Support'),
                iconName: 'person',
                suboptions: [
                    {
                        iconName: 'star',
                        displayText: this.translate.instant('Rate Us'),
                        component: 'FeedbackPage'
                    },
                    {
                        iconName: 'mail',
                        displayText: this.translate.instant('Contact Us'),
                        component: 'ContactUsPage'
                    }
                ]
            });
            this.options.push({
                displayText: this.translate.instant('Profile'),
                iconName: 'person',
                component: 'ProfilePage'
            });
        }
        else {
            // if (this.islogin.role == undefined && this.islogin.isSuperAdmin == false && this.islogin.isDealer == false) {
            if (this.islogin.isSuperAdmin === false && (this.islogin.isDealer === false || this.islogin.isDealer === undefined)) {
                this.options.push({
                    iconName: 'home',
                    displayText: this.translate.instant('Home'),
                    component: this.pageRoot,
                });
                this.options.push({
                    iconName: 'people',
                    displayText: this.translate.instant('Groups'),
                    component: 'GroupsPage'
                });
                this.options.push({
                    iconName: 'list',
                    displayText: this.translate.instant('POI List'),
                    component: 'PoiListPage'
                });
                this.options.push({
                    iconName: 'cash',
                    displayText: this.translate.instant('Expenses'),
                    component: 'ExpensesPage'
                });
                this.options.push({
                    displayText: this.translate.instant('Fuel'),
                    iconName123: 'arrow-left',
                    iconName: 'custom-fuel',
                    suboptions: this.fuelSuboptions
                });
                this.options.push({
                    displayText: this.translate.instant('Vehicle Maintenance'),
                    iconName123: 'arrow-left',
                    iconName: 'cog',
                    suboptions: [
                        {
                            iconName: 'notifications',
                            displayText: this.translate.instant('Reminders'),
                            component: 'MaintenanceReminderPage'
                        }
                    ]
                });
                this.options.push({
                    displayText: this.translate.instant(this.translate.instant('Reports')),
                    iconName123: 'arrow-left',
                    iconName: 'clipboard',
                    suboptions: this.suboptions
                });
                // Load special options
                // -----------------------------------------------
                // this.options.push({
                //   displayText: this.translate.instant('Tolls'),
                //   iconName: 'custom-toll',
                //   suboptions: [
                //     {
                //       iconName: 'custom-toll',
                //       displayText: this.translate.instant('Toll List'),
                //       component: 'TollPage'
                //     },
                //     {
                //       iconName: 'custom-toll',
                //       displayText: this.translate.instant('Toll Report'),
                //       component: 'TollReportPage'
                //     }
                //   ]
                // });
                this.options.push({
                    displayText: this.translate.instant('Customer Support'),
                    iconName: 'person',
                    suboptions: [
                        {
                            iconName: 'star',
                            displayText: this.translate.instant('Rate Us'),
                            component: 'FeedbackPage'
                        },
                        {
                            iconName: 'mail',
                            displayText: this.translate.instant('Contact Us'),
                            component: 'ContactUsPage'
                        }
                    ]
                });
                this.options.push({
                    displayText: this.translate.instant('Profile'),
                    iconName: 'person',
                    component: 'ProfilePage'
                });
            }
            else {
                if (this.islogin.isSuperAdmin === false && this.islogin.isDealer === true) {
                    this.options.push({
                        iconName: 'home',
                        displayText: this.translate.instant('Home'),
                        component: this.pageRoot,
                    });
                    this.options.push({
                        iconName: 'people',
                        displayText: this.translate.instant('Groups'),
                        component: 'GroupsPage'
                    });
                    this.options.push({
                        iconName: 'contacts',
                        displayText: this.translate.instant('Customers'),
                        component: 'CustomersPage'
                    });
                    this.options.push({
                        iconName: 'list',
                        displayText: this.translate.instant('POI List'),
                        component: 'PoiListPage'
                    });
                    this.options.push({
                        iconName: 'cash',
                        displayText: this.translate.instant('Expenses'),
                        component: 'ExpensesPage'
                    });
                    this.options.push({
                        displayText: this.translate.instant('Fuel'),
                        iconName123: 'arrow-left',
                        iconName: 'custom-fuel',
                        suboptions: this.fuelSuboptions
                    });
                    this.options.push({
                        displayText: this.translate.instant('Vehicle Maintenance'),
                        iconName123: 'arrow-left',
                        iconName: 'cog',
                        suboptions: [
                            {
                                iconName: 'notifications',
                                displayText: this.translate.instant('Reminders'),
                                component: 'MaintenanceReminderPage'
                            }
                        ]
                    });
                    this.options.push({
                        displayText: this.translate.instant(this.translate.instant('Reports')),
                        iconName123: 'arrow-left',
                        iconName: 'clipboard',
                        suboptions: this.suboptions
                    });
                    // no reports fro superadmin and dealer
                    // -----------------------------------------------
                    // this.options.push({
                    //   displayText: this.translate.instant('Tolls'),
                    //   iconName: 'custom-toll',
                    //   suboptions: [
                    //     {
                    //       iconName: 'custom-toll',
                    //       displayText: this.translate.instant('Toll List'),
                    //       component: 'TollPage'
                    //     },
                    //     {
                    //       iconName: 'custom-toll',
                    //       displayText: this.translate.instant('Toll Report'),
                    //       component: 'TollReportPage'
                    //     }
                    //   ]
                    // });
                    this.options.push({
                        displayText: this.translate.instant('Customer Support'),
                        iconName: 'person',
                        suboptions: [
                            {
                                iconName: 'star',
                                displayText: this.translate.instant('Rate Us'),
                                component: 'FeedbackPage'
                            },
                            {
                                iconName: 'mail',
                                displayText: this.translate.instant('Contact Us'),
                                component: 'ContactUsPage'
                            }
                        ]
                    });
                    this.options.push({
                        displayText: this.translate.instant('Profile'),
                        iconName: 'person',
                        component: 'ProfilePage'
                    });
                }
                else {
                    this.options.push({
                        iconName: 'home',
                        displayText: this.translate.instant('Home'),
                        component: this.pageRoot,
                    });
                    this.options.push({
                        iconName: 'people',
                        displayText: this.translate.instant('Groups'),
                        component: 'GroupsPage'
                    });
                    this.options.push({
                        iconName: 'list',
                        displayText: this.translate.instant('POI List'),
                        component: 'PoiListPage'
                    });
                    this.options.push({
                        iconName: 'cash',
                        displayText: this.translate.instant('Expenses'),
                        component: 'ExpensesPage'
                    });
                    this.options.push({
                        displayText: this.translate.instant('Fuel'),
                        iconName123: 'arrow-left',
                        iconName: 'custom-fuel',
                        suboptions: this.fuelSuboptions
                    });
                    this.options.push({
                        displayText: this.translate.instant('Vehicle Maintenance'),
                        iconName123: 'arrow-left',
                        iconName: 'cog',
                        suboptions: [
                            {
                                iconName: 'notifications',
                                displayText: this.translate.instant('Reminders'),
                                component: 'MaintenanceReminderPage'
                            }
                        ]
                    });
                    // Load options with nested items (with icons)
                    // -----------------------------------------------
                    this.options.push({
                        displayText: this.translate.instant(this.translate.instant('Reports')),
                        iconName123: 'arrow-left',
                        iconName: 'clipboard',
                        suboptions: this.suboptions
                    });
                    // Load special options
                    // -----------------------------------------------
                    // this.options.push({
                    //   displayText: this.translate.instant('Tolls'),
                    //   iconName: 'custom-toll',
                    //   suboptions: [
                    //     {
                    //       iconName: 'custom-toll',
                    //       displayText: this.translate.instant('Toll List'),
                    //       component: 'TollPage'
                    //     },
                    //     {
                    //       iconName: 'custom-toll',
                    //       displayText: this.translate.instant('Toll Report'),
                    //       component: 'TollReportPage'
                    //     }
                    //   ]
                    // });
                    this.options.push({
                        displayText: this.translate.instant('Customer Support'),
                        iconName: 'person',
                        suboptions: [
                            {
                                iconName: 'star',
                                displayText: this.translate.instant('Rate Us'),
                                component: 'FeedbackPage'
                            },
                            {
                                iconName: 'mail',
                                displayText: this.translate.instant('Contact Us'),
                                component: 'ContactUsPage'
                            },
                        ]
                    });
                    this.options.push({
                        displayText: this.translate.instant('Profile'),
                        iconName: 'person',
                        component: 'ProfilePage'
                    });
                }
            }
        }
        var _DealerStat = localStorage.getItem("dealer_status");
        var _CustStat = localStorage.getItem("custumer_status");
        var onlyDeal = localStorage.getItem("OnlyDealer");
        if (_DealerStat != null || _CustStat != null) {
            if (_DealerStat == 'ON' && _CustStat == 'OFF') {
                this.options[2].displayText = this.translate.instant('Admin');
                this.options[2].iconName = 'person';
                this.options[2].component = 'DashboardPage';
                this.options[3].displayText = this.translate.instant('Customers');
                this.options[3].iconName = 'contacts';
                this.options[3].component = 'CustomersPage';
            }
            else {
                if (_DealerStat == 'OFF' && _CustStat == 'ON') {
                    this.options[2].displayText = this.translate.instant('Dealers');
                    this.options[2].iconName = 'person';
                    this.options[2].component = 'DashboardPage';
                }
                else {
                    if (_DealerStat == 'OFF' && _CustStat == 'OFF' && onlyDeal == null) {
                        this.options[2].displayText = this.translate.instant('Dealers');
                        this.options[2].iconName = 'person';
                        this.options[2].component = 'DealerPage';
                        this.options[3].displayText = this.translate.instant('Customers');
                        this.options[3].iconName = 'contacts';
                        this.options[3].component = 'CustomersPage';
                    }
                    else {
                        if (onlyDeal == 'true') {
                            this.options[2].displayText = this.translate.instant('Customers');
                            this.options[2].iconName = 'contacts';
                            this.options[2].component = 'CustomersPage';
                        }
                    }
                }
            }
        }
        this.events.subscribe("sidemenu:event", function (data) {
            if (data) {
                _this.options[2].displayText = _this.translate.instant('Dealers');
                _this.options[2].iconName = 'person';
                _this.options[2].component = 'DashboardPage';
            }
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('headerTag'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], MyApp.prototype, "headerTag", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('scrollableTag'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], MyApp.prototype, "scrollableTag", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Nav"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Nav"])
    ], MyApp.prototype, "nav", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_6__shared_side_menu_content_side_menu_content_component__["a" /* SideMenuContentComponent */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_6__shared_side_menu_content_side_menu_content_component__["a" /* SideMenuContentComponent */])
    ], MyApp.prototype, "sideMenu", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-main-page',template:/*ion-inline-start:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/app/app.html"*/'<ion-menu [content]="content" [swipeEnabled]="false" (ionOpen)="chkCondition()">\n\n  <ion-header>\n\n  </ion-header>\n\n  <ion-content id="outerNew">\n\n    <div class="headProf" #headerTag ion-fixed>\n\n      <img *ngIf="!fileUrl" src="assets/imgs/dummy-user-img.png" />\n\n      <img *ngIf="fileUrl" src="{{fileUrl}}" />\n\n      <div>\n\n        <h4>{{islogin.fn}}&nbsp;{{islogin.ln}}</h4>\n\n        <p style="font-size: 12px">\n\n          <ion-icon name="mail"></ion-icon>&nbsp;{{islogin.email}}\n\n        </p>\n\n        <p style="font-size: 12px">\n\n          <ion-icon name="call"></ion-icon>&nbsp;{{islogin.phn}}\n\n        </p>\n\n      </div>\n\n    </div>\n\n    <div style="margin-top: 66%;">\n\n      <side-menu-content style="width:100%; height:100vh" [settings]="sideMenuSettings" [options]="options"\n\n        (change)="onOptionSelected($event)"></side-menu-content>\n\n    </div>\n\n\n\n  </ion-content>\n\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>\n\n'/*ion-inline-end:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_11__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_4__providers_network_network__["a" /* NetworkProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["MenuController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_push__["a" /* Push */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["App"],
            __WEBPACK_IMPORTED_MODULE_8__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_text_to_speech__["a" /* TextToSpeech */],
            __WEBPACK_IMPORTED_MODULE_10__ionic_native_app_version__["a" /* AppVersion */],
            __WEBPACK_IMPORTED_MODULE_12__ionic_native_android_permissions__["a" /* AndroidPermissions */],
            __WEBPACK_IMPORTED_MODULE_15__providers_socket_connection_socket_connection__["a" /* SocketConnectionProvider */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 584:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SideMenuOptionSelect; });
// SideMenuOptionSelect constant
var SideMenuOptionSelect = 'sideMenu:optionSelect';
//# sourceMappingURL=side-menu-option-select-event.js.map

/***/ }),

/***/ 593:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export ChatMessage */
/* unused harmony export UserInfo */
/* unused harmony export userAvatar */
/* unused harmony export toUserAvatar */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_operators_map__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_operators_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(71);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ChatMessage = /** @class */ (function () {
    function ChatMessage() {
    }
    return ChatMessage;
}());

var UserInfo = /** @class */ (function () {
    function UserInfo() {
    }
    return UserInfo;
}());

var userAvatar = 'https://github.com/HsuanXyz/ionic3-chat/blob/master/src/assets/user.jpg?raw=true';
var toUserAvatar = 'https://github.com/HsuanXyz/ionic3-chat/blob/master/src/assets/to-user.jpg?raw=true';
var ChatService = /** @class */ (function () {
    function ChatService(http, events) {
        this.http = http;
        this.events = events;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    }
    ChatService.prototype.mockNewMsg = function (msg) {
        var _this = this;
        var mockMsg = {
            messageId: Date.now().toString(),
            userId: '210000198410281948',
            userName: 'Hancock',
            userAvatar: toUserAvatar,
            toUserId: '140000198202211138',
            time: Date.now(),
            message: msg.message,
            status: 'success'
        };
        setTimeout(function () {
            _this.events.publish('chat:received', mockMsg, Date.now());
        }, Math.random() * 1800);
    };
    ChatService.prototype.getMsgList = function () {
        var msgListUrl = 'https://raw.githubusercontent.com/HsuanXyz/ionic3-chat/master/src/assets/mock/msg-list.json';
        return this.http.get(msgListUrl)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators_map__["map"])(function (response) { return response.array.map(function (msg) { return (__assign({}, msg, { userAvatar: msg.userAvatar === './assets/user.jpg' ? userAvatar : toUserAvatar })); }); }));
    };
    ChatService.prototype.sendMsg = function (msg) {
        var _this = this;
        return new Promise(function (resolve) { return setTimeout(function () { return resolve(msg); }, Math.random() * 1000); })
            .then(function () { return _this.mockNewMsg(msg); });
    };
    ChatService.prototype.getUserInfo = function () {
        var userInfo = {
            id: this.islogin._id,
            name: 'Luff',
            avatar: userAvatar
        };
        return new Promise(function (resolve) { return resolve(userInfo); });
    };
    ChatService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"]])
    ], ChatService);
    return ChatService;
}());

//# sourceMappingURL=chat-service.js.map

/***/ }),

/***/ 79:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddDevicesPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return PopoverPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return CustomDurationPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return PaymantgatweyPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_geocoder_geocoder__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__immobilize_modal__ = __webpack_require__(348);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ion_bottom_drawer__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__profile_settings_notif_setting_time_picker_time_picker__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_call_number__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_social_sharing__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_tinyurl__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_tinyurl___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_tinyurl__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var AddDevicesPage = /** @class */ (function () {
    function AddDevicesPage(navCtrl, navParams, apiCall, alertCtrl, toastCtrl, modalCtrl, popoverCtrl, geocoderApi, plt, translate, events, callNumber, socialSharing) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.modalCtrl = modalCtrl;
        this.popoverCtrl = popoverCtrl;
        this.geocoderApi = geocoderApi;
        this.plt = plt;
        this.translate = translate;
        this.events = events;
        this.callNumber = callNumber;
        this.socialSharing = socialSharing;
        this.loadProgress = 0;
        this.isPayNow = false;
        this.isExpIred = false;
        this.isPayAlert = false;
        this.selectedArray = [];
        this.setselect = false;
        this.allDevices = [];
        this.allDevicesSearch = [];
        this.users_data = [];
        this.isPaymentPage = [];
        this.option_switch = false;
        this.searchCountryString = ''; // initialize your searchCountryString string empty
        this.drawerState1 = __WEBPACK_IMPORTED_MODULE_7_ion_bottom_drawer__["a" /* DrawerState */].Docked;
        this.dockedHeight1 = 300;
        this.distanceTop1 = 378;
        this.minimumHeight1 = 0;
        this.condition = 'gpsc';
        this.condition1 = 'whiteee';
        this.condition2 = 'whiteee';
        this.condition3 = 'whiteee';
        this.deviceDeatils = {};
        this.page = 0;
        this.limit = 8;
        this.searchQuery = '';
        this.clicked = false;
        this.checkIfStat = "ALL";
        this.toggled = false;
        this.all_devices = [];
        this.showEmptyStatus = false;
        this.showDrawer = false;
        this.shouldBounce = true;
        this.dockedHeight = 220;
        this.distanceTop = 556;
        this.drawerState = __WEBPACK_IMPORTED_MODULE_7_ion_bottom_drawer__["a" /* DrawerState */].Docked;
        this.states = __WEBPACK_IMPORTED_MODULE_7_ion_bottom_drawer__["a" /* DrawerState */];
        this.minimumHeight = 0;
        this.SetTimerForTenSec = false;
        this.hideMe = false;
        this.measurementUnit = 'MKS';
        this.AllData = [];
        this.runningData = [];
        this.stoppedData = [];
        this.idlingData = [];
        this.outOfReachData = [];
        this.toggled = false;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_1_moment__({ hours: 0 }).format();
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_1_moment__().format(); //new Date(a).toISOString();
        if (localStorage.getItem('Total_Vech') !== null) {
            this.t_veicle_count = JSON.parse(localStorage.getItem('Total_Vech'));
        }
        this.isDealer = this.islogin.isDealer;
        this.isSuperAdmin = this.islogin.isSuperAdmin;
        this.islogindealer = localStorage.getItem('isDealervalue');
        if (this.isDealer == false && this.isSuperAdmin == false) {
            this.dealer_Permission = false;
        }
        else {
            this.dealer_Permission = this.islogin.device_add_permission;
        }
        if (navParams.get("label") && navParams.get("value")) {
            this.stausdevice = localStorage.getItem('status');
            this.checkIfStat = this.stausdevice;
            this.singleVehicleCount = navParams.get("value");
        }
        else {
            this.stausdevice = undefined;
        }
        ////////////
        this.events.subscribe("Released:Dismiss", function () {
            _this.getdevices();
        });
        this.apiCall.getRechargePlanApi(this.islogin.supAdmin).subscribe(function (respData) {
            _this.isPaymentPage = respData;
        });
        this.getUserSettings();
    }
    AddDevicesPage.prototype.toggle = function () {
        this.toggled = !this.toggled;
    };
    AddDevicesPage.prototype.cancelSearch = function () {
        this.toggled = false;
    };
    AddDevicesPage.prototype.getUserSettings = function () {
        var _this = this;
        var url = this.apiCall.mainUrl + "users/get_user_setting";
        var payload = {
            uid: this.islogin._id,
        };
        this.apiCall.urlpasseswithdatatwo(url, payload).subscribe(function (respData) {
            _this.ddata = respData;
            _this.ddata = JSON.parse(_this.ddata._body).service1;
            var tempD = respData;
            _this.users_data = JSON.parse(tempD._body);
            if (_this.users_data.unit_measurement !== undefined) {
                _this.measurementUnit = _this.users_data.unit_measurement;
            }
            else {
                if (localStorage.getItem('MeasurementType') !== null) {
                    var measureType = localStorage.getItem('MeasurementType');
                    _this.measurementUnit = measureType;
                }
                else {
                    _this.measurementUnit = 'MKS';
                }
            }
        }, function (err) {
            console.log("oops got error: ", err);
        });
    };
    AddDevicesPage.prototype.showBottomDrawer = function (data) {
        this.drawerData = data;
        this.showDrawer = true;
        this.drawerState = __WEBPACK_IMPORTED_MODULE_7_ion_bottom_drawer__["a" /* DrawerState */].Docked;
    };
    AddDevicesPage.prototype.fonctionTest = function (d) {
        var _this = this;
        if (this.showDrawer) {
            this.showDrawer = false;
        }
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_8__profile_settings_notif_setting_time_picker_time_picker__["b" /* TimePickerModal */], {
            data: d,
            key: 'parking'
        });
        modal.onDidDismiss(function () {
            _this.getdevices();
        });
        modal.present();
    };
    AddDevicesPage.prototype.towAlertCall = function (d) {
        var _this = this;
        if (this.showDrawer) {
            this.showDrawer = false;
        }
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_8__profile_settings_notif_setting_time_picker_time_picker__["b" /* TimePickerModal */], {
            data: d,
            key: 'tow'
        });
        modal.onDidDismiss(function () {
            _this.getdevices();
        });
        modal.present();
    };
    AddDevicesPage.prototype.ngOnInit = function () {
        this.now = new Date().toISOString();
    };
    AddDevicesPage.prototype.activateVehicles = function () {
        if (this.selectedArray.length == 0) {
            var alert_1 = this.alertCtrl.create({
                message: "Please Select Vehicle",
                buttons: [
                    {
                        text: "ok",
                        handler: function () {
                            (function (err) {
                                console.log(err);
                            });
                        },
                    },
                ],
            });
            alert_1.present();
        }
        if (this.isPayAlert) {
            if (this.users_data.paymentgateway == true && this.isPaymentPage.length > 0) {
                this.navCtrl.push("PaytmwalletloginPage", {
                    'param': this.selectedArray
                });
            }
            else {
                this.navCtrl.push(PaymantgatweyPage);
            }
        }
    };
    AddDevicesPage.prototype.supportCall = function () {
        var number = this.ddata;
        if (number !== "" || number !== undefined) {
            if (this.plt.is('android')) {
                window.open('tel:' + number, '_system');
            }
            else if (this.plt.is('ios')) {
                this.callNumber.callNumber(number.toString(), true)
                    .then(function (res) { return console.log('Launched dialer!', res); })
                    .catch(function (err) { return console.log('Error launching dialer', err); });
            }
        }
        else {
            this.toastCtrl.create({
                message: 'Contact number not found!',
                position: 'middle',
                duration: 2000
            }).present();
        }
    };
    AddDevicesPage.prototype.activateVehiclesess = function () {
        var that = this;
        if (this.checkIfStat !== 'Expired') {
            var Exp_1 = "Expired";
            var alert_2 = that.alertCtrl.create({
                message: "GPS subscription for this vehicle expired.Do you want to renew ?",
                buttons: [{
                        text: "Cancel",
                        handler: function () {
                            (function (err) {
                                console.log(err);
                            });
                        },
                    },
                    {
                        text: "Renew",
                        handler: function () {
                            that.segmentChanged(Exp_1);
                            (function (err) {
                                console.log(err);
                            });
                        },
                    },
                ],
            });
            alert_2.present();
        }
    };
    AddDevicesPage.prototype.isSelected = function (d) {
        var item = this.selectedArray.filter(function (item) { return item.Device_ID == d.Device_ID; });
        return item.length > 0;
    };
    AddDevicesPage.prototype.selected = function (event, d) {
        if (this.checkIfStat === 'Expired') {
            this.isPayAlert = true;
            event.stopPropagation();
            var isSelected = this.selectedArray.findIndex(function (item) { return item.Device_ID == d.Device_ID; });
            if (isSelected > -1) {
                this.selectedArray = this.selectedArray.filter(function (item) { return item.Device_ID != d.Device_ID; });
            }
            else {
                this.selectedArray.push(d);
            }
            this.setselect = true;
        }
        else {
            this.activateVehiclesess();
        }
    };
    AddDevicesPage.prototype.ngOnDestroy = function () { };
    AddDevicesPage.prototype.ionViewDidLeave = function () {
        if (this.timerInterval != undefined) {
            clearInterval(this.timerInterval);
        }
    };
    AddDevicesPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        var tempVar = localStorage.getItem("AutoRefreshVehicleList");
        if (tempVar != null) {
            if (tempVar == "ON") {
                this.SetTimerForTenSec = true;
            }
            else {
                this.SetTimerForTenSec = false;
            }
        }
        this.getDefaultUserSetting();
        this.navBar.backButtonClick = function (ev) {
            _this.hideMe = true;
            _this.navCtrl.pop({
                animate: true, animation: 'transition-ios', direction: 'back'
            });
        };
        this.getdevices();
        this.cancelSearch();
        if (localStorage.getItem("SCREEN") != null) {
            this.navBar.backButtonClick = function (e) {
                if (localStorage.getItem("SCREEN") != null) {
                    if (localStorage.getItem("SCREEN") === 'live') {
                        _this.navCtrl.setRoot('LivePage');
                    }
                    else {
                        if (localStorage.getItem("SCREEN") === 'dashboard') {
                            _this.navCtrl.setRoot('DashboardPage');
                        }
                    }
                }
            };
        }
    };
    AddDevicesPage.prototype.getDefaultUserSetting = function () {
        var _this = this;
        this.apiCall.startLoading().present();
        this.apiCall.getcustToken(this.islogin._id)
            .subscribe(function (data) {
            _this.apiCall._customerData = data;
            _this.apiCall.stopLoading();
            if (data.cust.user_settings) {
                _this.relay_timer = data.cust.user_settings.immoblizer;
            }
        });
    };
    AddDevicesPage.prototype.upload = function () {
        this.navCtrl.push('UploadDocPage', { vehData: this.drawerData });
    };
    AddDevicesPage.prototype.segmentChanged = function (ev) {
        this.selectedArray = [];
        if (this.showDrawer) {
            this.showDrawer = false;
        }
        if (ev == "Expired") {
            this.checkIfStat = ev;
        }
        else {
            this.checkIfStat = ev._value;
        }
        if (this.checkIfStat !== 'ALL') {
            if (this.checkIfStat == "Expired") {
                this.stausdevice = ev;
            }
            else {
                this.stausdevice = ev._value;
            }
        }
        else {
            this.stausdevice = undefined;
        }
        this.page = 0;
        this.loadProgress = 100;
        this.getdevices();
    };
    AddDevicesPage.prototype.getdevices = function () {
        var _this = this;
        this.showEmptyStatus = false;
        this.showDrawer = false;
        var baseURLp, mainUrlNew;
        if (localStorage.getItem('BASE_URL') != null) {
            mainUrlNew = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
        }
        if (this.stausdevice) {
            baseURLp = (mainUrlNew ? mainUrlNew : this.apiCall.mainUrl) + 'devices/getDeviceByUserMobile?email=' + this.islogin.email + '&id=' + this.islogin._id + '&statuss=' + this.stausdevice + '&skip=' + this.page + '&limit=' + this.limit;
        }
        else {
            baseURLp = (mainUrlNew ? mainUrlNew : this.apiCall.mainUrl) + 'devices/getDeviceByUserMobile?id=' + this.islogin._id + '&email=' + this.islogin.email + '&skip=' + this.page + '&limit=' + this.limit;
        }
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.progressBar();
        if (this.loadProgress == 100) {
            this.loadProgress = 0;
        }
        if (this.SetTimerForTenSec == true) {
            this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
                .subscribe(function (data) {
                var that = _this;
                that.loadProgress = 100;
                clearInterval(that.progressIntervalId);
                _this.allDevicesSearch = [];
                _this.ndata = data.devices;
                _this.allDevices = _this.ndata;
                _this.allDevicesSearch = _this.ndata;
                if (_this.allDevicesSearch.length === 0) {
                    _this.showEmptyStatus = true;
                }
                that.userPermission = JSON.parse(localStorage.getItem('details'));
                if (that.userPermission.isDealer == true || that.userPermission.isSuperAdmin == true) {
                    that.option_switch = true;
                }
                else {
                    if (localStorage.getItem('isDealervalue') == 'true') {
                        that.option_switch = true;
                    }
                    else {
                        if (that.userPermission.isDealer == false) {
                            that.option_switch = false;
                        }
                    }
                }
            }, function (err) {
                console.log("error=> ", err);
                var that = _this;
                _this.showEmptyStatus = false;
                clearInterval(that.progressIntervalId);
            });
            this.timerInterval = setInterval(function () {
                _this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
                    .subscribe(function (data) {
                    var that = _this;
                    that.loadProgress = 100;
                    clearInterval(that.progressIntervalId);
                    _this.allDevicesSearch = [];
                    _this.ndata = data.devices;
                    _this.allDevices = _this.ndata;
                    _this.allDevicesSearch = _this.ndata;
                    if (_this.allDevicesSearch.length === 0) {
                        _this.showEmptyStatus = true;
                    }
                    that.userPermission = JSON.parse(localStorage.getItem('details'));
                    if (that.userPermission.isDealer == true || that.userPermission.isSuperAdmin == true) {
                        that.option_switch = true;
                    }
                    else {
                        if (localStorage.getItem('isDealervalue') == 'true') {
                            that.option_switch = true;
                        }
                        else {
                            if (that.userPermission.isDealer == false) {
                                that.option_switch = false;
                            }
                        }
                    }
                }, function (err) {
                    console.log("error=> ", err);
                    var that = _this;
                    _this.showEmptyStatus = false;
                    clearInterval(that.progressIntervalId);
                });
            }, 10000);
        }
        else {
            this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
                .subscribe(function (data) {
                // this.apiCall.stopLoading();
                // this.apiCall.toastMsgDismised();
                var that = _this;
                that.loadProgress = 100;
                clearInterval(that.progressIntervalId);
                _this.allDevicesSearch = [];
                _this.ndata = data.devices;
                _this.allDevices = _this.ndata;
                _this.allDevicesSearch = _this.ndata;
                // this.allDevicesSearch = this.ndata;
                if (_this.allDevicesSearch.length === 0) {
                    _this.showEmptyStatus = true;
                }
                that.userPermission = JSON.parse(localStorage.getItem('details'));
                if (that.userPermission.isDealer == true || that.userPermission.isSuperAdmin == true) {
                    that.option_switch = true;
                }
                else {
                    if (localStorage.getItem('isDealervalue') == 'true') {
                        that.option_switch = true;
                    }
                    else {
                        if (that.userPermission.isDealer == false) {
                            that.option_switch = false;
                        }
                    }
                }
            }, function (err) {
                console.log("error=> ", err);
                var that = _this;
                _this.showEmptyStatus = false;
                clearInterval(that.progressIntervalId);
            });
        }
    };
    AddDevicesPage.prototype.getdevicesTemp123 = function () {
        var _this = this;
        var baseURLp;
        if (this.checkIfStat != undefined && this.checkIfStat !== 'ALL') {
            baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&statuss=' + this.checkIfStat + '&skip=' + this.page + '&limit=' + this.limit;
        }
        else {
            this.limit = 10;
            baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&skip=' + this.page + '&limit=' + this.limit;
        }
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.progressBar();
        if (this.loadProgress == 100) {
            this.loadProgress = 0;
        }
        this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.loadProgress = 100;
            _this.ndata = data.devices;
            _this.allDevices = data.devices;
            // this.allDevicesSearch = this.ndata;
            var that = _this;
            ////////////////////////////////////////////
            _this.AllData = [];
            _this.runningData = [];
            _this.stoppedData = [];
            _this.idlingData = [];
            _this.outOfReachData = [];
            var expiredDevices = [];
            var nodataDevices = [];
            that.all_devices = [];
            for (var i = 0; i < data.devices.length; i++) {
                if (data.devices[i].status === 'RUNNING') {
                    that.runningData.push(data.devices[i]);
                }
                else if (data.devices[i].status === 'IDLING') {
                    that.idlingData.push(data.devices[i]);
                }
                else if (data.devices[i].status === 'STOPPED') {
                    that.stoppedData.push(data.devices[i]);
                }
                else if (data.devices[i].status === 'OUT OF REACH') {
                    that.outOfReachData.push(data.devices[i]);
                }
                else if (data.devices[i].status === 'Expired') {
                    expiredDevices.push(data.devices[i]);
                }
                else if (data.devices[i].status === 'NO DATA') {
                    nodataDevices.push(data.devices[i]);
                }
                that.all_devices.push(data.devices[i]);
            }
            if (_this.checkIfStat === 'ALL') {
                _this.allDevicesSearch = that.all_devices;
            }
            else if (_this.checkIfStat === 'RUNNING') {
                _this.allDevicesSearch = _this.runningData;
            }
            else if (_this.checkIfStat === 'IDLING') {
                _this.allDevicesSearch = _this.idlingData;
            }
            else if (_this.checkIfStat === 'OUT OF REACH') {
                _this.allDevicesSearch = _this.outOfReachData;
            }
            else if (_this.checkIfStat === 'STOPPED') {
                _this.allDevicesSearch = _this.stoppedData;
            }
            else if (_this.checkIfStat === 'Expired') {
                _this.allDevicesSearch = expiredDevices;
            }
            else if (_this.checkIfStat === 'NO DATA') {
                _this.allDevicesSearch = nodataDevices;
            }
            ////////////////////////////////////////////
            that.userPermission = JSON.parse(localStorage.getItem('details'));
            if (that.userPermission.isDealer == true || that.userPermission.isSuperAdmin == true) {
                that.option_switch = true;
            }
            else {
                if (localStorage.getItem('isDealervalue') == 'true') {
                    that.option_switch = true;
                }
                else {
                    if (that.userPermission.isDealer == false) {
                        that.option_switch = false;
                    }
                }
            }
        }, function (err) {
            console.log("error=> ", err);
        });
    };
    AddDevicesPage.prototype.settings = function () {
        var that = this;
        var profileModal = this.modalCtrl.create('DeviceSettingsPage', {
            param: that.drawerData
        });
        profileModal.present();
        profileModal.onDidDismiss(function () {
            that.callObjFunc(that.drawerData);
        });
    };
    AddDevicesPage.prototype.customT = function () {
        var that = this;
        var customTModal = this.modalCtrl.create(CustomDurationPage);
        customTModal.present();
        customTModal.onDidDismiss(function (param) {
            if (param != undefined) {
                that.tttime = (Number(param.param) * 60);
            }
            else {
                that.tttime = 15;
            }
        });
    };
    AddDevicesPage.prototype.geofence = function () {
        this.navCtrl.push('GeofencePage');
    };
    AddDevicesPage.prototype.callObjFunc = function (d) {
        var _this = this;
        var that = this;
        var _bUrl = that.apiCall.mainUrl + 'devices/getDevicebyId?deviceId=' + d.Device_ID;
        this.apiCall.startLoading().present();
        this.apiCall.getSOSReportAPI(_bUrl)
            .subscribe(function (resp) {
            _this.apiCall.stopLoading();
            if (!resp) {
                return;
            }
            else {
                that.drawerData = resp;
            }
        });
    };
    AddDevicesPage.prototype.activateVehicle = function (data) {
        this.navCtrl.push("PaytmwalletloginPage", {
            "param": data
        });
    };
    AddDevicesPage.prototype.timeoutAlert = function () {
        var _this = this;
        var alerttemp = this.alertCtrl.create({
            message: "the server is taking much time to respond. Please try in some time.",
            buttons: [{
                    text: this.translate.instant('Okay'),
                    handler: function () {
                        _this.navCtrl.setRoot("DashboardPage");
                    }
                }]
        });
        alerttemp.present();
    };
    AddDevicesPage.prototype.showVehicleDetails = function (vdata) {
        this.navCtrl.push('VehicleDetailsPage', {
            param: vdata,
            option_switch: this.option_switch
        });
        this.showDrawer = false;
    };
    AddDevicesPage.prototype.sharedevice = function (param) {
        var that = this;
        if (param == '15mins') {
            that.condition = 'gpsc';
            that.condition1 = 'whiteee';
            that.condition2 = 'whiteee';
            that.condition3 = 'whiteee';
            that.tttime = 15;
            // that.tttime  = (15 * 60000); //for miliseconds
        }
        else {
            if (param == '1hour') {
                that.condition1 = 'gpsc';
                that.condition = 'whiteee';
                that.condition2 = 'whiteee';
                that.condition3 = 'whiteee';
                that.tttime = 60;
                // that.tttime  = (1 * 3600000); //for miliseconds
            }
            else {
                if (param == '8hours') {
                    that.condition2 = 'gpsc';
                    that.condition = 'whiteee';
                    that.condition1 = 'whiteee';
                    that.condition3 = 'whiteee';
                    that.tttime = (8 * 60);
                    // that.tttime  = (8 * 3600000);
                }
                else {
                    if (param == 'custom') {
                        that.condition3 = 'gpsc';
                        that.condition = 'whiteee';
                        that.condition1 = 'whiteee';
                        that.condition2 = 'whiteee';
                        that.customT();
                        // that.tttime = 60;
                        // that.tttime  = (1 * 3600000); //for miliseconds
                    }
                }
            }
        }
    };
    AddDevicesPage.prototype.shareLivetemp = function () {
        var _this = this;
        var that = this;
        if (that.tttime == undefined) {
            that.tttime = 15;
        }
        var data = {
            id: that.drawerData._id,
            imei: that.drawerData.Device_ID,
            sh: this.islogin._id,
            ttl: that.tttime // set to 1 hour by default
        };
        this.apiCall.startLoading().present();
        this.apiCall.shareLivetrackCall(data)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.resToken = data.t;
            _this.liveShare();
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    AddDevicesPage.prototype.liveShare = function () {
        debugger;
        var that = this;
        var link = 'https://www.oneqlik.in/' + "share/liveShare?t=" + that.resToken;
        __WEBPACK_IMPORTED_MODULE_11_tinyurl__["shorten"](link).then(function (res) {
            console.log("tinyurl: " + res);
            that.socialSharing.share(that.islogin.fn + " " + that.islogin.ln + " has shared " + that.drawerData.Device_Name + " live trip with you. Please follow below link to track", "OneQlik- Live Trip", "", res);
        });
        // that.socialSharing.share(that.userdetails.fn + " " + that.userdetails.ln + " has shared " + that.liveDataShare.Device_Name + " live trip with you. Please follow below link to track", "OneQlik- Live Trip", "", link);
        that.showActionSheet = true;
        that.showFooter = false;
        that.tttime = undefined;
    };
    AddDevicesPage.prototype.doRefresh = function (refresher) {
        var that = this;
        that.page = 0;
        that.limit = 5;
        this.getdevices();
        this.getdevicesTemp123();
        refresher.complete();
    };
    AddDevicesPage.prototype.shareVehicle = function () {
        var that = this;
        that.showActionSheet = false;
        that.drawerState1 = __WEBPACK_IMPORTED_MODULE_7_ion_bottom_drawer__["a" /* DrawerState */].Docked;
        that.showFooter = true;
    };
    AddDevicesPage.prototype.sharedevices = function (data, d_data) {
        var _this = this;
        var that = this;
        var devicedetails = {
            "did": d_data._id,
            "email": data.shareId,
            "uid": that.islogin._id
        };
        that.apiCall.startLoading().present();
        that.apiCall.deviceShareCall(devicedetails)
            .subscribe(function (data) {
            that.apiCall.stopLoading();
            var toast = that.toastCtrl.create({
                message: data.message,
                position: 'bottom',
                duration: 2000
            });
            toast.present();
        }, function (err) {
            that.apiCall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = _this.alertCtrl.create({
                message: msg.message,
                buttons: [_this.translate.instant('Okay')]
            });
            alert.present();
        });
    };
    AddDevicesPage.prototype.showDeleteBtn = function (b) {
        // debugger;
        var that = this;
        if (localStorage.getItem('isDealervalue') == 'true') {
            return false;
        }
        else {
            if (b) {
                var u = b.split(",");
                for (var p = 0; p < u.length; p++) {
                    if (that.islogin._id == u[p]) {
                        return true;
                    }
                }
            }
            else {
                return false;
            }
        }
    };
    AddDevicesPage.prototype.sharedVehicleDelete = function (device) {
        var that = this;
        that.deivceId = device;
        var alert = that.alertCtrl.create({
            message: this.translate.instant('Do you want to delete this share vehicle ?'),
            buttons: [{
                    text: this.translate.instant('YES PROCEED'),
                    handler: function () {
                        that.removeDevice(that.deivceId._id);
                    }
                },
                {
                    text: this.translate.instant('NO')
                }]
        });
        alert.present();
    };
    AddDevicesPage.prototype.removeDevice = function (did) {
        var _this = this;
        this.apiCall.startLoading().present();
        this.apiCall.dataRemoveFuncCall(this.islogin._id, did)
            .subscribe(function () {
            _this.apiCall.stopLoading();
            var toast = _this.toastCtrl.create({
                message: _this.translate.instant('Shared Device was deleted successfully!'),
                duration: 1500
            });
            toast.onDidDismiss(function () {
            });
            toast.present();
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    AddDevicesPage.prototype.showSharedBtn = function (a, b) {
        // debugger
        if (b) {
            return !(b.split(",").indexOf(a) + 1);
        }
        else {
            return true;
        }
    };
    AddDevicesPage.prototype.presentPopover = function (ev, data) {
        var _this = this;
        var popover = this.popoverCtrl.create(PopoverPage, {
            vehData: data
        }, {
            cssClass: 'contact-popover'
        });
        popover.onDidDismiss(function () {
            _this.getdevices();
        });
        popover.present({
            ev: ev
        });
    };
    AddDevicesPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        var that = this;
        that.page = that.page + 1;
        setTimeout(function () {
            var baseURLp;
            if (that.stausdevice) {
                baseURLp = _this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + that.islogin._id + '&email=' + that.islogin.email + '&statuss=' + that.stausdevice + '&skip=' + that.page + '&limit=' + that.limit;
            }
            else {
                baseURLp = _this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + that.islogin._id + '&email=' + that.islogin.email + '&skip=' + that.page + '&limit=' + that.limit;
            }
            if (_this.islogin.isSuperAdmin == true) {
                baseURLp += '&supAdmin=' + _this.islogin._id;
            }
            else {
                if (_this.islogin.isDealer == true) {
                    baseURLp += '&dealer=' + _this.islogin._id;
                }
            }
            that.ndata = [];
            that.apiCall.getdevicesForAllVehiclesApi(baseURLp)
                .subscribe(function (res) {
                if (res.devices.length > 0) {
                    that.ndata = res.devices;
                    for (var i = 0; i < that.ndata.length; i++) {
                        // that.allDevices.push(that.ndata[i]);
                        that.allDevicesSearch.push(that.ndata[i]);
                    }
                    // that.allDevicesSearch = that.allDevices;
                }
                infiniteScroll.complete();
            }, function (error) {
                console.log(error);
            });
        }, 100);
    };
    AddDevicesPage.prototype.progressBar = function () {
        var _this = this;
        // Test interval to show the progress bar
        this.progressIntervalId = setInterval(function () {
            if (_this.loadProgress < 100) {
                _this.loadProgress += 1;
            }
            else {
                clearInterval(_this.loadProgress);
            }
        }, 100);
    };
    AddDevicesPage.prototype.livetrack = function (device) {
        if (this.showDrawer) {
            this.showDrawer = false;
        }
        localStorage.setItem("LiveDevice", "LiveDevice");
        var animationsOptions;
        if (this.plt.is('android')) {
            this.navCtrl.push('LiveSingleDevice', { device: device });
        }
        else {
            if (this.plt.is('ios')) {
                animationsOptions = {
                    animation: 'ios-transition',
                    duration: 1000
                };
                this.navCtrl.push('LiveSingleDevice', { device: device }, animationsOptions);
            }
        }
    };
    AddDevicesPage.prototype.showHistoryDetail = function (device) {
        this.navCtrl.push('HistoryDevicePage', {
            device: device
        });
        if (this.showDrawer) {
            this.showDrawer = false;
        }
    };
    AddDevicesPage.prototype.device_address = function (device, index) {
        var _this = this;
        var that = this;
        var tempcord = {};
        if (!device.last_location) {
            that.allDevicesSearch[index].address = "N/A";
            if (!device.last_lat && !device.last_lng) {
                that.allDevicesSearch[index].address = "N/A";
            }
            else {
                tempcord = {
                    "lat": device.last_lat,
                    "long": device.last_lng
                };
                this.apiCall.getAddress(tempcord)
                    .subscribe(function (res) {
                    if (res.message === "Address not found in databse") {
                        _this.geocoderApi.reverseGeocode(device.last_lat, device.last_lng)
                            .then(function (res) {
                            var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
                            that.saveAddressToServer(str, device.last_lat, device.last_lng);
                            that.allDevicesSearch[index].address = str;
                        });
                    }
                    else {
                        that.allDevicesSearch[index].address = res.address;
                    }
                });
            }
        }
        else {
            tempcord = {
                "lat": device.last_location.lat,
                "long": device.last_location.long
            };
            this.apiCall.getAddress(tempcord)
                .subscribe(function (res) {
                if (res.message === "Address not found in databse") {
                    _this.geocoderApi.reverseGeocode(device.last_location.lat, device.last_location.long)
                        .then(function (res) {
                        var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
                        that.saveAddressToServer(str, device.last_location.lat, device.last_location.long);
                        that.allDevicesSearch[index].address = str;
                    });
                }
                else {
                    that.allDevicesSearch[index].address = res.address;
                }
            });
        }
    };
    AddDevicesPage.prototype.saveAddressToServer = function (address, lat, lng) {
        var payLoad = {
            "lat": lat,
            "long": lng,
            "address": address
        };
        this.apiCall.saveGoogleAddressAPI(payLoad)
            .subscribe(function () {
        }, function (err) {
            console.log("getting err while trying to save the address: ", err);
        });
    };
    AddDevicesPage.prototype.getDuration = function (device, index) {
        var that = this;
        that.allDevicesSearch[index].duration = "0hrs 0mins";
        if (!device.status_updated_at) {
            that.allDevicesSearch[index].duration = "0hrs 0mins";
        }
        else if (device.status_updated_at) {
            var a = __WEBPACK_IMPORTED_MODULE_1_moment__(new Date().toISOString()); //now
            var b = __WEBPACK_IMPORTED_MODULE_1_moment__(new Date(device.status_updated_at).toISOString());
            var mins = void 0;
            mins = a.diff(b, 'minutes') % 60;
            that.allDevicesSearch[index].duration = a.diff(b, 'hours') + 'hrs ' + mins + 'mins';
        }
    };
    AddDevicesPage.prototype.callSearch = function (ev) {
        var _this = this;
        var searchKey = ev.target.value;
        var _baseURL;
        if (this.islogin.isDealer == true) {
            _baseURL = this.apiCall.mainUrl + "devices/getDeviceByUser?email=" + this.islogin.email + "&id=" + this.islogin._id + "&skip=0&limit=10&search=" + searchKey + "&dealer=" + this.islogin._id;
        }
        else {
            if (this.islogin.isSuperAdmin == true) {
                _baseURL = this.apiCall.mainUrl + "devices/getDeviceByUser?email=" + this.islogin.email + "&id=" + this.islogin._id + "&skip=0&limit=10&search=" + searchKey + "&supAdmin=" + this.islogin._id;
            }
            else {
                _baseURL = this.apiCall.mainUrl + "devices/getDeviceByUser?email=" + this.islogin.email + "&id=" + this.islogin._id + "&skip=0&limit=10&search=" + searchKey;
            }
        }
        this.apiCall.callSearchService(_baseURL)
            .subscribe(function (data) {
            _this.allDevicesSearch = data.devices;
            _this.allDevices = data.devices;
        }, function (err) {
            console.log(err);
        });
    };
    AddDevicesPage.prototype.getDistanceReport = function (device, i) {
        var distanceReport = [];
        var user = device.user;
        var user_id;
        if (typeof user === 'object') {
            user_id = user._id;
        }
        else {
            user_id = user;
        }
        this.apiCall.getDistanceReportApi(new Date(this.datetimeStart).toISOString(), new Date(this.datetimeEnd).toISOString(), user_id, device._id)
            .subscribe(function (data) {
            distanceReport = data;
            var dd123 = (distanceReport.length > 0) ? parseFloat(distanceReport[0].distance).toFixed(2) : 0;
            device['distance'] = dd123;
        }, function (error) {
            device['distance'] = 0;
            console.log(error);
        });
    };
    AddDevicesPage.prototype.innerFunc = function (distanceReport) {
        var outerthis = this;
        var i = 0, howManyTimes = distanceReport.length;
        function f() {
            outerthis.allDevicesSearch.push({
                'distance': distanceReport[i].distance,
            });
            i++;
            if (i < howManyTimes) {
                setTimeout(f, 100);
            }
        }
        f();
    };
    AddDevicesPage.prototype.checkImmobilizePassword = function () {
        // const rurl = this.apiCall.mainUrl + 'users/get_user_setting';
        // var Var = { uid: this.islogin._id };
        // this.apiCall.urlpasseswithdata(rurl, Var)
        //   .subscribe(data => {
        //     if (!data.engine_cut_psd) {
        //       this.checkedPass = 'PASSWORD_NOT_SET';
        //     } else {
        //       this.checkedPass = 'PASSWORD_SET';
        //     }
        //   })
        var that = this;
        if (!that.users_data.engine_cut_psd) {
            this.checkedPass = 'PASSWORD_NOT_SET';
        }
        else {
            this.checkedPass = 'PASSWORD_SET';
        }
    };
    // checkPointsAvailability() {
    //   let url = this.apiCall.mainUrl + "users/checkPointAvailablity?u_id=" + this.islogin._id + "&demanded_points=0";
    //   this.apiCall.getSOSReportAPI(url)
    //     .subscribe((resp) => {
    //     },
    //       err => {
    //         console.log("error ", err);
    //       })
    // }
    AddDevicesPage.prototype.IgnitionOnOff = function (d) {
        var _this = this;
        this.mili = new Date(d.last_ping_on).getTime();
        this.d = new Date();
        this.n = this.d.getTime();
        this.a = this.n - this.mili;
        if (this.a >= 900000) {
            var Popup = this.alertCtrl.create({
                title: this.translate.instant('Warning'),
                message: this.translate.instant('Device has not updated data since more than 15 mins'),
                buttons: [this.translate.instant('Okay')]
            });
            Popup.present();
        }
        else {
            if (this.showDrawer) {
                this.showDrawer = false;
            }
            var pModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__immobilize_modal__["a" /* ImmobilizeModelPage */], {
                param: d
            });
            pModal.onDidDismiss(function () {
                _this.getdevices();
            });
            pModal.present();
        }
    };
    ;
    AddDevicesPage.prototype.toastmsg = function (msg) {
        this.toastCtrl.create({
            message: msg,
            duration: 1500,
            position: 'bottom'
        }).present();
    };
    AddDevicesPage.prototype.dialNumber = function (number) {
        if (this.showDrawer) {
            this.showDrawer = false;
        }
        if (number !== "" || number !== undefined) {
            if (this.plt.is('android')) {
                window.open('tel:' + number, '_system');
            }
            else if (this.plt.is('ios')) {
                this.callNumber.callNumber(number.toString(), true)
                    .then(function (res) { return console.log('Launched dialer!', res); })
                    .catch(function (err) { return console.log('Error launching dialer', err); });
            }
        }
        else {
            this.toastCtrl.create({
                message: 'Contact number not found!',
                position: 'middle',
                duration: 2000
            }).present();
        }
    };
    AddDevicesPage.prototype.getItems = function (ev) {
        var val = ev.target.value.trim();
        this.allDevicesSearch = this.allDevices.filter(function (item) {
            return (item.Device_Name.toLowerCase().indexOf(val.toLowerCase()) > -1);
        });
    };
    AddDevicesPage.prototype.onClear = function (ev) {
        this.getdevices();
        ev.target.value = '';
    };
    AddDevicesPage.prototype.openAdddeviceModal = function () {
        var _this = this;
        var profileModal = this.modalCtrl.create('AddDeviceModalPage');
        profileModal.onDidDismiss(function () {
            _this.getdevices();
        });
        profileModal.present();
    };
    AddDevicesPage.prototype.iconCheck = function (status, iconType) {
        var devStatus = status.split(" ");
        if ((iconType == 'car') && (devStatus[0] == "OUT")) {
            this.devIcon = "../../assets/imgs/car_blue_icon.png";
            return this.devIcon;
        }
        else if ((iconType == 'car') && (devStatus[0] == "RUNNING")) {
            this.devIcon = "../../assets/imgs/car_green_icon.png";
            return this.devIcon;
        }
        else if ((iconType == 'car') && (devStatus[0] == "STOPPED")) {
            this.devIcon = "../../assets/imgs/car_red_icon.png";
            return this.devIcon;
        }
        else if ((iconType == 'car') && (devStatus[0] == "IDLING")) {
            this.devIcon = "../../assets/imgs/car_yellow_icon.png";
            return this.devIcon;
        }
        else if ((iconType == 'car') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
            this.devIcon = "../../assets/imgs/car_grey_icon.png";
            return this.devIcon;
        }
        else if ((iconType == 'bike') && (devStatus[0] == "OUT")) {
            this.devIcon = "../../assets/imgs/bike_blue_icon.png";
            return this.devIcon;
        }
        else if ((iconType == 'bike') && (devStatus[0] == "RUNNING")) {
            this.devIcon = "../../assets/imgs/bike_green_icon.png";
            return this.devIcon;
        }
        else if ((iconType == 'bike') && (devStatus[0] == "STOPPED")) {
            this.devIcon = "../../assets/imgs/bike_red_icon.png";
            return this.devIcon;
        }
        else if ((iconType == 'bike') && (devStatus[0] == "IDLING")) {
            this.devIcon = "../../assets/imgs/bike_yellow_icon.png";
            return this.devIcon;
        }
        else if ((iconType == 'bike') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
            this.devIcon = "../../assets/imgs/bike_grey_icon.png";
            return this.devIcon;
        }
        else if ((iconType == 'bus') && (devStatus[0] == "OUT")) {
            this.devIcon = "../../assets/imgs/bus_blue.png";
            return this.devIcon;
        }
        else if ((iconType == 'bus') && (devStatus[0] == "RUNNING")) {
            this.devIcon = "../../assets/imgs/bus_green.png";
            return this.devIcon;
        }
        else if ((iconType == 'bus') && (devStatus[0] == "STOPPED")) {
            this.devIcon = "../../assets/imgs/bus_red.png";
            return this.devIcon;
        }
        else if ((iconType == 'bus') && (devStatus[0] == "IDLING")) {
            this.devIcon = "../../assets/imgs/bus_yellow.jpg";
            return this.devIcon;
        }
        else if ((iconType == 'bus') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
            this.devIcon = "../../assets/imgs/bus_gray.png";
            return this.devIcon;
        }
        else if ((iconType == 'truck') && (devStatus[0] == "OUT")) {
            this.devIcon = "../../assets/imgs/truck_icon_blue.png";
            return this.devIcon;
        }
        else if ((iconType == 'truck') && (devStatus[0] == "RUNNING")) {
            this.devIcon = "../../assets/imgs/truck_icon_green.png";
            return this.devIcon;
        }
        else if ((iconType == 'truck') && (devStatus[0] == "STOPPED")) {
            this.devIcon = "../../assets/imgs/truck_icon_red.png";
            return this.devIcon;
        }
        else if ((iconType == 'truck') && (devStatus[0] == "IDLING")) {
            this.devIcon = "../../assets/imgs/truck_icon_yellow.png";
            return this.devIcon;
        }
        else if ((iconType == 'truck') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
            this.devIcon = "../../assets/imgs/truck_icon_grey.png";
            return this.devIcon;
        }
        else if ((iconType == 'tractor') && (devStatus[0] == "OUT")) {
            this.devIcon = "../../assets/imgs/tractor_blue.png";
            return this.devIcon;
        }
        else if ((iconType == 'tractor') && (devStatus[0] == "RUNNING")) {
            this.devIcon = "../../assets/imgs/tractor_green.png";
            return this.devIcon;
        }
        else if ((iconType == 'tractor') && (devStatus[0] == "STOPPED")) {
            this.devIcon = "../../assets/imgs/tractor_red.png";
            return this.devIcon;
        }
        else if ((iconType == 'tractor') && (devStatus[0] == "IDLING")) {
            this.devIcon = "../../assets/imgs/tractor_yellow.png";
            return this.devIcon;
        }
        else if ((iconType == 'tractor') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
            this.devIcon = "../../assets/imgs/tractor_gray.png";
            return this.devIcon;
        }
        else if ((!iconType) && (devStatus[0] == "OUT")) {
            this.devIcon = "../../assets/imgs/car_blue_icon.png";
            return this.devIcon;
        }
        else if ((!iconType) && (devStatus[0] == "RUNNING")) {
            this.devIcon = "../../assets/imgs/car_green_icon.png";
            return this.devIcon;
        }
        else if ((!iconType) && (devStatus[0] == "STOPPED")) {
            this.devIcon = "../../assets/imgs/car_red_icon.png";
            return this.devIcon;
        }
        else if ((!iconType) && (devStatus[0] == "IDLING")) {
            this.devIcon = "../../assets/imgs/car_yellow_icon.png";
            return this.devIcon;
        }
        else if ((!iconType) && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
            this.devIcon = "../../assets/imgs/car_grey_icon.png";
            return this.devIcon;
        }
        else if ((iconType == 'user') && (devStatus[0] == "OUT")) {
            this.devIcon = "../../assets/imgs/user.png";
            return this.devIcon;
        }
        else if ((iconType == 'user') && (devStatus[0] == "RUNNING")) {
            this.devIcon = "../../assets/imgs/user.png";
            return this.devIcon;
        }
        else if ((iconType == 'user') && (devStatus[0] == "STOPPED")) {
            this.devIcon = "../../assets/imgs/user.png";
            return this.devIcon;
        }
        else if ((iconType == 'user') && (devStatus[0] == "IDLING")) {
            this.devIcon = "../../assets/imgs/user.png";
            return this.devIcon;
        }
        else if ((iconType == 'user') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
            this.devIcon = "../../assets/imgs/user.png";
            return this.devIcon;
        }
        else if ((iconType == 'jcb') && (devStatus[0] == "OUT")) {
            this.devIcon = "../../assets/imgs/jcb_blue.png";
            return this.devIcon;
        }
        else if ((iconType == 'jcb') && (devStatus[0] == "RUNNING")) {
            this.devIcon = "../../assets/imgs/jcb_green.png";
            return this.devIcon;
        }
        else if ((iconType == 'jcb') && (devStatus[0] == "STOPPED")) {
            this.devIcon = "../../assets/imgs/jcb_red.png";
            return this.devIcon;
        }
        else if ((iconType == 'jcb') && (devStatus[0] == "IDLING")) {
            this.devIcon = "../../assets/imgs/jcb_yellow.png";
            return this.devIcon;
        }
        else if ((iconType == 'jcb') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
            this.devIcon = "../../assets/imgs/jcb_gray.png";
            return this.devIcon;
        }
        else if ((iconType == 'ambulance') && (devStatus[0] == "OUT")) {
            this.devIcon = "../../assets/imgs/ambulance_blue.png";
            return this.devIcon;
        }
        else if ((iconType == 'ambulance') && (devStatus[0] == "RUNNING")) {
            this.devIcon = "../../assets/imgs/ambulance_green.png";
            return this.devIcon;
        }
        else if ((iconType == 'ambulance') && (devStatus[0] == "STOPPED")) {
            this.devIcon = "../../assets/imgs/ambulance_red.png";
            return this.devIcon;
        }
        else if ((iconType == 'ambulance') && (devStatus[0] == "IDLING")) {
            this.devIcon = "../../assets/imgs/ambulance_yellow.png";
            return this.devIcon;
        }
        else if ((iconType == 'ambulance') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
            this.devIcon = "../../assets/imgs/ambulance_gray.png";
            return this.devIcon;
        }
        else if ((iconType == 'auto') && (devStatus[0] == "OUT")) {
            this.devIcon = "../../assets/imgs/auto_blue.png";
            return this.devIcon;
        }
        else if ((iconType == 'auto') && (devStatus[0] == "RUNNING")) {
            this.devIcon = "../../assets/imgs/auto_green.png";
            return this.devIcon;
        }
        else if ((iconType == 'auto') && (devStatus[0] == "STOPPED")) {
            this.devIcon = "../../assets/imgs/auto_red.png";
            return this.devIcon;
        }
        else if ((iconType == 'auto') && (devStatus[0] == "IDLING")) {
            this.devIcon = "../../assets/imgs/auto_yellow.png";
            return this.devIcon;
        }
        else if ((iconType == 'auto') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
            this.devIcon = "../../assets/imgs/auto_gray.png";
            return this.devIcon;
        }
        else {
            this.devIcon = "../../assets/imgs/noIcon.png";
            return this.devIcon;
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChildren"])("step"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["QueryList"])
    ], AddDevicesPage.prototype, "steps", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChildren"])('myGauge'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["QueryList"])
    ], AddDevicesPage.prototype, "myGauge", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('popoverContent', { read: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] }),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AddDevicesPage.prototype, "content", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('popoverText', { read: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] }),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AddDevicesPage.prototype, "text", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["Navbar"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["Navbar"])
    ], AddDevicesPage.prototype, "navBar", void 0);
    AddDevicesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-add-devices',template:/*ion-inline-start:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/add-devices/add-devices.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title (click)="toggled = false">{{ "Vehicle List" | translate }}</ion-title>\n    <ion-buttons end>\n      <div>\n        <ion-icon style="font-size: 2.2em;" color="light" *ngIf="!toggled" (click)="toggle()" name="search"></ion-icon>\n        <ion-searchbar *ngIf="toggled" (ionInput)="callSearch($event)" (ionClear)="onClear($event)"\n          (ionCancel)="cancelSearch($event)"></ion-searchbar>\n      </div>\n    </ion-buttons>\n    <ion-buttons end *ngIf="((dealer_Permission == true)||(dealer_Permission === undefined)) || isSuperAdmin">\n      <button ion-button icon-only (click)="openAdddeviceModal()">\n        <ion-icon name="add"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n  <progress-bar *ngIf="loadProgress !== 100" [progress]="loadProgress"></progress-bar>\n  <ion-segment mode="md" color="gpsc" scrollable="true" (ionChange)="segmentChanged($event)" [(ngModel)]="checkIfStat">\n    <ion-segment-button class="seg" style="color: #f0810f;" value="ALL">\n      <ion-icon name="car" style="color: #f0810f;"></ion-icon>\n    </ion-segment-button>\n    <ion-segment-button style="color: #11a46e;" value="RUNNING">\n      <ion-icon name="car" style="color: #11a46e;"></ion-icon>\n    </ion-segment-button>\n    <ion-segment-button style="color: #ff3f32;" value="STOPPED">\n      <ion-icon name="car" style="color: #ff3f32;"></ion-icon>\n    </ion-segment-button>\n    <ion-segment-button style="color: #ffc13c;" value="IDLING">\n      <ion-icon name="car" style="color: #ffc13c;"></ion-icon>\n    </ion-segment-button>\n    <ion-segment-button style="color: #1bb6d4;" value="OUT OF REACH">\n      <ion-icon name="car" style="color: #1bb6d4;"></ion-icon>\n    </ion-segment-button>\n    <ion-segment-button style="color: #0e1f22;" value="Expired">\n      <ion-icon name="car" style="color: #0e1f22;"></ion-icon>\n    </ion-segment-button>\n  </ion-segment>\n</ion-header>\n\n<ion-content *ngIf="!hideMe">\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content pullingIcon="arrow-dropdown" pullingText="{{\'Pull to refresh\' | translate}}"\n      refreshingSpinner="circles" refreshingText="{{\'Refreshing...\' | translate}}">\n    </ion-refresher-content>\n  </ion-refresher>\n  <div *ngIf="(allDevicesSearch.length === 0) && !showEmptyStatus" style="text-align: center; margin-top: 2%;">\n    <ion-spinner color="gpsc"></ion-spinner>\n    <p>Loading...</p>\n  </div>\n\n  <div *ngIf="showEmptyStatus" style="text-align: center; margin-top: 2%;">\n    <p>No <span *ngIf="checkIfStat != \'ALL\'">{{checkIfStat | titlecase}}</span> vehicle(s) found.</p>\n  </div>\n\n  <div *ngIf="allDevicesSearch.length > 0">\n    <div *ngIf="checkIfStat === \'Expired\'"style="display: flex;justify-content: space-around;">\n      <button ion-button  (click)="activateVehicles()" style="background-color: black;color: white;padding: 10px 20px;margin: 10px;font-size: 18px;border-radius: 25px;">\n      <span>Pay Now</span>\n      </button>\n      <button ion-button (click)="supportCall()" style="background-color: black;color: white;padding: 10px 20px;margin: 10px;font-size: 18px;border-radius: 25px;">\n       <ion-icon ios="ios-call" md="md-call" style="color: white;"></ion-icon>Support\n     </button>\n    </div>\n    <ion-card *ngFor="let d of allDevicesSearch; let i = index">\n\n     <!-- <div *ngIf="d.expiration_date < now" style="position: relative">\n        <div>\n          <ion-item style="background-color: rgba(0, 0, 0, 0.9)">\n            <ion-avatar item-start *ngIf="iconCheck(d.status,d.iconType)">\n              <img *ngIf="d.iconType === \'jcb\'" width=\'30px\' style="margin-left: 10px;" src={{devIcon}}>\n              <img *ngIf="d.iconType != \'jcb\'" width=\'20px\' style="margin-left: 10px;" src={{devIcon}}>\n            </ion-avatar>\n            <div>\n              <h2 style="color: gray">{{ d.Device_Name }}</h2>\n              <p style="color:gray;font-size: 10px;font-weight: 400;margin:0px;" ion-text text-wrap>\n                <span style="text-transform: uppercase; color: red;">{{ d.status }}\n                </span>\n                <span *ngIf="d.status_updated_at">{{ "since" | translate }}&nbsp;{{\n                  d.status_updated_at | date: "medium"\n                }}\n                </span>\n              </p>\n            </div>\n          </ion-item>\n          <ion-row style="background-color: rgba(0, 0, 0, 0.9)" padding-right>\n            <ion-col width-20 style="text-align:center"></ion-col>\n            <ion-col width-20 style="text-align:center"></ion-col>\n            <ion-col width-20 style="text-align:center"></ion-col>\n            <ion-col width-20 style="text-align:center"></ion-col>\n            <ion-col width-20 style="text-align:center"></ion-col>\n            <ion-col width-20 style="text-align:center">\n              <button ion-button outline color="secondary" (tap)="activateVehicle(d)">\n                Activate\n              </button></ion-col>\n          </ion-row>\n        </div>\n      </div>\n    -->\n\n\n    <div *ngIf="d.expiration_date < now" style="position: relative">\n      <div>\n        <ion-item  [ngStyle]="{background: isSelected(d) ? \'#888686\': \'black\'}"   (click)="selected($event,d)">\n\n          <ion-avatar item-start *ngIf="iconCheck(d.status,d.iconType)" (click)="selected($event,d)">\n            <img *ngIf="d.iconType === \'jjcb\'" width=\'30px\' style="margin-left: 10px;" src={{devIcon}}>\n            <img *ngIf="!isSelected(d)" width=\'20px\' style="margin-left: 10px;" src={{devIcon}}>\n            <img *ngIf="isSelected(d)" width=\'20px\' style="margin-left: 10px;" src="assets/imgs/cheackright.png">\n             <!-- <ion-icon ios="ios-add-circle" md="md-add-circle" *ngIf="selectedArray.length > 0" style="color: white; font-size: 20px;"></ion-icon> -->\n          </ion-avatar>\n          <div>\n            <h2 style="color: rgb(255, 255, 255)">{{ d.Device_Name }}</h2>\n            <p style="color:rgb(255, 255, 255);font-size: 10px;font-weight: 400;margin:0px;" ion-text text-wrap>\n              <span style="text-transform: uppercase; color: red;">{{ d.status }}\n              </span>\n              <span *ngIf="d.status_updated_at">{{ "since" | translate }}&nbsp;{{\n                d.status_updated_at | date: "medium"\n              }}\n              </span>\n            </p>\n          </div>\n        </ion-item>\n      </div>\n    </div>\n      <div *ngIf="d.expiration_date == null || d.expiration_date > now">\n        <ion-item style="padding: 0px;" (click)="showBottomDrawer(d)">\n          <ion-avatar item-start *ngIf="iconCheck(d.status,d.iconType)">\n\n            <img *ngIf="d.iconType === \'jcb\'" width=\'30px\' style="margin-left: 10px;" src={{devIcon}} />\n            <img *ngIf="d.iconType != \'jcb\'" width=\'20px\' style="margin-left: 10px;" src={{devIcon}} />\n          </ion-avatar>\n          <div>\n            <p style="color: black; font-size: 4vw; padding-right: 8px;">\n              {{ d.Device_Name }} &nbsp;&nbsp;\n            </p>\n\n            <p style="color:#cf1c1c;font-size:8px;font-weight: 400;margin:0px;" ion-text text-wrap\n              (onCreate)="getDuration(d, i)">\n              <span style="text-transform: uppercase; color:#11a46e"\n                *ngIf="d.status === \'RUNNING\'">{{ d.status | lowercase }} </span>\n              <span style="text-transform: uppercase; color:#1bb6d4"\n                *ngIf="d.status === \'OUT OF REACH\'">{{ d.status | lowercase }} </span>\n              <span style="text-transform: uppercase; color:#ff3f32"\n                *ngIf="d.status === \'STOPPED\'">{{ d.status | lowercase }} </span>\n              <span style="text-transform: uppercase; color:#ffc13c"\n                *ngIf="d.status === \'IDLING\'">{{ d.status | lowercase }} </span>\n              <span style="text-transform: uppercase; color:#5a5e5c"\n                *ngIf="d.status === \'NO DATA\' || d.status === \'NO GPS FIX\'">{{ d.status | lowercase }} </span>\n              <span *ngIf="d.last_ping_on">{{ "since" | translate }}&nbsp;{{\n                d.duration}} <br /> {{"Last updated - " | translate}} {{d.last_ping_on | date:\'shortTime\'}},\n                {{d.last_ping_on | date: \'mediumDate\'}}\n              </span>\n            </p>\n          </div>\n\n          <!-- <div item-end style="padding: 3px;" *ngIf="islogin.fuel_unit === \'PERCENTAGE\'"> -->\n            <div item-end  *ngIf="islogin.fuel_unit === \'PERCENTAGE\'">\n            <ion-row>\n              <ion-col col-12 style="text-align: center; padding: 0px;">\n                <img src="assets/imgs/fuel.png" style="width: 25px; height: 25px; margin: auto;" />\n                <!-- <ion-icon name="speedometer" color="gpsc" style="font-size: 1.5em;"></ion-icon> -->\n              </ion-col>\n              <ion-col col-12 style="text-align: center; padding: 0px;">\n                <p style="margin: 0px;font-weight: 600;">{{ d.fuel_percent ? d.fuel_percent : "N/A" }} <span\n                    style="font-size: 0.7em;font-weight: 600;">%</span></p>\n              </ion-col>\n            </ion-row>\n          </div>\n\n          <!-- <div item-end style="padding: 3px;" *ngIf="islogin.fuel_unit === \'LITRE\'"> -->\n            <div item-end  *ngIf="islogin.fuel_unit === \'LITRE\'">\n            <ion-row>\n              <ion-col col-12 style="text-align: center; padding: 0px;">\n                <img src="assets/imgs/fuel.png" style="width: 25px; height: 25px; margin: auto;" />\n                <!-- <ion-icon name="speedometer" color="gpsc" style="font-size: 1.5em;"></ion-icon> -->\n              </ion-col>\n              <ion-col col-12 style="text-align: center; padding: 0px;">\n                <p style="margin: 0px;font-weight: 600;">{{ d.currentFuel ? d.currentFuel : 0 }} <span\n                    style="font-size: 0.7em;font-weight: 600;">{{ (measurementUnit == \'MKS\') ? ("L" | translate) : ("G" | translate)}}</span>\n                </p>\n              </ion-col>\n            </ion-row>\n          </div>\n\n          <!-- <div item-end style="padding: 3px;"> -->\n            <div item-end>\n            <ion-row>\n              <ion-col col-12 style="text-align: center; padding: 0px;">\n                <ion-icon name="speedometer" color="gpsc" style="font-size: 1.4em;"></ion-icon>\n              </ion-col>\n              <ion-col col-12 style="text-align: center; padding: 0px;">\n                <p style="margin: 0px;font-weight: 600;">{{ d.last_speed ? d.last_speed : 0 }} <span\n                    style="font-size: 0.7em;font-weight: 600;">{{ (measurementUnit == \'MKS\') ? ("KMPH" | translate) : ("Miles/hr" | translate)}}</span>\n                </p>\n              </ion-col>\n            </ion-row>\n          </div>\n\n          <div *ngIf="!showDeleteBtn(d.SharedWith)" item-end>\n            <button ion-button clear color="gpsc" (click)="presentPopover($event, d); $event.stopPropagation()"\n              *ngIf="option_switch" style="font-size: 4vw; margin: 0px;">\n              <ion-icon ios="ios-more" md="md-more"></ion-icon>\n            </button>\n          </div>\n\n          <div *ngIf="showDeleteBtn(d.SharedWith)">\n            <button ion-button item-end clear color="danger" (click)="sharedVehicleDelete(d)"\n              style="font-size: 4vw; margin: 0px;">\n              <ion-icon ios="ios-remove-circle" md="md-remove-circle"></ion-icon>\n            </button>\n          </div>\n        </ion-item>\n        <!-- <ion-item item-start class="itemStyle" style="background:rgb(215, 216, 218); padding-left: 5px;"> -->\n        <ion-row style="padding-left: 5px;margin-top: -10px;">\n          <ion-col col-1 class="colSt2">\n            <ion-icon name="pin" style="font-size: 0.9em; color: #696666;"></ion-icon>\n          </ion-col>\n          <ion-col col-10 (onCreate)="device_address(d, i)" style="margin-left: -15px;" class="colSt2">\n            <div class="overme">\n              {{ d.address }}\n            </div>\n          </ion-col>\n        </ion-row>\n        <!-- </ion-item> -->\n        <ion-item item-start class="itemStyle" style="padding-left: 5px;">\n          <ion-row style="margin-right: -3px;">\n            <ion-col col-6 style="padding: 10px 0px 0px 0px;">\n              <ion-row>\n                <ion-col width-2 style="text-align:center">\n                  <img src="assets/imgs/statusIcons/ign_red.png" *ngIf="d.last_ACC == \'0\'" width="20" height="20" />\n                  <img src="assets/imgs/statusIcons/ign_green.png" *ngIf="d.last_ACC == \'1\'" width="20" height="20" />\n                  <img src="assets/imgs/statusIcons/ign_idle.png" *ngIf="d.last_ACC == null" width="20" height="20" />\n                </ion-col>\n                <ion-col width-2 style="text-align:center">\n                  <img src="assets/imgs/statusIcons/ac_na.png" *ngIf="d.ac == null" width="20" height="20" />\n                  <img src="assets/imgs/statusIcons/ac_present.png" *ngIf="d.ac == \'1\'" width="20" height="20" />\n                  <img src="assets/imgs/statusIcons/ac_empty.png" *ngIf="d.ac == \'0\'" width="20" height="20" />\n                </ion-col>\n                <ion-col width-2 style="text-align:center">\n                  <img src="assets/imgs/statusIcons/fuel_available.png" *ngIf="d.currentFuel != null" width="20"\n                    height="20" />\n                  <img src="assets/imgs/statusIcons/fuel_empty.png" *ngIf="d.currentFuel == null" width="20"\n                    height="20" />\n                </ion-col>\n                <ion-col width-2 style="text-align:center">\n                  <img src="assets/imgs/statusIcons/power_na.png" *ngIf="d.power == null" width="20" height="20" />\n                  <img src="assets/imgs/statusIcons/pwer_disconnected.png" *ngIf="d.power == \'0\'" width="20"\n                    height="20" />\n                  <img src="assets/imgs/statusIcons/power_connected.png" *ngIf="d.power == \'1\'" width="20"\n                    height="20" />\n                </ion-col>\n                <ion-col width-2 style="text-align:center">\n                  <img src="assets/imgs/statusIcons/gps_na.png" *ngIf="d.gpsTracking == null" width="20" height="20" />\n                  <img src="assets/imgs/statusIcons/gps_notPresent.png" *ngIf="d.gpsTracking == \'0\'" width="20"\n                    height="20" />\n                  <img src="assets/imgs/statusIcons/gps_present.png" *ngIf="d.gpsTracking == \'1\'" width="20"\n                    height="20" />\n                </ion-col>\n                <ion-col width-2 style="text-align:center">\n                  <ion-icon name="thermometer" *ngIf="d.temp == null" style="color: gray; font-size: 1.4em;">\n                  </ion-icon>\n                  <ion-icon name="thermometer" *ngIf="d.temp == \'0\'" style="font-size: 1.4em; color: #ff3f32;">\n                  </ion-icon>\n                  <ion-icon name="thermometer" *ngIf="d.temp == \'1\'" style="font-size: 1.4em; color: #11a46e;">\n                  </ion-icon>\n                </ion-col>\n                <ion-col width-2 style="text-align:center">\n                  <img src="assets/imgs/statusIcons/car_no_data.png" *ngIf="d.door == null" width="20" height="20" />\n                  <img src="assets/imgs/statusIcons/car_door_open.png" *ngIf="d.door == 0" width="20" height="20" />\n                  <img src="assets/imgs/statusIcons/car_door_close.png" *ngIf="d.door == 1" width="20" height="20" />\n                </ion-col>\n                <ion-col width-2 style="text-align:center">\n                  <ion-icon name="lock" style="color:#ff3f32;font-size: 1.2em;" *ngIf="d.ignitionLock == \'1\'">\n                  </ion-icon>\n                  <ion-icon name="unlock" style="color:#11a46e;font-size: 1.2em;" *ngIf="d.ignitionLock == \'0\'">\n                  </ion-icon>\n                </ion-col>\n              </ion-row>\n            </ion-col>\n\n            <ion-col col-6 class="colSt123" (click)="getDistanceReport(d)">\n              <p style="color:#fff;font-size: 10px;font-weight: 400;margin-top: 6px; margin-left: 49%;">\n                {{ "Today\'s Odo" | translate }}\n              </p>\n              <ion-icon name="refresh" style="color:#fff;margin-top: 6px; margin-left: 60%;"\n                *ngIf="d.distance === undefined"></ion-icon>\n              <p style="color:#fff;font-size: 11px;font-weight: bold;margin-top: 8px; margin-left: 49%;"\n                *ngIf="d.distance !== undefined">\n                {{ d.distance }}\n                {{ (measurementUnit == \'MKS\') ? ("Km" | translate) : ("Miles" | translate)}}\n              </p>\n            </ion-col>\n          </ion-row>\n        </ion-item>\n        <ion-row style="background-color: #000;\n      color: white;\n      font-weight: bold;font-size: 0.6em;">\n          <ion-col col-6>{{"Licence Purchased On" | translate}} - {{d.created_on | date:\'mediumDate\'}}</ion-col>\n          <ion-col col-6 style="text-align: right;">{{"Licence Expires On" | translate}} -\n            {{d.expiration_date | date:\'mediumDate\'}}\n          </ion-col>\n        </ion-row>\n      </div>\n    </ion-card>\n  </div>\n  <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n    <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="{{ \'Loading more data...\' | translate }}">\n    </ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content>\n<div padding>\n  <ion-bottom-drawer *ngIf="showDrawer" [(state)]="drawerState" [minimumHeight]="minimumHeight"\n    [dockedHeight]="dockedHeight" [shouldBounce]="shouldBounce" [distanceTop]="distanceTop">\n    <div class="drawer-content">\n      <div id="content">\n        <ion-row style="padding-bottom: 16px;">\n          <ion-col col-4></ion-col>\n          <ion-col col-4 style="border-top: 3px solid lightgray;"></ion-col>\n          <ion-col col-4></ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-3 no-padding style="text-align: center;" (click)="livetrack(drawerData)">\n            <img src="assets/imgs/segment/map_active.png" style="width: 30px;" />\n          </ion-col>\n          <ion-col col-3 no-padding style="text-align: center;" (click)="showHistoryDetail(drawerData)">\n            <img src="assets/imgs/segment/history_active.png" style="width: 30px;" />\n          </ion-col>\n          <ion-col col-3 no-padding style="text-align: center;" (click)="fonctionTest(drawerData)">\n            <img *ngIf="drawerData.theftAlert"\n              src="assets/imgs/anti-theft_anti_theft_protection_hacker_investigate-512.png" style="width: 30px;" />\n            <img *ngIf="!drawerData.theftAlert" src="assets/imgs/anti-theft_anti_theftred.png" style="width: 30px;" />\n          </ion-col>\n          <!-- <ion-col col-3 no-padding style="text-align: center;" (click)="IgnitionOnOff(drawerData)">\n            <ion-icon name="lock" style="color:#ef473a;font-size: 2em;" *ngIf="drawerData.ignitionLock == \'1\'">\n            </ion-icon>\n            <ion-icon name="unlock" style="color:#1de21d;font-size: 2em;" *ngIf="drawerData.ignitionLock == \'0\'">\n            </ion-icon>\n          </ion-col> -->\n          <ion-col col-3 no-padding style="text-align: center;" (click)="IgnitionOnOff(drawerData)" *ngIf = "this.relay_timer ==true">\n            <ion-icon name="lock" style="color:#ef473a;font-size: 2em;" *ngIf="drawerData.ignitionLock == \'1\'">\n            </ion-icon>\n            <ion-icon name="unlock" style="color:#1de21d;font-size: 2em;" *ngIf="drawerData.ignitionLock == \'0\'">\n            </ion-icon>\n          </ion-col>\n          <ion-col col-3 no-padding style="text-align: center;" (click)="IgnitionOnOff(drawerData)" *ngIf = "this.relay_timer ==undefined">\n            <ion-icon name="lock" style="color:#ef473a;font-size: 2em;" *ngIf="drawerData.ignitionLock == \'1\'">\n            </ion-icon>\n            <ion-icon name="unlock" style="color:#1de21d;font-size: 2em;" *ngIf="drawerData.ignitionLock == \'0\'">\n            </ion-icon>\n          </ion-col>\n          <ion-col col-3 style="background: rgba(0, 0, 0, 0.75);" no-padding style="text-align: center;" *ngIf = "this.relay_timer ==false" readonly>\n            <ion-icon name="lock" style="color:white;font-size: 2em;" *ngIf="drawerData.ignitionLock == \'1\'">\n            </ion-icon>\n            <ion-icon name="unlock" style="color:white;font-size: 2em;" *ngIf="drawerData.ignitionLock == \'0\'">\n            </ion-icon>\n          </ion-col>\n        </ion-row>\n        <ion-row style="padding-bottom: 10px;">\n          <ion-col col-3 no-padding style="text-align: center;">{{"Live" | translate}}</ion-col>\n          <ion-col col-3 no-padding style="text-align: center;">{{"History" | translate}}</ion-col>\n          <ion-col col-3 no-padding style="text-align: center;">{{"Parking" | translate}}</ion-col>\n          <!-- <ion-col col-3 no-padding style="text-align: center;">{{"Immobilize" | translate}}</ion-col> -->\n          <ion-col col-3 no-padding style="text-align: center;" *ngIf = "this.relay_timer ==true">{{"Immobilize" | translate}}</ion-col>\n          <ion-col col-3 no-padding style="text-align: center;" *ngIf = "this.relay_timer ==undefined">{{"Immobilize" | translate}}</ion-col>\n          <ion-col col-3 no-padding style="text-align: center;" style="color: white;" *ngIf = "this.relay_timer ==false" readonly>{{"Immobilize" | translate}}</ion-col>\n        </ion-row>\n\n        <ion-row>\n          <ion-col col-3 no-padding style="text-align: center;" (click)="towAlertCall(drawerData)">\n            <ion-icon name="alert" style="color:#ef473a;font-size: 2em;" *ngIf="!drawerData.towAlert"></ion-icon>\n            <ion-icon name="alert" style="color:#1de21d;font-size: 2em;" *ngIf="drawerData.towAlert"></ion-icon>\n          </ion-col>\n          <ion-col col-3 no-padding style="text-align: center;" (click)="dialNumber(drawerData.contact_number)">\n            <!-- <ion-col col-3 no-padding style="text-align: center;"> -->\n            <!-- <a href="tel:{{drawerData.contact_number}}"> -->\n            <ion-icon name="call" style="color:#3a8eef;font-size: 2em;"></ion-icon>\n            <!-- </a> -->\n          </ion-col>\n          <ion-col col-3 no-padding style="text-align: center;" (click)="showVehicleDetails(drawerData)">\n            <ion-icon name="stats" style="color:#fc8803;font-size: 2em;"></ion-icon>\n          </ion-col>\n          <ion-col col-3 no-padding style="text-align: center;"\n            (click)="shareVehicle(drawerData)">\n            <ion-icon ios="ios-share" md="md-share" color="gpsc" style="font-size: 2em;"\n              *ngIf="showSharedBtn(islogin._id, drawerData.SharedWith)"></ion-icon>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-3 no-padding style="text-align: center;">{{"Tow" | translate}}</ion-col>\n          <ion-col col-3 no-padding style="text-align: center;">{{"Driver" | translate}}</ion-col>\n          <ion-col col-3 no-padding style="text-align: center;">{{"Analytics" | translate}}</ion-col>\n          <ion-col col-3 no-padding style="text-align: center;" >{{"Share" | translate}}</ion-col>\n        </ion-row>\n\n        <ion-row style="padding-top: 7px;">\n          <ion-col col-3 no-padding style="text-align: center;" (click)="settings()">\n            <ion-icon name="cog" style="color:#3a8eef;font-size: 2em;"></ion-icon>\n          </ion-col>\n          <ion-col col-3 no-padding style="text-align: center;" (click)="geofence()">\n            <ion-icon name="disc" style="color:#3a8eef;font-size: 2em;"></ion-icon>\n          </ion-col>\n          <ion-col col-3 no-padding style="text-align: center;" (click)="upload()">\n            <ion-icon name="cloud-upload" style="color:#3a8eef;font-size: 2em;"></ion-icon>\n          </ion-col>\n        </ion-row>\n        <ion-row >\n          <ion-col col-3 no-padding style="text-align: center;">{{"Settings" | translate}}</ion-col>\n          <ion-col col-3 no-padding style="text-align: center;">{{"Geofence" | translate}}</ion-col>\n          <ion-col col-3 no-padding style="text-align: center;">{{"Document" | translate}}</ion-col>\n        </ion-row>\n      </div>\n    </div>\n  </ion-bottom-drawer>\n</div>\n\n<div *ngIf="showFooter">\n  <ion-bottom-drawer [(state)]="drawerState1" [dockedHeight]="dockedHeight1" [shouldBounce]="shouldBounce"\n    [distanceTop]="distanceTop1" [minimumHeight]="minimumHeight1">\n    <div class="drawer-content">\n      <p style="font-size:1.2em; color:black;">\n        {{ "Share Live Vehicle" | translate }}\n      </p>\n      <ion-row>\n        <ion-col col-4 style="padding: 5px">\n          <button ion-button block color="{{ condition }}" (click)="sharedevice(\'15mins\')">\n            {{ "15 mins" | translate }}\n          </button>\n        </ion-col>\n        <ion-col col-4 style="padding: 5px">\n          <button ion-button block color="{{ condition1 }}" (click)="sharedevice(\'1hour\')">\n            {{ "1 hour" | translate }}\n          </button>\n        </ion-col>\n        <ion-col col-4 style="padding: 5px">\n          <button ion-button block color="{{ condition2 }}" (click)="sharedevice(\'8hours\')">\n            {{ "8 hours" | translate }}\n          </button>\n        </ion-col>\n        <ion-col col-4 style="padding: 5px">\n          <button ion-button block color="{{ condition3 }}" (click)="sharedevice(\'custom\')">\n            Custom\n          </button>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-4></ion-col>\n        <ion-col col-4></ion-col>\n        <ion-col col-4 style="text-align: right;">\n          <ion-fab style="right: calc(10px + env(safe-area-inset-right));">\n            <button ion-fab mini (click)="shareLivetemp()" color="gpsc">\n              <ion-icon name="send" color="black"></ion-icon>\n            </button>\n          </ion-fab>\n        </ion-col>\n      </ion-row>\n    </div>\n  </ion-bottom-drawer>\n</div>\n\n\n'/*ion-inline-end:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/add-devices/add-devices.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["PopoverController"],
            __WEBPACK_IMPORTED_MODULE_4__providers_geocoder_geocoder__["a" /* GeocoderProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_call_number__["a" /* CallNumber */],
            __WEBPACK_IMPORTED_MODULE_10__ionic_native_social_sharing__["a" /* SocialSharing */]])
    ], AddDevicesPage);
    return AddDevicesPage;
}());

var PopoverPage = /** @class */ (function () {
    function PopoverPage(navParams, modalCtrl, alertCtrl, apiCall, toastCtrl, navCtrl, viewCtrl, translate) {
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.translate = translate;
        this.vehData = navParams.get("vehData");
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    }
    PopoverPage.prototype.ngOnInit = function () { };
    PopoverPage.prototype.editItem = function () {
        var _this = this;
        var modal = this.modalCtrl.create('UpdateDevicePage', {
            vehData: this.vehData
        });
        modal.onDidDismiss(function () {
            _this.viewCtrl.dismiss();
        });
        modal.present();
    };
    PopoverPage.prototype.deleteItem = function () {
        var that = this;
        var alert = this.alertCtrl.create({
            message: this.translate.instant('Do you want to delete this vehicle ?'),
            buttons: [{
                    text: this.translate.instant('YES PROCEED'),
                    handler: function () {
                        that.deleteDevice(that.vehData.Device_ID);
                    }
                },
                {
                    text: this.translate.instant('NO')
                }]
        });
        alert.present();
    };
    PopoverPage.prototype.deleteDevice = function (d_id) {
        var _this = this;
        this.apiCall.startLoading().present();
        this.apiCall.deleteDeviceCall(d_id, this.islogin._id)
            .subscribe(function () {
            _this.apiCall.stopLoading();
            var toast = _this.toastCtrl.create({
                message: _this.translate.instant('Vehicle deleted successfully!'),
                position: 'bottom',
                duration: 2000
            });
            toast.onDidDismiss(function () {
                _this.viewCtrl.dismiss();
            });
            toast.present();
        }, function (err) {
            _this.apiCall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = _this.alertCtrl.create({
                message: msg.message,
                buttons: [_this.translate.instant('Okay')]
            });
            alert.present();
        });
    };
    PopoverPage.prototype.shareItem = function () {
        var that = this;
        var prompt = this.alertCtrl.create({
            title: this.translate.instant('Share Vehicle'),
            inputs: [
                {
                    name: 'device_name',
                    value: that.vehData.Device_Name
                },
                {
                    name: 'shareId',
                    placeholder: this.translate.instant('Enter Email Id/Mobile Number')
                },
            ],
            buttons: [
                {
                    text: this.translate.instant('Cancel'),
                    handler: function () {
                    }
                },
                {
                    text: this.translate.instant('Share'),
                    handler: function (data) {
                        that.sharedevices(data);
                    }
                }
            ]
        });
        prompt.present();
    };
    PopoverPage.prototype.sharedevices = function (data) {
        var _this = this;
        var that = this;
        var devicedetails = {
            "did": that.vehData._id,
            "email": data.shareId,
            "uid": that.islogin._id
        };
        that.apiCall.startLoading().present();
        that.apiCall.deviceShareCall(devicedetails)
            .subscribe(function (data) {
            that.apiCall.stopLoading();
            var toast = that.toastCtrl.create({
                message: data.message,
                position: 'bottom',
                duration: 2000
            });
            toast.present();
        }, function (err) {
            that.apiCall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = _this.alertCtrl.create({
                message: msg.message,
                buttons: [_this.translate.instant('Okay')]
            });
            alert.present();
        });
    };
    PopoverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            template: "\n    <ion-list>\n      <ion-item class=\"text-palatino\" (click)=\"editItem()\">\n        <ion-icon name=\"create\"></ion-icon>&nbsp;&nbsp;{{'edit' | translate}}\n      </ion-item>\n      <ion-item class=\"text-san-francisco\" (click)=\"deleteItem()\">\n        <ion-icon name=\"trash\"></ion-icon>&nbsp;&nbsp;{{'delete' | translate}}\n      </ion-item>\n      <ion-item class=\"text-seravek\" (click)=\"shareItem()\">\n        <ion-icon name=\"share\"></ion-icon>&nbsp;&nbsp;{{'share' | translate}}\n      </ion-item>\n    </ion-list>\n  "
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["c" /* TranslateService */]])
    ], PopoverPage);
    return PopoverPage;
}());

var CustomDurationPage = /** @class */ (function () {
    function CustomDurationPage(viewCtrl, toastCtrl) {
        this.viewCtrl = viewCtrl;
        this.toastCtrl = toastCtrl;
    }
    CustomDurationPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    CustomDurationPage.prototype._submit = function () {
        if (this.c_duration == undefined) {
            var toast = this.toastCtrl.create({
                message: "Please enter duration to proceed further.",
                duration: 2000,
                position: "middle"
            });
            toast.present();
        }
        else {
            this.viewCtrl.dismiss({
                param: this.c_duration
            });
        }
    };
    CustomDurationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-custom-duration',template:/*ion-inline-start:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/add-devices/custom-duration.html"*/'\n\n    <div padding style="background: white;\n\n    height: auto;\n\n    width: 90%;\n\n    margin: auto;\n\n    border-radius: 5px;">\n\n        <ion-row>\n\n          <ion-col col-12>\n\n            <p style="text-align: center; font-size: 2rem;">\n\n              <b>{{\'Custom Duration\' | translate}}</b>\n\n             &nbsp;&nbsp;&nbsp; <ion-icon class="close-button" id="close-button" name="close-circle" (tap)="dismiss()"></ion-icon>\n\n            </p>\n\n          </ion-col>\n\n        </ion-row>\n\n        <br />\n\n        <ion-row>\n\n          <ion-col col-12><b>{{\'Enter Duration:\' | translate}}</b></ion-col>\n\n          <ion-col col-12>\n\n            <ion-input type="number" [(ngModel)]="c_duration"></ion-input>\n\n          </ion-col>\n\n        </ion-row>\n\n        \n\n        <ion-row>\n\n          <ion-col col-12>\n\n            <button ion-button block (click)="_submit()" color="gpsc">{{\'SUBMIT\' | translate}}</button>\n\n          </ion-col>\n\n        </ion-row>\n\n      </div>\n\n    '/*ion-inline-end:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/add-devices/custom-duration.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["ViewController"], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["ToastController"]])
    ], CustomDurationPage);
    return CustomDurationPage;
}());

var PaymantgatweyPage = /** @class */ (function () {
    function PaymantgatweyPage(apiCall, navParam, navCtrl, callNumbers, plt, toastCtrl) {
        this.apiCall = apiCall;
        this.navParam = navParam;
        this.navCtrl = navCtrl;
        this.callNumbers = callNumbers;
        this.plt = plt;
        this.toastCtrl = toastCtrl;
        this.paramData = {};
        this.ddata = [];
        this.paymentHistory = [];
        this.expDate = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    }
    PaymantgatweyPage.prototype.ngOnInit = function () {
        this.getDetails();
    };
    PaymantgatweyPage.prototype.callNumber = function (number) {
        if (number !== "" || number !== undefined) {
            if (this.plt.is('android')) {
                window.open('tel:' + number, '_system');
            }
            else if (this.plt.is('ios')) {
                this.callNumbers.callNumber(number.toString(), true)
                    .then(function (res) { return console.log('Launched dialer!', res); })
                    .catch(function (err) { return console.log('Error launching dialer', err); });
            }
        }
        else {
            this.toastCtrl.create({
                message: 'Contact number not found!',
                position: 'middle',
                duration: 2000
            }).present();
        }
    };
    PaymantgatweyPage.prototype.getDetails = function () {
        var _this = this;
        var url = this.apiCall.mainUrl + "users/get_user_setting";
        var payload = {
            uid: this.islogin._id,
        };
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdatatwo(url, payload).subscribe(function (respData) {
            _this.ddata = respData;
            _this.ddata = JSON.parse(_this.ddata._body).Service;
            _this.apiCall.stopLoading();
        }, function (err) {
            console.log("oops got error: ", err);
            _this.apiCall.stopLoading();
        });
    };
    PaymantgatweyPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/add-devices/paymantgatwey-false.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Payment </ion-title>\n  </ion-navbar>\n</ion-header>\n<!-- <ion-content>\n  <ion-content\n    *ngIf="paymentHistory.length == 0"\n    style="text-align: center; margin: 15px; font-size: 20px"\n  >\n    History Not Found</ion-content\n  >\n  <ion-card *ngFor="let item of paymentHistory">\n    <ion-card-content>\n      <ion-row>\n        <ion-col col-6 style="font-size: 17px; font-weight: bold">\n          Status\n        </ion-col>\n        <ion-col style="font-size: 17px; font-weight: bold">\n          {{item?.Status}}\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6 style="font-size: 17px; font-weight: bold">\n          TransactionID\n        </ion-col>\n        <ion-col style="font-size: 17px; font-weight: bold">\n          {{item?.TransactionID}}\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6 style="font-size: 17px; font-weight: bold">\n          Amount\n        </ion-col>\n        <ion-col style="font-size: 17px; font-weight: bold">\n          {{item?.Amount}}\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6 style="font-size: 17px; font-weight: bold">\n          Date\n        </ion-col>\n        <ion-col style="font-size: 17px; font-weight: bold">\n          {{item?.createdAt}}\n        </ion-col>\n      </ion-row>\n    </ion-card-content>\n  </ion-card>\n</ion-content> -->\n<ion-content>\n<ion-card>\n<ion-card-content>\n    <p>Please contact on below numbers for renewal of GPS subscription: <span *ngFor="let d of ddata" (click)="callNumber(d)">{{d}} {{ddata.length>1?\',\':\'\'}}</span><br></p>\n  </ion-card-content>\n  </ion-card>\n</ion-content>\n<!-- [{"_id":"61b9cd046fff072a0c11c857","Customer_ID":"5cee7da97a38f414a4e5dfa3","Amount":1500,"Items":"GPS Renewal","PaymentMode":"Cheque","Remarks":"test","Status":"Paid","TransactionID":"4545454","supAdmin":"59cbbdbe508f164aa2fef3d8","__v":0,"VehicleNo":["615d910c4ef4e221a09e7549"]},{"_id":"61b9cd2d6fff072a0c11c85e","Customer_ID":"5cee7da97a38f414a4e5dfa3","Amount":2000,"Items":"GPS Renewal","PaymentMode":"Cash","Remarks":"renewal","Status":"Paid","TransactionID":"455544","supAdmin":"59cbbdbe508f164aa2fef3d8","__v":0,"VehicleNo":["60b5da4f8b50340d4e22c4f2"]},{"_id":"61ba0689dc5f282fae013b55","Customer_ID":"5cee7da97a38f414a4e5dfa3","Items":"SIM + Software","PaymentMode":"UPI","Remarks":"455","Status":"Paid","supAdmin":"59cbbdbe508f164aa2fef3d8","__v":0,"VehicleNo":[]},{"_id":"61ba11b2f521042fb8f60a56","Customer_ID":"5cee7da97a38f414a4e5dfa3","Amount":2,"Items":"SIM + Software","PaymentMode":"UPI","Remarks":"455","Status":"Paid","supAdmin":"59cbbdbe508f164aa2fef3d8","__v":0,"VehicleNo":["MH14HG8396","MH12KQ1871"]},{"_id":"61ba12b3f521042fb8f60a6a","Customer_ID":"5cee7da97a38f414a4e5dfa3","Amount":2,"Items":"SIM + Software","PaymentMode":"UPI","Remarks":"455","Status":"Paid","TransactionID":"pay_IXoKZzWLq2CQHr","supAdmin":"59cbbdbe508f164aa2fef3d8","__v":0,"VehicleNo":["MH14HG8396","MH12KQ1871"]}]-->\n'/*ion-inline-end:"/Users/apple/Desktop/screenshots/PrimeTechnoGPSNew/src/pages/add-devices/paymantgatwey-false.html"*/,
            styles: [
                "\n      .col {\n        padding: 0px;\n      }\n    ",
            ],
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_9__ionic_native_call_number__["a" /* CallNumber */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["Platform"], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["ToastController"]])
    ], PaymantgatweyPage);
    return PaymantgatweyPage;
}());

//# sourceMappingURL=add-devices.js.map

/***/ })

},[454]);
//# sourceMappingURL=main.js.map